<?php include('header.php') ?>

<script>window.print();</script>

<div class="container">
	<div class="row">
			<img class="img-responsive pull-left" src="img/logo_contrato.png" alt="" style="margin-top: 90px;width: 20%;height: 20%;">

	</div>
<div class="row contrato">
	<div class="col-md-12">
			<h4>CONTRATO DE PRESTAÇÃO DE SERVIÇOS</h4>

			<p>
			O presente Contrato de tem como objetivo fornecer descontos em serviços através de convênios com empresas nas áreas de saúde, lazer, educação entre outras;
			</p>

			<p>
			As empresas conveniadas ao Cartão Sol poderão ser acessadas no site da empresa www.cartaosol.com.br;
			</p>

			<p>
			O Titular do Cartão bem como seu cônjuge e filhos até a idade de 21 anos, devidamente cadastrados e vinculados ao Cartão terão direito as vantagens oferecidas pelas empresas parceiras, desde que rigorosamente em dia com suas mensalidades;
			</p>

			<p>
			O Titular obriga-se a pagar ao Cartão Sol o valor mensal de R$19,50 tendo como opção débito de 12 parcelas no cartão de credito ou boleto bancário ou outra forma a combinar desde que se assegure a adimplência das mensalidades;
			</p>

			<p>
			Será cobrado no ato da aceitação desse contrato a quantia de R$ 39,50 a título de taxa de abertura.
			</p>

			<p>
			O reajuste anual da mensalidade ocorrerá em abril de cada ano de acordo com o IGPM integral da FGV do ano anterior sendo publicado em seu site.
			</p>

			<p>
			O presente contrato tem validade pelo prazo de 12 (doze) meses, renovando-se automaticamente por prazo indeterminado, caso não haja manifestação expressa em contrário por uma das partes.
			</p>

			<p>
			Após a renovação automática do presente contrato por prazo indeterminado, o mesmo poderá ser rescindido por qualquer uma das partes, sem multa, mediante comunicação prévia de 30 (trinta) dias, por escrito, diretamente em qualquer uma das unidades do Cartão Sol.
			</p>

			<p>
			No caso de cancelamento antes dos 12 (doze) meses previstos no parágrafo anterior, será devida multa equivalente a 50% sobre o valor total da soma das parcelas a vencer. O cancelamento não poderá ser efetivado caso haja parcelas em atraso.
			</p>

			<p>
			As informações prestadas pelo Titular referente este contrato, são de inteira responsabilidade do mesmo.
			</p>

			<p>
			O Titular declara que está de acordo com as cláusulas do presente contrato, bem como está ciente de que o cartão de desconto não é operadora de saúde, e não garante e não se responsabiliza pelos serviços oferecidos e pelo pagamento das despesas, nem assegura descontos em todos os serviços obrigatoriamente garantidos por qualquer operadora de saúde. Tudo o que o cliente usar ou comprar será por ele diretamente pago ao prestador, assegurando-se apenas os preços e descontos que constam na relação de empresas e serviços conveniados divulgados no site www.caratosol.com.br
			</p><br><br><br><br>
			<p>________________________________</p>
			<h4>Assinatura do Titular</h4>

			</p><br><br><br><br>
			<p>_____________________em________de______de 20______.</p>
			</p><br><br><br><br>

		</div>
</div>
</div>

<?php include('bottom.php') ?>
