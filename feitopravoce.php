<?php include('header.php') ?>

<div class="banner1">

  <div class="jumbotron" style="background-image: url(img/banner1.png); background-size: cover; height: 100%; background-repeat: no-repeat; background-position: center;margin-bottom: 0px;">
  
    <div class="container">
    <div class="row text-banner1">
      <div class="col-md-12">
        <h1 class="subli">FEITO PARA VOCÊ</h1>
           
      <p>MUITO MAIS <b>DESCONTOS</b> PARA <br> <b>APROVEITAR</b></p>
         
      <a href="#"><i class="fa fa-plus fa-3x" aria-hidden="true"></i></a>
      </div>
    </div>
  </div>

  </div>
  
</div>

<div class="banner1">

  <div class="jumbotron" style="background-image: url(img/banner2.png); background-size: cover; height: 100%; background-repeat: no-repeat; background-position: center;margin-bottom: 0px;">
    
    <div class="container">
    <div class="row text-banner2">
      <div class="col-md-5">
        <h1>UM CARTÃO QUE ATENDE SUAS <b class="sub">NECESSIDADES</b></h1>
                                              
      <p class="text-justify">O grande intuito do Cartão Sol é favorecer a inclusão social das pessoas. Dessa forma, oferecemos saúde de qualidade aos colaboradores. O melhor disso tudo é que o preço é extremamente acessível. O Cartão Sol chegou para se tornar o melhor cartão de descontos do país, possibilitando mais qualidade de vida para todos.
</p>
         
   
      <a href="contato.php" style="text-decoration: none;"font-family: 'Open Sans', sans-serif;><p>CLIQUE E VEJA COMO PEDIR O SEU</p></a>
      </div>
    </div>
  </div>

  </div>
  
</div>

<div class="banner1">


   <div class="jumbotron" style="background-image: url(img/banner4.png); background-size: cover; height: 100%; background-size: cover; background-repeat: no-repeat; background-position: center;margin-bottom: 0px;">
      
      <div class="container">
    <div class="row text-banner3">
      <div class="col-md-12 pull-right">
        <h1>UM CARTÃO QUE ATENDE SUAS <b class="su">NECESSIDADES</b></h1>
                                          
        
      <p>IMPOSSÍVEL NÃO SE <b style="font-size: 36px;">SURPREENDER</b> <br> COM TANTOS <b style="font-size: 36px;">BENEFÍCIOS</b> </p>
         
      <a href="#"><i class="fa fa-plus fa-3x" aria-hidden="true"></i></a>
      </div>
    </div>
  </div>

   </div>
  
</div>







<?php include('footer.php') ?>

<?php include('bottom.php') ?>
