$("#estado").on("change", function () {
    $.getJSON($("head").data("info") + "support/getCidades",
            {ajax: true, estado: $("#estado").val()},
            function (r) {
                if (r.resultado) {
                    $("#cidade")
                            .empty()
                            .append("<option value=''>--</option>");
                    $.each(r.cidades, function (key, cidade) {
                        $("#cidade").append("<option value='" + cidade.id + "'>" + cidade.cidade + "</option>");
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON"
            )
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de suporte"
                });
            });
});

$("#formAdicionarCidade").validate({
    rules: {
        estado: {required: true},
        cidade: {required: true}
//        tipo: {required: true}
    },
    messages: {
        estado: {required: "Campo obrigatório"},
        cidade: {required: "Campo obrigatório"}
//        tipo: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAdicionarCidade")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "cidades/adicionar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formAdicionarCidadeBtnAdicionar")
                        .empty()
                        .append("Adicionando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg,
                    buttons: {
                        sim: "sim",
                        nao: "nao"
                    }
                }).then((value) => {
                    switch (value) {
                        case "sim":
                            $("#formAdicionarCidade").trigger("reset");
                            break;
                        case "nao":
                            location.href = $("head").data("info") + "cidades";
                        default :
                            location.href = $("head").data("info") + "cidades";
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarCidadeBtnAdicionar")
                    .empty()
                    .append("Adicionar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAdicionarCidadeModal").validate({
    rules: {
        modalAdicionarCidadeEstado: {required: true},
        modalAdicionarCidadeCidade: {required: true}
    },
    messages: {
        modalAdicionarCidadeEstado: {required: "Campo obrigatório"},
        modalAdicionarCidadeCidade: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAdicionarCidade")[0]);
        dados.append("ajax", true);
        dados.append("estado", $("#modalAdicionarCidadeEstado").val());
        dados.append("cidade", $("#modalAdicionarCidadeCidade").val());
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "support/adicionarCidade",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAdicionarCidadeBtnAdicionar")
                        .empty()
                        .append("Adicionando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    $("#modalAdicionarCidade").modal("hide");
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarCidadeBtnAdicionar")
                    .empty()
                    .append("Adicionar")
                    .removeAttr("disabled");
        });
    }
});