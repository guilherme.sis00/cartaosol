$().ready(function () {
    $("#dataInicial").mask("99/99/9999");
    $("#dataFinal").mask("99/99/9999");
});

$("#formBuscar").validate({
    rules: {
        dataInicial: {required: true},
        dataFinal: {
            required: true,
            minlength: 8
        },
        categoria: {required: true}
    },
    messages: {
        dataInicial: {required: "Campo obrigatório"},
        dataFinal: {
            required: "Campo obrigatório",
            minlength: "Digite uma data válida"
        },
        categoria: {required: "Campo obrigatório"}
    },
    errorClass: 'help-block',
    validClass: "help-block",
    errorElement: "small",
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        var dados = new FormData($("#formBuscar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/contasPagar/buscar",
            data: dados,
            dataType: "JSON",
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formBuscarBtnBuscar")
                        .html("Buscando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                let pago = 0;
                let pendente = 0;

                $("#tabelaContasReceber").empty();
                $.each(r.contasPagar, function (key, contaPagar) {
                    switch (contaPagar.status) {
                        case "pago":
                            pago += parseFloat(contaPagar.valor);
                            break;
                        case "pendente":
                            pendente += parseFloat(contaPagar.valor);
                            break;
                    }
                    tabelaContasPagarAdicionar(contaPagar);
                });

                $("#total-pago").html(`R$ ${formatarValor(pago)}`);
                $("#total-pendente").html(`R$ ${formatarValor(pendente)}`);
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });

                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha de comunicação",
                icon: "error",
                text: "Por favor entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarBtnBuscar")
                    .html("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#mesContasReceber").on("change", function () {
    let mes = $(this).val();
    let toast = $.toast({
        heading: "<strong style='color: white'>Processando</strong>",
        icon: "info",
        text: "Buscando contas a pagar",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "financeiro/contasPagar/buscarPorMes",
            {ajax: true, mes: mes},
            function (r) {
                if (r.resultado) {
                    let pago = 0;
                    let pendente = 0;

                    $("#tabelaContasPagar").empty();
                    $.each(r.contasPagar, function (key, contaPagar) {
                        switch (contaPagar.status) {
                            case "pago":
                                pago += parseFloat(contaPagar.valor);
                                break;
                            case "pendente":
                                pendente += parseFloat(contaPagar.valor);
                                break;
                        }
                        tabelaContasPagarAdicionar(contaPagar);
                    });

                    $("#total-pago").html(`R$ ${formatarValor(pago)}`);
                    $("#total-pendente").html(`R$ ${formatarValor(pendente)}`);
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });

                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

$("#tabelaContasPagar").on("click", ".btn-deletar", function () {
    let id = $(this).data("id");
    let status = $(`#status-${id}`).text();

    let toast = $.toast({
        heading: "<strong style='color: white'>Processando</strong>",
        icon: "info",
        text: "Deletando conta a pagar",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "financeiro/contasPagar/deletar",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: "<strong style='color: white'>Tudo certo</strong>",
                        icon: "success",
                        text: r.msg,
                        position: "top-right",
                        beforeShow: function () {
                            let valorContaPagar = $(`#valor-${id}`).text().slice(3).replace(",", ".");

                            switch (status) {
                                case "Pago":
                                    let totalPago = $(`#total-pago`).text().slice(3).replace(",", ".");
                                    $("#total-pago").html(`R$ ${formatarValor(parseFloat(totalPago) - parseFloat(valorContaPagar))}`);
                                    break;
                                case "Pendente":
                                    let totalPendente = $(`#total-pendente`).text().slice(3).replace(",", ".");
                                    $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) - parseFloat(valorContaPagar))}`);
                                    break;
                            }
                            $(`#${id}`).remove();
                        },
                        hideAfter: 4000
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });

                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

$("#tabelaContasPagar").on("click", ".btn-status", function () {
    let id = $(this).data("id");
    let status = $(this).data("status");
    let toast = $.toast({
        heading: "<strong style='color: white'>Processando</strong>",
        icon: "info",
        text: "Atualizando status",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "financeiro/contasPagar/atualizarStatus",
            {ajax: true, id: id, status: status, mesSelecionado: $("#mesContasPagar").val()},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: "<strong style='color: white'>Tudo certo</strong>",
                        icon: "success",
                        text: r.msg,
                        position: "top-right",
                        beforeShow: function () {
                            $(`#status-${id}`).html(status.charAt(0).toUpperCase() + status.slice(1));
                            analisarStatus({id: id, status: status});
                        },
                        hideAfter: 4000
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });

                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

function analisarStatus(params) {
    let valorContaPagar = $(`#valor-${params.id}`).text().slice(3).replace(",", ".");
    let totalPago = $(`#total-pago`).text().slice(3).replace(",", ".");
    let totalPendente = $(`#total-pendente`).text().slice(3).replace(",", ".");

    switch (params.status) {
        case "pago":
            $(`#${params.id}`).removeClass("danger");

            $("#total-pago").html(`R$ ${formatarValor(parseFloat(totalPago) + parseFloat(valorContaPagar))}`);
            $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) - parseFloat(valorContaPagar))}`);
            
            break;
        case "pendente":
            let dataAtual = new Date($("#dataAtual").val());
            let dataContaReceber = new Date($(`#data-${params.id}`).text());

            $("#total-pago").html(`R$ ${formatarValor(parseFloat(totalPago) - parseFloat(valorContaPagar))}`);
            $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) + parseFloat(valorContaPagar))}`);

            if (dataAtual > dataContaReceber) {
                $(`#${params.id}`).addClass("danger");
            }
            
            break;
    }
}

function tabelaContasPagarAdicionar(contaPagar) {
    let atrasada = false;
    if (contaPagar.status == "pendente") {
        let dataAtual = new Date($("#dataAtual").val());
        let dataContaReceber = new Date(formatarData(contaPagar.data));

        atrasada = dataAtual > dataContaReceber;
    }

    $("#tabelaContasPagar").append(`<tr id="${contaPagar.id}" class="${atrasada ? "danger" : ""}">
                                        <td id="descricao-${contaPagar.id}">${contaPagar.descricao}</td>
                                        <td id="categoria-${contaPagar.id}">${contaPagar.categoria}</td>
                                        <td id="valor-${contaPagar.id}">R$ ${formatarValor(parseFloat(contaPagar.valor))}</td>
                                        <td id="data-${contaPagar.id}">${formatarData(contaPagar.data)}</td>
                                        <td id="status-${contaPagar.id}>">${contaPagar.status.charAt(0).toUpperCase() + contaPagar.status.slice(1)}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Ações <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="${$("head").data("info") + "financeiro/conta-pagar/" + contaPagar.id}")">Atualizar</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-status" data-id="${contaPagar.id}" data-status="pago">Pago</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-status" data-id="${contaPagar.id}" data-status="pendente">Pendente</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-deletar" data-id="${contaPagar.id}">Deletar</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>`);
}