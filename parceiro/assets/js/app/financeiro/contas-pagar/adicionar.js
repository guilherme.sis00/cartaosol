$().ready(function () {
    $("#data").mask("99/99/9999");
    $("#valor").mask("#.##0,00", {reverse: true});
});

$("#formAdicionarContaPagar").validate({
    rules: {
        descricao: {required: true},
        fornecedor: {required: true},
        valor: {required: true},
        data: {
            required: true,
            minlength: 8
        },
        categoria: {required: true},
        status: {required: true}
    },
    messages: {
        descricao: {required: "Campo obrigatório"},
        fornecedor: {required: "Campo obrigatório"},
        valor: {required: "Campo obrigatório"},
        data: {
            required: "Campo obrigatório",
            minlength: "Digite uma data válida"
        },
        categoria: {required: "Campo obrigatório"},
        status: {required: "Campo obrigatório"}
    },
    errorClass: 'help-block',
    validClass: "help-block",
    errorElement: "small",
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        var valor = $("#valor").val().replace(".", "");
        valor = parseFloat(valor.replace(",", "."));
        var dados = new FormData($("#formAdicionarContaPagar")[0]);
        dados.append("ajax", true);
        dados.set("valor", valor);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/contasPagar/adicionar",
            data: dados,
            dataType: "JSON",
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAdicionarContaPagarBtnAdicionar")
                        .html("Adicionando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: "<strong style='color: white'>Tudo certo</strong>",
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    beforeShow: function () {
                        $("#formAdicionarContaPagar").trigger("reset");
                        $("#descricao").focus();
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });

                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha de comunicação",
                icon: "error",
                text: "Por favor entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarContaPagarBtnAdicionar")
                    .html("Adicionar")
                    .removeAttr("disabled");
        });
    }
});