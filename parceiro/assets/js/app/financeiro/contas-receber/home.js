$().ready(function () {
    $("#cpf").mask("000.000.000-00");
    $("#dataInicial").mask("99/99/9999");
    $("#dataFinal").mask("99/99/9999");
});

$("#formBuscar").validate({
    rules: {
        codigoContrato: {digits: true},
        categoria: {required: true},
        dataInicial: {required: true},
        dataFinal: {
            required: true,
            minlength: 8
        }
    },
    messages: {
        codigoContrato: {digits: "Código inválido"},
        categoria: {required: "Campo obrigatório"},
        dataInicial: {required: "Campo obrigatório"},
        dataFinal: {
            required: "Campo obrigatório",
            minlength: "Digite uma data válida"
        }
    },
    errorClass: 'help-block',
    validClass: "help-block",
    errorElement: "small",
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        var dados = new FormData($("#formBuscar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/contasReceber/buscar",
            data: dados,
            dataType: "JSON",
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formBuscarBtnBuscar")
                        .html("Buscando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                let recebido = 0;
                let pendente = 0;

                $("#tabelaContasReceber").empty();
                $.each(r.contasReceber, function (key, contaReceber) {
                    switch (contaReceber.status) {
                        case "recebido":
                            recebido += parseFloat(contaReceber.valor);
                            break;
                        case "pendente":
                            pendente += parseFloat(contaReceber.valor);
                            break;
                    }
                    tabelaContasReceberAdicionar(contaReceber);
                });

                $("#total-recebido").html(`R$ ${formatarValor(recebido)}`);
                $("#total-pendente").html(`R$ ${formatarValor(pendente)}`);
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });

                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha de comunicação",
                icon: "error",
                text: "Por favor entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarBtnBuscar")
                    .html("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#mesContasReceber").on("change", function () {
    let mes = $(this).val();
    let toast = $.toast({
        heading: "<strong style='color: white'>Processando</strong>",
        icon: "info",
        text: "Buscando contas a pagar",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "financeiro/contasReceber/buscarPorMes",
            {ajax: true, mes: mes},
            function (r) {
                if (r.resultado) {
                    let recebido = 0;
                    let pendente = 0;

                    $("#tabelaContasReceber").empty();
                    $.each(r.contasReceber, function (key, contaReceber) {
                        switch (contaReceber.status) {
                            case "recebido":
                                recebido += parseFloat(contaReceber.valor);
                                break;
                            case "pendente":
                                pendente += parseFloat(contaReceber.valor);
                                break;
                        }
                        tabelaContasReceberAdicionar(contaReceber);
                    });

                    $("#total-recebido").html(`R$ ${formatarValor(recebido)}`);
                    $("#total-pendente").html(`R$ ${formatarValor(pendente)}`);
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });

                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

$("#tabelaContasReceber").on("click", ".btn-deletar", function () {
    let id = $(this).data("id");
    let status = $(`#status-${id}`).text();

    let toast = $.toast({
        heading: "<strong style='color: white'>Processando</strong>",
        icon: "info",
        text: "Deletando conta a receber",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "financeiro/contasReceber/deletar",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: "<strong style='color: white'>Tudo certo</strong>",
                        icon: "success",
                        text: r.msg,
                        position: "top-right",
                        beforeShow: function () {
                            let valorContaPagar = $(`#valor-${id}`).text().slice(3).replace(",", ".");

                            switch (status) {
                                case "Recebido":
                                    let totalPago = $(`#total-recebido`).text().slice(3).replace(",", ".");
                                    $("#total-recebido").html(`R$ ${formatarValor(parseFloat(totalPago) - parseFloat(valorContaPagar))}`);
                                    break;
                                case "Pendente":
                                    let totalPendente = $(`#total-pendente`).text().slice(3).replace(",", ".");
                                    $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) - parseFloat(valorContaPagar))}`);
                                    break;
                            }

                            $(`#${id}`).remove();
                        },
                        hideAfter: 4000
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });

                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

$("#tabelaContasReceber").on("click", ".btn-status", function () {
    let id = $(this).data("id");
    let idParcela = $(this).data("id-parcela");
    let status = $(this).data("status");
    let toast = $.toast({
        heading: "<strong style='color: white'>Processando</strong>",
        icon: "info",
        text: "Atualizando status",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "financeiro/contasReceber/atualizarStatus",
            {ajax: true, id: id, idParcela: idParcela, status: status, mesSelecionado: $("#mesContasReceber").val()},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: "<strong style='color: white'>Tudo certo</strong>",
                        icon: "success",
                        text: r.msg,
                        position: "top-right",
                        beforeShow: function () {
                            $(`#status-${id}`).html(status.charAt(0).toUpperCase() + status.slice(1));
                            analisarStatus({id: id, status: status});
                        },
                        hideAfter: 4000
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });

                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

function analisarStatus(params) {
    let valorContaPagar = $(`#valor-${params.id}`).text().slice(3).replace(",", ".");
    let totalPago = $(`#total-recebido`).text().slice(3).replace(",", ".");
    let totalPendente = $(`#total-pendente`).text().slice(3).replace(",", ".");

    switch (params.status) {
        case "recebido":
            $(`#${params.id}`).removeClass("danger");
            $(`#${params.id}`).addClass("success");
            $(`#baixada-${params.id}`).html(`<i class="ti-arrow-down"></i>`);

            $("#total-recebido").html(`R$ ${formatarValor(parseFloat(totalPago) + parseFloat(valorContaPagar))}`);
            $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) - parseFloat(valorContaPagar))}`);

            break;
        case "pendente":
            let dataAtual = new Date($("#dataAtual").val());
            let dataContaReceber = new Date(desformatarData($(`#data-${params.id}`).text()));

            $("#total-recebido").html(`R$ ${formatarValor(parseFloat(totalPago) - parseFloat(valorContaPagar))}`);
            $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) + parseFloat(valorContaPagar))}`);
            $(`#baixada-${params.id}`).empty();
            $(`#${params.id}`).removeClass("success");

            if (dataAtual > dataContaReceber) {
                $(`#${params.id}`).addClass("danger");
            }

            break;
    }
}

function tabelaContasReceberAdicionar(contaReceber) {
    let atrasada = false;
    if (contaReceber.status == "pendente") {
        let dataAtual = new Date($("#dataAtual").val());
        let dataContaReceber = new Date(contaReceber.data);

        atrasada = dataAtual > dataContaReceber;
    }

    $("#tabelaContasReceber").append(`<tr id="${contaReceber.id}" class="${atrasada ? "danger" : (contaReceber.status == "recebido" ? "success" : "")}">
                                        <td id="baixada-${contaReceber.id}">${contaReceber.status == "recebido" ? `<i class="ti-arrow-down"></i>` : ""}</td>
                                        <td id="descricao-${contaReceber.id}">${contaReceber.descricao}</td>
                                        <td id="categoria-${contaReceber.id}">${contaReceber.categoria}</td>
                                        <td id="valor-${contaReceber.id}">R$ ${formatarValor(parseFloat(contaReceber.valor))}</td>
                                        <td id="data-${contaReceber.id}">${formatarData(contaReceber.data)}</td>
                                        <td id="status-${contaReceber.id}">${contaReceber.status.charAt(0).toUpperCase() + contaReceber.status.slice(1)}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Ações <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="${$("head").data("info") + "financeiro/conta-receber/" + contaReceber.id}")">Atualizar</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a target="_blank" href="${$("head").data("info") + "financeiro/contas-receber/boleto/" + contaReceber.id}")">Visualizar boleto</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-status" data-id="${contaReceber.id}" data-id-parcela="${contaReceber.idParcela}" data-status="recebido">Recebido</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-status" data-id="${contaReceber.id}" data-id-parcela="${contaReceber.idParcela}" data-status="pendente">Pendente</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-deletar" data-id="${contaReceber.id}">Deletar</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>`);
}