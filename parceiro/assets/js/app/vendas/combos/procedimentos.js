$().ready(function () {
    $("#valor").mask("#.##0,00", {reverse: true});
});

$("#formAtualizarProcedimentos").validate({
    rules: {
        id: {required: true},
        procedimento: {required: true},
        quantidade: {required: true},
        valor: {required: true}
    },
    messages: {
        procedimento: {required: true},
        quantidade: {required: true},
        valor: {required: true}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let valor = $("#valor").val().replace(".", "");
        valor = parseFloat(valor.replace(",", "."));
        let dados = new FormData($("#formAtualizarProcedimentos")[0]);
        dados.append("ajax", true);
        dados.set("valor", valor);
        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "vendas/combos/adicionarProcedimento",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAtualizarProcedimentosBtnAdicionar")
                        .html("Adicionando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Tudo certo</strong>`,
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    hideAfter: 4000,
                    beforeShow: function () {
                        $("#formAtualizarProcedimentos").trigger("reset");
                        
                        $("#tabelaItens").append(` <tr id="${r.procedimento.id}">
                                                    <td>${r.procedimento.idEspecialidade}</td>
                                                    <td>${r.procedimento.especialidade}</td>
                                                    <td>${r.procedimento.quantidade}</td>
                                                    <td>${formatarValor(parseFloat(r.procedimento.valor))}</td>
                                                    <td><button type="button" data-id="${r.procedimento.id}" class="btn-deletar btn btn-small btn-fill btn-danger"><i class='ti-close'></i></button></td>
                                                </tr>`);
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAtualizarProcedimentosBtnAdicionar")
                    .html("Adicionar")
                    .removeAttr("disabled");
        });
    }
});

$("#tabelaItens").on("click", ".btn-deletar", function () {
    let button = $(this);
    let id = $(this).data("id");
    let toast = $.toast({
        heading: `<strong style="color:white">Processando</strong>`,
        icon: "info",
        text: "Deletando procedimento do combo",
        position: "top-right",
        hideAfter: false
    });
    button.attr("disabled", "disabled");
    $.post($("head").data("info") + "vendas/combos/deletarProcedimento",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: `<strong style="color:white">Tudo certo</strong>`,
                        icon: "success",
                        text: r.msg,
                        position: "top-right",
                        hideAfter: 4000,
                        beforeShow: function () {
                            $(`#${id}`).remove();
                        }
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON"
            )
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                button.removeAttr("disabled");
                toast.reset();
                $("#formAtualizarProcedimentosBtnAdicionar")
                        .html("Adicionar")
                        .removeAttr("disabled");
            });
});