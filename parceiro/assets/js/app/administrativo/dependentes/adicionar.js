$().ready(function () {
    $("#cpf").mask("000.000.000-00");
    $("#numeroCartao").mask("0000.0000.0000.0000");
    $("#dataNascimento").mask("00/00/0000");
    $("#dataVinculacao").mask("00/00/0000");
});

$("#formAdicionarDependente").validate({
    rules: {
        titular: {required: true},
        nome: {required: true},
        cpf: {
            required: true,
            minlength: 14
        },
        numeroCartao: {
            required: true,
            minlength: 17
        },
        dataNascimento: {
            required: true,
            minlength: 9
        },
        dataVinculacao: {
            required: true,
            minlength: 9
        }
    },
    messages: {
        titular: {required: "Campo obrigatório"},
        nome: {required: "Campo obrigatório"},
        cpf: {
            required: "Campo obrigatório",
            minlength: "Digite um CPF válido"
        },
        numeroCartao: {
            required: "Campo obrigatório",
            minlength: "Digite um número de cartão válido"
        },
        dataNascimento: {
            required: "Campo obrigatório",
            minlength: "Digite uma data de nascimento válida"
        },
        dataVinculacao: {
            required: "Campo obrigatório",
            minlength: "Digite uma data de vinculacao válida"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAdicionarDependente")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "administrativo/dependentes/adicionar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAdicionarDependenteBtnAdicionar")
                        .html("Adicionando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Tudo certo</strong>`,
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    hideAfter: 4000,
                    beforeShow: function () {
                        $("#formAdicionarDependente").trigger("reset");
                        $("#titular").focus();
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarDependenteBtnAdicionar")
                    .html("Adicionar")
                    .removeAttr("disabled");
        });
    }
});