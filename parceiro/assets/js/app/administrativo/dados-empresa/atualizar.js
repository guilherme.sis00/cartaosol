$().ready(function () {
    $("#cnpj").mask("00.000.000/0000-00");
    $("#carteira").mask("000");
    $("#conta").mask("00000000-0");
    $("#agencia").mask("0000-0");
    $("#codigoTransmissao").mask("000000000000000");
    $("#codigoPsk").mask("0000000");
    $("#codigoBanco").mask("000");
    $("#contaFidc").mask("00000000-0");
    $("#telefone").mask("(00) 0000-0000");
    $("#celular").mask("(00) 00000-0000");
    $("#cep").mask("00000-000");
});

$("#formAtualizarDadosEmpresa").validate({
    rules: {
        id: {required: true},
        nomeFantasia: {required: true},
        razaoSocial: {required: true},
        cnpj: {
            required: true,
            minlength: 17
        },
        carteira: {required: true},
        conta: {required: true},
        agencia: {required: true},
        ios: {required: true},
        codigoTransmissao: {required: true},
        codigoPsk: {required: true},
        codigoBanco: {required: true},
        contaFidc: {required: true},
        telefone: {required: true},
        celular: {required: true},
        email: {
            required: true,
            email: true
        },
        cep: {required: true},
        logradouro: {required: true},
        numero: {required: true},
        bairro: {required: true},
        estado: {required: true},
        cidade: {required: true}
    },
    messages: {
        nomeFantasia: {required: "Campo obrigatório"},
        razaoSocial: {required: "Campo obrigatório"},
        cnpj: {
            required: "Campo obrigatório",
            minlength: "Digite um CNPJ válido"
        },
        carteira: {required: "Campo obrigatório"},
        conta: {required: "Campo obrigatório"},
        agencia: {required: "Campo obrigatório"},
        ios: {required: "Campo obrigatório"},
        codigoTransmissao: {required: "Campo obrigatório"},
        codigoPsk: {required: "Campo obrigatório"},
        codigoBanco: {required: "Campo obrigatório"},
        contaFidc: {required: "Campo obrigatório"},
        telefone: {required: "Campo obrigatório"},
        celular: {required: "Campo obrigatório"},
        email: {
            required: "Campo obrigatório",
            email: "Digite um e-mail válido"
        },
        cep: {required: "Campo obrigatório"},
        logradouro: {required: "Campo obrigatório"},
        numero: {required: "Campo obrigatório"},
        bairro: {required: "Campo obrigatório"},
        estado: {required: "Campo obrigatório"},
        cidade: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAtualizarDadosEmpresa")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "administrativo/dadosEmpresa/atualizar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formAtualizarDadosEmpresaBtnAtualizar")
                        .html("Atualizando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Tudo certo",
                    icon: "success",
                    text: r.msg
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAtualizarDadosEmpresaBtnAtualizar")
                    .html("Atualizar")
                    .removeAttr("disabled");
        });
    }
});

$("#estado").on("change", function () {
    $.getJSON($("head").data("info") + "support/getCidades",
            {ajax: true, estado: $("#estado").val()},
            function (r) {
                if (r.resultado) {
                    $("#cidade")
                            .empty()
                            .append("<option value=''>--</option>");
                    $.each(r.cidades, function (key, cidade) {
                        $("#cidade").append("<option value='" + cidade.id + "'>" + cidade.cidade + "</option>");
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON"
            )
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de suporte"
                });
            });
});