$().ready(function () {
    $("#modalAtualizarVendedorCelular").mask("(00) 00000-0000");
    $("#modalAtualizarVendedorCpf").mask("000.000.000-00");
});

$("#formBuscar").validate({
    rules: {
        termo: {
            required: true,
            minlength: 3
        }
    },
    messages: {
        termo: {
            required: "Campo obrigatório",
            minlength: "Digite pelo menos 3 caracteres"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formBuscar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "administrativo/vendedores/buscar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formBuscarBtnBuscar")
                        .html("Buscando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#tabelaVendedores").empty();
                $("#divPaginacao").empty();

                $.each(r.vendedores, function (key, vendedor) {
                    tabelaVendedoresAdicionar(vendedor);
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarBtnBuscar")
                    .html("Adicionar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAtualizarVendedor").validate({
    rules: {
        modalAtualizarVendedorId: {required: true},
        modalAtualizarVendedorNome: {required: true},
        modalAtualizarVendedorCpf: {required: true},
        modalAtualizarVendedorCelular: {required: true}
    },
    messages: {
        modalAtualizarVendedorNome: {required: "Campo obrigatório"},
        modalAtualizarVendedorCpf: {required: "Campo obrigatório"},
        modalAtualizarVendedorCelular: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData();
        dados.append("ajax", true);
        dados.append("id", $("#modalAtualizarVendedorId").val());
        dados.append("nome", $("#modalAtualizarVendedorNome").val());
        dados.append("cpf", $("#modalAtualizarVendedorCpf").val());
        dados.append("celular", $("#modalAtualizarVendedorCelular").val());

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "administrativo/vendedores/atualizar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAtualizarVendedorBtnAtualizar")
                        .html("Atualizando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    $("#formAtualizarVendedor").trigger("reset");
                    $("#modalAtualizarVendedor").modal("hide");
                    $(`#nome-${dados.get("id")}`).html(dados.get("nome"));
                    $(`#cpf-${dados.get("id")}`).html(dados.get("cpf"));
                    $(`#celular-${dados.get("id")}`).html(dados.get("celular"));
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAtualizarVendedorBtnAtualizar")
                    .html("Atualizar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAtualizarSenha").validate({
    rules: {
        modalAtualizarSenhaId: {required: true},
        modalAtualizarSenhaSenha: {
            required: true,
            minlength: 4
        },
        modalAtualizarSenhaRepetirSenha: {
            required: true,
            equalTo: "#modalAtualizarSenhaSenha"
        }
    },
    messages: {
        modalAtualizarSenhaId: {required: "Campo obrigatório"},
        modalAtualizarSenhaSenha: {
            required: "Campo obrigatório",
            minlength: "A senha deve ter pelo menos 4 digítos"
        },
        modalAtualizarSenhaRepetirSenha: {
            required: "Campo obrigatório",
            equalTo: "As senhas não coincidem"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData();
        dados.append("ajax", true);
        dados.append("id", $("#modalAtualizarSenhaId").val());
        dados.append("senha", $("#modalAtualizarSenhaSenha").val());
        dados.append("repetirSenha", $("#modalAtualizarSenhaRepetirSenha").val());

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "administrativo/vendedores/atualizarSenha",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAtualizarSenhaBtnAtualizar")
                        .html("Atualizando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    $("#formAtualizarSenha").trigger("reset");
                    $("#modalAtualizarSenha").modal("hide");
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAtualizarSenhaBtnAtualizar")
                    .html("Atualizar")
                    .removeAttr("disabled");
        });
    }
});

$("#modalAtualizarVendedor").on("show.bs.modal", function (e) {
    let id = $(e.relatedTarget).data("id");

    $.post($("head").data("info") + "administrativo/vendedores/getVendedor",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $("#modalAtualizarVendedorId").val(r.vendedor.id);
                    $("#modalAtualizarVendedorNome").val(r.vendedor.nome);
                    $("#modalAtualizarVendedorCpf").val(r.vendedor.cpf);
                    $("#modalAtualizarVendedorCelular").val(r.vendedor.celular);
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON"
            )
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de suporte"
                });
            });
});

$("#modalAtualizarSenha").on("show.bs.modal", function (e) {
    $("#modalAtualizarSenhaId").val($(e.relatedTarget).data("id-usuario"));
});

$("#tabelaVendedores").on("click", ".btn-atualizar-status", function () {
    let idUsuario = $(this).data("id-usuario");
    let id = $(this).data("id");
    let status = $(this).data("status");
    let toast = $.toast({
        heading: "<strong style='color: white'>Processando</strong>",
        icon: "info",
        text: "Atualizando status",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "administrativo/vendedores/atualizarStatus",
            {ajax: true, id: idUsuario, status: status},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: "<strong style='color: white'>Tudo certo</strong>",
                        icon: "success",
                        text: "Status atualizando",
                        position: "top-right",
                        hideAfter: 4000,
                        beforeShow: function () {
                            $(`#status-${id}`).html(status.charAt(0).toUpperCase() + status.slice(1));
                        }
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favo entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

function tabelaVendedoresAdicionar(vendedor) {
    $("#tabelaVendedores").append(`<tr id="${vendedor.id}">
                                        <td id="nome-${vendedor.id}">${vendedor.nome}</td>
                                        <td id="cpf-${vendedor.id}">${vendedor.cpf}</td>
                                        <td id="celular-${vendedor.id}">${vendedor.celular}</td>
                                        <td id="status-${vendedor.id}">${vendedor.status.charAt(0).toUpperCase() + vendedor.status.slice(1)}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Ações <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#" data-id="${vendedor.id}" data-toggle="modal" data-target="#modalAtualizarVendedor">Atualizar</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" data-id-usuario="${vendedor.idUsuario} ?>" data-toggle="modal" data-target="#modalAtualizarSenha">Atualizar senha</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-atualizar-status" data-id="${vendedor.id}" data-id-id="${vendedor.idUsuario}" data-status="ativo">Ativar</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-atualizar-status" data-id="${vendedor.id}" data-id-id="${vendedor.idUsuario}" data-status="inativo">Desativar</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>`);
}