$().ready(function () {
    $("#cpf").mask("999.999.999-99");
    $("#celular").mask("(99) 99999-9999");
});

$("#formAdicionarVendedor").validate({
    rules: {
        nome: {required: true},
        usuario: {
            required: true,
            email: true
        },
        senha: {
            required: true,
            minlength: 4
        },
        repetirSenha: {
            required: true,
            equalTo: "#senha"
        }
    },
    messages: {
        nome: {required: "Campo obrigatório"},
        usuario: {
            required: "Campo obrigatório",
            email: "Digite um e-mail válido"
        },
        senha: {
            required: "Campo obrigatório",
            minlength: "A senha deve ter pelo menos 4 caracteres"
        },
        repetirSenha: {
            required: "Campo obrigatório",
            equalTo: "As senhas não coincidem"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAdicionarVendedor")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "administrativo/vendedores/adicionar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAdicionarVendedorBtnAdicionar")
                        .html("Adicionando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Tudo certo</strong>`,
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    beforeShow: function () {
                        $("#formAdicionarVendedor").trigger("reset");
                        $("#nome").focus();
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarVendedorBtnAdicionar")
                    .html("Adicionar")
                    .removeAttr("disabled");
        });
    }
});