$().ready(function () {
    $("#parcelas").mask("99");
    $("#vencimento").mask("99/99/9999");
    $("#valor").mask("#.##0,00", {reverse: true});
    $("#modalAtualizarParcelaValor").mask("#.##0,00", {reverse: true});
    $("#modalAtualizarParcelaVencimento").mask("99/99/9999");
});

$("#formGerarParcelas").validate({
    rules: {
        parcelas: {required: true},
        vencimento: {
            required: true,
            minlength: 9
        },
        categoria: {required: true},
        valor: {required: true}
    },
    messages: {
        parcelas: {required: "Campo obrigatório"},
        vencimento: {
            required: "Campo obrigatório",
            minlength: "Digite uma data válida"
        },
        categoria: {required: "Campo obrigatório"},
        valor: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        var valor = $("#valor").val().replace(".", "");
        valor = parseFloat(valor.replace(",", "."));
        let dados = new FormData($("#formGerarParcelas")[0]);
        dados.append("ajax", true);
        dados.set("valor", valor);
        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "administrativo/contratos/gerarParcelas",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formGerarParcelasBtnGerar")
                        .html("Gerando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Tudo certo</strong>`,
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    beforeShow: function () {
                        tabelaParcelasListar(r.parcelas);
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formGerarParcelasBtnGerar")
                    .html("Gerar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAtualizarParcela").validate({
    rules: {
        modalAtualizrParcelaId: {required: true},
        modalAtualizrParcelaValor: {required: true},
        modalAtualizrParcelaVencimento: {
            required: true,
            minlength: 9
        }
    },
    messages: {
        modalAtualizrParcelaId: {required: "Campo obrigatório"},
        modalAtualizrParcelaValor: {required: "Campo obrigatório"},
        modalAtualizrParcelaVencimento: {
            required: "Campo obrigatório",
            minlength: "Digite uma data válida"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        var valor = $("#modalAtualizarParcelaValor").val().replace(".", "");
        valor = parseFloat(valor.replace(",", "."));
        let dados = new FormData();
        dados.append("ajax", true);
        dados.set("id", $("#modalAtualizarParcelaId").val());
        dados.set("idContaReceber", $("#modalAtualizarParcelaIdContaReceber").val());
        dados.set("valor", valor);
        dados.set("vencimento", $("#modalAtualizarParcelaVencimento").val());
        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/parcelas/atualizar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAtualizarParcelaBtnAtualizar")
                        .html("Atualizando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Tudo certo</strong>`,
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    beforeShow: function () {
                        $(`#valor-${dados.get("id")}`).html(`R$ ${formatarValor(parseFloat(dados.get("valor")))}`);
                        $(`#vencimento-${dados.get("id")}`).html(`${dados.get("vencimento")}`);
                        $("#formAtualizarParcela").trigger("reset");
                        $("#modalAtualizarParcela").modal("hide");
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAtualizarParcelaBtnAtualizar")
                    .html("Atualizar")
                    .removeAttr("disabled");
        });
    }
});

$("#modalAtualizarParcela").on("show.bs.modal", function (e) {
    let id = $(e.relatedTarget).data("id");
    $("#modalAtualizarParcelaLoader").html(`<div class="alert alert-info" role="alert"><i class="fa fa-spinner fa-spin"></i> Buscando</div>`);
    $.post($("head").data("info") + "financeiro/parcelas/getParcela",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $("#modalAtualizarParcelaId").val(r.parcela.id);
                    $("#modalAtualizarParcelaIdContaReceber").val(r.parcela.idContaReceber);
                    $("#modalAtualizarParcelaValor").val(formatarValor(parseFloat(r.parcela.valor)));
                    $("#modalAtualizarParcelaVencimento").val(formatarData(r.parcela.vencimento));
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: r.msg
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                $("#modalAtualizarParcelaLoader").empty();
            });
});

$("#tabelaParcelas").on("click", ".btn-baixar", function () {
    let id = $(this).data("id");
    let idContaReceber = $(this).data("id-conta-receber");

    let toast = $.toast({
        heading: `<strong style="color:white">Processando</strong>`,
        icon: "info",
        text: "Dando baixa na parcela",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "financeiro/parcelas/baixar",
            {ajax: true, id: id, idContaReceber: idContaReceber},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: `<strong style="color:white">Tudo certo</strong>`,
                        icon: "success",
                        text: r.msg,
                        position: "top-right",
                        beforeShow: function () {
                            $(`#${id}`).addClass("success");
                            $(`#baixada-${id}`).html(`<i class="ti-arrow-down"></i>`);
                            $(`#status-${id}`).html(`Recebido`);
                        },
                        hideAfter: 4000
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

$("#tabelaParcelas").on("click", ".btn-deletar", function () {
    let id = $(this).data("id");
    let toast = $.toast({
        heading: `<strong style="color:white">Processando</strong>`,
        icon: "info",
        text: "Deletando parcela",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "financeiro/parcelas/deletar",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: `<strong style="color:white">Tudo certo</strong>`,
                        icon: "success",
                        text: r.msg,
                        position: "top-right",
                        beforeShow: function () {
                            $(`#${id}`).remove();
                        },
                        hideAfter: 4000
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

function tabelaParcelasListar(parcelas) {
    $("#tabelaParcelas").empty();
    $.each(parcelas, function (key, parcela) {
        $("#tabelaParcelas").append(`<tr id="${parcela.id}">
                                        <td id="baixada-${parcela.id}"></td>
                                        <td id="parcela-${parcela.id}">${parcela.parcela}/${parcela.totalParcelamento}</td>
                                        <td id="valor-${parcela.id}">R$ ${formatarValor(parseFloat(parcela.valor))}</td>
                                        <td id="vencimento-${parcela.id}">${formatarData(parcela.vencimento)}</td>
                                        <td id="status-${parcela.id}">${parcela.status.charAt(0).toUpperCase() + parcela.status.slice(1)}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Ações <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" data-id="${parcela.id}" data-toggle="modal" data-target="#modalAtualizarParcela">Atualizar</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" data-id="${parcela.id}" data-id-conta-receber="${parcela.idContaReceber}">Baixar</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="${$("head").data("info") + "financeiro/boleto/" + parcela.id}" target="_blank">Visualizar boleto</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-deletar" data-id="${parcela.id}">Deletar</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>`);
    });
}