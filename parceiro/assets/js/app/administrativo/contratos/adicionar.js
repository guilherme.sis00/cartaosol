$().ready(function () {
    $("#ano").mask("9999");
    $("#duracao").mask("99");
    $("#dataAssinatura").mask("99/99/9999");
});

$("#formAdicionarContrato").validate({
    rules: {
        cliente: {required: true},
        vendedor: {required: true},
        codigoContrato: {
            required: true,
            digits: true
        },
        ano: {required: true},
        duracao: {required: true},
        situacao: {required: true},
        dataAssinatura: {required: true}
    },
    messages: {
        cliente: {required: "Campo obrigatório"},
        vendedor: {required: "Campo obrigatório"},
        codigoContrato: {
            required: "Campo obrigatório",
            digits: "Código inválido"
        },
        ano: {required: "Campo obrigatório"},
        duracao: {required: "Campo obrigatório"},
        situacao: {required: "Campo obrigatório"},
        dataAssinatura: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAdicionarContrato")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "administrativo/contratos/adicionar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formAdicionarContratoBtnAdicionar")
                        .html("Adicionando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Tudo certo</strong>`,
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    beforeShow: function () {
                        $("#formAdicionarContrato").trigger("reset");
                        $("#cliente").focus();
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarContratoBtnAdicionar")
                    .html("Adicionar")
                    .removeAttr("disabled");
        });
    }
});