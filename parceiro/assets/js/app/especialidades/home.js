$("#formBuscarEspecialidade").validate({
    rules: {
        termo: {
            required: true,
            minlength: 3
        },
        tipoEspecialidade: {required: true}
    },
    messages: {
        termo: {
            required: "Campo obrigatório",
            minlength: "Digite pelo menos 3 caracteres"
        },
        tipoEspecialidade: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formBuscarEspecialidade")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "especialidades/buscar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formBuscarEspecialidadeBtnBuscar")
                        .empty()
                        .append("Buscando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#tabelaEspecialidades").empty();
                $("#divPaginacao").empty();
                $.each(r.especialidades, function (key, especialidade) {
                    $("#tabelaEspecialidades").append(`<tr>
                                                            <td>${especialidade.especialidade}</td>
                                                            <td>${especialidade.tipo}</td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="${$("head").data("info")}/especialidade/${especialidade.id}">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="${$("head").data("info")}/especialidade/deletar/${especialidade.id}">Deletar</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>`);
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarEspecialidadeBtnBuscar")
                    .empty()
                    .append("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAdicionarCidade").validate({
    rules: {
        modalAdicionarCidadeEstado: {required: true},
        modalAdicionarCidadeCidade: {required: true}
    },
    messages: {
        modalAdicionarCidadeEstado: {required: "Campo obrigatório"},
        modalAdicionarCidadeCidade: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAdicionarCidade")[0]);
        dados.append("ajax", true);
        dados.append("estado", $("#modalAdicionarCidadeEstado").val());
        dados.append("cidade", $("#modalAdicionarCidadeCidade").val());
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "support/adicionarCidade",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAdicionarCidadeBtnAdicionar")
                        .empty()
                        .append("Adicionando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    $("#modalAdicionarCidade").modal("hide");
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarCidadeBtnAdicionar")
                    .empty()
                    .append("Adicionar")
                    .removeAttr("disabled");
        });
    }
});

$("#modalVisualizarCidade").on("show.bs.modal", function (e) {
    let id = $(e.relatedTarget).data("id");

    $.post($("head").data("info") + "cidades/getById",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $("#modalVisualizarCidadeEstado").val(r.cidade.estado);
                    $("#modalVisualizarCidadeCidade").val(r.cidade.cidade);
                    $("#modalVisualizarCidadeTipo").val(r.cidade.tipo[0].toUpperCase() + r.cidade.tipo.slice(1));
                    $("#modalVisualizarCidadeBtnAtualizar").attr("href", $("head").data("info") + "cidade/" + r.cidade.id);
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON"
            )
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de suporte"
                });
            });
});