$().ready(function () {
    $("#cpf").mask("000.000.000-00");
    $("#cartao").mask("0000.0000.0000.0000");
});

$("#formConsultar").on("submit", function (e) {
    e.preventDefault();

    if ($("#nome").val() !== "" || $("#cpf").val() !== "" || $("#cartao").val() !== "") {
        $("#formConsultarBtnConsultar")
                .html("Consultando")
                .attr("disabled", "disabled");

        $.post($("head").data("info") + "dashboard/consultarCliente",
                {ajax: true, nome: $("#nome").val(), cpf: $("#cpf").val(), cartao: $("#cartao").val()},
                function (r) {
                    if (r.resultado) {
                        $("#tabelaClientes").empty();
                        $("#tabelaDependentes").empty();
                        
                        $.each(r.clientes, function (key, cliente) {
                            $("#tabelaClientes").append(`<tr>
                                                            <td class="text-center"><i class="fa fa-circle" style="color:${r.farois[cliente.id]}"></i></td>
                                                            <td>${cliente.nome}</td>
                                                            <td>${cliente.cpf}</td>
                                                        </tr>`);
                        });
                        
                        $.each(r.dependentes, function (key, cliente) {
                            $("#tabelaDependentes").append(`<tr>
                                                            <td class="text-center"><i class="fa fa-circle" style="color:${r.faroisDependentes[cliente.idCliente]}"></i></td>
                                                            <td>${cliente.nome}</td>
                                                            <td>${cliente.cpf}</td>
                                                            </td>
                                                        </tr>`);
                        });
                    } else {
                        if (r.sessoExpirada) {
                            swal({
                                title: "Atenção",
                                icon: "info",
                                text: "Sua sessão expirou"
                            }).then(() => {
                                location.href = $("head").data("info");
                            });
                        } else {
                            swal({
                                title: "Atenção",
                                icon: "warning",
                                text: r.msg
                            });
                        }
                    }
                },
                "JSON")
                .fail(function () {
                    swal({
                        title: "Falha de comunicação",
                        icon: "error",
                        text: "Por favor, entre em contato com a equipe de spuorte"
                    });
                })
                .always(function () {
                    $("#formConsultarBtnConsultar")
                            .html("Consultar")
                            .removeAttr("disabled");
                });
    } else {
        $.toast({
            heading: `<strong style="color:white">Atenção</strong>`,
            icon: "warning",
            position: "top-right",
            text: "Preencha o NOME ou o CPF do cliente",
            hideAfter: 4000
        });
    }
});

$("#modalVisualizarDependentes").on("show.bs.modal", function (e) {
    $("#modalVisualizarDependentesLoader").html(`<div class="alert alert-info">
                                                <i class="fa fa-circle-o-notch fa-spin"></i> Buscando dependentes
                                            </div>`);
    $("#tabelaDependentes").empty();

    let idCliente = $(e.relatedTarget).data("id");


    $.post($("head").data("info") + "dashboard/getDependentes",
            {ajax: true, idCliente: idCliente},
            function (r) {
                if (r.resultado) {
                    $("#tabelaDependentes").empty();

                    $.each(r.dependentes, function (key, dependente) {
                        $("#tabelaDependentes").append(`<tr>
                                                            <!--td class="text-center"><i class="fa fa-circle" style="color:green"></i></td-->
                                                            <td>${dependente.nome}<br>${dependente.cpf}</td>
                                                            <td>${formatarData(dependente.dataNascimento)}<br>${formatarData(dependente.dataVinculacao)}</td>
                                                            <td>${dependente.numero}</td>
                                                        </tr>`);
                    });
                } else {
                    if (r.sessoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de spuorte"
                });
            })
            .always(function () {
                $("#modalVisualizarDependentesLoader").empty();
            });
});