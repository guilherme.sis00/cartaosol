$("#formBuscarCidade").validate({
    rules: {
        termo: {
            required: true,
            minlength: 3
        },
        tipoBusca: {required: true}
    },
    messages: {
        termo: {
            required: "Campo obrigatório",
            minlength: "Digite pelo menos 3 caracteres"
        },
        tipoBusca: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formBuscarCidade")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "cidades/buscar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formBuscarCidadeBtnBuscar")
                        .empty()
                        .append("Buscando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#tabelaCidades").empty();
                $("#divPaginacao").empty();
                $.each(r.cidades, function (key, cidade) {
                    $("#tabelaCidades").append(`<tr>
                                                    <td>${cidade.estado}</td>
                                                    <td>${cidade.cidade}</td>
                                                    <td>${cidade.tipo[0].toUpperCase() + cidade.tipo.slice(1)}</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Ações <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" data-id="${cidade.id}" data-toggle="modal" data-target="#modalVisualizarCidade">Visualizar</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="${$("head").data("info")}/cidade/${cidade.id}">Atualizar</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="${$("head").data("info")}cidade/deletar/${cidade.id}")">Deletar</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>`);
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarCidadeBtnBuscar")
                    .empty()
                    .append("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAlterarSenha").validate({
    rules: {
        modalAlterarSenhaIdUsuario: {required: true},
        modalAlterarSenhaSenha: {required: true},
        modalAlterarSenhaRepetirSenha: {
            required: true,
            equalTo: "#modalAlterarSenhaSenha"
        }
    },
    messages: {
        modalAlterarSenhaIdUsuario: {required: "Campo obrigatório"},
        modalAlterarSenhaSenha: {required: "Campo obrigatório"},
        modalAlterarSenhaRepetirSenha: {
            required: "Campo obrigatório",
            equalTo: "As senha não coincidem"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAlterarSenha")[0]);
        dados.append("ajax", true);
        dados.append("id", $("#modalAlterarSenhaIdUsuario").val());
        dados.append("senha", $("#modalAlterarSenhaSenha").val());
        dados.append("repetirSenha", $("#modalAlterarSenhaRepetirSenha").val());

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "usuarios/alterarSenha",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#modalAlterarSenhaBtnAtualizar")
                        .empty()
                        .append("Atualizando senha...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    $("#modalAlterarSenha").modal("hide");
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#modalAlterarSenhaBtnAtualizar")
                    .empty()
                    .append("Atualizar senha")
                    .removeAttr("disabled");
        });
    }
});

$("#modalAlterarSenha").on("show.bs.modal", function (e) {
    $("#modalAlterarSenhaIdUsuario").val($(e.relatedTarget).data("id"));
    $("#modalAlterarSenhaUsuario").val($(e.relatedTarget).data("usuario"));
});