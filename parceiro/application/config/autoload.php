<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('session', 'database', 'form_validation','pagination', 'sessao', 'configuracao');
$autoload['drivers'] = array();
$autoload['helper'] = array('url','utils');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array('utils');
