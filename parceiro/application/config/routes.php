<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//LOGIN
$route["sair"] = "login/sair";


//NÃO FUNCIONAL
//MEUS DADOS
$route["meus-dados"] = "meusDados";

//USUÁRIOS
$route["usuarios/(:num)"] = "usuarios";
$route["usuario/adicionar"] = "usuarios/vAdicionar";
$route["usuario/(:num)"] = "usuarios/vAtualizar";

//ADMINISTRATIVO/DADOS EMPRESA
$route["administrativo/dados-empresa"] = "administrativo/dadosEmpresa";

//ADMINISTRATIVO/CONTRATOS
$route["administrativo/contratos/(:num)"] = "administrativo/contratos";
$route["administrativo/contrato/adicionar"] = "administrativo/contratos/vAdicionar";
$route["administrativo/contrato/(:num)"] = "administrativo/contratos/vAtualizar";
$route["administrativo/contrato/gerar-parcelas/(:num)"] = "administrativo/contratos/vGerarParcelas";

//ADMINISTRATIVO/DEPENDENTES
$route["administrativo/dependentes/(:num)"] = "administrativo/dependentes";
$route["administrativo/dependente/adicionar"] = "administrativo/dependentes/vAdicionar";
$route["administrativo/dependente/(:num)"] = "administrativo/dependentes/vAtualizar";

$route["administrativo/contrato/(:num)"] = "administrativo/contratos/vAtualizar";
$route["administrativo/contrato/gerar-parcelas/(:num)"] = "administrativo/contratos/vGerarParcelas";

//ADMINISTRATIVO/VENDEDORES
$route["administrativo/vendedores/(:num)"] = "administrativo/vendedores";
$route["administrativo/vendedor/adicionar"] = "administrativo/vendedores/vAdicionar";

//FINANCEIRO/CONTAS A RECEBER
$route["financeiro/contas-receber"] = "financeiro/contasReceber";
$route["financeiro/contas-receber/adicionar"] = "financeiro/contasReceber/vAdicionar";
$route["financeiro/conta-receber/(:num)"] = "financeiro/contasReceber/vAtualizar";

//FINANCEIRO/CONTAS A RECEBER
$route["financeiro/contas-pagar"] = "financeiro/contasPagar";
$route["financeiro/conta-pagar/(:num)"] = "financeiro/contasPagar/vAtualizar";
$route["financeiro/contas-pagar/adicionar"] = "financeiro/contasPagar/vAdicionar";

//FINANCEIRO/BOLETOS
$route["financeiro/boleto/(:num)"] = "financeiro/boletos/visualizarPorParcela";
$route["financeiro/contas-receber/boleto/(:num)"] = "financeiro/boletos/visualizarPorReceita";

//VENDAS/COMBOS
$route["vendas/combos/(:num)"] = "vendas/combos";
$route["vendas/combo/adicionar"] = "vendas/combos/vAdicionar";
$route["vendas/combo/(:num)"] = "vendas/combos/vAtualizar";
$route["vendas/combos/procedimentos/(:num)"] = "vendas/combos/vProcedimentos";

// NÃO ORGANIZADOS
//CLIENTES
$route["clientes/(:num)"] = "clientes";
$route["cliente/adicionar"] = "clientes/vAdicionar";
$route["cliente/(:num)"] = "clientes/vAtualizar";

//CIDADES
$route["cidades/(:num)"] = "cidades";
$route["cidade/adicionar"] = "cidades/vAdicionar";
$route["cidade/(:num)"] = "cidades/vAtualizar";
$route["cidade/deletar/(:num)"] = "cidades/deletar";

//ESPECIALIDADES
$route["especialidades/(:num)"] = "especialidades";
$route["especialidade/adicionar"] = "especialidades/vAdicionar";
$route["especialidade/(:num)"] = "especialidades/vAtualizar";
$route["especialidade/deletar/(:num)"] = "especialidades/deletar";

//PARCEIROS
$route["parceiros/(:num)"] = "parceiros";
$route["parceiro/adicionar"] = "parceiros/vAdicionar";
$route["parceiro/(:num)"] = "parceiros/vAtualizar";
