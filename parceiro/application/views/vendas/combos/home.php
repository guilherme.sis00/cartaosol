<?php $this->load->view("static/header", ["title" => "Combos"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "vendas"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Vendas / Combos"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-<?= $this->session->flashdata("tipo") ?>" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <form id="formBuscar">
                                            <div class="col-md-8 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="termo" name="termo" class="form-control border-input" placeholder="Digite um termo de busca">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarBtnBuscar">Buscar</button>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("vendas/combo/adicionar") ?>" class="btn btn-success btn-fill btn-block">Adicionar</a>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Combo</th>
                                                        <th>Valor</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaCombos">
                                                    <?php foreach ($combos as $combo) : ?>
                                                        <tr id="<?= $combo->id ?>">
                                                            <td id="nome-<?= $combo->id ?>"><?= $combo->nome ?></td>
                                                            <td id="valor-<?= $combo->id ?>">R$ <?= number_format($combo->valor, 2, ",", ".") ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                                        <li><a href="#" data-toggle="modal" data-target="#modalAtualizarCombo" data-id="<?= $combo->id ?>">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="<?= base_url("vendas/combos/procedimentos/{$combo->id}") ?>">Procedimentos</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" class="btn-deletar" data-id="<?= $combo->id ?>">Deletar</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" id="divPaginacao">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8">
                                            <?= $paginacao ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <?php $this->load->view("static/modals/atualizarCombo") ?>
</body>
<script src="<?= base_url("assets/js/app/vendas/combos/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
