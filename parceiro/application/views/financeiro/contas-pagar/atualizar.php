<?php $this->load->view("static/header", ["title" => "Financeiro - Contas a pagar"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "contas-pagar"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Contas a pagar / Atualizar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAtualizarContaPagar">
                                        <input type="hidden" id="id" name="id" value="<?= $contaPagar->id ?>">
                                        <div class="row">
                                            <div class="form-group col-md-6 col-xs-12">
                                                <label for="descricao">Descrição</label>
                                                <input type="text" class="form-control border-input" id="descricao" name="descricao" placeholder="Descrição da saída" value="<?= $contaPagar->descricao ?>" autofocus="">
                                            </div>
                                            <div class="form-group col-md-3 col-xs-12">
                                                <label for="fornecedor">Fornecedor</label>
                                                <select class="form-control border-input" id="fornecedor" name="fornecedor">
                                                    <option value="">--</option>
                                                    <?php foreach ($fornecedores as $fornecedor) : ?>
                                                        <?php if ($fornecedor->id == $contaPagar->idFornecedor): ?>
                                                            <option selected="selected" value="<?= $fornecedor->id ?>"><?= $fornecedor->nome ?></option>
                                                        <?php else: ?>
                                                            <option value="<?= $fornecedor->id ?>"><?= $fornecedor->nome ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3 col-xs-12">
                                                <label for="valor">Valor (R$)</label>
                                                <input type="text" class="form-control border-input" id="valor" name="valor" value="<?= number_format($contaPagar->valor, 2, ".", ",") ?>" placeholder="0,00">
                                            </div>
                                        </div>
                                        <div class="row margin-b">
                                            <div class="form-group col-md-4 col-xs-12">
                                                <label for="data">Data</label>
                                                <input type="text" class="form-control border-input" id="data" name="data" value="<?= date("d/m/Y", strtotime($contaPagar->data)) ?>" placeholder="99/99/9999">
                                            </div>
                                            <div class="form-group col-md-4 col-xs-12">
                                                <label for="categoria">Categoria</label>
                                                <select id="categoria" name="categoria" class="form-control border-input">
                                                    <option value="">--</option>
                                                    <?php foreach ($categorias as $categoria) : ?>
                                                        <?php if ($contaPagar->idCategoria == $categoria->id): ?>
                                                            <option selected="selected" value="<?= $categoria->id ?>"><?= $categoria->categoria ?></option>
                                                        <?php else: ?>
                                                            <?php if ($categoria->tipo == "saida"): ?>
                                                                <option value="<?= $categoria->id ?>"><?= $categoria->categoria ?></option>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 col-xs-12">
                                                <label for="status">Status</label>
                                                <select id="status" name="status" class="form-control border-input">
                                                    <option <?= $contaPagar->status == "pago" ? "selected='selected'" : "" ?> value="pago">Pago
                                                    <option <?= $contaPagar->status == "pendente" ? "selected='selected'" : "" ?> value="pendente">Pendente</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <a href="<?= base_url("financeiro/contas-pagar") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <a href="#" data-toggle="modal" data-target="#modalAdicionarCategoria" class="btn btn-primary btn-fill btn-block">Categoria</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAtualizarContaPagarBtnAtualizar">Atualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <?php $this->load->view("static/modals/adicionarCategoriaFinanceira") ?>
</body>
<script src="<?= base_url("assets/js/app/financeiro/contas-pagar/atualizar.js") ?>"></script>
<script src="<?= base_url("assets/js/app/utils/categoriasFinanceiras.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
