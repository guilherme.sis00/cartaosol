<?php $this->load->view("static/header", ["title" => "Financeiro - Contas a pagar"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "contas-pagar"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Contas a pagar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row margin-b">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-warning" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <input type="hidden" id="dataAtual" name="dataAtual" value="<?= date("d/m/Y") ?>">
                                    <form id="formBuscar">
                                        <div class="row">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="termo" name="termo" class="form-control border-input" placeholder="Buscar conta a pagar">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <select id="categoria" name="categoria" class="form-control border-input">
                                                        <option value="todas">Todas</option>
                                                        <?php foreach ($categorias as $categoria) : ?>
                                                            <option value="<?= $categoria->id ?>"><?= $categoria->categoria ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="dataInicial" name="dataInicial" class="form-control border-input" placeholder="Data inicial">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="dataFinal" name="dataFinal" class="form-control border-input" placeholder="Data final">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12 col-md-offset-8 margin-b">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarBtnBuscar">Buscar</button>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("financeiro/contas-pagar/adicionar") ?>" class="btn btn-success btn-fill btn-block">Adicionar</a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8">
                                            <label for="mesContasPagar">Mês atual</label>
                                            <select id="mesContasPagar" name="mesContasPagar" class="form-control border-input">
                                                <option <?= $mesAtual == "Janeiro" ? "selected='selected'" : "" ?>value="01">Janeiro</option>
                                                <option <?= $mesAtual == "Fevereiro" ? "selected='selected'" : "" ?>value="02">Fevereiro</option>
                                                <option <?= $mesAtual == "Março" ? "selected='selected'" : "" ?>value="03">Março</option>
                                                <option <?= $mesAtual == "Abril" ? "selected='selected'" : "" ?>value="04">Abril</option>
                                                <option <?= $mesAtual == "Maio" ? "selected='selected'" : "" ?>value="05">Maio</option>
                                                <option <?= $mesAtual == "Junho" ? "selected='selected'" : "" ?>value="06">Junho</option>
                                                <option <?= $mesAtual == "Julho" ? "selected='selected'" : "" ?>value="07">Julho</option>
                                                <option <?= $mesAtual == "Agosto" ? "selected='selected'" : "" ?>value="08">Agosto</option>
                                                <option <?= $mesAtual == "Setembro" ? "selected='selected'" : "" ?>value="09">Setembro</option>
                                                <option <?= $mesAtual == "Outubro" ? "selected='selected'" : "" ?>value="10">Outubro</option>
                                                <option <?= $mesAtual == "Novembro" ? "selected='selected'" : "" ?>value="11">Novembro</option>
                                                <option <?= $mesAtual == "Dezembro" ? "selected='selected'" : "" ?>value="12">Dezembro</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8 content table-responsive">
                                            <table class="table">
                                                <tbody id="tabelaContasPagarInformacoes">
                                                    <tr class="success">
                                                        <td id="">Total pago</td>
                                                        <td id="total-pago">R$ <?= number_format($pago->total, 2, ",", ".") ?></td>
                                                    </tr>
                                                    <tr class="danger">
                                                        <td id="">Total pendente</td>
                                                        <td id="total-pendente">R$ <?= number_format($pendente->total, 2, ",", ".") ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Descrição</th>
                                                        <th>Fornecedor</th>
                                                        <th>Categoria</th>
                                                        <th>Valor</th>
                                                        <th>Data</th>
                                                        <th>Status</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaContasPagar">
                                                    <?php foreach ($contasPagar as $contaPagar) : ?>
                                                        <tr id="<?= $contaPagar->id ?>" class="<?= (strtotime($contaPagar->data) < strtotime(date("Y-m-d"))) && $contaPagar->status == "pendente" ? "danger" : "" ?>">
                                                            <td id="descricao-<?= $contaPagar->id ?>"><?= $contaPagar->descricao ?></td>
                                                            <td id="fornecedor-<?= $contaPagar->id ?>"><?= $contaPagar->nome ?></td>
                                                            <td id="categoria-<?= $contaPagar->id ?>"><?= $contaPagar->categoria ?></td>
                                                            <td id="valor-<?= $contaPagar->id ?>">R$ <?= number_format($contaPagar->valor, 2, ",", ".") ?></td>
                                                            <td id="data-<?= $contaPagar->id ?>"><?= date("d/m/Y", strtotime($contaPagar->data)) ?></td>
                                                            <td id="status-<?= $contaPagar->id ?>"><?= ucfirst($contaPagar->status) ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                                        <li><a href="<?= base_url("financeiro/conta-pagar/{$contaPagar->id}") ?>">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" class="btn-status" data-id="<?= $contaPagar->id ?>" data-status="pago">Pago</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" class="btn-status" data-id="<?= $contaPagar->id ?>" data-status="pendente">Pendente</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" class="btn-deletar" data-id="<?= $contaPagar->id ?>">Deletar</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/financeiro/contas-pagar/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
