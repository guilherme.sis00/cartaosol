<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Boleto | O cartão para todos</title>
    </head>
    <body>
        <?php if ($this->session->flashdata("msg")): ?>
            <h1><?= $this->session->flashdata("msg") ?></h1>
        <?php else: ?>
            <?= $boleto; ?>
        <?php endif; ?>
    </body>
</html>