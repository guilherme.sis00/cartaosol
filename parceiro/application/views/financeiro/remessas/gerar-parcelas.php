<?php $this->load->view("static/header", ["title" => "Administrativo - Contratos"]) ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "contratos"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Contratos / Gerar parcelas"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formGerarParcelas">
                                        <input type="hidden" id="contrato" name="contrato" value="<?= $contrato->id ?>">
                                        <input type="hidden" id="nomeCliente" name="nomeCliente" value="<?= $contrato->nome ?>">
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="contrato">Contrato</label>
                                                    <input type="text" disabled id="contrato" name="contrato" class="form-control border-input" value="<?= $contrato->id ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cliente">Cliente</label>
                                                    <input type="text" disabled id="cliente" name="cliente" class="form-control border-input" value="<?= $contrato->nome ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="duracao">Duração do contrato (meses)</label>
                                                    <input disabled type="text" id="duracao" name="duracao" class="form-control border-input" placeholder="00" value="<?= $contrato->duracao ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="dataAssinatura">Assinatura do contrato</label>
                                                    <input disabled type="text" id="dataAssinatura" name="dataAssinatura" class="form-control border-input" placeholder="00/00/0000" value="<?= date("d/m/Y", strtotime($contrato->dataAssinatura)) ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="parcelas">Parcelas</label>
                                                    <input type="text" id="parcelas" name="parcelas" class="form-control border-input" placeholder="00" value="<?= $contrato->duracao ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="vencimento">Data do primeiro vencimento</label>
                                                    <input type="text" id="vencimento" name="vencimento" class="form-control border-input" placeholder="00" value="<?= date("d/m/Y", strtotime("+30 days", strtotime($contrato->dataAssinatura))) ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="valor">Valor (R$)</label>
                                                    <input type="text" id="valor" name="valor" class="form-control border-input" placeholder="0,00">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="categoria">Categoria</label>
                                                    <select id="categoria" name="categoria" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($categorias as $categoria) : ?>
                                                            <option value="<?= $categoria->id ?>"><?= $categoria->categoria ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="instrucao1">Instrução 1</label>
                                                    <input type="text" id="instrucao1" name="instrucao1" class="form-control border-input" placeholder="Instrução">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="instrucao2">Instrução 2</label>
                                                    <input type="text" id="instrucao2" name="instrucao2" class="form-control border-input" placeholder="Instrução">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="instrucao3">Instrução 3</label>
                                                    <input type="text" id="instrucao3" name="instrucao3" class="form-control border-input" placeholder="Instrução">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="instrucao4">Instrução 4</label>
                                                    <input type="text" id="instrucao4" name="instrucao4" class="form-control border-input" placeholder="Instrução">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("administrativo/contratos") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formGerarParcelasBtnGerar">Gerar</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Parcela</th>
                                                        <th>Valor</th>
                                                        <th>Vencimento</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaParcelas">
                                                    <?php foreach ($parcelas as $parcela): ?>
                                                        <tr id="<?= $parcela->id ?>">
                                                            <td id="parcela-<?= $parcela->id ?>"><?= "{$parcela->parcela}/{$parcela->totalParcelamento}" ?></td>
                                                            <td id="valor-<?= $parcela->id ?>">R$ <?= number_format($parcela->valor, 2, ",", ".") ?></td>
                                                            <td id="vencimento-<?= $parcela->id ?>"><?= date("d/m/Y", strtotime($parcela->vencimento)) ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#" data-id="<?= $parcela->id ?>" data-toggle="modal" data-target="#modalAtualizarParcela">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="<?= base_url("financeiro/boleto/{$parcela->id}") ?>" target="_blank">Visualizar boleto</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <?= date("Y") ?> | Sistema OCTP | v1.0.0
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<div class="modal fade" id="modalAtualizarParcela" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Atualizar parcela</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-xs-12" id="modalAtualizarParcelaLoader">
                        <div class="alert alert-info" role="alert"><i class="fa fa-spinner fa-spin"></i> Buscando</div>
                    </div>
                </div>
                <form id="formAtualizarParcela">
                    <input type="hidden" id="modalAtualizarParcelaId" name="modalAtualizarParcelaId">
                    <input type="hidden" id="modalAtualizarParcelaIdContaReceber" name="modalAtualizarParcelaIdContaReceber">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalAtualizarParcelaValor">Valor (R$)</label>
                                <input type="text" id="modalAtualizarParcelaValor" name="modalAtualizarParcelaValor" class="form-control border-input" placeholder="0,00">
                            </div>
                        </div>
                    </div>
                    <div class="row margin-b">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalAtualizarParcelaVencimento">Vencimento</label>
                                <input type="text" id="modalAtualizarParcelaVencimento" name="modalAtualizarParcelaVencimento" class="form-control border-input" placeholder="00/00/0000">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <button type="submit" class="btn btn-success btn-fill btn-block" id="formAtualizarParcelaBtnAtualizar">Atualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("static/scripts") ?>
<script src="<?= base_url("assets/js/app/administrativo/contratos/gerarParcelas.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
