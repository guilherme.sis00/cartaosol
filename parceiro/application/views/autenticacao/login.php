<!doctype html>
<html lang="pt-br">
    <head data-info="<?= base_url() ?>">
        <meta charset="utf-8" />
        <!--<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">-->
        <!--<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Login | O cartão para todos</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
        <link href="<?= base_url('assets/css/animate.min.css') ?>" rel="stylesheet"/>
        <link href="<?= base_url('assets/css/paper-dashboard.css') ?>" rel="stylesheet"/>
        <link href="<?= base_url('assets/css/demo.css') ?>" rel="stylesheet" />
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="<?= base_url('assets/css/themify-icons.css') ?>" rel="stylesheet">
        <link href="<?= base_url('assets/css/login.css') ?>" rel="stylesheet">
        <script src="<?= base_url("assets/js/jquery-1.10.2.js") ?>" type="text/javascript"></script>
    </head>
    <body style="background-color: #F4F3EF">
        <div class="container">
            <div class="row" id="login">
                <div class="col-md-4 col-xs-12 col-md-offset-4" id="login-marca">
                    <a href="http://www.ocartaoparatodos.com.br"><img src="<?= base_url("assets/img/marca.png") ?>" class="img-responsive center-block" alt="Marca O cartão para todos"></a>
                </div>
                <div class="col-md-4 col-xs-12 col-md-offset-4" id="login-form">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div>
                                <form id="formAutenticar">
                                    <div class="form-group">
                                        <label for="usuario">Usuário</label>
                                        <input type="email" class="form-control border-input" id="usuario" name="usuario" placeholder="Digite o seu usuário">
                                    </div>
                                    <div class="form-group">
                                        <label for="senha">Senha</label>
                                        <input type="password" class="form-control border-input" id="senha" name="senha" placeholder="Digite a senha senha">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-success btn-fill btn-block" id="formAutenticarBtnEntrar">Entrar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery-validate/jquery.validate.min.js"></script>
    <script src="assets/js/jquery-validate/additional-methods.min.js"></script>
    <script src="assets/js/app/login/autenticacao.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</html>
