<?php $this->load->view("static/header", ["title" => "Administrativo - Contratos"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "contratos"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Contratos / Adicionar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAdicionarContrato">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cliente">Cliente</label>
                                                    <select id="cliente" name="cliente" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($clientes as $cliente) : ?>
                                                            <option value="<?= $cliente->id ?>"><?= $cliente->nome ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="vendedor">Vendedor</label>
                                                    <select id="vendedor" name="vendedor" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($vendedores as $vendedor) : ?>
                                                            <option value="<?= $vendedor->id ?>"><?= $vendedor->nome ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="codigoContrato">Código do contrato</label>
                                                    <input id="codigoContrato" name="codigoContrato" class="form-control border-input" placeholder="0000">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-b">
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="ano">Ano do contrato</label>
                                                    <input type="text" id="ano" name="ano" class="form-control border-input" placeholder="0000">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="duracao">Duração do contrato (meses)</label>
                                                    <input type="text" id="duracao" name="duracao" class="form-control border-input" placeholder="00">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="situacao">Situação</label>
                                                    <select id="situacao" name="situacao" class="form-control border-input">
                                                        <option value="ativo">Ativo</option>
                                                        <option value="inativo">Inativo</option>
                                                        <option value="inadimplente">Inadimplente</option>
                                                        <option value="finalizado">Finalizado</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="dataAssinatura">Assinatura do contrato</label>
                                                    <input type="text" id="dataAssinatura" name="dataAssinatura" class="form-control border-input" placeholder="00/00/0000">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("administrativo/contratos") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarContratoBtnAdicionar">Adicionar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <?= date("Y") ?> | Sistema OCTP | v1.0.0
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/administrativo/contratos/adicionar.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
