<?php $this->load->view("static/header", ["title" => "Administrativo - Contratos"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "contratos"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Contratos"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-warning" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <form id="formBuscar">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="codigoContrato" name="codigoContrato" class="form-control border-input" placeholder="Código do contrato">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="cliente" name="cliente" class="form-control border-input" placeholder="Nome do cliente">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="cpf" name="cpf" class="form-control border-input" placeholder="000.000.000-00">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12 col-md-offset-8">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarBtnBuscar">Buscar</button>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("administrativo/contrato/adicionar") ?>" class="btn btn-success btn-fill btn-block">Adicionar</a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Contrato</th>
                                                        <th>Cliente</th>
                                                        <th>Duração</th>
                                                        <th>Ano</th>
                                                        <th>Situação</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaContratos">
                                                    <?php foreach ($contratos as $contrato) : ?>
                                                        <tr id="<?= $contrato->id ?>">
                                                            <td id="contrato-<?= $contrato->id ?>" class="text-center"><?= $contrato->codigoContrato ?></td>
                                                            <td id="nome-<?= $contrato->id ?>"><?= $contrato->nome ?></td>
                                                            <td id="duracao-<?= $contrato->id ?>"><?= $contrato->duracao ?> meses</td>
                                                            <td id="ano-<?= $contrato->id ?>"><?= $contrato->ano ?></td>
                                                            <td id="situacao-<?= $contrato->id ?>"><?= ucfirst($contrato->situacao) ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a class="<?= $contrato->situacao == "finalizado" ? "disabled" : "" ?>" href="<?= base_url("administrativo/contrato/gerar-parcelas/{$contrato->id}") ?>">Gerar parcelas</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="<?= base_url("administrativo/contrato/{$contrato->id}") ?>">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" class="btn-deletar" data-id="<?= $contrato->id ?>">Deletar</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" id="divPaginacao">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8">
                                            <?= $paginacao ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <div class="modal fade" id="modalVisualizarCliente" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Visualizar cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteNome">Nome</label>
                                <input type="text" disabled id="modalVisualizarClienteNome" name="modalVisualizarClienteNome" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteCpf">CPF</label>
                                <input type="text" disabled id="modalVisualizarClienteCpf" name="modalVisualizarClienteCpf" class="form-control border-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteCelular">Celular</label>
                                <input type="text" disabled id="modalVisualizarClienteCelular" name="modalVisualizarClienteCelular" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteWhatsapp">Whatsapp</label>
                                <input type="text" disabled id="modalVisualizarClienteWhatsapp" name="modalVisualizarClienteWhatsapp" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteEmail">E-mail</label>
                                <input type="text" disabled id="modalVisualizarClienteEmail" name="modalVisualizarClienteEmail" class="form-control border-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteEstado">Estado</label>
                                <input type="text" disabled id="modalVisualizarClienteEstado" name="modalVisualizarClienteEstado" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteCidade">Cidade</label>
                                <input type="text" disabled id="modalVisualizarClienteCidade" name="modalVisualizarClienteCidade" class="form-control border-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteObservacoes">Observações</label>
                                <textarea rows="5" disabled id="modalVisualizarClienteObservacoes" name="modalVisualizarClienteObservacoes" class="form-control border-input"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-primary btn-fill" id="modalVisualizarClienteBtnAtualizar">Atualizar</a>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/administrativo/contratos/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
