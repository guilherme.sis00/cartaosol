<?php $this->load->view("static/header", ["title" => "Administrativo - Dependentes"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "dependentes"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Dependentes / Adicionar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAdicionarDependente">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="titular">Títular</label>
                                                    <select id="titular" name="titular" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($clientes as $cliente) : ?>
                                                            <option value="<?= $cliente->id ?>"><?= $cliente->nome ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" placeholder="Nome do dependente">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cpf">CPF</label>
                                                    <input type="text" id="cpf" name="cpf" class="form-control border-input" placeholder="000.000.000-00">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-b">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="numeroCartao">Número do cartão</label>
                                                    <input type="text" id="numeroCartao" name="numeroCartao" class="form-control border-input" placeholder="0000.0000.0000.0000">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="dataNascimento">Data de nascimento</label>
                                                    <input type="text" id="dataNascimento" name="dataNascimento" class="form-control border-input" placeholder="00/00/0000">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="dataVinculacao">Data de vinculação</label>
                                                    <input type="text" id="dataVinculacao" name="dataVinculacao" class="form-control border-input" placeholder="00/00/0000" value="<?= date("d/m/Y") ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("administrativo/dependentes") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarDependenteBtnAdicionar">Adicionar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <?= date("Y") ?> | Sistema OCTP | v1.0.0
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/administrativo/dependentes/adicionar.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
