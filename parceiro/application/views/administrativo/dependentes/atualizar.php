<?php $this->load->view("static/header", ["title" => "Administrativo - Dependentes"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "dependentes"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Dependentes / Atualizar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAtualizarDependente">
                                        <input type="hidden" id="id" name="id" value="<?= $dependente->id ?>">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="titular">Títular</label>
                                                    <select id="titular" name="titular" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($clientes as $cliente) : ?>
                                                            <?php if ($cliente->id == $dependente->idCliente): ?>
                                                                <option selected="selected" value="<?= $cliente->id ?>"><?= $cliente->nome ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $cliente->id ?>"><?= $cliente->nome ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" placeholder="Nome do dependente" value="<?= $dependente->nome ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cpf">CPF</label>
                                                    <input type="text" id="cpf" name="cpf" class="form-control border-input" placeholder="000.000.000-00" value="<?= $dependente->cpf ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-b">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="numeroCartao">Número do cartão</label>
                                                    <input type="text" id="numeroCartao" name="numeroCartao" class="form-control border-input" placeholder="0000.0000.0000.0000" value="<?= $dependente->numero ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="dataNascimento">Data de nascimento</label>
                                                    <input type="text" id="dataNascimento" name="dataNascimento" class="form-control border-input" placeholder="00/00/0000" value="<?= date("d/m/Y", strtotime($dependente->dataNascimento)) ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="dataVinculacao">Data de vinculação</label>
                                                    <input type="text" id="dataVinculacao" name="dataVinculacao" class="form-control border-input" placeholder="00/00/0000" value="<?= date("d/m/Y", strtotime($dependente->dataVinculacao)) ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("administrativo/dependentes") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAtualizarDependenteBtnAtualizar">Atualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/administrativo/dependentes/atualizar.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
