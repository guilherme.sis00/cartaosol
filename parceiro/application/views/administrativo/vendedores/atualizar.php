<?php $this->load->view("static/header", ["title" => "Cliente"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "clientes"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Cliente / Atualizar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAtualizarCliente">
                                        <input type="hidden" id="id" name="id" value="<?= $cliente->id ?>">
                                        <div class="row">
                                            <div class="col-md-8 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" placeholder="Digite o nome do cliente" value="<?= $cliente->nome ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cpf">CPF</label>
                                                    <input type="text" id="cpf" name="cpf" class="form-control border-input" placeholder="000.000.000-00" value="<?= $cliente->cpf ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="celular">Celular</label>
                                                    <input type="text" id="celular" name="celular" class="form-control border-input" placeholder="(00) 00000-0000" value="<?= $cliente->celular ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="whatsapp">Whatsapp</label>
                                                    <input type="text" id="whatsapp" name="whatsapp" class="form-control border-input" placeholder="(00) 00000-0000" <?= !empty($cliente->whatsapp) ? $cliente->whatsapp : "" ?>>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="email">E-mail</label>
                                                    <input type="text" id="email" name="email" class="form-control border-input" placeholder="email@email.com" <?= !empty($cliente->whatsapp) ? $cliente->email : "" ?>>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="estado">Estado</label>
                                                    <select id="estado" name="estado" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($estados as $estado) : ?>
                                                            <?php if ($estado->id === $cliente->idEstado): ?>
                                                                <option selected="selected" value="<?= $estado->id ?>"><?= $estado->estado ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $estado->id ?>"><?= $estado->estado ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cidade">Cidade</label>
                                                    <select id="cidade" name="cidade" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($cidades as $cidade) : ?>
                                                            <?php if ($cidade->id === $cliente->idCidade): ?>
                                                                <option selected="selected" value="<?= $cidade->id ?>"><?= $cidade->cidade ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $cidade->id ?>"><?= $cidade->cidade ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="observacoes">Observações</label>
                                                    <textarea rows="8" id="observacoes" name="observacoes" class="form-control border-input"><?= !empty($cliente->observacoes) ? $cliente->observacoes : "" ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-xs-12">
                                                <a href="<?= base_url("clientes") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <button type="button" class="btn btn-primary btn-fill btn-block" data-toggle="modal" data-target="#modalAdicionarCidade">Adicionar cidade</button>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarClienteBtnAtualizar">Atualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <?= date("Y") ?> | Sistema OCTP | v1.0.0
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <div class="modal fade" id="modalAdicionarCidade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Adicionar cidade</h4>
                </div>
                <div class="modal-body">
                    <form id="formAdicionarCidade">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAdicionarCidadeEstado">Estado</label>
                                    <select id="modalAdicionarCidadeEstado" name="modalAdicionarCidadeEstado" class="form-control border-input">
                                        <option value="">--</option>
                                        <?php foreach ($estados as $estado) : ?>
                                            <option value="<?= $estado->id ?>"><?= $estado->estado ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAdicionarCidadeCidade">Cidade</label>
                                    <input type="text" id="modalAdicionarCidadeCidade" name="modalAdicionarCidadeCidade" class="form-control border-input" placeholder="Digite o nome da cidade">
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top:20px">
                            <div class="col-md-12 col-xs-12">
                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarCidadeBtnAdicionar">Adicionar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/clientes/atualizar.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
