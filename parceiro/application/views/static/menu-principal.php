
<div class="sidebar" data-background-color="white" data-active-color="danger">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="<?= base_url("dashboard") ?>" class="simple-text">
                <img src="<?= base_url("assets/img/marca-reduzida.png") ?>" class="img-responsive center-block" alt="Marca O cartão para todos">
            </a>
        </div>
        <ul class="nav">
            <li class="<?= $menu == "dashboard" ? 'active' : "" ?>">
                <a href="<?= base_url("dashboard") ?>">
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a href="<?= base_url("sair") ?>">
                    <p>Sair</p>
                </a>
            </li>
        </ul>
    </div>
</div>