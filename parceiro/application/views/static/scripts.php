<script src="<?= base_url("assets/js/jquery-1.10.2.js") ?>"></script>
<script src="<?= base_url("assets/js/bootstrap.min.js") ?>"></script>
<script src="<?= base_url("assets/js/paper-dashboard.js") ?>"></script>
<script src="<?= base_url("assets/js/demo.js") ?>"></script>
<script src="<?= base_url("assets/js/jquery-validate/jquery.validate.min.js") ?>"></script>
<script src="<?= base_url("assets/js/jquery-validate/additional-methods.min.js") ?>"></script>
<script src="<?= base_url("assets/js/jquery.mask.min.js") ?>"></script>
<script src="<?= base_url("assets/js/jquery-toast/jquery.toast.min.js") ?>"></script>
<script src="<?= base_url("assets/js/app/utils/utils.js") ?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>