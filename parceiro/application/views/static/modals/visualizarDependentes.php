<div class="modal fade" id="modalVisualizarDependentes" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Visualizar dependentes</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12" id="modalVisualziarDependentesLoader">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th>Nome<br>CPF</th>
                                    <th>Data de nascimento<br>Data de vinculação</th>
                                    <th>Número do cartão</th>
                                </tr>
                            </thead>
                            <tbody id="tabelaDependentes">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>