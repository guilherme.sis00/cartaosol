<?php $this->load->view("static/header", ["title" => "Especialidades"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "especialidades"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Especialidades"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-<?= $this->session->flashdata("tipo") ?>" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <form id="formBuscarEspecialidade">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="termo" name="termo" class="form-control border-input" placeholder="Digite uma especialidade">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <select id="tipoEspecialidade" name="tipoEspecialidade" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($tiposEspecialdiades as $tipoEspecialidade) : ?>
                                                            <option value="<?= $tipoEspecialidade->id ?>"><?= $tipoEspecialidade->tipo ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarEspecialidadeBtnBuscar">Buscar</button>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("especialidade/adicionar") ?>" class="btn btn-success btn-fill btn-block">Adicionar</a>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Especialidade</th>
                                                        <th>Tipo</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaEspecialidades">
                                                    <?php foreach ($especialidades as $especialidade) : ?>
                                                        <tr>
                                                            <td><?= $especialidade->especialidade ?></td>
                                                            <td><?= $especialidade->tipo ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="<?= base_url("especialidade/{$especialidade->id}") ?>">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="<?= base_url("especialidade/deletar/{$especialidade->id}") ?>">Deletar</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" id="divPaginacao">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8">
                                            <?= $paginacao ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/especialidades/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
