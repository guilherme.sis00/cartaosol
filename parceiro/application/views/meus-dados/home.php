<?php $this->load->view("static/header", ["title" => "Meus dados"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "meus-dados"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Meus dados"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <div class="row">
                                        <form id="formMeusDados">
                                            <input type="hidden" id="id" name="id" value="<?= $usuario->id ?>">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" value="<?= $usuario->nome ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="usuario">Usuário</label>
                                                    <input type="text" id="usuario" name="usuario" class="form-control border-input" value="<?= $usuario->usuario ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="senha">Senha</label>
                                                    <input type="password" id="senha" name="senha" class="form-control border-input" placeholder="Digite uma nova senha">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12 col-md-offset-9">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formMeusDadosBtnAtualizar">Atualizar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <?= date("Y") ?> | Sistema OCTP | v1.0.0
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/meus-dados/controller.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
