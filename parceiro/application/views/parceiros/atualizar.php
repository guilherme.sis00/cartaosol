<?php $this->load->view("static/header", ["title" => "Parceiro"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "parceiros"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Parceiro / Atualizar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAtualizarParceiro">
                                        <input type="hidden" id="id" name="id" value="<?= $parceiro->id ?>">
                                        <div class="row">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" value="<?= $parceiro->nome ?>" placeholder="Digite o nome do parceiro">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="endereco">Endereço</label>
                                                    <input type="text" id="endereco" name="endereco" class="form-control border-input" value="<?= $parceiro->endereco ?>" placeholder="Digite o endereço">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-xs-12">
                                                <div>
                                                    <label>
                                                        <input type="checkbox" name="parceiro" id="parceiro" <?= $parceiro->parceiro == 1 ? "checked='true'" : "" ?> value="1">
                                                        Parceiro
                                                    </label>
                                                </div>
                                                <div>
                                                    <label>
                                                        <input type="checkbox" name="clinica" id="clinica" <?= $parceiro->clinica == 1 ? "checked='true'" : "" ?> value="1">
                                                        Clínica
                                                    </label>
                                                </div>
                                                <div>
                                                    <label>
                                                        <input type="checkbox" name="exame" id="exame" <?= $parceiro->exame == 1 ? "checked='true'" : "" ?> value="1">
                                                        Exame
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12 <?= $parceiro->exame == 1 ? "" : "hidden" ?>" id="divEspecialidades">
                                                <?php foreach ($especialidades as $especialidade) : ?>
                                                    <?php $checked = FALSE; ?>
                                                    <?php foreach ($especialidadesParceiro as $especialidadeParceiro) : ?>
                                                        <?php if ($especialidade->id == $especialidadeParceiro->idEspecialidade): ?>
                                                            <?php
                                                            $checked = TRUE;
                                                            break;
                                                            ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                    <?php if ($checked): ?>
                                                        <label>
                                                            <input type="checkbox" checked="true" class="especialidade" name="especialidade[]" id="especialidade" value="<?= $especialidade->id ?>">
                                                            <?= $especialidade->especialidade ?>
                                                        </label>
                                                    <?php else: ?>
                                                        <label>
                                                            <input type="checkbox" class="especialidade" name="especialidade[]" id="especialidade" value="<?= $especialidade->id ?>">
                                                            <?= $especialidade->especialidade ?>
                                                        </label>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="estado">Estado</label>
                                                    <select id="estado" name="estado" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($estados as $estado) : ?>
                                                            <?php if ($estado->id == $parceiro->idEstado): ?>
                                                                <option selected="selected" value="<?= $estado->id ?>"><?= $estado->estado ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $estado->id ?>"><?= $estado->estado ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cidade">Cidade</label>
                                                    <select id="cidade" name="cidade" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($cidades as $cidade) : ?>
                                                            <?php if ($cidade->id == $parceiro->idCidade): ?>
                                                                <option selected="selected" value="<?= $cidade->id ?>"><?= $cidade->cidade ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $cidade->id ?>"><?= $cidade->cidade ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="telefone">Telefone</label>
                                                    <input type="text" id="telefone" name="telefone" class="form-control border-input" value="<?= $parceiro->telefone ?>" placeholder="(00) 00000-0000">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="whatsapp">Whatsapp</label>
                                                    <input type="text" id="whatsapp" name="whatsapp" class="form-control border-input" value="<?= $parceiro->whatsapp ?>" placeholder="(00) 00000-0000">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="email">E-mail</label>
                                                    <input type="text" id="email" name="email" class="form-control border-input" value="<?= $parceiro->email ?>" placeholder="email@email.com">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="descricao">Descrição</label>
                                                    <textarea class="form-control border-input" rows="5" id="descricao" name="descricao"><?= $parceiro->descricao ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-xs-12">
                                                <a href="<?= base_url("parceiros") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <button type="button" class="btn btn-primary btn-fill btn-block" data-toggle="modal" data-target="#modalAdicionarCidade">Adicionar cidade</button>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAtualizarParceiroBtnAtualizar">Atualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <div class="modal fade" id="modalAdicionarCidade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Adicionar cidade</h4>
                </div>
                <div class="modal-body">
                    <form id="formAdicionarCidade">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAdicionarCidadeEstado">Estado</label>
                                    <select id="modalAdicionarCidadeEstado" name="modalAdicionarCidadeEstado" class="form-control border-input">
                                        <option value="">--</option>
                                        <?php foreach ($estados as $estado) : ?>
                                            <option value="<?= $estado->id ?>"><?= $estado->estado ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAdicionarCidadeCidade">Cidade</label>
                                    <input type="text" id="modalAdicionarCidadeCidade" name="modalAdicionarCidadeCidade" class="form-control border-input" placeholder="Digite o nome da cidade">
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top:20px">
                            <div class="col-md-12 col-xs-12">
                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarCidadeBtnAdicionar">Adicionar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/parceiros/atualizar.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
