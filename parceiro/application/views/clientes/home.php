<?php $this->load->view("static/header", ["title" => "Clientes"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "administrativo"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Administrativo / Clientes"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-warning" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <form id="formBuscarCliente">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="termo" name="termo" class="form-control border-input" placeholder="Digite um termo de busca">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <select id="tipoBusca" name="tipoBusca" class="form-control border-input">
                                                        <option value="nome">Nome</option>
                                                        <option value="cpf">CPF</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarClienteBtnBuscar">Buscar</button>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("cliente/adicionar") ?>" class="btn btn-success btn-fill btn-block">Adicionar</a>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nome</th>
                                                        <th>CPF</th>
                                                        <th>Celular</th>
                                                        <th>E-mail</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaClientes">
                                                    <?php foreach ($clientes as $cliente) : ?>
                                                        <tr id="<?= $cliente->id ?>">
                                                            <td><?= $cliente->nome ?></td>
                                                            <td><?= $cliente->cpf ?></td>
                                                            <td><?= $cliente->celular ?></td>
                                                            <td><?= $cliente->email ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#" data-id="<?= $cliente->id ?>" data-toggle="modal" data-target="#modalVisualizarCliente">Visualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="<?= base_url("cliente/{$cliente->id}") ?>">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" class="btn-deletar" data-id="<?= $cliente->id ?>">Deletar</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" id="divPaginacao">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8">
                                            <?= $paginacao ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <div class="modal fade" id="modalVisualizarCliente" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Visualizar cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteNome">Nome</label>
                                <input type="text" disabled id="modalVisualizarClienteNome" name="modalVisualizarClienteNome" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteCpf">CPF</label>
                                <input type="text" disabled id="modalVisualizarClienteCpf" name="modalVisualizarClienteCpf" class="form-control border-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteCelular">Celular</label>
                                <input type="text" disabled id="modalVisualizarClienteCelular" name="modalVisualizarClienteCelular" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteWhatsapp">Whatsapp</label>
                                <input type="text" disabled id="modalVisualizarClienteWhatsapp" name="modalVisualizarClienteWhatsapp" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteEmail">E-mail</label>
                                <input type="text" disabled id="modalVisualizarClienteEmail" name="modalVisualizarClienteEmail" class="form-control border-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteDataNascimento">Data de nascimento</label>
                                <input type="text" disabled id="modalVisualizarClienteDataNascimento" name="modalVisualizarClienteDataNascimento" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteNumeroCartao">Número cartão</label>
                                <input type="text" disabled id="modalVisualizarClienteNumeroCartao" name="modalVisualizarClienteNumeroCartao" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteEstado">Estado</label>
                                <input type="text" disabled id="modalVisualizarClienteEstado" name="modalVisualizarClienteEstado" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteCidade">Cidade</label>
                                <input type="text" disabled id="modalVisualizarClienteCidade" name="modalVisualizarClienteCidade" class="form-control border-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarClienteObservacoes">Observações</label>
                                <textarea rows="5" disabled id="modalVisualizarClienteObservacoes" name="modalVisualizarClienteObservacoes" class="form-control border-input"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-primary btn-fill" id="modalVisualizarClienteBtnAtualizar">Atualizar</a>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/clientes/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
