<?php $this->load->view("static/header", ["title" => "Usuarios"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "usuarios"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Usuarios"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-warning" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <form id="formBuscarUsuario">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="termo" name="termo" class="form-control border-input" placeholder="Digite um termo de busca">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <select id="tipoBusca" name="tipoBusca" class="form-control border-input">
                                                        <option value="nome">Nome</option>
                                                        <option value="usuario">Usuário</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarUsuarioBtnBuscar">Buscar</button>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("usuario/adicionar") ?>" class="btn btn-success btn-fill btn-block">Adicionar</a>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nome</th>
                                                        <th>Usuário</th>
                                                        <th>Status</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaUsuarios">
                                                    <?php foreach ($usuarios as $usuario) : ?>
                                                        <tr>
                                                            <td><?= $usuario->nome ?></td>
                                                            <td><?= $usuario->usuario ?></td>
                                                            <td><?= ucfirst($usuario->status) ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="<?= base_url("usuario/{$usuario->id}") ?>">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" data-id="<?= $usuario->id ?>" data-usuario="<?= $usuario->usuario ?>" data-toggle="modal" data-target="#modalAlterarSenha">Alterar senha</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" id="divPaginacao">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8">
                                            <?= $paginacao ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <div class="modal fade" id="modalAlterarSenha" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Alterar senha</h4>
                </div>
                <form id="formAlterarSenha">
                    <input type="hidden" id="modalAlterarSenhaIdUsuario" name="modalAlterarSenhaIdUsuario">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAlterarSenhaUsuario">Usuário</label>
                                    <input type="text" disabled id="modalAlterarSenhaUsuario" name="modalAlterarSenhaUsuario" class="form-control border-input">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAlterarSenhaSenha">Senha</label>
                                    <input type="password" id="modalAlterarSenhaSenha" name="modalAlterarSenhaSenha" class="form-control border-input" placeholder="Digite a nova senha">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAlterarSenhaRepetirSenha">Repetir senha</label>
                                    <input type="password" id="modalAlterarSenhaRepetirSenha" name="modalAlterarSenhaRepetirSenha" class="form-control border-input" placeholder="Repita a nova senha">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-fill" id="modalAlterarSenhaBtnAtualizar">Alterar senha</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/usuarios/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
