<?php $this->load->view("static/header", ["title" => "Dashboard"]) ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "dashboard"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Dashboard"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <div class="row" style="margin-bottom:10px">
                                        <div class="col-md-4 col-xs-12">
                                            <ul style="list-style-type: none;padding-left: 0">
                                                <li style="margin-bottom:10px"><i class="fa fa-circle" style="color:green"></i> Em dias</li>
                                                <li style="margin-bottom:10px"><i class="fa fa-circle" style="color:yellow"></i> 1 débito existente</li>
                                                <li style="margin-bottom:10px"><i class="fa fa-circle" style="color:red"></i> 2 ou mais débitos existentes</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <form id="formConsultar">
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" placeholder="Nome do cliente">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cpf">CPF</label>
                                                    <input type="text" id="cpf" name="cpf" class="form-control border-input" placeholder="000.000.000-00">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="caetao">Número do cartão</label>
                                                    <input type="text" id="cartao" name="cartao" class="form-control border-input" placeholder="0000.0000.0000.0000">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formConsultarBtnConsultar">Consultar</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <h3>Clientes</h3>
                                        </div>
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Farol</th>
                                                        <th>Nome</th>
                                                        <th>CPF</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaClientes">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <h3>Dependentes</h3>
                                        </div>
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Farol</th>
                                                        <th>Nome</th>
                                                        <th>CPF</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaDependentes">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <?php $this->load->view("static/modals/visualizarDependentes") ?>
</body>
<?php $this->load->view("static/scripts") ?>
<script src="<?= base_url("assets/js/app/dashboard/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
