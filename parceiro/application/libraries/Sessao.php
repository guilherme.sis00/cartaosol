<?php

Class Sessao {

    public function isLogado($sessao = "") {
        if ($sessao->userdata("usuario") != NULL) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isAutorizado($sessao = "", $credencial = "") {
        if ($sessao->userdata("usuario") != NULL && $sessao->userdata($credencial) == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
