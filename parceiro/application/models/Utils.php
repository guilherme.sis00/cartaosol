<?php

class Utils extends CI_Model {

    public function countAll($tabela) {
        return $this->db->count_all($tabela);
    }

    public function getEspecialidades() {
        return $this->db
                        ->order_by("especialidade", "ASC")
                        ->get("ocpt_especialidades")->result();
    }
    
    public function getTiposEspecialidades() {
        return $this->db
                        ->order_by("tipo", "ASC")
                        ->get("ocpt_tipos_especialidades")->result();
    }
    
    public function getEstados() {
        return $this->db
                        ->order_by("estado", "ASC")
                        ->get("ocpt_estados")->result();
    }

    public function getCidadesByIdEstado($idEstado, $returnArray = TRUE) {
        $response = $this->db
                        ->where("idEstado", $idEstado)
                        ->order_by("cidade", "ASC")
                        ->get("ocpt_cidades")->result();

        if (!empty($response)) {
            if ($returnArray) {
                return ["resultado" => TRUE, "cidades" => $response];
            } else {
                return $response;
            }
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhuma cidade encontrada"];
        }
    }

    public function adicionarCidade($dados) {
        $this->db->trans_start();
        $this->db
                ->set([
                    "cidade" => $dados["cidade"],
                    "idEstado" => $dados["estado"],
                ])
                ->insert("ocpt_cidades");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Cidade adicionada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar cidade"];
        }
    }

}
