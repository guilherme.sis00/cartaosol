<?php

class ContaPagar extends CI_Model {

    private $id;
    private $descricao;
    private $idFornecedor;
    private $valor;
    private $idCategoria;
    private $data;
    private $status;

    public function preencherDados($dados) {
        $this->id = !empty($dados["id"]) ? $dados['id'] : NULL;
        $this->descricao = !empty($dados["descricao"]) ? $dados['descricao'] : NULL;
        $this->idFornecedor = !empty($dados["fornecedor"]) ? $dados["fornecedor"] : NULL;
        $this->valor = !empty($dados["valor"]) ? $dados["valor"] : NULL;
        $this->idCategoria = !empty($dados["categoria"]) ? $dados['categoria'] : NULL;
        $this->data = !empty($dados["data"]) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados['data']))) : NULL;
        $this->status = !empty($dados["status"]) ? $dados['status'] : NULL;
    }

    public function getByPeriodo($dataInicial, $dataFinal, $returnArray = TRUE) {
        $response = $this->db
                        ->select("contas_pagar.*, fornecedores.nome, categorias_financeiras.categoria")
                        ->join("categorias_financeiras", "categorias_financeiras.id=contas_pagar.idCategoria")
                        ->join("fornecedores", "fornecedores.id=contas_pagar.idFornecedor")
                        ->where('contas_pagar.data BETWEEN "' . $dataInicial . '" AND "' . $dataFinal . '"')
                        ->order_by("contas_pagar.data", "ASC")
                        ->get("contas_pagar")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "contasPagar" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma conta a pagar encontrada nesse período"];
            }
        } else {
            return $response;
        }
    }

    public function getById($id, $returnArray = TRUE) {
        $response = $this->db
                        ->select("contas_pagar.*, categorias_financeiras.categoria")
                        ->join("categorias_financeiras", "categorias_financeiras.id=contas_pagar.idCategoria")
                        ->where("contas_pagar.id", $id)
                        ->get("contas_pagar")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "contaPagar" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma conta a pagar encontrada"];
            }
        } else {
            return $response;
        }
    }

    public function getTotalByStatus($status, $dataInicial, $dataFinal) {
        return $this->db
                        ->select("COALESCE(SUM(valor),0) AS total")
                        ->where("status", $status)
                        ->where('(contas_pagar.data BETWEEN "' . $dataInicial . '" AND "' . $dataFinal . '")')
                        ->get("contas_pagar")->row();
    }

    public function buscar($dados) {
        $this->db
                ->select("contas_pagar.*, categorias_financeiras.categoria")
                ->join("categorias_financeiras", "categorias_financeiras.id=contas_pagar.idCategoria")
                ->where('contas_pagar.data BETWEEN "' . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataInicial"]))) . '" AND "' . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataFinal"]))) . '"')
                ->order_by("contas_pagar.data", "ASC");

        if (!empty($dados["termo"])) {
            $this->db->like("contas_pagar.descricao", $dados["termo"]);
        }
        if ($dados["categoria"] != "todas") {
            $this->db->where("contas_pagar.idCategoria", $dados["categoria"]);
        }

        $response = $this->db->get("contas_pagar")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "contasPagar" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhuma conta a pagar encontrada"];
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("contas_pagar", $this->toArray());
        $this->id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Conta a pagar adicionada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar conta a pagar"];
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->where("id", $this->id)
                ->update("contas_pagar", $this->toArray());
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Conta a pagar atualizada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao atualizar conta a pagar"];
        }
    }

    public function atualizarStatus() {
        $this->db->trans_start();
        $this->db
                ->where("id", $this->id)
                ->update("contas_pagar", ["status" => $this->status]);
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Status atualizado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao atualizar status"];
        }
    }

    public function deletar($id) {
        $this->db->delete("contas_pagar", ["id" => $id]);

        if ($this->db->affected_rows() > 0) {
            return ["resultado" => TRUE, "msg" => "Conta a pagar deletada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao deletar conta a pagar"];
        }
    }

    //NÃO FUNCIONAL
    public function alterarStatus() {
        $this->db->trans_start();
        $this->db
                ->set("status", $this->status)
                ->where("id", $this->id)
                ->update("admin_entradas");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            $responseEntrada = $this->getById($this->id);
            $ultimoDiaMes = cal_days_in_month(CAL_GREGORIAN, date("m", strtotime($this->data)), date("Y", strtotime($this->data)));
            $dataInicial = date("Y-m") . "-01";
            $dataFinal = date("Y-m") . "-" . $ultimoDiaMes;

            return [
                "resultado" => TRUE,
                "msg" => "Status alterado",
                "entrada" => $responseEntrada["entrada"],
                "recebido" => $this->getTotalByStatus(1, $dataInicial, $dataFinal),
                "pendente" => $this->getTotalByStatus(-1, $dataInicial, $dataFinal),
                "mesAtual" => getMesExtenso(date("m"))
            ];
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao alterar status");
        }
    }

    public function toArray() {
        return [
            'descricao' => $this->descricao,
            'idFornecedor' => $this->idFornecedor,
            'valor' => $this->valor,
            'idCategoria' => $this->idCategoria,
            'data' => $this->data,
            'status' => $this->status
        ];
    }

}
