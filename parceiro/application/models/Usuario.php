<?php

class Usuario extends CI_Model {

    private $id;
    private $nome;
    private $usuario;
    private $senha;
    private $idParceiro;
    private $status;

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->nome = !empty($dados['nome']) ? $dados['nome'] : NULL;
        $this->usuario = !empty($dados['usuario']) ? $dados['usuario'] : NULL;
        $this->senha = !empty($dados['senha']) ? $dados['senha'] : NULL;
        $this->idParceiro = !empty($dados['idParceiro']) ? $dados['idParceiro'] : NULL;
        $this->status = !empty($dados['status']) ? $dados['status'] : "ativo";
    }

    public function get($atributo) {
        switch ($atributo) {
            case "id":
                return $this->id;
        }
    }

    public function autenticar() {
        $response = $this->db
                        ->select("usuarios_parceiros.*, parceiros.id")
                        ->join("parceiros", "parceiros.id=usuarios_parceiros.idParceiro")
                        ->where([
                            "usuarios_parceiros.usuario" => $this->usuario,
                            "usuarios_parceiros.senha" => $this->senha,
                        ])
                        ->get("usuarios_parceiros")->row();

        if (!empty($response)) {
            return $this->isAtivado($response);
        } else {
            return array("resultado" => FALSE, "msg" => "Usuário ou senha inválidos");
        }
    }

    private function isAtivado($dados) {
        switch ($dados->status) {
            case "ativo":
                return array("resultado" => TRUE, "dados" => $dados);
            case "bloqueado":
                return array("resultado" => FALSE, "msg" => "Sua conta está bloqueada");
            default:
                return array("resultado" => FALSE, "msg" => "Não foi possível verificar o status da sua conta, entre em contato com a equipe de suporte");
        }
    }

    //NÃO FUNCIONAL
    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE) {
        if ($paginacao) {
            return $this->db
                            ->order_by("nome", "ASC")
                            ->limit($limit, $offset)
                            ->where("tipo <>", "vendedor")
                            ->get("usuarios")->result();
        } else {
            return $this->db
                            ->order_by("nome", "ASC")
                            ->where("tipo <>", "vendedor")
                            ->get("usuarios")->result();
        }
    }

    public function getById($id) {
        $response = $this->db
                        ->where("id", $id)
                        ->get("ocpt_usuarios")->row();

        if (!empty($response)) {
            return ["resultado" => TRUE, "usuario" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Usuário não encontrado"];
        }
    }

    public function adicionar() {
        if ($this->validarUsuarioExistente(NULL, $this->usuario)) {
            $this->db->trans_start();
            $this->db->insert("ocpt_usuarios", $this->toArray());
            $this->id = $this->db->insert_id();
            $this->db->trans_complete();

            if ($this->db->trans_status()) {
                return ["resultado" => TRUE, "msg" => "Usuário cadastrado, continuar adicionando ?"];
            } else {
                return ["resultado" => FALSE, "msg" => "Falha ao adicionar usuário"];
            }
        } else {
            return ["resultado" => FALSE, "msg" => "Este nome de usuário já está sendo utilizado"];
        }
    }

    public function atualizar() {
        if ($this->validarUsuarioExistente($this->id, $this->usuario)) {
            $this->db->trans_start();
            $this->db->set([
                "nome" => $this->nome,
                "usuario" => $this->usuario
            ]);

            if (!empty($this->senha)) {
                $this->db->set("senha", $this->senha);
            }

            $this->db
                    ->where("id", $this->id)
                    ->update("ocpt_usuarios");

            $this->db->trans_complete();
            if ($this->db->trans_status()) {
                return ["resultado" => TRUE, "msg" => "Dados atualizados"];
            } else {
                return ["resultado" => FALSE, "msg" => "Falha ao atualizar dados"];
            }
        } else {
            return ["resultado" => FALSE, "msg" => "Já existe um cadastro com o mesmo usuário informado"];
        }
    }

    public function deletar($id) {
        $this->db->delete("ocpt_usuarios", ["id" => $id]);

        if ($this->db->affected_rows() > 0) {
            return ["resultado" => TRUE, "msg" => "Usuário deletado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao deletar usuário"];
        }
    }

    public function atualizarUsuario() {
        if ($this->validarUsuarioExistente($this->id, $this->usuario)) {
            $this->db->trans_start();
            $this->db
                    ->set($this->toArray())
                    ->where("id", $this->id)
                    ->update("ocpt_usuarios");

            $this->db->trans_complete();
            if ($this->db->trans_status()) {
                return ["resultado" => TRUE, "msg" => "Usuário atualizado"];
            } else {
                return ["resultado" => FALSE, "msg" => "Falha ao atualizar usuário"];
            }
        } else {
            return ["resultado" => FALSE, "msg" => "Este nome de usuário já está sendo utilizado"];
        }
    }

    public function atualizarStatus($dados) {
        $this->db->trans_start();
        $this->db
                ->set("status", $dados["status"])
                ->where("id", $dados["id"])
                ->update("ocpt_usuarios");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Status atualizado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao atualizar status"];
        }
    }

    public function alterarSenha($dados) {
        $this->db->trans_start();
        $this->db
                ->set("senha", $dados["senha"])
                ->where("id", $dados["id"])
                ->update("ocpt_usuarios");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Senha alterada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao alterar senha"];
        }
    }

    public function recuperarSenha($dados) {
        $this->db->trans_start();
        $this->db
                ->set("senha", $dados["novaSenha"])
                ->where("usuario", $dados["usuario"])
                ->update("ocpt_usuarios");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE];
        } else {
            return ["resultado" => FALSE];
        }
    }

    private function toArray() {
        return array(
            "nome" => $this->nome,
            "usuario" => $this->usuario,
            "senha" => $this->senha,
            "tipo" => $this->tipo,
            "status" => $this->status
        );
    }

    private function validarUsuarioExistente($id, $usuario) {
        if ($id) {
            $response = $this->db
                            ->where([
                                "usuario" => $usuario,
                                "id !=" => $id
                            ])
                            ->get("ocpt_usuarios")->row();
        } else {
            $response = $this->db
                            ->where("usuario", $usuario)
                            ->get("ocpt_usuarios")->row();
        }
        return empty($response);
    }

}
