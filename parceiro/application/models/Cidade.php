<?php

class Cidade extends CI_Model {

    private $id;
    private $idCidade;
    private $idEstado;
//    private $tipo;

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->idCidade = !empty($dados['cidade']) ? $dados['cidade'] : NULL;
        $this->idEstado = !empty($dados['estado']) ? $dados['estado'] : NULL;
//        $this->tipo = !empty($dados['tipo']) ? $dados['tipo'] : NULL;
    }

    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE) {
        if ($paginacao) {
            return $this->db
                            ->select("ocpt_convenios_descontos.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->limit($limit, $offset)
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_convenios_descontos.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_convenios_descontos.idEstado")
                            ->order_by("ocpt_estados.estado", "ASC")
                            ->order_by("ocpt_cidades.cidade", "ASC")
                            ->get("ocpt_convenios_descontos")->result();
        } else {
            return $this->db
                            ->select("ocpt_convenios_descontos.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->order_by("ocpt_estados.estado", "ASC")
                            ->order_by("ocpt_cidades.cidade", "ASC")
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_convenios_descontos.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_convenios_descontos.idEstado")
                            ->get("ocpt_convenios_descontos")->result();
        }
    }

    public function getById($id, $returnArray = TRUE) {
        if ($returnArray) {
            $response = $this->db
                            ->select("ocpt_convenios_descontos.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->where("ocpt_convenios_descontos.id", $id)
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_convenios_descontos.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_convenios_descontos.idEstado")
                            ->get("ocpt_convenios_descontos")->row();

            if (!empty($response)) {
                return ["resultado" => TRUE, "cidade" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum cliente foi encontrado"];
            }
        } else {
            return $this->db
                            ->select("ocpt_convenios_descontos.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->where("ocpt_convenios_descontos.id", $id)
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_convenios_descontos.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_convenios_descontos.idEstado")
                            ->get("ocpt_convenios_descontos")->row();
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("ocpt_convenios_descontos", $this->toArray());
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Cidade adicionada, continuar adicionando ?"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar cidade"];
        }
    }

    public function buscar($dados) {
        $response = $this->db
                        ->select("ocpt_convenios_descontos.*,"
                                . "ocpt_cidades.cidade,"
                                . "ocpt_estados.estado")
                        ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_convenios_descontos.idCidade")
                        ->join("ocpt_estados", "ocpt_estados.id=ocpt_convenios_descontos.idEstado")
                        ->like("ocpt_" . $dados["tipoBusca"], $dados["termo"])
                        ->order_by("ocpt_estados.estado", "ASC")
                        ->order_by("ocpt_cidades.cidade", "ASC")
                        ->get("ocpt_convenios_descontos")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "cidades" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhuma cidade encontrada"];
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->set($this->toArray())
                ->where("id", $this->id)
                ->update("ocpt_convenios_descontos");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Cidade atualizada");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao atualizar cidade");
        }
    }

    public function deletar($id) {
        $this->db
                ->where("id", $id)
                ->delete("ocpt_convenios_descontos");

        if ($this->db->affected_rows()) {
            return array("resultado" => TRUE, "msg" => "Cidade deletada");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao deletar cidade");
        }
    }

    private function toArray() {
        return [
            "idCidade" => $this->idCidade,
            "idEstado" => $this->idEstado
//            "tipo" => $this->tipo
        ];
    }

}
