<?php

class Cliente extends CI_Model {

    public function consultar($dados) {
        $this->db
                ->select("clientes.id,"
                        . "clientes.nome,"
                        . "clientes.cpf")
                ->join("cartoes_clientes", "clientes.id=cartoes_clientes.idCliente")
                ->order_by("nome", "ASC")
                ->where("status", "ativo");

        if (!empty($dados["nome"])) {
            $this->db->like("clientes.nome", $dados["nome"]);
        }
        if (!empty($dados["cpf"])) {
            $this->db->where("clientes.cpf", $dados["cpf"]);
        }

        if (!empty($dados["cartao"])) {
            $this->db->where("cartoes_clientes.numero", $dados["cartao"]);
        }

        $responseClientes = $this->db->get("clientes")->result();

        $responseFarois = $this->getFarois($responseClientes);

        $this->db
                ->select("dependentes.nome,"
                        . "dependentes.cpf,"
                        . "clientes_dependentes.idCliente")
                ->join("cartoes_dependentes", "dependentes.id=ocpt_cartoes_dependentes.idDependente")
                ->join("ocpt_clientes_dependentes", "dependentes.id=ocpt_clientes_dependentes.idDependente")
                ->order_by("nome", "ASC");

        if (!empty($dados["nome"])) {
            $this->db->like("dependentes.nome", $dados["nome"]);
        }
        if (!empty($dados["cpf"])) {
            $this->db->where("dependentes.cpf", $dados["cpf"]);
        }

        if (!empty($dados["cartao"])) {
            $this->db->where("ocpt_cartoes_dependentes.numero", $dados["cartao"]);
        }

        $responseDependentes = $this->db->get("dependentes")->result();

        $responseFaroisDependentes = $this->getFarois($responseDependentes, TRUE);

        if (!empty($responseClientes) || !empty($responseDependentes)) {
            return ["resultado" => TRUE, "clientes" => $responseClientes, "dependentes" => $responseDependentes, "farois" => $responseFarois, "faroisDependentes" => $responseFaroisDependentes];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum cliente/dependente encontrado"];
        }
    }

    public function getFarois($clientes, $isDependentes = FALSE) {
        if (!empty($clientes)) {
            $farois = [];
            foreach ($clientes as $cliente) {
                if ($isDependentes) {
                    $idCliente = is_object($cliente) ? $cliente->idCliente : $cliente["idCliente"];
                } else {
                    $idCliente = is_object($cliente) ? $cliente->id : $cliente["id"];
                }

                $responseFarol = $this->db
                                ->select("COUNT(ocpt_boletos.id) as total")
                                ->join("parcelas", "boletos.idParcela=parcelas.id")
                                ->join("contratos", "parcelas.idContrato=contratos.id")
                                ->join("boletos_contas_receber", "boletos_contas_receber.idBoleto=boletos.id")
                                ->join("contas_receber", "boletos_contas_receber.idContaReceber=contas_receber.id")
                                ->where('parcelas.vencimento <= "' . date("Y-m-d") . '"')
                                ->where([
                                    "contratos.idCliente" => $idCliente,
                                    "contas_receber.status" => "pendente"
                                ])
                                ->get("boletos")->row();

                $farois[$idCliente] = $responseFarol->total == 0 ? "green" : ($responseFarol->total <= 2 ? "yellow" : "red");
            }

            return $farois;
        } else {
            return FALSE;
        }
    }

    public function getDependentes($idCliente) {
        $response = $this->db
                        ->select("dependentes.*, cartoes_dependentes.numero")
                        ->join("clientes_dependentes", "clientes_dependentes.idDependente=dependentes.id")
                        ->join("cartoes_dependentes", "cartoes_dependentes.idDependente=dependentes.id")
                        ->where("clientes_dependentes.idCliente", $idCliente)
                        ->order_by("dependentes.nome", "ASC")
                        ->get("dependentes")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "dependentes" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum dependente foi encontrado"];
        }
    }

}
