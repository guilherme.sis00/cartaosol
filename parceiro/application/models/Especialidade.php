<?php

class Especialidade extends CI_Model {

    private $id;
    private $especialidade;
    private $idTipoEspecialidade;

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->especialidade = !empty($dados['especialidade']) ? $dados['especialidade'] : NULL;
        $this->idTipoEspecialidade = !empty($dados['tipo']) ? $dados['tipo'] : NULL;
    }

    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE) {
        if ($paginacao) {
            return $this->db
                            ->select("ocpt_especialidades.*,"
                                    . "ocpt_tipos_especialidades.tipo")
                            ->limit($limit, $offset)
                            ->join("ocpt_tipos_especialidades", "ocpt_tipos_especialidades.id=ocpt_especialidades.idTipoEspecialidade")
                            ->order_by("ocpt_especialidades.especialidade", "ASC")
                            ->get("ocpt_especialidades")->result();
        } else {
            return $this->db
                            ->select("ocpt_especialidades.*,"
                                    . "ocpt_tipos_especialidades.tipo")
                            ->join("ocpt_tipos_especialidades", "ocpt_tipos_especialidades.id=ocpt_especialidades.idTipoEspecialidade")
                            ->order_by("ocpt_especialidades.especialidade", "ASC")
                            ->get("ocpt_especialidades")->result();
        }
    }

    public function getById($id, $returnArray = TRUE) {
        if ($returnArray) {
            $response = $this->db
                            ->select("ocpt_especialidades.*,"
                                    . "ocpt_tipos_especialidades.tipo")
                            ->join("ocpt_tipos_especialidades", "ocpt_tipos_especialidades.id=ocpt_especialidades.idTipoEspecialidade")
                            ->where("ocpt_especialidades.id", $id)
                            ->get("ocpt_especialidades")->row();

            if (!empty($response)) {
                return ["resultado" => TRUE, "especialidade" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma especialidade foi encontrada"];
            }
        } else {
            return $this->db
                            ->select("ocpt_especialidades.*,"
                                    . "ocpt_tipos_especialidades.tipo")
                            ->join("ocpt_tipos_especialidades", "ocpt_tipos_especialidades.id=ocpt_especialidades.idTipoEspecialidade")
                            ->where("ocpt_especialidades.id", $id)
                            ->get("ocpt_especialidades")->row();
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("ocpt_especialidades", $this->toArray());
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Especialidade adicionada, continuar adicionando ?"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar especialidade"];
        }
    }

    public function buscar($dados) {
        $response = $this->db
                        ->select("ocpt_especialidades.*,"
                                . "ocpt_tipos_especialidades.tipo")
                        ->join("ocpt_tipos_especialidades", "ocpt_tipos_especialidades.id=ocpt_especialidades.idTipoEspecialidade")
                        ->order_by("ocpt_especialidades.especialidade", "ASC")
                        ->where("ocpt_especialidades.idTipoEspecialidade", $dados['tipoEspecialidade'])
                        ->like("ocpt_especialidades.especialidade", $dados["termo"])
                        ->get("ocpt_especialidades")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "especialidades" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhuma especialidade encontrada"];
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->set($this->toArray())
                ->where("id", $this->id)
                ->update("ocpt_especialidades");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Especialidade atualizada");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao atualizar especialidade");
        }
    }

    public function deletar($id) {
        $this->db
                ->where("id", $id)
                ->delete("ocpt_especialidades");

        if ($this->db->affected_rows()) {
            return array("resultado" => TRUE, "msg" => "Especialidade deletada");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao deletar especialidade");
        }
    }

    private function toArray() {
        return [
            "especialidade" => $this->especialidade,
            "idTipoEspecialidade" => $this->idTipoEspecialidade
        ];
    }

}
