<?php

class Contrato extends CI_Model {

    private $id;
    private $codigoContrato;
    private $ano;
    private $idCliente;
    private $idVendedor;
    private $duracao;
    private $situacao;
    private $dataAssinatura;

    public function __construct() {
        parent::__construct();
        $this->load->model([
            "financeiro/boleto",
            "financeiro/parcela",
            "financeiro/contaReceber",
            "cliente"
        ]);
    }

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->ano = !empty($dados['ano']) ? $dados['ano'] : NULL;
        $this->codigoContrato = !empty($dados['codigoContrato']) ? $dados['codigoContrato'] : 0;
        $this->idCliente = !empty($dados['cliente']) ? $dados['cliente'] : NULL;
        $this->idVendedor = !empty($dados['vendedor']) ? $dados['vendedor'] : NULL;
        $this->duracao = !empty($dados['duracao']) ? $dados['duracao'] : NULL;
        $this->situacao = !empty($dados['situacao']) ? $dados['situacao'] : NULL;
        $this->dataAssinatura = !empty($dados['dataAssinatura']) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados['dataAssinatura']))) : NULL;
    }

    public function count() {
        $response = $this->db
                        ->select("COUNT(ocpt_contratos.idCliente) as total")
                        ->where("contratos.idVendedor", $this->session->vendedor)
                        ->get("contratos")->row();
        
        return $response->total;
    }
    
    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE) {
        $this->db
                ->select("contratos.id,"
                        . "contratos.codigoContrato,"
                        . "contratos.duracao,"
                        . "contratos.situacao,"
                        . "contratos.ano,"
                        . "clientes.nome")
                ->join("clientes", "clientes.id=contratos.idCliente")
                ->order_by("clientes.nome", "ASC");
        if ($paginacao) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->get("contratos")->result();
    }

    public function getById($id, $returnArray = TRUE) {
        $response = $this->db
                        ->select("contratos.*,"
//                                . "clientes.id as idCliente,"
                                . "clientes.nome")
                        ->where("contratos.id", $id)
                        ->join("clientes", "clientes.id=contratos.idCliente")
                        ->get("contratos")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "contrato" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum contrato foi encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function buscar($dados) {
        $this->db
                ->select("contratos.*,"
                        . "clientes.nome")
                ->join("clientes", "clientes.id=contratos.idCliente")
                ->like("clientes.nome", $dados["cliente"])
                ->order_by("clientes.nome", "ASC");

        if (!empty($dados["codigoContrato"])) {
            $this->db->where("contratos.codigoContrato", $dados["codigoContrato"]);
        }

        if (!empty($dados["cpf"])) {
            $this->db->where("clientes.cpf", $dados["cpf"]);
        }

        $response = $this->db->get("contratos")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "contratos" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum contrato encontrado"];
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("contratos", $this->toArray());
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Contrato adicionado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar contrato"];
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->set($this->toArray())
                ->where("id", $this->id)
                ->update("contratos");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Contrato atualizado");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao atualizar contrato");
        }
    }

    public function deletar($id) {
        $this->db->delete("contratos", ["id" => $id]);

        if ($this->db->affected_rows() > 0) {
            return ["resultado" => TRUE, "msg" => "Contrato deletado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao deletar contrato"];
        }
    }

    public function gerarParcelas($dados) {
        $parcelas = $this->configurarParcelas($dados);
        $this->db->trans_start();

        foreach ($parcelas as $parcela) {
            $this->parcela->preencherDados($parcela);
            $this->parcela->adicionar();

            $responseParcela = $this->parcela->toArray();

            $responseBoleto = $this->boleto->gerar([
                "idCliente" => $dados["idCliente"],
                "idParcela" => $this->parcela->get("id"),
                "valor" => $responseParcela['valor'],
                "parcela" => $responseParcela['parcela'],
                "totalParcelamento" => $responseParcela['totalParcelamento'],
                "vencimento" => $responseParcela['vencimento'],
                "instrucao1" => $dados["instrucao1"],
                "instrucao2" => $dados["instrucao2"],
                "instrucao3" => $dados["instrucao3"],
                "instrucao4" => $dados["instrucao4"],
            ]);

            $this->boleto->preencherDados([
                "parcela" => $this->parcela->get("id"),
                "dataEmissao" => date("d/m/Y"),
                "nossoNumero" => $responseBoleto->getNossoNumero(),
                "instrucao1" => $dados["instrucao1"],
                "instrucao2" => $dados["instrucao2"],
                "instrucao3" => $dados["instrucao3"],
                "instrucao4" => $dados["instrucao4"],
                "status" => 1
            ]);

            $this->boleto->adicionar();

            $contaReceber = $this->configurarContaReceber(array_merge($parcela, ["categoria" => $dados["categoria"], "nomeCliente" => $dados["nomeCliente"]]));
            $this->contaReceber->preencherDados($contaReceber);
            $this->contaReceber->adicionar();

            $this->db->insert("boletos_contas_receber", ["idBoleto" => $this->boleto->get("id"), "idContaReceber" => $this->contaReceber->get("id")]);
        }
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            $response = $this->parcela->getByIdContrato($dados["contrato"], FALSE);
            return ["resultado" => TRUE, "msg" => "Parcelas geradas", "parcelas" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao gerar parcelas"];
        }
    }

    public function toArray() {
        return [
            "ano" => $this->ano,
            "codigoContrato" => $this->codigoContrato,
            "idCliente" => $this->idCliente,
            "idVendedor" => $this->idVendedor,
            "duracao" => $this->duracao,
            "situacao" => $this->situacao,
            "dataAssinatura" => $this->dataAssinatura
        ];
    }

    private function configurarParcelas($dados) {
        $parcelas = [];

        for ($i = 1; $i <= $dados["parcelas"]; $i++) {
            if ($i < 2) {
                $vencimento = date("Y-m-d", strtotime(str_replace("/", "-", $dados["vencimento"])));
            } else {
                $vencimento = date("Y-m-d", strtotime("+" . (($i - 1) * 30) . " days", strtotime(str_replace("/", "-", $dados["vencimento"]))));
            }

            array_push($parcelas, [
                "contrato" => $dados["contrato"],
                "valor" => $dados["valor"],
                "vencimento" => $vencimento,
                "parcela" => $i,
                "totalParcelamento" => $dados["parcelas"]
            ]);
        }

        return $parcelas;
    }

    private function configurarContaReceber($dados) {
        return [
            "categoria" => $dados["categoria"],
            "descricao" => "Mensalidade {$dados["parcela"]}/{$dados["totalParcelamento"]} - Cliente {$dados["nomeCliente"]}",
            "valor" => $dados["valor"],
            "data" => $dados["vencimento"],
            "status" => "pendente"
        ];
    }

}
