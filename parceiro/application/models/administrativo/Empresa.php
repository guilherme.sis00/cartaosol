<?php

class Empresa extends CI_Model {

    private $id;
    private $nomeFantasia;
    private $razaoSocial;
    private $cnpj;
    private $carteira;
    private $conta;
    private $agencia;
    private $ios;
    private $codigoTransmissao;
    private $codigoPsk;
    private $codigoBanco;
    private $contaFidc;
    private $telefone;
    private $celular;
    private $email;
    private $instrucao1;
    private $instrucao2;
    private $instrucao3;
    private $instrucao4;
    private $instrucao5;
    private $cep;
    private $logradouro;
    private $numero;
    private $bairro;
    private $idEstado;
    private $idCidade;

    public function preencherDados($dados) {
        $this->id = !empty($dados["id"]) ? $dados["id"] : NULL;
        $this->nomeFantasia = !empty($dados["nomeFantasia"]) ? $dados["nomeFantasia"] : NULL;
        $this->razaoSocial = !empty($dados["razaoSocial"]) ? $dados["razaoSocial"] : NULL;
        $this->cnpj = !empty($dados["cnpj"]) ? $dados["cnpj"] : NULL;
        $this->carteira = !empty($dados["carteira"]) ? $dados["carteira"] : NULL;
        $this->conta = !empty($dados["conta"]) ? $dados["conta"] : NULL;
        $this->agencia = !empty($dados["agencia"]) ? $dados["agencia"] : NULL;
        $this->ios = !empty($dados["ios"]) || $dados["ios"] == 0 ? $dados["ios"] : NULL;
        $this->codigoTransmissao = !empty($dados["codigoTransmissao"]) ? $dados["codigoTransmissao"] : NULL;
        $this->codigoPsk = !empty($dados["codigoPsk"]) ? $dados["codigoPsk"] : NULL;
        $this->codigoBanco = !empty($dados["codigoBanco"]) ? $dados["codigoBanco"] : NULL;
        $this->contaFidc = !empty($dados["contaFidc"]) ? $dados["contaFidc"] : NULL;
        $this->telefone = !empty($dados["telefone"]) ? $dados["telefone"] : NULL;
        $this->celular = !empty($dados["celular"]) ? $dados["celular"] : NULL;
        $this->email = !empty($dados["email"]) ? $dados["email"] : NULL;
        $this->instrucao1 = !empty($dados["instrucao1"]) ? $dados["instrucao1"] : NULL;
        $this->instrucao2 = !empty($dados["instrucao2"]) ? $dados["instrucao2"] : NULL;
        $this->instrucao3 = !empty($dados["instrucao3"]) ? $dados["instrucao3"] : NULL;
        $this->instrucao4 = !empty($dados["instrucao4"]) ? $dados["instrucao4"] : NULL;
        $this->instrucao5 = !empty($dados["instrucao5"]) ? $dados["instrucao5"] : NULL;
        $this->cep = !empty($dados["cep"]) ? $dados["cep"] : NULL;
        $this->logradouro = !empty($dados["logradouro"]) ? $dados["logradouro"] : NULL;
        $this->numero = !empty($dados["numero"]) ? $dados["numero"] : NULL;
        $this->bairro = !empty($dados["bairro"]) ? $dados["bairro"] : NULL;
        $this->idEstado = !empty($dados["estado"]) ? $dados["estado"] : NULL;
        $this->idCidade = !empty($dados["cidade"]) ? $dados["cidade"] : NULL;
    }

    public function get($returnArray = TRUE) {
        $response = $this->db
                        ->select("dados_empresa.*,"
                                . "estados.estado,"
                                . "estados.uf,"
                                . "cidades.cidade")
                        ->join("estados", "estados.id=dados_empresa.idEstado")
                        ->join("cidades", "cidades.id=dados_empresa.idCidade")
                        ->get("dados_empresa")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "dados" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum dado encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->set($this->toArray())
                ->where("id", $this->id)
                ->update("dados_empresa");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Dados atualizados");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao atualizar dados");
        }
    }

    private function toArray() {
        return [
            "nomeFantasia" => $this->nomeFantasia,
            "razaoSocial" => $this->razaoSocial,
            "cnpj" => $this->cnpj,
            "carteira" => $this->carteira,
            "conta" => $this->conta,
            "agencia" => $this->agencia,
            "ios" => $this->ios,
            "codigoTransmissao" => $this->codigoTransmissao,
            "codigoPsk" => $this->codigoPsk,
            "codigoBanco" => $this->codigoBanco,
            "contaFidc" => $this->contaFidc,
            "telefone" => $this->telefone,
            "celular" => $this->celular,
            "email" => $this->email,
            "instrucao1" => $this->instrucao1,
            "instrucao2" => $this->instrucao2,
            "instrucao3" => $this->instrucao3,
            "instrucao4" => $this->instrucao4,
            "instrucao5" => $this->instrucao5,
            "cep" => $this->cep,
            "logradouro" => $this->logradouro,
            "numero" => $this->numero,
            "bairro" => $this->bairro,
            "idEstado" => $this->idEstado,
            "idCidade" => $this->idCidade
        ];
    }

}
