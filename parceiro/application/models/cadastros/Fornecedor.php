<?php

class Fornecedor extends CI_Model {

    private $id;
    private $nome;

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->nome = !empty($dados['nome']) ? $dados['nome'] : NULL;
    }

    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE, $returnArray = FALSE) {
        $this->db->order_by("fornecedores.nome", "ASC");

        if ($paginacao) {
            $this->db->limit($limit, $offset);
        }

        $response = $this->db->get("fornecedores")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "fornecedores" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum fornecedor encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("fornecedores", ["nome" => $this->nome]);
        $this->id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            $responseFornecedor = $this->db
                            ->where("id", $this->id)
                            ->get("fornecedores")->row();

            return ["resultado" => TRUE, "msg" => "Fornecedor adicionado", "fornecedor" => $responseFornecedor];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar fornecedor"];
        }
    }

    public function buscar($dados) {
        $response = $this->db
                        ->like("fornecedores.nome", $dados["termo"])
                        ->order_by("fornecedores.nome", "ASC")
                        ->get("fornecedores")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "fornecedores" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum fornecedor encontrado"];
        }
    }

    //NÃO FUNCIONAL
    public function getById($id, $returnArray = TRUE) {
        if ($returnArray) {
            $response = $this->db
                            ->select("ocpt_convenios_descontos.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->where("ocpt_convenios_descontos.id", $id)
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_convenios_descontos.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_convenios_descontos.idEstado")
                            ->get("ocpt_convenios_descontos")->row();

            if (!empty($response)) {
                return ["resultado" => TRUE, "cidade" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum cliente foi encontrado"];
            }
        } else {
            return $this->db
                            ->select("ocpt_convenios_descontos.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->where("ocpt_convenios_descontos.id", $id)
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_convenios_descontos.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_convenios_descontos.idEstado")
                            ->get("ocpt_convenios_descontos")->row();
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->set($this->toArray())
                ->where("id", $this->id)
                ->update("ocpt_convenios_descontos");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Cidade atualizada");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao atualizar cidade");
        }
    }

    public function deletar($id) {
        $this->db
                ->where("id", $id)
                ->delete("ocpt_convenios_descontos");

        if ($this->db->affected_rows()) {
            return array("resultado" => TRUE, "msg" => "Cidade deletada");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao deletar cidade");
        }
    }

}
