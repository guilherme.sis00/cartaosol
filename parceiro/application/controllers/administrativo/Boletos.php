<?php

use CnabPHP\Remessa;
use OpenBoleto\Banco\Santander;
use OpenBoleto\Agente;
use OpenBoleto\BoletoAbstract;

class Boletos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["administrativo/empresa", "financeiro/boleto"]);
    }

    public function a() {
        $str = "9452079";
        echo substr($str, 0, 6) . "</br>";
        echo substr($str, 6, 7);
    }

    public function info() {
        $responseDados = $this->empresa->get(FALSE);
        $remessa = $this->db->where("id", 20)->get("remessas")->row();
        $agencia = explode("-", $responseDados->agencia);
        $conta = explode("-", $responseDados->conta);
        $contaFidc = explode("-", $responseDados->contaFidc);

        # REGISTRO HEADER DO ARQUIVO REMESSA
        $dados[] = [
            [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
            [4, 0, 0, 'Lote de serviço'],
            [1, 0, 0, 'Tipo de registro'],
            [8, '', ' ', 'Reservado (uso Banco)'],
            [1, 2, null, 'Tipo de inscrição da empresa'],
            [15, $responseDados->cnpj, 0, 'Nº de inscrição da empresa'],
            [15, $responseDados->codigoTransmissao, 0, 'Código de Transmissão'],
            [25, '', ' ', 'Reservado (uso Banco)'],
            [30, $responseDados->nomeFantasia, ' ', 'Nome da empresa'],
            [30, 'Banco Santander', ' ', 'Nome do Banco'],
            [10, '', ' ', 'Reservado (uso Banco)'],
            [1, 1, 0, 'Código remessa'],
            [8, $remessa->data, 0, 'Data de geração do arquivo'],
            [6, '', ' ', 'Reservado (uso Banco)'],
            [6, $remessa->id, 0, 'Nº seqüencial do arquivo'],
            [3, '040', 0, 'Nº da versão do layout do arquivo'],
            [74, '', ' ', 'Reservado (uso Banco)'],
        ];

        # REGISTRO HEADER DO LOTE REMESSA
        $dados[] = [
            [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
            [4, 1, 0, 'Numero do lote remessa'],
            [1, 1, 0, 'Tipo de registro'],
            [1, 'R', 0, 'Tipo de operação'],
            [2, 01, 0, 'Tipo de serviço'],
            [2, '', ' ', 'Reservado (uso Banco)'],
            [3, '030', 0, 'Nº da versão do layout do lote'],
            [1, '', ' ', 'Reservado (uso Banco)'],
            [1, 2, 0, 'Tipo de inscrição da empresa'],
            [15, $responseDados->cnpj, 0, 'Nº de inscrição da empresa'],
            [20, '', ' ', 'Reservado (uso Banco)'],
            [15, $responseDados->codigoTransmissao, '0', 'Código de Transmissão'],
            [5, '', ' ', 'Reservado uso Banco'],
            [30, $responseDados->razaoSocial, ' ', 'Nome do Beneficiário'],
            [40, $responseDados->instrucao1, ' ', 'Mensagem 1'],
            [40, $responseDados->instrucao2, ' ', 'Mensagem 2'],
            [8, 0, 0, 'Número remessa/retorno'],
            [8, $remessa->data, 0, 'Data da gravação remessa/retorno'],
            [41, '', ' ', 'Reservado (uso Banco)'],
        ];

        $sequencial = 0;

        $boletos = $this->boleto->getByIdsParcelas([113, 114, 115], FALSE);
        foreach ($boletos as $i => $v) {
            $sequencial++;
            $dados[] = [
                [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
                [4, 1, 0, 'Numero do lote remessa'],
                [1, 3, 0, 'Tipo de registro'],
                [5, $sequencial, 0, 'Nº seqüencial do registro de lote'],
                [1, 'P', ' ', 'Cód. Segmento do registro detalhe'],
                [1, '', ' ', 'Reservado (uso Banco)'],
                [2, 1, 0, 'Código de movimento remessa'],
                [4, $agencia[0], 0, 'Agência do Destinatária FIDC'],
                [1, $agencia[1], 0, 'Dígito da Ag do Destinatária FIDC'],
                [9, $conta[0], 0, 'Número da conta corrente'],
                [1, $conta[1], 0, 'Dígito verificador da conta'],
                [9, $contaFidc[0], 0, 'Conta cobrança Destinatária FIDC'],
                [1, $contaFidc[1], 0, 'Dígito da conta cobrança Destinatária FIDC'],
                [2, '', ' ', 'Reservado (uso Banco)'],
                [13, $v->nossoNumero, 0, 'Identificação do título no Banco'],
                [1, 5, 0, 'Tipo de cobrança'],
                [1, 1, 0, 'Forma de Cadastramento'],
                [1, 1, 0, 'Tipo de documento'],
                [1, '', ' ', 'Reservado (uso Banco)'],
                [1, '', ' ', 'Reservado (uso Banco)'],
                [15, $v->id, 0, 'Nº do documento'],
                [8, $v->vencimento, 0, 'Data de vencimento do título'],
                [15, $v->valor, 'float', 'Valor nominal do título'],
                [4, 0, 0, 'Agência encarregada da cobrança'],
                [1, 0, 0, 'Dígito da Agência do Beneficiário'],
                [1, '', ' ', 'Reservado (uso Banco)'],
                [2, '02', ' ', 'Espécie do título'],
                [1, 'A', ' ', 'Identif. de título Aceito/Não Aceito'],
                [8, $v->dataEmissao, 0, 'Data da emissão do título'],
                [1, 3, 0, 'Código do juros de mora'],
                [8, 0, 0, 'Data do juros de mora'],
                [15, 0, 0, 'Valor da mora/dia ou Taxa mensal'],
                [1, 0, 0, 'Código do desconto 1'],
                [8, 0, 0, 'Data de desconto 1'],
                [15, 0, 'float', 'Valor ou Percentual do desconto concedido'],
                [15, 0, 'float', 'Valor do IOF a ser recolhido'],
                [15, 0, 'float', 'Valor do abatimento'],
                [25, $v->id, 0, 'Identificação do título na empresa'],
                [1, 0, 0, 'Código para protesto'],
                [2, 0, 0, 'Número de dias para protesto'],
                [1, 1, 0, 'Código para Baixa/Devolução'],
                [1, 0, 0, 'Reservado (uso Banco)'],
                [2, 15, 0, 'Número de dias para Baixa/Devolução'],
                [2, 00, 0, 'Código da moeda'],
                [11, '', ' ', 'Reservado (uso Banco)'],
            ];

//            $cliente = $v->voCliente();

            $sequencial++;
            $dados[] = [
                [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
                [4, 1, 0, 'Numero do lote remessa'],
                [1, 3, 0, 'Tipo de registro'],
                [5, $sequencial, 0, 'Nº seqüencial do registro no lote'],
                [1, 'Q', ' ', 'Cód. segmento do registro detalhe'],
                [1, '', ' ', 'Reservado (uso Banco)'],
                [2, 1, 0, 'Código de movimento remessa'],
                [1, 1, 0, 'Tipo de inscrição do Pagador'],
                [15, $v->cpf, 0, 'Número de inscrição do Pagador'],
                [40, $v->nome, ' ', 'Nome Pagador'],
                [40, "Endereço cliente", ' ', 'Endereço Pagador'],
                [15, "Bairro cliente", ' ', 'Bairro Pagador'],
                [5 + 3, "00000-000", 0, 'Cep Pagador'],
                [15, "Cidade", ' ', 'Cidade do Pagador'],
                [2, "UF", ' ', 'Unidade da federação do Pagador'],
                [1, 0, 0, 'Tipo de inscrição Sacador/avalista'],
                [15, 0, 0, 'Nº de inscrição Sacador/avalista'],
                [40, '', ' ', 'Nome do Sacador/avalista'],
                [3, 0, 0, 'Identificador de carne'],
                [3, 0, 0, 'Seqüencial da Parcela ou número inicial da parcela'],
                [3, 0, 0, 'Quantidade total de parcelas'],
                [3, 0, 0, 'Número do plano'],
                [19, '', ' ', 'Reservado (uso Banco)'],
            ];
        }

        # TRAILER DE LOTE REMESSA
        $dados[] = [
            [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
            [4, 0001, 0, 'Numero do lote remessa'],
            [1, 5, 0, 'Tipo de registro'],
            [9, '', ' ', 'Reservado (uso Banco)'],
            [6, 2 * count($boletos) + 2, 0, 'Quantidade de registros do lote'],
            [217, '', ' ', 'Reservado (uso Banco)'],
        ];

        # TRAILER DE ARQUIVO REMESSA
        $dados[] = [
            [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
            [4, 9999, 0, 'Numero do lote remessa'],
            [1, 9, 0, 'Tipo de registro'],
            [9, '', ' ', 'Reservado (uso Banco)'],
            [6, 1, 0, 'Quantidade de lotes do arquivo'],
            [6, 1 + 1 + 0 + count($boletos) * 2 + 1 + 1, 0, 'Quantidade de registros do arquivo'],
            [211, '', ' ', 'Reservado (uso Banco)'],
        ];

        $replace = [
            '/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/' => '$3$2$1', // Data
            '/^([0-9]{4})\-([0-9]{2})\-([0-9]{2}) [0-9]{2}:[0-9]{2}:[0-9]{2}$/' => '$3$2$1', // Data
            '/^([0-9]{5})\-([0-9]{3})$/' => '$1$2',
            '/^([0-9]{2})\.([0-9]{3})\.([0-9]{3})\/([0-9]{4})\-([0-9]{2})$/' => '$1$2$3$4$5', // CNPJ
            '/^([0-9]{3})\.([0-9]{3})\.([0-9]{3})\-([0-9]{2})$/' => '$1$2$3$4' // CPF
        ];

        foreach ($dados as $key => $registros) {
            $linha = '';
            $linhaSize = 0;
            $posicao = 0;
            foreach ($registros as $values) {
                list($size, $value, $padValue, $descricao) = [$values[0], $values[1], $values[2], $values[3]];

                if ($padValue === 'float') {
                    $value = (int) ($value * 100);
                    $padValue = 0;
                } else {
                    $value = preg_replace(array_keys($replace), array_values($replace), $value);
                }

                $value = str_pad($value, $size, $padValue, $padValue === ' ' ? STR_PAD_RIGHT : STR_PAD_LEFT);

                $value = substr($value, 0, $size);

                if (FALSE) {

                    $posicao++;
                    $posicaoFinal = $posicao + $size - 1;
                    $size = zerofill($size, 2);

                    $linhaSize += strlen($value);
                    $linha .= "<div> {$size}({$posicao}-{$posicaoFinal}){";
                    $linha .= $value;
                    $linha .= "} // {$descricao}</div>";

                    $posicao = $posicaoFinal;
                } else {
                    $linha .= $value;
                }
            }

            if (FALSE) {
                $linha .= "<br /> Total de {$linhaSize} caracteres<hr />";
            }

            $dados[$key] = $linha;
        }

        $conteudo = implode("\n", $dados);
        $file = 'remessa-teste.txt';
        file_put_contents("./assets/arquivos/{$file}", $conteudo);
//        ob_end_clean();

        $this->load->helper('download');
        force_download($file, file_get_contents("./assets/arquivos/{$file}"));
//        if (!FALSE) {
//            header('Content-Disposition: attachment; filename="remessa.rem"');
//            header("Content-type: text/plain; charset=UTF-8");
//            $remessa->save();
//            \system\crud\Conn::commit();
//        } else {
//            header("Content-type: text/html; charset=UTF-8");
//            \system\crud\Conn::rollBack();
//        }
//        exit($remessa->conteudo);
    }

    public function index() {
        $sacado = new Agente('Nome do SACADO', 'CPF DO SACADO', 'ENDEREÇO DO SACADO', 'NÚMERO DO SACADO', 'ESTADO SACADO', 'CIDADE');
        $cedente = new Agente('NOME DA EMPRESA', 'CNPJ', 'ENDEREÇO', 'TELEFONE', 'ESTADO', 'UF');

        $boleto = new Santander(array(
// Parâmetros obrigatórios
            'dataVencimento' => new DateTime('2013-01-24'),
            'valor' => 23.00,
            'sequencial' => 12345678901, // Até 13 dígitos
            'sacado' => $sacado,
            'cedente' => $cedente,
            'agencia' => 1234, // Até 4 dígitos
            'carteira' => 102, // 101, 102 ou 201
            'conta' => 1234567, // Código do cedente: Até 7 dígitos
// IOS – Seguradoras (Se 7% informar 7. Limitado a 9%)
// Demais clientes usar 0 (zero)
            'ios' => '0', // Apenas para o Santander
// Parâmetros recomendáveis
            'logoPath' => 'http://www.ocartaoparatodos.com.br/app/assets/img/marca.png', // Logo da sua empresa
            'contaDv' => 2,
            'agenciaDv' => 1,
            'descricaoDemonstrativo' => array(// Até 5
                'Compra de materiais cosméticos',
                'Compra de alicate',
            ),
            'instrucoes' => array(// Até 8
                'Após o dia 30/11 cobrar 2% de mora e 1% de juros ao dia.',
                'Não receber após o vencimento.',
            ),
                // Parâmetros opcionais
//'resourcePath' => '../resources',
//'moeda' => Santander::MOEDA_REAL,
//'dataDocumento' => new DateTime(),
//'dataProcessamento' => new DateTime(),
//'contraApresentacao' => true,
//'pagamentoMinimo' => 23.00,
//'aceite' => 'N',
//'especieDoc' => 'ABC',
//'numeroDocumento' => '123.456.789',
//'usoBanco' => 'Uso banco',
//'layout' => 'layout.phtml',
//'logoPath' => 'http://boletophp.com.br/img/opensource-55x48-t.png',
//'sacadorAvalista' => new Agente('Antônio da Silva', '02.123.123/0001-11'),
//'descontosAbatimentos' => 123.12,
//'moraMulta' => 123.12,
//'outrasDeducoes' => 123.12,
//'outrosAcrescimos' => 123.12,
//'valorCobrado' => 123.12,
//'valorUnitario' => 123.12,
//'quantidade' => 1,
        ));

        $dados['boleto'] = $boleto->getOutput();
        $this->load->view('teste', $dados);
    }

    public function gerarRemessa() {
        $codigo_banco = Cnab\Banco::SANTANDER;
        $arquivo = new Cnab\Remessa\Cnab240\Arquivo($codigo_banco);
        $arquivo->configure(array(
            'data_geracao' => new DateTime(),
            'data_gravacao' => new DateTime(),
            'nome_fantasia' => 'Nome Fantasia da sua empresa', // seu nome de empresa
            'razao_social' => 'Razão social da sua empresa', // sua razão social
            'cnpj' => '111', // seu cnpj completo
            'banco' => $codigo_banco, //código do banco
            'logradouro' => 'Logradouro da Sua empresa',
            'numero' => 'Número do endereço',
            'bairro' => 'Bairro da sua empresa',
            'cidade' => 'Cidade da sua empresa',
            'uf' => 'Sigla da cidade, ex SP',
            'cep' => 'CEP do endereço da sua cidade',
            'agencia' => '1234',
            'conta' => '1234567', // número da conta
            'conta_dac' => '2', // digito da conta
        ));

// você pode adicionar vários boletos em uma remessa
        $arquivo->insertDetalhe(array(
            'codigo_de_ocorrencia' => 1, // 1 = Entrada de título, futuramente poderemos ter uma constante
            'nosso_numero' => '1234567',
            'numero_documento' => '1234567',
            'carteira' => '109',
            'especie' => Cnab\Especie::CNAB240_OUTROS, // Você pode consultar as especies Cnab\Especie
            'valor' => 100.39, // Valor do boleto
            'instrucao1' => 2, // 1 = Protestar com (Prazo) dias, 2 = Devolver após (Prazo) dias, futuramente poderemos ter uma constante
            'instrucao2' => 0, // preenchido com zeros
            'sacado_nome' => 'Nome do cliente', // O Sacado é o cliente, preste atenção nos campos abaixo
            'sacado_tipo' => 'cpf', //campo fixo, escreva 'cpf' (sim as letras cpf) se for pessoa fisica, cnpj se for pessoa juridica
            'sacado_cpf' => '111.111.111-11',
            'sacado_logradouro' => 'Logradouro do cliente',
            'sacado_bairro' => 'Bairro do cliente',
            'sacado_cep' => '11111222', // sem hífem
            'sacado_cidade' => 'Cidade do cliente',
            'sacado_uf' => 'SP',
            'data_vencimento' => new DateTime('2014-06-08'),
            'data_cadastro' => new DateTime('2014-06-01'),
            'juros_de_um_dia' => 0.10, // Valor do juros de 1 dia'
            'data_desconto' => new DateTime('2014-06-01'),
            'valor_desconto' => 10.0, // Valor do desconto
            'prazo' => 10, // prazo de dias para o cliente pagar após o vencimento
            'taxa_de_permanencia' => '0', //00 = Acata Comissão por Dia (recomendável), 51 Acata Condições de Cadastramento na CAIXA
            'mensagem' => 'Descrição do boleto',
            'data_multa' => new DateTime('2014-06-09'), // data da multa
            'valor_multa' => 10.0, // valor da multa
        ));

// para salvar
        $arquivo->save('.assets/arquivos/remessa.txt');
    }

    public function teste() {
        $arquivo = new Remessa("033", 'cnab240', array(
            'nome_empresa' => "Empresa ABC", // seu nome de empresa
            'tipo_inscricao' => 2, // 1 para cpf, 2 cnpj
            'numero_inscricao' => '123.122.123-56', // seu cpf ou cnpj completo
            'agencia' => 3300, // agencia sem o digito verificador
            'agencia_dv' => 6, // somente o digito verificador da agencia
            'conta' => '12345678', // número da conta
            'conta_dv' => 9, // digito da conta
            'codigo_transmissao' => '12345678901234567890',
            'codigo_beneficiario' => '13002905', // codigo fornecido pelo banco
            'codigo_beneficiario_dv' => '2', // codigo fornecido pelo banco
            'numero_sequencial_arquivo' => 1,
                // 'situacao_arquivo' =>'P' // use T para teste e P para produção
        ));
        $lote = $arquivo->addLote(array('tipo_servico' => 1, 'codigo_transmissao' => '12345678901234567890')); // tipo_servico  = 1 para cobrança registrada, 2 para sem registro
        $lote->inserirDetalhe(array(
            'conta_cobranca' => '12345678', // número da conta cobranca obs(verificar se eh o mesmo da conta movimento)
            'data_segundo_desconto' => '',
            'codigo_movimento' => 1, //1 = Entrada de título, para outras opções ver nota explicativa C004 manual Cnab_SIGCB na pasta docs
            'nosso_numero' => 51, // numero sequencial de boleto
            'seu_numero' => 43, // se nao informado usarei o nosso numero
            /* campos necessarios somente para itau e siccob,  não precisa comentar se for outro layout    */
            'carteira_banco' => 109, // codigo da carteira ex: 109,RG esse vai o nome da carteira no banco
            'cod_carteira' => "01", // I para a maioria ddas carteiras do itau
            /* campos necessarios somente para itau,  não precisa comentar se for outro layout    */
            'especie_titulo' => "DM", // informar dm e sera convertido para codigo em qualquer laytou conferir em especie.php
            'valor' => 100.00, // Valor do boleto como float valido em php
            'emissao_boleto' => 2, // tipo de emissao do boleto informar 2 para emissao pelo beneficiario e 1 para emissao pelo banco
            'protestar' => 3, // 1 = Protestar com (Prazo) dias, 3 = Devolver após (Prazo) dias
            'prazo_protesto' => 5, // Informar o numero de dias apos o vencimento para iniciar o protesto
            'nome_pagador' => "JOSÉ da SILVA ALVES", // O Pagador é o cliente, preste atenção nos campos abaixo
            'tipo_inscricao' => 1, //campo fixo, escreva '1' se for pessoa fisica, 2 se for pessoa juridica
            'numero_inscricao' => '123.122.123-56', //cpf ou ncpj do pagador
            'endereco_pagador' => 'Rua dos developers,123 sl 103',
            'bairro_pagador' => 'Bairro da insonia',
            'cep_pagador' => '12345-123', // com hífem
            'cidade_pagador' => 'Londrina',
            'uf_pagador' => 'PR',
            'data_vencimento' => '2016-04-09', // informar a data neste formato
            'data_emissao' => '2016-04-09', // informar a data neste formato
            'vlr_juros' => 0.15, // Valor do juros de 1 dia'
            'data_desconto' => '2016-04-09', // informar a data neste formato
            'vlr_desconto' => '0', // Valor do desconto
            'baixar' => 1, // codigo para indicar o tipo de baixa '1' (Baixar/ Devolver) ou '2' (Não Baixar / Não Devolver)
            'prazo_baixa' => 90, // prazo de dias para o cliente pagar após o vencimento
            'mensagem' => 'JUROS de R$0,15 ao dia' . PHP_EOL . "Não receber apos 30 dias",
            'email_pagador' => 'rogerio@ciatec.net', // data da multa
            'data_multa' => '2016-04-09', // informar a data neste formato, // data da multa
            'vlr_multa' => 30.00, // valor da multa
            // campos necessários somente para o sicoob
            'taxa_multa' => 30.00, // taxa de multa em percentual
            'taxa_juros' => 30.00, // taxa de juros em percentual
        ));
        $lote->inserirDetalhe(array(
//            'conta_cobranca' => '12345678', // número da conta cobranca obs(verificar se eh o mesmo da conta movimento)
//            'data_segundo_desconto' => '',
            'codigo_movimento' => 1, //1 = Entrada de título, para outras opções ver nota explicativa C004 manual Cnab_SIGCB na pasta docs
            'nosso_numero' => 50, // numero sequencial de boleto
            'seu_numero' => 43, // se nao informado usarei o nosso numero
//            'especie_titulo' => "DM", // informar dm e sera convertido para codigo em qualquer laytou conferir em especie.php
            'valor' => 100.00, // Valor do boleto como float valido em php
            'emissao_boleto' => 2, // tipo de emissao do boleto informar 2 para emissao pelo beneficiario e 1 para emissao pelo banco
//            'protestar' => 3, // 1 = Protestar com (Prazo) dias, 3 = Devolver após (Prazo) dias
//            'prazo_protesto' => 5, // Informar o numero de dias apos o vencimento para iniciar o protesto
            'nome_pagador' => "JOSÉ da SILVA ALVES", // O Pagador é o cliente, preste atenção nos campos abaixo
            'tipo_inscricao' => 1, //campo fixo, escreva '1' se for pessoa fisica, 2 se for pessoa juridica
            'numero_inscricao' => '123.122.123-56', //cpf ou ncpj do pagador
            'endereco_pagador' => "Endereço",
            'bairro_pagador' => 'Bairro',
            'cep_pagador' => '00000-000', // com hífem
            'cidade_pagador' => 'Londrina',
            'uf_pagador' => 'PR',
            'data_vencimento' => '2016-04-09', // informar a data neste formato
            'data_emissao' => '2016-04-09', // informar a data neste formato
            'mensagem' => 'JUROS de R$0,15 ao dia' . PHP_EOL . "Não receber apos 30 dias",
        ));
        $resultado = utf8_decode($arquivo->getText()); // observar a header do seu php para não gerar comflitos de codificação de caracteres;
        file_put_contents('./assets/arquivos/remessas/remessa-test.rem', $resultado);
        echo $resultado;
    }

    public function teste2() {
//        \system\crud\Conn::startTransaction();
//
//        $ignorar = (array) $_GET['ignorar'];
//
//        /* @var $remessa RemessaVO */
//        $remessa = new RemessaVO(RemessasModel::instance());
//
//        $remessa->setData(date('Y-m-d'));
//        $remessa->save();
//
//        $config = DadosModel::get();
//
//        /* @var $boletos ReceitaVO[] */
//        $boletos = ReceitasModel::busca($_GET, 1, 9999)->getRegistros();
//
//        $dados = [];

        $responseEmpresa = $this->empresa->get(FALSE);

        # REGISTRO HEADER DO ARQUIVO REMESSA
        $dados[] = [
            [3, 033, 0, 'Código do Banco na compensação'],
            [4, 0, 0, 'Lote de serviço'],
            [1, 0, 0, 'Tipo de registro'],
            [8, '', ' ', 'Reservado (uso Banco)'],
            [1, 1, 'Tipo de inscrição da empresa'],
            [15, $responseEmpresa->cnpj, 0, 'Nº de inscrição da empresa'],
            [15, $responseEmpresa->codigoTransmissao, 0, 'Código de Transmissão'],
            [25, '', ' ', 'Reservado (uso Banco)'],
            [30, $responseEmpresa->nomeFantasia, ' ', 'Nome da empresa'],
            [30, 'Banco Santander', ' ', 'Nome do Banco'],
            [10, '', ' ', 'Reservado (uso Banco)'],
            [1, 1, 0, 'Código remessa'],
            [8, date("Y-m-d"), 0, 'Data de geração do arquivo'],
            [6, '', ' ', 'Reservado (uso Banco)'],
            [6, 1, 0, 'Nº seqüencial do arquivo'],
            [3, '040', 0, 'Nº da versão do layout do arquivo'],
            [74, '', ' ', 'Reservado (uso Banco)'],
        ];

        # REGISTRO HEADER DO LOTE REMESSA
        $dados[] = [
            [3, 033, 0, 'Código do Banco na compensação'],
            [4, 1, 0, 'Numero do lote remessa'],
            [1, 1, 0, 'Tipo de registro'],
            [1, 'R', 0, 'Tipo de operação'],
            [2, 01, 0, 'Tipo de serviço'],
            [2, '', ' ', 'Reservado (uso Banco)'],
            [3, '030', 0, 'Nº da versão do layout do lote'],
            [1, '', ' ', 'Reservado (uso Banco)'],
            [1, 1, 'Tipo de inscrição da empresa'],
            [15, $responseEmpresa->cnpj, 0, 'Nº de inscrição da empresa'],
            [20, '', ' ', 'Reservado (uso Banco)'],
            [15, $responseEmpresa->codigoTransmissao, '0', 'Código de Transmissão'],
            [5, '', ' ', 'Reservado uso Banco'],
            [30, $responseEmpresa->razaoSocial, ' ', 'Nome do Beneficiário'],
            [40, $responseEmpresa->instrucao1, ' ', 'Mensagem 1'],
            [40, $responseEmpresa->instrucao2, ' ', 'Mensagem 2'],
            [8, 0, 0, 'Número remessa/retorno'],
            [8, date("Y-m-d"), 0, 'Data da gravação remessa/retorno'],
            [41, '', ' ', 'Reservado (uso Banco)'],
        ];

//        $sequencial = 0;
//
//        foreach ($boletos as $i => $v) {
//
//            if (in_array($v->getId(), $ignorar)) {
//                continue;
//            }
//
//            $v->setRemessa($remessa->getId());
//            $v->save();
//
//            $sequencial++;
        $agencia = explode("-", $responseEmpresa->agencia);
        $conta = explode("-", $responseEmpresa->conta);
        $contaFidc = explode("-", $responseEmpresa->conta);
        $dados[] = [
            [3, 033, 0, 'Código do Banco na compensação'],
            [4, 1, 0, 'Numero do lote remessa'],
            [1, 3, 0, 'Tipo de registro'],
            [5, 1, 0, 'Nº seqüencial do registro de lote'],
            [1, 'P', ' ', 'Cód. Segmento do registro detalhe'],
            [1, '', ' ', 'Reservado (uso Banco)'],
            [2, 1, 0, 'Código de movimento remessa'],
            [4, $agencia[0], 0, 'Agência do Destinatária FIDC'],
            [1, $agencia[1], 0, 'Dígito da Ag do Destinatária FIDC'],
            [9, $conta[0], 0, 'Número da conta corrente'],
            [1, $conta[1], 0, 'Dígito verificador da conta'],
            [9, $contaFidc[0], 0, 'Conta cobrança Destinatária FIDC'],
            [1, $contaFidc[1], 0, 'Dígito da conta cobrança Destinatária FIDC'],
            [2, '', ' ', 'Reservado (uso Banco)'],
            [13, 1, 0, 'Identificação do título no Banco'],
            [1, 5, 0, 'Tipo de cobrança'],
            [1, 1, 0, 'Forma de Cadastramento'],
            [1, 1, 0, 'Tipo de documento'],
            [1, '', ' ', 'Reservado (uso Banco)'],
            [1, '', ' ', 'Reservado (uso Banco)'],
            [15, 1, 0, 'Nº do documento'],
            [8, "2018-04-30", 0, 'Data de vencimento do título'],
            [15, 19.90, 'float', 'Valor nominal do título'],
            [4, 0, 0, 'Agência encarregada da cobrança'],
            [1, 0, 0, 'Dígito da Agência do Beneficiário'],
            [1, '', ' ', 'Reservado (uso Banco)'],
            [2, '02', ' ', 'Espécie do título'],
            [1, 'A', ' ', 'Identif. de título Aceito/Não Aceito'],
            [8, "2018-04-18", 0, 'Data da emissão do título'],
            [1, 3, 0, 'Código do juros de mora'],
            [8, 0, 0, 'Data do juros de mora'],
            [15, 0, 0, 'Valor da mora/dia ou Taxa mensal'],
            [1, 0, 0, 'Código do desconto 1'],
            [8, 0, 0, 'Data de desconto 1'],
            [15, 0, 'float', 'Valor ou Percentual do desconto concedido'],
            [15, 0, 'float', 'Valor do IOF a ser recolhido'],
            [15, 0, 'float', 'Valor do abatimento'],
            [25, 1, 0, 'Identificação do título na empresa'],
            [1, 0, 0, 'Código para protesto'],
            [2, 0, 0, 'Número de dias para protesto'],
            [1, 1, 0, 'Código para Baixa/Devolução'],
            [1, 0, 0, 'Reservado (uso Banco)'],
            [2, 15, 0, 'Número de dias para Baixa/Devolução'],
            [2, 00, 0, 'Código da moeda'],
            [11, '', ' ', 'Reservado (uso Banco)'],
        ];

//            $cliente = $v->voCliente();
//            $sequencial++;
        $dados[] = [
            [3, 033, 0, 'Código do Banco na compensação'],
            [4, 1, 0, 'Numero do lote remessa'],
            [1, 3, 0, 'Tipo de registro'],
            [5, 2, 0, 'Nº seqüencial do registro no lote'],
            [1, 'Q', ' ', 'Cód. segmento do registro detalhe'],
            [1, '', ' ', 'Reservado (uso Banco)'],
            [2, 1, 0, 'Código de movimento remessa'],
            [1, 2, 'Tipo de inscrição do Pagador'],
            [15, "014.338.762-64", 0, 'Número de inscrição do Pagador'],
            [40, "Nome do cliente", ' ', 'Nome Pagador'],
            [40, "Endereço do cliente", ' ', 'Endereço Pagador'],
            [15, "Bairro do cliente", ' ', 'Bairro Pagador'],
            [5 + 3, "69087170", 0, 'Cep Pagador'],
            [15, "Cidade do cliente", ' ', 'Cidade do Pagador'],
            [2, "UF do cliente", ' ', 'Unidade da federação do Pagador'],
            [1, 0, 0, 'Tipo de inscrição Sacador/avalista'],
            [15, 0, 0, 'Nº de inscrição Sacador/avalista'],
            [40, '', ' ', 'Nome do Sacador/avalista'],
            [3, 0, 0, 'Identificador de carne'],
            [3, 0, 0, 'Seqüencial da Parcela ou número inicial da parcela'],
            [3, 0, 0, 'Quantidade total de parcelas'],
            [3, 0, 0, 'Número do plano'],
            [19, '', ' ', 'Reservado (uso Banco)'],
        ];
//        }
        # TRAILER DE LOTE REMESSA
        $dados[] = [
            [3, 033, 0, 'Código do Banco na compensação'],
            [4, 0001, 0, 'Numero do lote remessa'],
            [1, 5, 0, 'Tipo de registro'],
            [9, '', ' ', 'Reservado (uso Banco)'],
            [6, 2 * 1 + 2, 0, 'Quantidade de registros do lote'],
            [217, '', ' ', 'Reservado (uso Banco)'],
        ];

        # TRAILER DE ARQUIVO REMESSA
        $dados[] = [
            [3, 033, 0, 'Código do Banco na compensação'],
            [4, 9999, 0, 'Numero do lote remessa'],
            [1, 9, 0, 'Tipo de registro'],
            [9, '', ' ', 'Reservado (uso Banco)'],
            [6, 1, 0, 'Quantidade de lotes do arquivo'],
            [6, 1 + 1 + 0 + 1 * 2 + 1 + 1, 0, 'Quantidade de registros do arquivo'],
            [211, '', ' ', 'Reservado (uso Banco)'],
        ];

        $replace = [
            '/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/' => '$3$2$1', // Data
            '/^([0-9]{4})\-([0-9]{2})\-([0-9]{2}) [0-9]{2}:[0-9]{2}:[0-9]{2}$/' => '$3$2$1', // Data
            '/^([0-9]{5})\-([0-9]{3})$/' => '$1$2',
            '/^([0-9]{2})\.([0-9]{3})\.([0-9]{3})\/([0-9]{4})\-([0-9]{2})$/' => '$1$2$3$4$5', // CNPJ
            '/^([0-9]{3})\.([0-9]{3})\.([0-9]{3})\-([0-9]{2})$/' => '$1$2$3$4' // CPF
        ];

        foreach ($dados as $key => $registros) {
            $linha = '';
            $linhaSize = 0;
            $posicao = 0;
            foreach ($registros as $values) {

                list($size, $value, $padValue, $descricao) = [$values[0], $values[1], $values[2], $values[3]];


                if ($padValue === 'float') {
                    $value = (int) ($value * 100);
                    $padValue = 0;
                } else {
                    $value = preg_replace(array_keys($replace), array_values($replace), $value);
                }

                $value = str_pad($value, $size, $padValue, $padValue === ' ' ? STR_PAD_RIGHT : STR_PAD_LEFT);

                $value = substr($value, 0, $size);

                if (IS_LOCAL) {
                    $posicao++;
                    $posicaoFinal = $posicao + $size - 1;
                    $size = zerofill($size, 2);

                    $linhaSize += strlen($value);
                    $linha .= "<div> {$size}({$posicao}-{$posicaoFinal}){";
                    $linha .= $value;
                    $linha .= "} // {$descricao}</div>";

                    $posicao = $posicaoFinal;
                } else {
                    $linha .= $value;
                }
            }

            if (IS_LOCAL) {
                $linha .= "<br /> Total de {$linhaSize} caracteres<hr />";
            }

            $dados[$key] = $linha;
        }

        $remessa->setContent(implode("\n", $dados));

        ob_end_clean();
//
//        if (!IS_LOCAL) {
        header('Content-Disposition: attachment; filename="remessa.rem"');
        header("Content-type: text/plain; charset=UTF-8");
//            $remessa->save();
//            \system\crud\Conn::commit();
//        } else {
//            header("Content-type: text/html; charset=UTF-8");
//            \system\crud\Conn::rollBack();
//        }
//
//        exit($remessa->getContent());
    }

    public function teste3() {
        $responseEmpresa = $this->empresa->get(FALSE);

        $agencia = explode("-", $responseEmpresa->agencia);
        $conta = explode("-", $responseEmpresa->conta);
        $contaFidc = explode("-", $responseEmpresa->contaFidc);

        $arquivo = new Remessa("033", 'cnab240', array(
            'nome_empresa' => $responseEmpresa->nomeFantasia, // seu nome de empresa
            'tipo_inscricao' => 2, // 1 para cpf, 2 cnpj
            'numero_inscricao' => $responseEmpresa->cnpj, // seu cpf ou cnpj completo
            'agencia' => $agencia[0], // agencia sem o digito verificador
            'agencia_dv' => $agencia[1], // somente o digito verificador da agencia
            'conta' => $conta[0], // número da conta
            'conta_dv' => $conta[1], // digito da conta
            'codigo_beneficiario' => $contaFidc[0],
            'codigo_beneficiario_dv' => $contaFidc[1], // codigo fornecido pelo banco
            'codigo_transmissao' => $responseEmpresa->codigoTransmissao,
            'numero_sequencial_arquivo' => 1,
        ));

        $lote = $arquivo->addLote(array('tipo_servico' => 1, 'codigo_transmissao' => $responseEmpresa->codigoTransmissao)); // tipo_servico  = 1 para cobrança registrada, 2 para sem registro

        $lote->inserirDetalhe([
            'codigo_movimento' => 1, //1 = Entrada de título, para outras opções ver nota explicativa C004 manual Cnab_SIGCB na pasta docs
            'nosso_numero' => 000000123, // numero sequencial de boleto
            'seu_numero' => 123, // numero sequencial de boleto
            'valor' => 100.00, // Valor do boleto como float valido em php
            'emissao_boleto' => 2, // tipo de emissao do boleto informar 2 para emissao pelo beneficiario e 1 para emissao pelo banco
            'nome_pagador' => "Pagador", // O Pagador é o cliente, preste atenção nos campos abaixo
            'tipo_inscricao' => 1, //campo fixo, escreva '1' se for pessoa fisica, 2 se for pessoa juridica
            'numero_inscricao' => "014.338.762-64", //cpf ou ncpj do pagador
            'cidade_pagador' => "Manaus",
            'uf_pagador' => "AM",
            'data_vencimento' => "2018-04-30", // informar a data neste formato
            'data_emissao' => "2018-30-21", // informar a data neste formato
            'endereco_pagador' => "Endereço",
            'bairro_pagador' => 'Bairro',
            'cep_pagador' => '00000-000', // com hífem
            'mensagem' => "MENSAGEM"
        ]);

        $resultado = utf8_decode($arquivo->getText()); // observar a header do seu php para não gerar comflitos de codificação de caracteres;
        $file = 'remessa-' . date("Y-m-d") . '.rem';
        file_put_contents("./assets/arquivos/remessas/{$file}", $resultado);

        if (file_exists("./assets/arquivos/remessas/{$file}")) {
//            $this->conteudo = $resultado;
//            $this->path = $file;
//            $this->status = 1;
//            $this->atualizar();
//
//            $this->boleto->baixaRemessa((array) $responseBoletos, $this->data);
            echo "Remessa gerada";
        } else {
            echo "Falha";
        }
    }

}
