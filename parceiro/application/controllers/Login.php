<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["usuario"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-parceiro")) {
            redirect("dashboard");
        } else {
            $this->load->view('autenticacao/login');
        }
    }

    public function autenticar() {
        if ($this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->usuario->preencherDados($this->input->post());
                $response = $this->usuario->autenticar();

                if ($response["resultado"]) {
                    $this->session->set_userdata([
                        'id' => $response['dados']->id,
                        'usuario' => $response['dados']->usuario,
                        'ocpt-parceiro' => TRUE
                    ]);
                }

                echo json_encode($response);
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            redirect("login");
        }
    }

    public function sair() {
        $this->session->sess_destroy();
        redirect("login");
    }

}
