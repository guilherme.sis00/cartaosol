<?php

class Parceiros extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["parceiro"]);
        $this->load->library(["upload"]);
        $this->load->helper(["string", "file"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $config = $this->configuracao->getConfigPagination(base_url('parceiros'), $this->utils->countAll("ocpt_parceiros"));
            $this->pagination->initialize($config);
            $dados['paginacao'] = $this->pagination->create_links();

            $offset = ($this->uri->segment(2)) ? (($this->uri->segment(2) - 1) * 10) : 0;
            $dados["parceiros"] = $this->parceiro->getAll($config['per_page'], $offset);
            $this->load->view("parceiros/home", $dados);
        } else {
            redirect("login");
        }
    }

    public function getById() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->parceiro->getById($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function vAdicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $dados["estados"] = $this->utils->getEstados();
            $dados["especialidades"] = $this->utils->getEspecialidades();

            $this->load->view("parceiros/adicionar", $dados);
        } else {
            redirect("login");
        }
    }

    public function vAtualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(2) && is_numeric($this->uri->segment(2)))) {
                $response = $this->parceiro->getById($this->uri->segment(2));

                if ($response['resultado']) {
                    $dados["estados"] = $this->utils->getEstados();
                    $dados["cidades"] = $this->utils->getCidadesByIdEstado($response['parceiro']->idEstado, FALSE);
                    $dados["especialidades"] = $this->utils->getEspecialidades();
                    $dados["parceiro"] = $response["parceiro"];
                    $dados["especialidadesParceiro"] = $response["parceiro"]->exame == 1 ? $response["especialidades"] : [];

                    $this->load->view("parceiros/atualizar", $dados);
                } else {
                    $this->session->set_flashdata("msg", "Cliente inexistente");
                    redirect("clientes");
                }
            } else {
                $this->session->set_flashdata("msg", "Cliente inexistente");
                redirect("clientes");
            }
        } else {
            redirect("login");
        }
    }

    public function adicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $config = array(
                    'upload_path' => './assets/img/logos',
                    'allowed_types' => 'png|jpeg|jpg',
                    'max_size' => '500000',
                    "file_name" => date("d-m-Y H-i-s") . "-" . random_string("alnum", 5),
                    'file_ext_tolower' => TRUE
                );

                $this->upload->initialize($config);

                if ($this->upload->do_upload('logo')) {
                    $dados = $this->input->post();
                    $dados["logo"] = $this->upload->data("file_name");
                    $this->parceiro->preencherDados($dados);
                    echo json_encode($this->parceiro->adicionar());
                } else {
                    echo json_encode(['resultado' => FALSE, 'msg' => "Erro ao fazer o upload da logo do parceiro, por favor tente novamente"]);
                }
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function atualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->parceiro->preencherDados($this->input->post());
                echo json_encode($this->parceiro->atualizar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function alterarLogo() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->input->post("logoAtual")) {
                $config = array(
                    'upload_path' => './assets/img/logos',
                    'allowed_types' => 'png|jpeg|jpg',
                    'max_size' => '500000',
                    "file_name" => date("d-m-Y H-i-s") . "-" . random_string("alnum", 5),
                    'file_ext_tolower' => TRUE,
                    'overwrite' => TRUE
                );

                $this->upload->initialize($config);

                if ($this->upload->do_upload('modalAlterarLogoLogo')) {
                    $this->parceiro->preencherDados([
                        "id" => $this->input->post("idParceiro"),
                        "logo" => $this->upload->data("file_name")
                    ]);

                    $response = $this->parceiro->alterarLogo();
                    if ($response["resultado"]) {
                        unlink("./assets/img/logos/{$this->input->post("logoAtual")}");
                    }
                    echo json_encode($response);
                } else {
                    echo json_encode(['resultado' => FALSE, 'msg' => "Erro ao fazer o upload da logo do parceiro, por favor tente novamente"]);
                }
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function buscar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("termo")) && !empty($this->input->post("tipoBusca"))) {
                echo json_encode($this->parceiro->buscar($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function deletar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(3) && is_numeric($this->uri->segment(3)))) {
                $response = $this->parceiro->deletar($this->uri->segment(3));

                if ($response['resultado']) {
                    $this->session->set_flashdata([
                        "msg" => $response["msg"],
                        "tipo" => "success",
                    ]);
                } else {
                    $this->session->set_flashdata([
                        "msg" => "Operação não executada",
                        "tipo" => "danger",
                    ]);
                }

                redirect("parceiros");
            } else {
                $this->session->set_flashdata([
                    "msg" => "Operação não executada",
                    "tipo" => "danger",
                ]);
                redirect("parceiros");
            }
        } else {
            redirect("login");
        }
    }
}
