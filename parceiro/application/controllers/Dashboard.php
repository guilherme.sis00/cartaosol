<?php

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["cliente"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-parceiro")) {
            $this->load->view("dashboard/home");
        } else {
            redirect("login");
        }
    }

    public function consultarCliente() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-parceiro") && $this->input->post("ajax")) {
            if (!empty($this->input->post("nome")) || !empty($this->input->post("cpf")) || !empty($this->input->post("cartao"))) {
                echo json_encode($this->cliente->consultar($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function getDependentes() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-parceiro") && $this->input->post("ajax")) {
            if (!empty($this->input->post("idCliente"))) {
                echo json_encode($this->cliente->getDependentes($this->input->post("idCliente")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }
}
