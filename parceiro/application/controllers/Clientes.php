<?php

class Clientes extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["cliente"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-vendas")) {
            $config = $this->configuracao->getConfigPagination(base_url('clientes'), $this->cliente->count());
            $this->pagination->initialize($config);
            $dados['paginacao'] = $this->pagination->create_links();

            $offset = ($this->uri->segment(2)) ? (($this->uri->segment(2) - 1) * 10) : 0;
            $dados["clientes"] = $this->cliente->getAll($config['per_page'], $offset);
            $this->load->view("clientes/home", $dados);
        } else {
            redirect("login");
        }
    }

    public function getById() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-vendas") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->cliente->getById($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function vAdicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-vendas")) {
            $dados["estados"] = $this->utils->getEstados();

            $this->load->view("clientes/adicionar", $dados);
        } else {
            redirect("login");
        }
    }

    public function vAtualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-vendas")) {
            if (!empty($this->uri->segment(2) && is_numeric($this->uri->segment(2)))) {
                $response = $this->cliente->getById($this->uri->segment(2));

                if ($response['resultado']) {
                    $dados["estados"] = $this->utils->getEstados();
                    $dados["cidades"] = $this->utils->getCidadesByIdEstado($response['cliente']->idEstado, FALSE);
                    $dados["cliente"] = $response["cliente"];

                    $this->load->view("clientes/atualizar", $dados);
                } else {
                    $this->session->set_flashdata("msg", "Cliente inexistente");
                    redirect("clientes");
                }
            } else {
                $this->session->set_flashdata("msg", "Cliente inexistente");
                redirect("clientes");
            }
        } else {
            redirect("login");
        }
    }

    public function adicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-vendas") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->cliente->preencherDados($this->input->post());
                echo json_encode($this->cliente->adicionar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function atualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-vendas") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->cliente->preencherDados($this->input->post());
                echo json_encode($this->cliente->atualizar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function deletar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-vendas") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->cliente->deletar($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function buscar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-vendas") && $this->input->post("ajax")) {
            if (!empty($this->input->post("termo")) && !empty($this->input->post("tipoBusca"))) {
                echo json_encode($this->cliente->buscar($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

}
