<?php

class Combos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["vendas/combo", "especialidade"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $config = $this->configuracao->getConfigPagination(base_url('vendas/combos'), $this->utils->countAll("combos"));
            $this->pagination->initialize($config);
            $dados['paginacao'] = $this->pagination->create_links();

            $offset = ($this->uri->segment(3)) ? (($this->uri->segment(3) - 1) * 10) : 0;
            $dados["combos"] = $this->combo->getAll($config['per_page'], $offset);
            $this->load->view("vendas/combos/home", $dados);
        } else {
            redirect("login");
        }
    }

    public function get() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->combo->getById($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function vAdicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $dados["especialidades"] = $this->especialidade->getAll();

            $this->load->view("vendas/combos/adicionar", $dados);
        } else {
            redirect("login");
        }
    }

    public function vAtualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(3) && is_numeric($this->uri->segment(3)))) {
                $response = $this->combo->getById($this->uri->segment(3));

                if ($response['resultado']) {
                    $dados["especialidades"] = $this->especialidade->getAll();
                    $dados["especialidadesCombo"] = $this->combo->getEspecialidadesByIdCombo($response['combo']->id, FALSE);
                    $dados["combo"] = $response["combo"];

                    $this->load->view("vendas/combos/atualizar", $dados);
                } else {
                    $this->session->set_flashdata([
                        "msg", $response["msg"],
                        "tipo", "warning"
                    ]);
                    redirect("vendas/combos");
                }
            } else {
                $this->session->set_flashdata([
                    "msg", "Falha ao processar requisição",
                    "tipo", "warning"
                ]);
                redirect("vendas/combos");
            }
        } else {
            redirect("login");
        }
    }

    public function vProcedimentos() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(4) && is_numeric($this->uri->segment(4)))) {
                $response = $this->combo->getEspecialidadesByIdCombo($this->uri->segment(4));

                if ($response['resultado']) {
                    $dados["combo"] = $this->uri->segment(4);
                    $dados["especialidades"] = $this->especialidade->getAll();
                    $dados["especialidadesCombo"] = $response["especialidades"];

                    $this->load->view("vendas/combos/procedimentos", $dados);
                } else {
                    $this->session->set_flashdata([
                        "msg", $response["msg"],
                        "tipo", "warning"
                    ]);
                    redirect("vendas/combos");
                }
            } else {
                $this->session->set_flashdata([
                    "msg", "Falha ao processar requisição",
                    "tipo", "warning"
                ]);
                redirect("vendas/combos");
            }
        } else {
            redirect("login");
        }
    }

    public function adicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("nome")) && !empty($this->input->post("valorCombo")) && !empty($this->input->post("itens"))) {
                $this->combo->preencherDados($this->input->post());
                echo json_encode($this->combo->adicionar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function atualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id")) && !empty($this->input->post("nome")) && !empty($this->input->post("valorCombo"))) {
                $this->combo->preencherDados($this->input->post());
                echo json_encode($this->combo->atualizar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function buscar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("termo"))) {
                echo json_encode($this->combo->buscar($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function deletar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->combo->deletar($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação de dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => TRUE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function adicionarProcedimento() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                echo json_encode($this->combo->adicionarProcedimento($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }
    
    public function deletarProcedimento() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->combo->deletarProcedimento($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação de dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => TRUE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

}
