<?php

class MeusDados extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["usuario"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $dados = $this->usuario->getById($this->session->id);
            $this->load->view("meus-dados/home", $dados);
        } else {
            redirect("login");
        }
    }

    public function atualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->usuario->preencherDados($this->input->post());
                echo json_encode($this->usuario->atualizar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados informados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

}
