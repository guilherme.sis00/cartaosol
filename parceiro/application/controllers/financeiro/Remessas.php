<?php

class Remessas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["financeiro/boleto", "financeiro/rem"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $this->load->view('financeiro/remessas/home');
        } else {
            redirect("login");
        }
    }

    public function getBoletos() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("exportado"))) {
                echo json_encode($this->boleto->getBoletosParaRemessa($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação de dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function gerar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            echo json_encode($this->rem->gerar($this->input->post()));
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function baixar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->input->get("file"))) {
                if (file_exists("./assets/arquivos/remessas/{$this->input->get("file")}")) {
                    $this->load->helper('download');
                    force_download($this->input->get("file"), file_get_contents("./assets/arquivos/remessas/{$this->input->get("file")}"));
                } else {
                    redirect("financeiro/remessas");
                }
            } else {
                redirect("login");
            }
        } else {
            redirect("login");
        }
    }

}