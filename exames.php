<?php include('header.php') ?>
<?php include('controller.php') ?>

<div class="adquira_clinicas" style="margin-top: 67px;bottom: 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="media">
                    <div class="media-left">
                        <i class="fa fa-credit-card fa-2x" aria-hidden="true"
                           style="margin-top: 30px;border: 1px solid rgb(0,138,168);border-radius: 100px;padding: 10px 9px 9px 9px"></i>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading" style="padding-top: 30px;"><b>Adquira o seu cartão</b></h3>
                        <h5 class="font_chamadas">Faça parte desta rede de beneficiados que cresce todo dia, e
                            comece a economizar diariamente em suas compras.</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-xs-12 adquira_form">

                <form class="form-inline" class="form-inline" id="formulario-contato" action="mail_adquira.php" method="post" enctype="multipart/form-data">
                    <input type="nome" class="form-control" id="nome" name="nome" placeholder="SEU NOME"
                           style="width: 100% !important;">

                    <div class="form-group">
                        <input type="celular" class="form-control" id="celular" name="celular" placeholder="CELULAR">

                        <input type="email" class="form-control" id="email" name="email" placeholder="E-MAIL">
                    </div>

                    <button type="submit" class="btn btn-primary pull-right" style="margin-top: 1px;"><i
                            class="fa fa-plus" aria-hidden="true"></i></button>

                </form>
            </div>
            <div class="col-md-3">
                <img class="img-responsive center-block" src="img/cartao.png" alt="">
            </div>
        </div>
    </div>
</div>


<div class="jumbotron"
     style="background-image: url(img/bannerclinica.png); background-size: cover; height: 100%; background-repeat: no-repeat; background-position: center;margin-bottom: 0px;padding-bottom: 0px;padding-top: 0px;">

    <div class="container">
        <div class="row text-clinica">
            <div class="col-md-12">

                <h1><b class="s">EXAMES</b></h1>

                <p>ENCONTRE AS MELHORES <br> CLÍNICAS DA SUA REGIÃO</p>
            </div>
        </div>
    </div>

</div>


<!-- <div class="barraselect">
      <div class="container">
          <div class="row">
              <div class="col-md-3">
                  <div class="checkbox">
                <label>
                  <input type="checkbox"> HOPITAIS E CLÍNICAS
                </label>
              </div>
          </div>
              <div class="col-md-6">
                  <div class="checkbox">
                <label>
                  <input type="checkbox"> EXAMES LABORATORIAIS E COMPLEMENTARES
                </label>
              </div>
              </div>
              <div class="col-md-2">
                  <div class="checkbox">
                <label>
                  <input type="checkbox"> PARCEIROS
                </label>
              </div>
              </div>
          </div>
      </div>
  </div> -->

<div class="container">
    <div class="row">
        <br>
    </div>
    <!-- LABORATORIAIS -->
    <div class="panel panel-default">
        <div class="panel-heading"><span class="glyphicon glyphicon-tint" aria-hidden="true"></span> Laboratoriais
        </div>
        <ul class="list-group">
            <?php $laboratoriais = getEspecialidades(); ?>
            <?php if (!empty($laboratoriais)): ?>
                <?php while ($especialidade = mysqli_fetch_array($laboratoriais)) : ?>
                    <?php if ($especialidade["tipo"] == "Laboratoriais"): ?>
                        <li class="list-group-item"><?php echo utf8_encode($especialidade["especialidade"]); ?>  <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-id="<?= $especialidade["id"] ?>" data-target="#modalAgendamento">AGENDAR</button></li>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </ul>
    </div>

    <!-- IMAGEM -->
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-rss" aria-hidden="true"></i> Imagem
        </div>
        <ul class="list-group">
            <?php $imagens = getEspecialidades(); ?>
            <?php if (!empty($imagens)): ?>
                <?php while ($especialidade = mysqli_fetch_array($imagens)) : ?>
                    <?php if ($especialidade["tipo"] == "Imagem"): ?>
                        <li class="list-group-item"><?php echo utf8_encode($especialidade["especialidade"]); ?>  <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#modalAgendamento">AGENDAR</button></li>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </ul>
    </div>

    <!-- OUTROS -->
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-flask" aria-hidden="true"></i> Outros
        </div>
        <ul class="list-group">
            <?php $outros = getEspecialidades(); ?>
            <?php if (!empty($outros)): ?>
                <?php while ($especialidade = mysqli_fetch_array($outros)) : ?>
                    <?php if ($especialidade["tipo"] == "Outros"): ?>
                        <li class="list-group-item"><?php echo utf8_encode($especialidade["especialidade"]); ?>  <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#modalAgendamento">AGENDAR</button></li>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </ul>
    </div>

    <!-- Modal > AGENDAMENTO -->
    <div class="modal fade" id="modalAgendamento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agendamento de Exames</h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="modalAgendamentoParceiros">
                    </div>
                    <!--                    <h2>Ligue e agende</h2>
                                        <h4>Para fazer o agendamento do seu exame, ligue das 08:00 as 23:00 no telefone (11) 4675-5166, e informe que é do O Cartão Para Todos.</h4>-->
                </div>
                <!--<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>-->
            </div>
        </div>
    </div>

    <div class="row">
        <br>
    </div>
</div>

<script>
    $("#modalAgendamento").on("show.bs.modal", function (e) {
        let id = $(e.relatedTarget).data("id");
        $("#modalAgendamentoParceiros").empty();

        $.post("controller.php",
                {idEspecialidade: id},
                function (r) {
                    if (r.resultado) {
                        $.each(r.parceiros, function (key, parceiro) {
                            $("#modalAgendamentoParceiros").append(`<div class="col-md-12 col-xs-12">
                                                                        <span style="font-size: 1.7em">${parceiro.nome}</span></br>
                                                                        <span style="font-size: 1.2em">${parceiro.cidade} / ${parceiro.uf}</span></br>
                                                                        <span style="font-size: 1.2em">${parceiro.telefone}</span></br>
                                                                        <span style="font-size: 1.2em">${parceiro.endereco}</span>
                                                                    </div>
                                                                    <div class="col-md-12 col-xs-12">
                                                                        <hr>
                                                                    </div>`);
                        });
                    } else {
                        $("#modalAgendamentoParceiros").append(`<div class="col-md-12 col-xs-12">
                                                                        <h3>${r.msg}</h3>
                                                                    </div>`);
                    }
                },
                "JSON"
                );
    });
</script>

<?php include('footer.php') ?>

<?php include('bottom.php') ?>