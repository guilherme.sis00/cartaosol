<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

include("controllers/Exames.php");
include("controllers/Parceiros.php");
include("controllers/Clinicas.php");

if (isset($_POST["idEspecialidade"])) {
    getParceirosByEspecialidade($_POST["idEspecialidade"]);
}

if (isset($_GET["idEspecialidade"])) {
    getParceirosByEspecialidade($_GET["idEspecialidade"]);
}

function getEspecialidades() {
    $exames = new Exames();
    $response = $exames->getExames();

    if ($response["resultado"]) {
        return $response["exames"];
    } else {
        return FALSE;
    }
}

function getClinicas() {
    $clinicas = new Clinicas();
    $response = $clinicas->getClinicas();

    if ($response['resultado']) {
        return $response["clinicas"];
    } else {
        return FALSE;
    }
}

function getParceiros() {
    $parceiros = new Parceiros();
    $response = $parceiros->getAllParceiros();

    if ($response['resultado']) {
        return $response["parceiros"];
    } else {
        return FALSE;
    }
}

function getParceirosByEspecialidade($especialidade) {
    $parceiros = new Parceiros();
    $response = $parceiros->getByEspecialidade($especialidade);
    $p = ["resultado" => FALSE, "parceiros" => []];
    if ($response["resultado"]) {
        $p["resultado"] = TRUE;
        while ($parceiro = mysqli_fetch_array($response["parceiros"])) {
            array_push($p["parceiros"], [
                "nome" => utf8_encode($parceiro["nome"]),
                "cidade" => utf8_encode($parceiro["cidade"]),
                "uf" => utf8_encode($parceiro["uf"]),
                "telefone" => utf8_encode($parceiro["telefone"]),
                "endereco" => utf8_encode($parceiro["endereco"])
            ]);
        }
//        utf8_decode(var_dump($p));
        echo json_encode($p);
    } else {
        echo json_encode($response);
    }
}
