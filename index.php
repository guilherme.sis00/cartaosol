<?php include('header.php') ?>

<header>
    <div class="container" style="padding-top: 80px;">
        <div class="row">
          <!--LOGO-->
            <div class="col-xs-12 col-md-3 hidden-xs">
                <img class="center-block img-responsive" src="img/logo.png" alt="" class="logo_home">
            </div>

             <!--WHATSAPP-->
            <div class="text-center col-xs-12 col-md-6">
                 <div class="whatsapp">


                        <h5 style="font-family: 'Roboto Condensed', sans-serif;line-height: 7px; font-size: 13px;"><b>Peça já pelo nosso Whatsapp</b></h5>
                        <i class="fa fa-whatsapp" style="color: #66c251" aria-hidden="true"></i>
                        ¹¹ 9 5454 6840

                  </div>
            </div>

             <!--BTN PECA SEU CARTAO-->
            <div class="text-center col-xs-12 col-md-2 pull-right">
                    <a href=""  style="text-decoration: none;"><div class="btnPadrao"><i class="fa fa-credit-card" aria-hidden="true"></i> SOLICITE JÁ O SEU</div></a>
            </div>
            <!--TXT COMO FUNCIONA-->
           <!-- <div class="col-xs-12 col-md-2 hidden-xs">
                <div class="media funciona">
              <div class="media-left">
                  <i class="fa fa-shield fa-2x icon_menu" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                <h4 class="media-heading">COMO FUNCIONA</h4>
                <p class="font_chamadas" style="line-height: 0px;"><b>PROJETO E INVESTIDORES</b></p>
              </div>
            </div>
            </div> -->
            <!--TXT LOCALIZE-->
             <!--<div class="col-xs-12 col-md-3 hidden-xs">
                <div class="media funciona">
              <div class="media-left">
                  <i class="fa fa-map-marker fa-2x icon_menu" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                <h4 class="media-heading">LOCALIZE</h4>
                <p class="font_chamadas" style="line-height: 0px;"><b>CLÍNICAS E PARCEIROS</b></p>
              </div>
            </div>
            </div>-->


        </div>
    </div>

    <div class="banner_home hidden-xs">

      <div class="jumbotron" style="background-image: url(img/banner-home.png); background-size: cover; height: 300px; background-repeat: no-repeat; background-position: center;margin-bottom: 0px;">
  <div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-5">

        <h2>Agora ficou fácil comprar</h2>
        <p>Quem tem nosso cartão tem acesso a compras exclusivas em diversas regiões, além de descontos especiais.</p>

       <div style="display: inline-flex;">
          <div class="media" style="padding-top: 13px;">
              <div class="media-left">
                  <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
              </div>
              <div class="media-body" style="padding-top: 27px;">
                <h4 class="media-heading"> COMPRA</h4>
              </div>
          </div>

          <div class="media">
              <div class="media-left">
                  <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
              </div>
              <div class="media-body" style="padding-top: 28px;">
                <h4 class="media-heading"> ECONOMIA</h4>
              </div>
          </div>

        </div>
  </div>
</div>
    </div>
  </div>
</div>

   <div class="banner_home hidden-md hidden-lg hidden-sm visible-xs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-5">

        <h2>Agora ficou fácil comprar</h2>
        <p>Quem tem nosso cartão tem acesso a compras exclusivas em diversas regiões, além de descontos especiais.</p>

       <div style="display: inline-flex;">
          <div class="media" style="padding-top: 13px;">
              <div class="media-left">
                  <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
              </div>
              <div class="media-body" style="padding-top: 27px;">
                <h4 class="media-heading"> COMPRA</h4>
              </div>
          </div>

          <div class="media">
              <div class="media-left">
                  <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
              </div>
              <div class="media-body" style="padding-top: 28px;">
                <h4 class="media-heading"> ECONOMIA</h4>
              </div>
          </div>

        </div>
  </div>
</div>
    </div>

   </div>

   <div class="container">
     <div class="row selecione">
       <div class="col-md-3"  style="padding-left: 0px;">
         <img class="img-responsive center-block" src="img/doctor1.png" alt="">
       </div>
       <div class="col-xs-12 col-md-3">
          <div class="media exames">
              <div class="media-left">
                  <i class="fa fa-search fa-2x" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                <h4 class="media-heading">EXAMES LABORATORIAIS <br>  E COMPLEMENTARES</h4>
                <p class="font_chamadas" style="font-size: 16px!important; line-height:16px!important;">Encontre um parceiro para realizar <br> seus exames laboratoriais</p>
              </div>
          </div>

           <div class="text-center ">
               <a href="exames"  style="text-decoration: none;"><div class="btnPadrao"><i class="fa fa-flask" aria-hidden="true"></i> VER EXAMES</div></a>
           </div>
            <!--<div class="chosen" >
            <select class = "escolhido">
            <option> POR TIPO </option>
            </select>
          </div>
          <div class="chosen" >
            <select class = "escolhido">
            <option> POR CIDADE </option>
            </select>
          </div>-->
          <div class="barra hidden-xs"><p></p></div>
       </div>
       <div class="col-md-3">
         <img class="img-responsive center-block" src="img/doctor.png" alt="">
       </div>
       <div class="col-xs-12 col-md-3">
          <div class="media exames">
              <div class="media-left">
                  <i class="fa fa-stethoscope fa-2x" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                <h4 class="media-heading">HOSPITAIS <br> E CLÍNICAS</h4>
                <p class="font_chamadas" style="font-size: 16px!important; line-height:16px!important;">Localize filtrando por especialidade ou pela cidade mais proxima</p>
              </div>
            </div>

           <div class="text-center ">
               <a href="clinicas"  style="text-decoration: none;"><div class="btnPadrao"><i class="fa fa-plus-square" aria-hidden="true"></i> VER PARCEIROS</div></a>
           </div>

           <!-- <div class="chosen" >
            <select class = "escolhido">
            <option> POR ESPECIALIDADE </option>
            </select>
          </div>
          <div class="chosen" >
            <select class = "escolhido">
            <option> POR CIDADE </option>
            </select>
          </div>-->
          <div class="barra1 hidden-xs"></div>
       </div>

     </div>
   </div>

   <div class="container adquira">
    <div class="row">
      <div class="col-md-4">
        <div class="media">
              <div class="media-left">
                  <i class="fa fa-credit-card fa-2x" aria-hidden="true" style="margin-top: 30px;border: 1px solid rgb(0,138,168);border-radius: 100px;padding: 10px 9px 9px 9px"></i>
              </div>
              <div class="media-body">
                <h3 class="media-heading" style="padding-top: 30px;"><b>Adquira o seu cartão</b></h3>
                <h5 class="font_chamadas">Faça parte desta rede de beneficiados que cresce todo dia, e comece a economizar diariamente em suas compras.</h5>
              </div>
            </div>
      </div>
      <div class="col-md-5 col-xs-12 adquira_form">

        <form name="form1" class="form-inline" id="formulario-contato" action="mail_adquira.php" method="post" enctype="multipart/form-data">

    <input type="nome" class="form-control" id="nome" name="nome" placeholder="SEU NOME" style="width: 100% !important;">

  <div class="form-group">
    <!-- <input type="celular" class="form-control" id="celular" name="celular" placeholder="CELULAR"> -->
   <input required placeholder=" CELULAR" type="text" name="tel" id="celular"  maxlength="16" onKeyPress="MascaraTelefone(form1.tel);" onBlur="ValidaTelefone(form1.tel);" style="padding: 5px;">

    <input type="email" class="form-control" id="email" name="email" placeholder="E-MAIL">
  </div>

  <button type="submit" class="btn btn-primary pull-right" style="margin-top: 1px;"><i class="fa fa-plus" aria-hidden="true"></i></button>

</form>
      </div>
      <div class="col-md-3">
        <img class="img-responsive center-block" src="img/cartao.png" alt="" style="margin-top: -20px;">
      </div>
    </div>
  </div>


 <div class="container">
   <div class="row">
    <!-- <h3 class="text-primary text-center">COMENTÁRIOS</h3> -->

  <!--  <div class="col-md-12">
      <span class="label label_testemunho"><i class="fa fa-commenting fa-2x" aria-hidden="true"></i> TESTEMUNHOS</span>
   </div> -->

        <div class="col-xs-12 col-md-4 comentarios" >
          <div class="hoverzoom">
      <img class="img-responsive img-fluid" src="img/testemunho_rocha.jpeg" alt="" title="Testemunho">
      <div class="retina">
        <p>Esse cartão de desconto é fantastico. Já conseguimos sentir uma grande economia em nosso orcamento familiar. <b>Família Rocha</b></p>
       <!-- <p style="font-size: 12px;"><b>Família Rocha</b></p>--><!--
        <p style="line-height: 0px;font-size: 12px;"><b>Itaquacetuba</b></p>-->
      </div>
    </div>
    <!--<div class="hoverzoom">
      <img class="img-responsive center-block" src="img/comentario4.png" alt="" title="Testemunho" style="padding-right: 0px;">
      <div class="retina">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, temporibus, repellat? Placeat nihil itaque culpa eu.</p>
         <p style="font-size: 12px;"><b>Marcos Souza,</b></p>
        <p style="line-height: 0px;font-size: 12px;"><b>Itaquacetuba</b></p>
      </div>
    </div>-->

        </div>

        <div class="col-xs-12 col-md-4 comentarios">
<div class="hoverzoom">
  <img class="img-responsive img-fluid" src="img/testemunho_dora.jpeg" alt="" title="Testemunho" style="padding-right: 0px;">
  <div class="retina">
      <p>Agora eu posso fazer os exames que precisamos com muitos descontos. <b>Dona Vera e Kevin</b></p>
<!--     <p style="font-size: 12px;"><b>Marcos Souza,</b></p>
        <p style="line-height: 0px;font-size: 12px;"><b>Itaquacetuba</b></p>-->
  </div>

</div>



     <!-- <div class="hoverzoom">
  <img class="img-responsive center-block" src="img/comentario1.png" alt="" title="Testemunho" style="padding-right: 0px;">
  <div class="retina">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, temporibus, repellat? Placeat nihil itaque culpa eu.</p>
     <p style="font-size: 12px;"><b>Marcos Souza,</b></p>
        <p style="line-height: 0px;font-size: 12px;"><b>Itaquacetuba</b></p>
  </div>
</div>-->
    </div>


     <div class="col-xs-12 col-md-4 facebook">
      <div class="fb-page" data-href="https://www.facebook.com/ocartaoparatodos/" data-tabs="timeline" data-width="500" data-height="378" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
        <blockquote cite="https://www.facebook.com/ocartaoparatodos/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ocartaoparatodos/">O Cartão para todos</a>
        </blockquote>
      </div>
     </div>
   </div>
 </div>

</header>


<?php include('footer.php') ?>

<?php include('bottom.php') ?>
