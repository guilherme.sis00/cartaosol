<?php

require_once($_SERVER["DOCUMENT_ROOT"] . '/models/Clinica.php');

class Clinicas {

    private $modelClinica;

    public function __construct() {
        $this->modelClinica= new Clinica();
    }

    public function getClinicas() {
        return $this->modelClinica->getAll();
    }

}
