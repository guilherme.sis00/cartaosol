<?php

require_once($_SERVER["DOCUMENT_ROOT"] . '/models/Exame.php');

class Exames {

    private $modelExame;

    public function __construct() {
        $this->modelExame = new Exame();
    }

    public function getExames() {
        return $this->modelExame->getExames();
    }

}
