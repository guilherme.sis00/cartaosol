<?php

require_once($_SERVER["DOCUMENT_ROOT"] . '/models/Parceiro.php');

class Parceiros {

    private $modelParceiro;

    public function __construct() {
        $this->modelParceiro = new Parceiro();
    }

    public function getAllParceiros() {
        return $this->modelParceiro->getAll();
    }
    
    public function getParceiros() {
        return $this->modelParceiro->getByTipo();
    }

    public function getByEspecialidade($especialidade) {
        return $this->modelParceiro->getByEspecialidade($especialidade);
    }
}
