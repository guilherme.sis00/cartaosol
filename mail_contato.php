<?php
require('class.phpmailer.php');

// Inicia a classe PHPMailer
$mail = new PHPMailer();

// Define os dados do servidor e tipo de conexão
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsSMTP(); // Define que a mensagem será SMTP
$mail->Host = "localhost"; // Endereço do servidor SMTP
$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
$mail->Username = 'falecom@cartaosol.com.br'; // Usuário do servidor SMTP
$mail->Password = '@nova@00'; // Senha do servidor SMTP

// Define o remetente
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->From = "falecom@cartaosol.com.br"; // Seu e-mail
$mail->FromName = "Formulário de Contato"; // Seu nome

// Define os destinatário(s)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->AddAddress('falecom@cartaosol.com.br', 'Cartão Sol');
//$mail->AddAddress('ciclano@site.net');
//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta

// Define os dados técnicos da Mensagem
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
//$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)

// Define a mensagem (Texto e Assunto)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//$mail->Subject  = "Mensagem Teste"; // Assunto da mensagem
//$mail->Body = "Este é o corpo da mensagem de teste, em <b>HTML</b>! <br /> :)";
//$mail->AltBody = "Este é o corpo da mensagem de teste, em Texto Plano! \r\n :)";


$nome = $_POST['nome'];
$email = $_POST['email'];
$mensagem = $_POST['mensagem'];

$mail->Subject = 'Contato do site Cartão Sol';
$mail->Body .= '<b>Nome:</b>&nbsp;'.$nome.'<BR>
<b>Email:</b>&nbsp;'.$email.'<BR><br>
<b>Mensagem:</b><br>'.nl2br($mensagem);


// Define os anexos (opcional)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//$mail->AddAttachment("c:/temp/documento.pdf", "novo_nome.pdf");  // Insere um anexo

// Envia o e-mail
$enviado = $mail->Send();

// Limpa os destinatários e os anexos
$mail->ClearAllRecipients();
$mail->ClearAttachments();

// Exibe uma mensagem de resultado
if ($enviado) {
echo "<script>alert('E-mail enviado com sucesso!')</script>";
echo "<meta http-equiv=\"refresh\" content=\"0;url='http://cartaosol.com.br/'\">";

} else {
echo "Não foi possível enviar o e-mail.<br /><br />";
echo "<b>Informações do erro:</b> <br />" . $mail->ErrorInfo;
}
    

?>