$().ready(function () {
    $("#cpf").mask("000.000.000-00");
});

$("#formBuscar").validate({
    rules: {
        cliente: {
            required: true,
            minlength: 3
        },
        codigoContrato: {digits: true},
        cpf: {minlength: 14}
    },
    messages: {
        cliente: {
            required: "Campo obrigatório",
            minlength: "Digite pelo menos 3 caracteres"
        },
        codigoContrato: {digits: "Código inválido"},
        cpf: {minlength: "Digite um CPF válido"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formBuscar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "administrativo/contratos/buscar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formBuscarBtnBuscar")
                        .html("Buscando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#tabelaContratos").empty();
                $("#divPaginacao").empty();

                $.each(r.contratos, function (key, contrato) {
                    tabelaContratosAdicionar(contrato);
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarBtnBuscar")
                    .html("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#tabelaContratos").on("click", ".btn-deletar", function () {
    let id = $(this).data("id");
    let toast = $.toast({
        heading: `<strong style="color:white">Processando</strong>`,
        icon: "info",
        text: "Deletando contrato",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "administrativo/contratos/deletar",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: `<strong style="color:white">Tudo certo</strong>`,
                        icon: "success",
                        text: "Contrato deletado",
                        position: "top-right",
                        hideAfter: 4000,
                        beforeShow: function () {
                            $(`#${id}`).remove();
                        }
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON"
            )
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

function tabelaContratosAdicionar(contrato) {
    $("#tabelaContratos").append(`<tr id="${contrato.id}">
                                    <td id="contrato-${contrato.id}" class="text-center">${contrato.codigoContrato}</td>
                                    <td id="nome-${contrato.id}">${contrato.nome}</td>
                                    <td id="duracao-${contrato.id}">${contrato.duracao} meses</td>
                                    <td id="ano-${contrato.id}">${contrato.ano}</td>
                                    <td id="situacao-${contrato.id}">${contrato.situacao.charAt(0).toUpperCase() + contrato.situacao.slice(1)}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Ações <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a class="${contrato.situacao == "finalizado" ? "disabled" : ""}" href="${$("head").data("info") + `administrativo/contrato/gerar-parcelas/${contrato.id}`}">Gerar parcelas</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="${$("head").data("info") + `administrativo/contrato/${contrato.id}`}">Atualizar</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#" class="btn-deletar" data-id="${contrato.id}">Deletar</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>`);
}