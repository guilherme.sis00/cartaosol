$().ready(function () {
    $("#cpf").mask("000.000.000-00");
});

$("#formBuscar").validate({
    rules: {
        nome: {
            required: true,
            minlength: 3
        },
        cpf: {minlength: 14}
    },
    messages: {
        nome: {
            required: "Campo obrigatório",
            minlength: "Digite pelo menos 3 caracteres"
        },
        cpf: {minlength: "Digite um CPF válido"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formBuscar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "administrativo/dependentes/buscar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formBuscarBtnBuscar")
                        .html("Buscando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#tabelaDependentes").empty();
                $("#divPaginacao").empty();

                $.each(r.dependentes, function (key, dependente) {
                    tabelaDependentesAdicionar(dependente);
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarBtnBuscar")
                    .html("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#modalVisualizarDependente").on("show.bs.modal", function (e) {
    let id = $(e.relatedTarget).data("id");

    $.post($("head").data("info") + "administrativo/dependentes/getDependente",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $("#modalVisualizarDependenteNome").val(r.dependente.nome);
                    $("#modalVisualizarDependenteTitular").val(r.dependente.titular);
                    $("#modalVisualizarDependenteCpf").val(r.dependente.cpf);
                    $("#modalVisualizarDependenteNumeroCartao").val(r.dependente.numero);
                    $("#modalVisualizarDependenteDataNascimento").val(formatarData(r.dependente.dataNascimento));
                    $("#modalVisualizarDependenteDataVinculacao").val(formatarData(r.dependente.dataVinculacao));
                    $("#modalVisualizarDependenteBtnAtualizar").attr("href", $("head").data("info") + "administrativo/dependente/" + r.dependente.id);
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Falha",
                            icon: "error",
                            text: "Por favor entre em contato com a equipe de suporte"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            });
});

function tabelaDependentesAdicionar(dependente) {
    $("#tabelaDependentes").append(`<tr id="${dependente.id}">
                                    <td id="nome-${dependente.id}">${dependente.nome}</td>
                                    <td id="titular-${dependente.id}">${dependente.titular}</td>
                                    <td id="data-vinculacao-${dependente.id}">${formatarData(dependente.dataVinculacao)}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Ações <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#" data-id="${dependente.id}" data-toggle="modal" data-target="#modalVisualizarDependente">Visualizar</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="${$("head").data("info") + "administrativo/dependente/" + dependente.id}">Atualizar</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>`);
}