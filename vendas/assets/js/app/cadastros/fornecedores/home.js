$("#formBuscar").validate({
    rules: {
        termo: {
            required: true,
            minlength: 3
        }
    },
    messages: {
        termo: {
            required: "Campo obrigatório",
            minlength: "Digite pelo menos 3 caracteres"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formBuscar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "cadastros/fornecedores/buscar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formBuscarBtnBuscar")
                        .html("Buscando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#tabelaFornecedores").empty();
                $("#divPaginacao").empty();
                $.each(r.fornecedores, function (key, fornecedor) {
                    tabelaFornecedoresAdicionar(fornecedor);
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarBtnBuscar")
                    .html("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAdicionarFornecedor").validate({
    rules: {
        modalAdicionarFornecedorNome: {required: true}
    },
    messages: {
        modalAdicionarFornecedorNome: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData();
        dados.append("ajax", true);
        dados.append("nome", $("#modalAdicionarFornecedorNome").val());

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "cadastros/fornecedores/adicionar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAdicionarFornecedorBtnAdicionar")
                        .html("Adicionando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Tudo certo</strong>`,
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    hideAfter: 4000,
                    beforeShow: function () {
                        tabelaFornecedoresAdicionar(r.fornecedor);
                        $("#formAdicionarFornecedor").trigger("reset");
                        $("#modalAdicionarFornecedor").modal("hide");
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarFornecedorBtnAdicionar")
                    .html("Adicionar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAtualizarFornecedor").validate({
    rules: {
        modalAtualizarFornecedorId: {required: true},
        modalAtualizarFornecedorNome: {required: true}
    },
    messages: {
        modalAtualizarFornecedorId: {required: "Campo obrigatório"},
        modalAtualizarFornecedorNome: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData();
        dados.append("ajax", true);
        dados.append("id", $("#modalAtualizarFornecedorId").val());
        dados.append("nome", $("#modalAtualizarFornecedorNome").val());

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "cadastros/fornecedores/atualizar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAtualizarFornecedorBtnAdicionar")
                        .html("Atualizando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Tudo certo</strong>`,
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    hideAfter: 4000,
                    beforeShow: function () {
                        $(`#fornecedor-${dados.get("id")}`).html(dados.get("nome"));
                        $("#formAtualizarFornecedor").trigger("reset");
                        $("#modalAtualizarFornecedor").modal("hide");
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAtualizarFornecedorBtnAdicionar")
                    .html("Atualizar")
                    .removeAttr("disabled");
        });
    }
});

$("#modalAtualizarFornecedor").on("show.bs.modal", function (e) {
    let id = $(e.relatedTarget).data("id");

    $("#modalAtualizarFornecedorId").val(id);
    $("#modalAtualizarFornecedorNome").val($(`#fornecedor-${id}`).text());
});

function tabelaFornecedoresAdicionar(fornecedor) {
    $("#tabelaFornecedores").append(`<tr id="${fornecedor.id}">
                                        <td id="fornecedor-${fornecedor.id}">${fornecedor.nome}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Ações <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#" data-id="${fornecedor.id}" data-toggle="modal" data-target="#modalAtualizarFornecedor">Atualizar</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-deletar" data-id="${fornecedor.id}">Deletar</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>`);
}