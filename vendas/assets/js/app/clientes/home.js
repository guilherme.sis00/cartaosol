$("#formBuscarCliente").validate({
    rules: {
        termo: {
            required: true,
            minlength: 3
        },
        tipoBusca: {required: true}
    },
    messages: {
        termo: {
            required: "Campo obrigatório",
            minlength: "Digite pelo menos 3 caracteres"
        },
        tipoBusca: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formBuscarCliente")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "clientes/buscar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formBuscarClienteBtnBuscar")
                        .empty()
                        .append("Buscando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#tabelaClientes").empty();
                $("#divPaginacao").empty();
                $.each(r.clientes, function (key, cliente) {
                    $("#tabelaClientes").append(`<tr id="${cliente.id}">
                                                    <td>${cliente.nome}</td>
                                                    <td>${cliente.cpf}</td>
                                                    <td>${cliente.celular}</td>
                                                    <td>${cliente.email == null ? "" : cliente.email}</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Ações <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" data-id="${cliente.id}" data-toggle="modal" data-target="#modalVisualizarCliente">Visualizar</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="${$("head").data("info")}/cliente/${cliente.id}")">Atualizar</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="#" class="btn-deletar" data-id="${cliente.id}">Deletar</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>`);
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarClienteBtnBuscar")
                    .empty()
                    .append("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAdicionarCidade").validate({
    rules: {
        modalAdicionarCidadeEstado: {required: true},
        modalAdicionarCidadeCidade: {required: true}
    },
    messages: {
        modalAdicionarCidadeEstado: {required: "Campo obrigatório"},
        modalAdicionarCidadeCidade: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAdicionarCidade")[0]);
        dados.append("ajax", true);
        dados.append("estado", $("#modalAdicionarCidadeEstado").val());
        dados.append("cidade", $("#modalAdicionarCidadeCidade").val());
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "support/adicionarCidade",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAdicionarCidadeBtnAdicionar")
                        .empty()
                        .append("Adicionando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    $("#modalAdicionarCidade").modal("hide");
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarCidadeBtnAdicionar")
                    .empty()
                    .append("Adicionar")
                    .removeAttr("disabled");
        });
    }
});

$("#modalVisualizarCliente").on("show.bs.modal", function (e) {
    let id = $(e.relatedTarget).data("id");

    $.post($("head").data("info") + "clientes/getById",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $("#modalVisualizarClienteNome").val(r.cliente.nome);
                    $("#modalVisualizarClienteCpf").val(r.cliente.cpf);
                    $("#modalVisualizarClienteCelular").val(r.cliente.celular);
                    $("#modalVisualizarClienteWhatsapp").val(r.cliente.whastsapp);
                    $("#modalVisualizarClienteDataNascimento").val(formatarData(r.cliente.dataNascimento));
                    $("#modalVisualizarClienteNumeroCartao").val(r.cliente.numero);
                    $("#modalVisualizarClienteEmail").val(r.cliente.email);
                    $("#modalVisualizarClienteEstado").val(r.cliente.estado);
                    $("#modalVisualizarClienteCidade").val(r.cliente.cidade);
                    $("#modalVisualizarClienteObservacoes").val(r.cliente.observacoes);
                    $("#modalVisualizarClienteBtnAtualizar").attr("href", $("head").data("info") + "cliente/" + r.cliente.id);
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON"
            )
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de suporte"
                });
            });
});

$("#tabelaClientes").on("click", ".btn-deletar", function () {
    let id = $(this).data("id");
    let toast = $.toast({
        heading: `<strong style="color:white">Processando</strong>`,
        icon: "info",
        text: "Deletando cliente",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "clientes/deletar",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: `<strong style="color:white">Tudo certo</strong>`,
                        icon: "success",
                        text: r.msg,
                        position: "top-right",
                        beforeShow: function () {
                            $(`#${id}`).remove();
                        }
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});