$("#formBuscarParceiro").validate({
    rules: {
        termo: {
            required: true,
            minlength: 3
        },
        tipoBusca: {required: true}
    },
    messages: {
        termo: {
            required: "Campo obrigatório",
            minlength: "Digite pelo menos 3 caracteres"
        },
        tipoBusca: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formBuscarParceiro")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "parceiros/buscar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formBuscarParceiroBtnBuscar")
                        .empty()
                        .append("Buscando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#tabelaParceiros").empty();
                $("#divPaginacao").empty();
                $.each(r.parceiros, function (key, parceiro) {
                    $("#tabelaParceiros").append(`<tr>
                                                    <td>${parceiro.nome}</td>
                                                    <td>${parceiro.endereco}</td>
                                                    <td>${parceiro.estado}</td>
                                                    <td>${parceiro.telefone}</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Ações <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" data-id="${parceiro.id}" data-toggle="modal" data-target="#modalVisualizarParceiro">Visualizar</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="<?=${$("head").data("info") + "/parceiro/" + parceiro.id}">Atualizar</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="#" data-id="${parceiro.id}" data-parceiro="${parceiro.nome}" data-logo="${parceiro.logo}" data-toggle="modal" data-target="#modalAlterarLogo">Alterar logo</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="${$("head").data("info") + "parceiro/deletar/" + parceiro.id}">Deletar</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>`);
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarParceiroBtnBuscar")
                    .empty()
                    .append("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAlterarLogo").validate({
    rules: {
        logo: {
            required: true,
            extension: "png|jpeg|jpg"
        }
    },
    messages: {
        logo: {
            required: "Campo obrigatório",
            extension: "Somente arquivos com extensão: PNG|JPEG|JPG"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAlterarLogo")[0]);
        dados.append("ajax", true);
        dados.append("idParceiro", $("#modalAlterarLogoIdParceiro").val());
        dados.append("logoAtual", $("#modalAlterarLogoLogoAtual").val());
        dados.append("logo", $("#modalAlterarLogoLogo").val());

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "parceiros/alterarLogo",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAlterarLogoBtnAlterar")
                        .empty()
                        .append("Alterando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    location.reload();
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAlterarLogoBtnAlterar")
                    .empty()
                    .append("Alterar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAdicionarCidade").validate({
    rules: {
        modalAdicionarCidadeEstado: {required: true},
        modalAdicionarCidadeCidade: {required: true}
    },
    messages: {
        modalAdicionarCidadeEstado: {required: "Campo obrigatório"},
        modalAdicionarCidadeCidade: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAdicionarCidade")[0]);
        dados.append("ajax", true);
        dados.append("estado", $("#modalAdicionarCidadeEstado").val());
        dados.append("cidade", $("#modalAdicionarCidadeCidade").val());
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "support/adicionarCidade",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAdicionarCidadeBtnAdicionar")
                        .empty()
                        .append("Adicionando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    $("#modalAdicionarCidade").modal("hide");
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarCidadeBtnAdicionar")
                    .empty()
                    .append("Adicionar")
                    .removeAttr("disabled");
        });
    }
});

$("#modalVisualizarParceiro").on("show.bs.modal", function (e) {
    $("#divEspecialidades").addClass("hidden");
    let id = $(e.relatedTarget).data("id");

    $.post($("head").data("info") + "parceiros/getById",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $("#modalVisualizarParceiroNome").val(r.parceiro.nome);
                    $("#modalVisualizarParceiroCidade").val(r.parceiro.cidade);
                    $("#modalVisualizarParceiroEstado").val(r.parceiro.estado);
                    $("#modalVisualizarParceiroEndereco").val(r.parceiro.endereco);
                    $("#modalVisualizarParceiroParceiro").val(r.parceiro.parceiro == 1 ? "Sim" : "Não");
                    $("#modalVisualizarParceiroClinica").val(r.parceiro.clinica == 1 ? "Sim" : "Não");
                    $("#modalVisualizarParceiroExame").val(r.parceiro.exame == 1 ? "Sim" : "Não");
                    $("#modalVisualizarParceiroLogo").attr("href", $("head").data("info") + "assets/img/logos/" + r.parceiro.logo);
                    $("#modalVisualizarParceiroTelefone").val(r.parceiro.telefone);
                    $("#modalVisualizarParceiroWhatsapp").val(r.parceiro.whatsapp);
                    $("#modalVisualizarParceiroEmail").val(r.parceiro.email);
                    $("#modalVisualizarParceiroDescricao").val(r.parceiro.descricao);
                    $("#modalVisualizarParceiroBtnAtualizar").attr("href", $("head").data("info") + "parceiro/" + r.parceiro.id);
                    r.parceiro.exame ? preencherEspecialidades(r.especialidades) : null;
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON"
            )
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de suporte"
                });
            });
});

$("#modalAlterarLogo").on("show.bs.modal", function (e) {
    $("#modalAlterarLogoIdParceiro").val($(e.relatedTarget).data("id"));
    $("#modalAlterarLogoLogoAtual").val($(e.relatedTarget).data("logo"));
    $("#modalAlterarLogoParceiro").val($(e.relatedTarget).data("parceiro"));
});

function preencherEspecialidades(especialidades) {
    let temp = "";
    $.each(especialidades, function (key, especialidade) {
        temp += `${especialidade.especialidade}, `;
    });
    $("#divEspecialidades").removeClass("hidden");
    $("#modalVisualizarParceiroEspecialidades").text(temp.substr(0, (temp.length - 2)));

}