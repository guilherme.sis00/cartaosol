$().ready(function () {
    $("#valorCombo").mask("#.##0,00", {reverse: true});
    $("#valor").mask("#.##0,00", {reverse: true});
});

$("#formAdicionarCombo").validate({
    rules: {
        nome: {required: true},
        valorCombo: {required: true}
    },
    messages: {
        nome: {required: "Campo obrigatório"},
        valorCombo: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let valorCombo = $("#valorCombo").val().replace(".", "");
        valorCombo = parseFloat(valorCombo.replace(",", "."));

        let dados = new FormData($("#formAdicionarCombo")[0]);
        dados.append("ajax", true);
        dados.set("valorCombo", valorCombo);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "vendas/combos/adicionar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formAdicionarComboBtnAdicionar")
                        .html("Adicionando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Tudo certo</strong>`,
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    beforeShow: function () {
                        $("#formAdicionarCombo").trigger("reset");
                        $("#tabelaItens").empty();
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarComboBtnAdicionar")
                    .html("Adicionar")
                    .removeAttr("disabled");
        });
    }
});

$("#btnAdicionarItem").on("click", function () {
    if ($("#procedimento").val() != "" && $("#quantidade").val() != "" && $("#valor").val() != "") {
        let valor = $("#valor").val().replace(".", "");
        valor = parseFloat(valor.replace(",", "."));

        let item = {
            id: $("#procedimento").val(),
            procedimento: $("#procedimento :selected").text(),
            quantidade: $("#quantidade").val(),
            valor: $("#valor").val(),
            valorTratado: valor
        };
        $("#tabelaItens").append(`<tr id="${item.id}">
                                    <input type="hidden" name="itens[${item.id}][procedimento]" id="item" value="${item.id}">
                                    <input type="hidden" name="itens[${item.id}][quantidade]" id="item" value="${item.quantidade}">
                                    <input type="hidden" name="itens[${item.id}][valor]" id="item" value="${item.valorTratado}">
                                    <td>${item.id}</td>
                                    <td>${item.procedimento}</td>
                                    <td>${item.quantidade}</td>
                                    <td>R$ ${item.valor}</td>
                                    <td><button type="button" data-id="${item.id}" class="btn-excluir btn btn-small btn-fill btn-danger"><i class='ti-close'></i></button></td>
                                </tr>`);
        $("#procedimento :selected").removeAttr("selected");
        $("#quantidade").val("0");
        $("#valor").val("0,00");
    }
});

$("#tabelaItens").on("click", ".btn-excluir", function () {
    $(`#${$(this).data("id")}`).remove();
});