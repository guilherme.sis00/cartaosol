<?php

class Configuracao {

    public function getConfigPagination($base_url = NULL, $total_rows = NULL,$uriSegment = 2) {
        return array(
            'attributes' => array('class' => 'page-link'),
            'base_url' => $base_url,
            'per_page' => 10,
            'num_links' => 3,
            'use_page_numbers' => TRUE,
            'uri_segment' => $uriSegment,
            'total_rows' => $total_rows,
            'full_tag_open' => "<ul class='pagination justify-content-end'>",
            'full_tag_close' => "</ul>",
            'first_link' => "<i class='fa fa-angle-double-left'></i>",
            'last_link' => "<i class='fa fa-angle-double-right'></i>",
            'first_tag_open' => "<li class='page-item' title='Primeira página'>",
            'first_tag_close' => "</li>",
            'last_tag_open' => "<li class='page-item' title='Última página'>",
            'last_tag_close' => "</li>",
            'prev_link' => "<i class='fa fa-angle-left'></i>",
            'prev_tag_open' => "<li class='page-item'>",
            'prev_tag_close' => "</li>",
            'next_link' => "<i class='fa fa-angle-right'></i>",
            'next_tag_open' => "<li class='page-item'>",
            'next_tag_close' => "</li>",
            'cur_tag_open' => "<li class='page-item active'><a class='page-link' href='#'>",
            'cur_tag_close' => "</a></li>",
            'num_tag_open' => "<li class='page-item'>",
            'num_tag_close' => "</li>"
        );
    }

}
