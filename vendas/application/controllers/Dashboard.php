<?php

class Dashboard extends CI_Controller {

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt-vendas")) {
            $this->load->view("dashboard/home");
        } else {
            redirect("login");
        }
    }

}
