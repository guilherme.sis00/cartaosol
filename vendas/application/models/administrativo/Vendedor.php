<?php

class Vendedor extends CI_Model {

    private $id;
    private $nome;
    private $cpf;
    private $celular;
    private $idUsuario;

    public function __construct() {
        parent::__construct();
        $this->load->model(["cadastros/fornecedor", "usuario"]);
    }

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->nome = !empty($dados['nome']) ? $dados['nome'] : NULL;
        $this->cpf = !empty($dados['cpf']) ? $dados['cpf'] : NULL;
        $this->celular = !empty($dados['celular']) ? $dados['celular'] : NULL;
        $this->idUsuario = !empty($dados['idUsuario']) ? $dados['idUsuario'] : NULL;
        $this->usuario->preencherDados(array_merge($dados,["tipo"=>"vendedor"]));
    }

    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE) {
        $this->db
                ->select("vendedores.*,"
                        . "usuarios.status")
                ->join("usuarios", "usuarios.id=vendedores.idUsuario")
                ->order_by("vendedores.nome", "ASC");

        if ($paginacao) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->get("vendedores")->result();
    }

    public function getById($id, $returnArray = TRUE) {
        $response = $this->db
                        ->select("vendedores.*,"
                                . "usuarios.status")
                        ->join("usuarios", "usuarios.id=vendedores.idUsuario")
                        ->order_by("vendedores.nome", "ASC")
                        ->where("vendedores.id", $id)
                        ->get("vendedores")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "vendedor" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum vendedor foi encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function buscar($dados) {
        $response = $this->db
                        ->select("vendedores.*,"
                                . "usuarios.status")
                        ->join("usuarios", "usuarios.id=vendedores.idUsuario")
                        ->order_by("vendedores.nome", "ASC")
                        ->like("vendedores.nome", $dados["termo"])
                        ->get("vendedores")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "vendedores" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum vendedor encontrado"];
        }
    }

    public function adicionar() {
        $response = $this->usuario->adicionar();

        if ($response["resultado"]) {
            $this->db->trans_start();
            $this->idUsuario = $this->usuario->get("id");
            $this->db->insert("vendedores", $this->toArray());
            $this->id = $this->db->insert_id();
            $this->db->trans_complete();

            if ($this->db->trans_status()) {
                $this->fornecedor->preencherDados(["nome" => $this->nome, "idVendedor" => $this->id]);
                $responseFornecedor = $this->fornecedor->adicionar();
                if ($responseFornecedor["resultado"]) {
                    $this->db->trans_start();
                    $this->db->insert("vendedores_fornecedores", ["idVendedor" => $this->id, "idFornecedor" => $responseFornecedor["fornecedor"]->id]);
                    $this->db->trans_complete();
                    return ["resultado" => TRUE, "msg" => "Vendedor adicionado"];
                } else {
                    $this->vendedor->deletar($this->deletar($this->id));
                    $this->usuário->deletar($this->usuario->get("id"));
                    return ["resultado" => FALSE, "msg" => "Falha ao adicionar vendedor no cadastro de fornecedores"];
                }
            } else {
                $this->usuário->deletar($this->usuario->get("id"));
                return ["resultado" => FALSE, "msg" => "Falha ao adicionar vendedor"];
            }
        } else {
            return $response;
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->set([
                    "nome" => $this->nome,
                    "cpf" => $this->cpf,
                    "celular" => $this->celular
                ])
                ->where("id", $this->id)
                ->update("vendedores");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Vendedor atualizado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao atualizar vendedor"];
        }
    }

    public function toArray() {
        return [
            "nome" => $this->nome,
            "cpf" => $this->cpf,
            "celular" => $this->celular,
            "idUsuario" => $this->idUsuario
        ];
    }

}
