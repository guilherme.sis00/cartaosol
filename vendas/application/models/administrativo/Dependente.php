<?php

class Dependente extends CI_Model {

    private $id;
    private $idTitular;
    private $nome;
    private $cpf;
    private $dataNascimento;
    private $dataVinculacao;
    private $celular;
    private $numero;

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->idTitular = !empty($dados['titular']) ? $dados['titular'] : NULL;
        $this->nome = !empty($dados['nome']) ? $dados['nome'] : NULL;
        $this->cpf = !empty($dados['cpf']) ? $dados['cpf'] : NULL;
        $this->dataNascimento = !empty($dados['dataNascimento']) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados['dataNascimento']))) : NULL;
        $this->dataVinculacao = !empty($dados['dataVinculacao']) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados['dataVinculacao']))) : NULL;
        $this->celular = !empty($dados['celular']) ? $dados['celular'] : NULL;
        $this->numero = !empty($dados['numeroCartao']) ? $dados['numeroCartao'] : NULL;
    }

    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE) {
        $this->db
                ->select("dependentes.*,"
                        . "clientes.nome as titular")
                ->join("clientes_dependentes", "clientes_dependentes.idDependente=dependentes.id")
                ->join("clientes", "clientes_dependentes.idCliente=clientes.id")
                ->order_by("dependentes.nome", "ASC");

        if ($paginacao) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->get("dependentes")->result();
    }

    public function getById($id, $returnArray = TRUE) {
        $response = $this->db
                        ->select("dependentes.*,"
                                . "clientes_dependentes.idCliente,"
                                . "clientes.nome as titular,"
                                . "cartoes_dependentes.numero")
                        ->join("clientes_dependentes", "clientes_dependentes.idDependente=dependentes.id")
                        ->join("clientes", "clientes_dependentes.idCliente=clientes.id")
                        ->join("cartoes_dependentes", "cartoes_dependentes.idDependente=dependentes.id")
                        ->where("dependentes.id", $id)
                        ->order_by("dependentes.nome", "ASC")
                        ->get("dependentes")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "dependente" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum dependente foi encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("dependentes", $this->toArray());
        $this->id = $this->db->insert_id();
        $this->db->insert("clientes_dependentes", ["idCliente" => $this->idTitular, "idDependente" => $this->id]);
        $this->db->insert("cartoes_dependentes", ["idDependente" => $this->id, "numero" => $this->numero]);
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Dependente adicionado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar dependente"];
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->set($this->toArray())
                ->where("id", $this->id)
                ->update("dependentes");
        $this->db
                ->set("idCliente", $this->idTitular)
                ->where("idDependente", $this->id)
                ->update("clientes_dependentes");
        $this->db
                ->set("numero", $this->numero)
                ->where("idDependente", $this->id)
                ->update("cartoes_dependentes");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Dependente atualizado");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao atualizar dependente");
        }
    }

    public function toArray() {
        return [
            "nome" => $this->nome,
            "cpf" => $this->cpf,
            "dataNascimento" => $this->dataNascimento,
            "dataVinculacao" => $this->dataVinculacao,
        ];
    }

    public function buscar($dados) {
        $this->db
                ->select("dependentes.*,"
                        . "clientes_dependentes.idCliente,"
                        . "clientes.nome as titular,"
                        . "cartoes_dependentes.numero")
                ->join("clientes_dependentes", "clientes_dependentes.idDependente=dependentes.id")
                ->join("clientes", "clientes_dependentes.idCliente=clientes.id")
                ->join("cartoes_dependentes", "cartoes_dependentes.idDependente=dependentes.id")
                ->like("dependentes.nome",$dados["nome"])
                ->order_by("dependentes.nome", "ASC");

        if (!empty($dados["titular"])) {
            $this->db->like("clientes.nome", $dados["titular"]);
        }

        if (!empty($dados["cpf"])) {
            $this->db->where("dependentes.cpf", $dados["cpf"]);
        }

        $response = $this->db->get("dependentes")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "dependentes" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum dependente encontrado"];
        }
    }

}
