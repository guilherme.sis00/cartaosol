<?php

class ContaReceber extends CI_Model {

    private $id;
    private $descricao;
    private $valor;
    private $idCategoria;
    private $data;
    private $status;
    private $dados;

    public function preencherDados($dados) {
        $this->id = !empty($dados["id"]) ? $dados['id'] : NULL;
        $this->descricao = !empty($dados["descricao"]) ? $dados['descricao'] : NULL;
        $this->valor = !empty($dados["valor"]) ? $dados["valor"] : NULL;
        $this->idCategoria = !empty($dados["categoria"]) ? $dados['categoria'] : NULL;
        $this->data = !empty($dados["data"]) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados['data']))) : NULL;
        $this->status = !empty($dados["status"]) ? $dados['status'] : NULL;
        $this->dados = $dados;
    }

    public function get($atributo) {
        switch ($atributo) {
            case "id":
                return $this->id;
        }
    }

    public function getByPeriodo($dataInicial, $dataFinal, $status, $returnArray = TRUE) {
        $response = $this->db
                        ->select("contas_receber.*, boletos.idParcela, categorias_financeiras.categoria")
                        ->join("categorias_financeiras", "categorias_financeiras.id=contas_receber.idCategoria")
                        ->join("boletos_contas_receber", "contas_receber.id=boletos_contas_receber.idContaReceber")
                        ->join("boletos", "boletos.id=boletos_contas_receber.idBoleto")
                        ->where('contas_receber.data BETWEEN "' . $dataInicial . '" AND "' . $dataFinal . '"')
                        ->where("contas_receber.status", $status)
                        ->order_by("contas_receber.data", "ASC")
                        ->get("contas_receber")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "contasReceber" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma conta a receber encontrada nesse período"];
            }
        } else {
            return $response;
        }
    }

    public function getById($id, $returnArray = TRUE) {
        $response = $this->db
                        ->select("contas_receber.*, categorias_financeiras.categoria")
                        ->join("categorias_financeiras", "categorias_financeiras.id=contas_receber.idCategoria")
                        ->where("contas_receber.id", $id)
                        ->get("contas_receber")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "contaReceber" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma conta a receber encontrada"];
            }
        } else {
            return $response;
        }
    }

    public function getTotalByStatus($status, $dataInicial, $dataFinal) {
        return $this->db
                        ->select("COALESCE(SUM(valor),0) AS total")
                        ->where("status", $status)
                        ->where('(contas_receber.data BETWEEN "' . $dataInicial . '" AND "' . $dataFinal . '")')
                        ->get("contas_receber")->row();
    }

    public function buscar($dados) {
        $this->db
                ->select("contas_receber.*, boletos.idParcela, categorias_financeiras.categoria")
                ->join("boletos_contas_receber", "boletos_contas_receber.idContaReceber=contas_receber.id")
                ->join("boletos", "boletos_contas_receber.idBoleto=boletos.id")
                ->join("parcelas", "boletos.idParcela=parcelas.id")
                ->join("contratos", "parcelas.idContrato=contratos.id")
                ->join("clientes", "contratos.idCliente=clientes.id")
                ->join("categorias_financeiras", "categorias_financeiras.id=contas_receber.idCategoria")
                ->where('contas_receber.data BETWEEN "' . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataInicial"]))) . '" AND "' . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataFinal"]))) . '"')
                ->where("contas_receber.status", $dados["status"])
                ->order_by("contas_receber.data", "ASC");

        if (!empty($dados["termo"])) {
            $this->db->like("clientes.nome", $dados["termo"]);
        }

        if (!empty($dados["cpf"])) {
            $this->db->where("clientes.cpf", $dados["cpf"]);
        }

        if (!empty($dados["codigoContrato"])) {
            $this->db->where("contratos.codigoContrato", $dados["codigoContrato"]);
        }

        if ($dados["categoria"] != "todas") {
            $this->db->where("contas_receber.idCategoria", $dados["categoria"]);
        }

        $response = $this->db->get("contas_receber")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "contasReceber" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhuma conta a receber encontrada"];
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $parcela = $this->configurarParcela();
        $this->parcela->preencherDados($parcela);
        $this->parcela->adicionar();
        $responseParcela = $this->parcela->toArray();

        $responseContrato = $this->contrato->getById($this->dados["contrato"], FALSE);

        $responseBoleto = $this->boleto->gerar([
            "idCliente" => $responseContrato->idCliente,
            "idParcela" => $this->parcela->get("id"),
            "valor" => $responseParcela['valor'],
            "parcela" => $responseParcela['parcela'],
            "totalParcelamento" => $responseParcela['totalParcelamento'],
            "vencimento" => $responseParcela['vencimento'],
            "instrucao1" => $this->dados["instrucao1"],
            "instrucao2" => $this->dados["instrucao2"],
            "instrucao3" => $this->dados["instrucao3"],
            "instrucao4" => $this->dados["instrucao4"],
        ]);

        $this->boleto->preencherDados([
            "parcela" => $this->parcela->get("id"),
            "dataEmissao" => date("d/m/Y"),
            "nossoNumero" => $responseBoleto->getNossoNumero(),
            "instrucao1" => $this->dados["instrucao1"],
            "instrucao2" => $this->dados["instrucao2"],
            "instrucao3" => $this->dados["instrucao3"],
            "instrucao4" => $this->dados["instrucao4"],
            "status" => 1
        ]);

        $this->boleto->adicionar();

        $this->db->insert("contas_receber", $this->toArray());
        $this->id = $this->db->insert_id();

        $this->db->insert("boletos_contas_receber", ["idBoleto" => $this->boleto->get("id"), "idContaReceber" => $this->id]);
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Conta a receber adicionada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar conta a receber"];
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->where("id", $this->id)
                ->update("contas_receber", $this->toArray());
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Conta a receber atualizada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao atualizar conta a receber"];
        }
    }

    public function atualizarStatus($dados) {
        $this->db->trans_start();
        $this->db
                ->where("id", $dados["id"])
                ->update("contas_receber", ["status" => $this->status]);
        $this->db
                ->where("idParcela", $dados["idParcela"])
                ->update("boletos", ["dataPagamento" => date("Y-m-d"), "status" => 2]);

        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Status atualizado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao atualizar status"];
        }
    }

    public function deletar($id) {
        $responseBoletoContaReceber = $this->db
                        ->where("idContaReceber", $id)
                        ->get("boletos_contas_receber")->row();
        $responseBoleto = $this->db
                        ->where("id", $responseBoletoContaReceber->idBoleto)
                        ->get("boletos")->row();

        $this->db->delete("boletos_contas_receber", ["id" => $responseBoletoContaReceber->id]);
        $this->db->delete("boletos", ["id" => $responseBoleto->id]);
        $this->db->delete("contas_receber", ["id" => $id]);
        $this->db->delete("parcelas", ["id" => $responseBoleto->idParcela]);

        if ($this->db->affected_rows() > 0) {
            return ["resultado" => TRUE, "msg" => "Conta a receber deletada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao deletar conta a receber"];
        }
    }

    private function configurarParcela() {
        return [
            "contrato" => $this->dados["contrato"],
            "valor" => $this->valor,
            "vencimento" => $this->data,
            "parcela" => 1,
            "totalParcelamento" => 1
        ];
    }

    public function toArray() {
        return [
            'descricao' => $this->descricao,
            'valor' => $this->valor,
            'idCategoria' => $this->idCategoria,
            'data' => $this->data,
            'status' => $this->status
        ];
    }

    //NÃO FUNCIONAL
    public function alterarStatus() {
        $this->db->trans_start();
        $this->db
                ->set("status", $this->status)
                ->where("id", $this->id)
                ->update("admin_entradas");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            $responseEntrada = $this->getById($this->id);
            $ultimoDiaMes = cal_days_in_month(CAL_GREGORIAN, date("m", strtotime($this->data)), date("Y", strtotime($this->data)));
            $dataInicial = date("Y-m") . "-01";
            $dataFinal = date("Y-m") . "-" . $ultimoDiaMes;

            return [
                "resultado" => TRUE,
                "msg" => "Status alterado",
                "entrada" => $responseEntrada["entrada"],
                "recebido" => $this->getTotalByStatus(1, $dataInicial, $dataFinal),
                "pendente" => $this->getTotalByStatus(-1, $dataInicial, $dataFinal),
                "mesAtual" => getMesExtenso(date("m"))
            ];
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao alterar status");
        }
    }

}
