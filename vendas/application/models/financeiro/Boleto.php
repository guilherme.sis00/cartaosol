<?php

use OpenBoleto\Banco\Santander;
use OpenBoleto\Agente;

class Boleto extends CI_Model {

    private $id;
    private $idParcela;
    private $dataPagamento;
    private $dataEmissao;
    private $nossoNumero;
    private $instrucao1;
    private $instrucao2;
    private $instrucao3;
    private $instrucao4;
    private $status;

    public function __construct() {
        parent::__construct();
        $this->load->model(["administrativo/empresa", "cliente"]);
    }

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->idParcela = !empty($dados["parcela"]) ? $dados["parcela"] : NULL;
        $this->dataPagamento = !empty($dados['dataPagamento']) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados['dataPagamento']))) : "0000-00-00";
        $this->dataEmissao = !empty($dados["dataEmissao"]) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataEmissao"]))) : date("Y-m-d");
        $this->nossoNumero = !empty($dados["nossoNumero"]) ? $dados["nossoNumero"] : 0;
        $this->instrucao1 = !empty($dados['instrucao1']) ? $dados['instrucao1'] : "";
        $this->instrucao2 = !empty($dados["instrucao2"]) ? $dados["instrucao2"] : "";
        $this->instrucao3 = !empty($dados['instrucao3']) ? $dados['instrucao3'] : "";
        $this->instrucao4 = !empty($dados["instrucao4"]) ? $dados["instrucao4"] : "";
        $this->status = !empty($dados["status"]) ? $dados["status"] : 0;
    }

    public function get($atributo) {
        switch ($atributo) {
            case "id":
                return $this->id;
        }
    }

    public function getByIdParcela($idParcela, $returnArray = TRUE) {
        $response = $this->db
                        ->select("boletos.*,"
                                . "parcelas.parcela,"
                                . "parcelas.totalParcelamento,"
                                . "parcelas.valor,"
                                . "parcelas.vencimento,"
                                . "contratos.idCliente")
                        ->where("boletos.idParcela", $idParcela)
                        ->join("parcelas", "parcelas.id=boletos.idParcela")
                        ->join("contratos", "parcelas.idContrato=contratos.id")
                        ->get("boletos")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "boleto" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum boleto encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function getByIdReceita($idReceita, $returnArray = TRUE) {
        $response = $this->db
                        ->select("boletos.*,"
                                . "parcelas.parcela,"
                                . "parcelas.totalParcelamento,"
                                . "parcelas.valor,"
                                . "parcelas.vencimento,"
                                . "contratos.idCliente")
                        ->where("boletos_contas_receber.idContaReceber", $idReceita)
                        ->join("boletos_contas_receber", "boletos.id=boletos_contas_receber.idBoleto")
                        ->join("parcelas", "parcelas.id=boletos.idParcela")
                        ->join("contratos", "parcelas.idContrato=contratos.id")
                        ->get("boletos")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "boleto" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum boleto encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function getByIdsParcelas($ids, $returnArray = TRUE) {
        $primeiro = FALSE;
        $this->db
                ->select("boletos.*,"
                        . "parcelas.parcela,"
                        . "parcelas.totalParcelamento,"
                        . "parcelas.valor,"
                        . "parcelas.vencimento,"
                        . "contratos.idCliente,"
                        . "clientes.nome,"
                        . "clientes.cpf,"
                        . "cidades.cidade,"
                        . "estados.uf")
                ->join("parcelas", "parcelas.id=boletos.idParcela")
                ->join("contratos", "parcelas.idContrato=contratos.id")
                ->join("clientes", "contratos.idCliente=clientes.id")
                ->join("cidades", "clientes.idCidade=cidades.id")
                ->join("estados", "clientes.idEstado=estados.id");

        foreach ($ids as $id) {
            if ($primeiro) {
                $this->db->where("boletos.id", $id);
                $primeiro = FALSE;
            } else {
                $this->db->or_where("boletos.id", $id);
            }
        }

        $response = $this->db->get("boletos")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "boletos" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum boleto encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function baixaRemessa($dados, $data) {
        $this->db->trans_start();
        $primeiro = TRUE;
        $this->db
                ->set([
                    "exportado" => 2,
                    "dataRemessa" => $data
        ]);

        foreach ($dados as $boleto) {
            if ($primeiro) {
                $this->db->where("boletos.id", $boleto);
                $primeiro = FALSE;
            } else {
                $this->db->or_where("boletos.id", $boleto);
            }
        }

        $this->db->update("boletos");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Boletos atualizados"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao atualizar boletos"];
        }
    }

    public function getBoletosParaRemessa($dados) {
        $this->db
                ->select("boletos.id,"
                        . "boletos.dataEmissao,"
                        . "boletos.dataRemessa,"
                        . "boletos.exportado,"
                        . "boletos.status,"
                        . "clientes.nome,"
                        . "parcelas.vencimento,"
                        . "parcelas.valor")
                ->join("parcelas", "parcelas.id=boletos.idParcela")
                ->join("contratos", "parcelas.idContrato=contratos.id")
                ->join("clientes", "clientes.id=contratos.idCliente")
                ->order_by("clientes.nome")
                ->where("boletos.exportado", $dados["exportado"]);

        if (!empty($dados["dataInicial"]) && !empty($dados["dataFinal"])) {
            $this->db->where("parcelas.vencimento BETWEEN '" . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataInicial"]))) . "' AND '" . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataFinal"]))) . "'");
        }
//        } else {
//            if (!empty($dados["dataInicial"])) {
//                $this->db->where("parcelas.vencimento BETWEEN '" . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataInicial"]))) . "' AND '" . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataFinal"]))) . "'");
//            } else {
//                $this->db->where("parcelas.vencimento BETWEEN '" . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataFinal"]))) . "' AND '" . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataFinal"]))) . "'");
//            }
//        }

        $response = $this->db->get("boletos")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "boletos" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum boleto encontrado"];
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("boletos", $this->toArray());
        $this->id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Boleto adicionado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar boleto"];
        }
    }

    public function gerar($dados) {
        $responseCliente = $this->cliente->getById($dados["idCliente"], FALSE);
        $responseEmpresa = $this->empresa->get(FALSE);

        $sacado = new Agente($responseCliente->nome, $responseCliente->cpf, '', '', $responseCliente->estado, $responseCliente->cidade);
        $cedente = new Agente($responseEmpresa->nomeFantasia, $responseEmpresa->cnpj, "{$responseEmpresa->cep} - {$responseEmpresa->logradouro}, {$responseEmpresa->numero}, {$responseEmpresa->cidade}/{$responseEmpresa->uf}", $responseEmpresa->telefone, $responseEmpresa->estado, $responseEmpresa->uf);

        $agencia = explode("-", $responseEmpresa->agencia);
        $conta = explode("-", $responseEmpresa->conta);
        $boleto = new Santander([
            // Parâmetros obrigatórios
            'dataVencimento' => new DateTime($dados["vencimento"]),
            'valor' => $dados["valor"],
            'sequencial' => $dados["idParcela"], // Até 13 dígitos
            'sacado' => $sacado,
            'cedente' => $cedente,
            'agencia' => $agencia[0], // Até 4 dígitos
            'carteira' => $responseEmpresa->carteira, // 101, 102 ou 201
            'conta' => $responseEmpresa->codigoPsk, // Código do cedente: Até 7 dígitos
            // IOS – Seguradoras (Se 7% informar 7. Limitado a 9%)
            // Demais clientes usar 0 (zero)
            'ios' => $responseEmpresa->ios, // Apenas para o Santander
            // Parâmetros recomendáveis
            'logoPath' => base_url("assets/img/marca.png"), // Logo da sua empresa
            'agenciaDv' => $agencia[1],
            'contaDv' => $conta[1],
            'descricaoDemonstrativo' => [// Até 5
                "Parcela {$dados["parcela"]}/{$dados["totalParcelamento"]}",
                "Após vencimento, juros de 1,99% ao mês"
            ],
            'instrucoes' => [// Até 8
                "Parcela {$dados["parcela"]}/{$dados["totalParcelamento"]}",
                "Após vencimento, juros de 1,99% ao mês",
                $dados["instrucao1"],
                $dados["instrucao2"],
                $dados["instrucao3"],
                $dados["instrucao4"]
            ]
        ]);

        return $boleto;
    }

    //Pendente de análise
    public function gerarRemessa($dados) {
        $responseEmpresa = $this->empresa->get(FALSE);

        $agencia = explode("-", $responseEmpresa->agencia);
        $conta = explode("-", $responseEmpresa->conta);

        $codigo_banco = Cnab\Banco::SANTANDER;

        $arquivo = new Cnab\Remessa\Cnab400\Arquivo($codigo_banco);
        $arquivo->configure(array(
            'data_geracao' => new DateTime(),
            'data_gravacao' => new DateTime(),
            'nome_fantasia' => $responseEmpresa->nomeFantasia, // seu nome de empresa
            'razao_social' => $responseEmpresa->razaoSocial, // sua razão social
            'cnpj' => $responseEmpresa->cnpj, // seu cnpj completo
            'banco' => $codigo_banco, //código do banco
            'logradouro' => $responseEmpresa->logradouro,
            'numero' => $responseEmpresa->numero,
            'bairro' => $responseEmpresa->bairro,
            'cidade' => $responseEmpresa->cidade,
            'uf' => $responseEmpresa->uf,
            'cep' => $responseEmpresa->cep,
            'agencia' => $agencia[0],
            'conta' => $conta[0], // número da conta
            'conta_dac' => $conta[1], // digito da conta
        ));

//        $arquivo->insertDetalhe(array(
//            'codigo_de_ocorrencia' => 1, // 1 = Entrada de título, futuramente poderemos ter uma constante
//            'nosso_numero' => '1234567',
//            'numero_documento' => '1234567',
//            'carteira' => '109',
//            'especie' => Cnab\Especie::ITAU_DUPLICATA_DE_SERVICO, // Você pode consultar as especies Cnab\Especie
//            'valor' => 100.39, // Valor do boleto
//            'instrucao1' => 2, // 1 = Protestar com (Prazo) dias, 2 = Devolver após (Prazo) dias, futuramente poderemos ter uma constante
//            'instrucao2' => 0, // preenchido com zeros
//            'sacado_nome' => 'Nome do cliente', // O Sacado é o cliente, preste atenção nos campos abaixo
//            'sacado_tipo' => 'cpf', //campo fixo, escreva 'cpf' (sim as letras cpf) se for pessoa fisica, cnpj se for pessoa juridica
//            'sacado_cpf' => '111.111.111-11',
//            'sacado_logradouro' => 'Logradouro do cliente',
//            'sacado_bairro' => 'Bairro do cliente',
//            'sacado_cep' => '11111222', // sem hífem
//            'sacado_cidade' => 'Cidade do cliente',
//            'sacado_uf' => 'SP',
//            'data_vencimento' => new DateTime('2014-06-08'),
//            'data_cadastro' => new DateTime('2014-06-01'),
//            'juros_de_um_dia' => 0.10, // Valor do juros de 1 dia'
//            'data_desconto' => new DateTime('2014-06-01'),
//            'valor_desconto' => 10.0, // Valor do desconto
//            'prazo' => 10, // prazo de dias para o cliente pagar após o vencimento
//            'taxa_de_permanencia' => '0', //00 = Acata Comissão por Dia (recomendável), 51 Acata Condições de Cadastramento na CAIXA
//            'mensagem' => 'Descrição do boleto',
//            'data_multa' => new DateTime('2014-06-09'), // data da multa
//            'valor_multa' => 10.0, // valor da multa
//        ));
//
//        $arquivo->save('./assets/arquivos/remessas/arquivo-' . date("Y-m-d_H:i:s") . '.txt');
    }

    public function gerarRemessa2($dados) {
        $responseEmpresa = $this->empresa->get(FALSE);

        $agencia = explode("-", $responseEmpresa->agencia);
        $conta = explode("-", $responseEmpresa->conta);

        $arquivo = new Remessa(104, 'cnab240_SIGCB', array(
            'nome_empresa' => "Empresa ABC", // seu nome de empresa
            'tipo_inscricao' => 2, // 1 para cpf, 2 cnpj 
            'numero_inscricao' => $empresa->empresas_cnpjcpf, // seu cpf ou cnpj completo
            'agencia' => '1234', // agencia sem o digito verificador 
            'agencia_dv' => 1, // somente o digito verificador da agencia 
            'conta' => '12345', // número da conta
            'conta_dv' => 1, // digito da conta
            'codigo_beneficiario' => '123456', // codigo fornecido pelo banco
            'numero_sequencial_arquivo' => 1, // sequencial do arquivo um numero novo para cada arquivo gerado
        ));
        $lote = $arquivo->addLote(array('tipo_servico' => 1)); // tipo_servico  = 1 para cobrança registrada, 2 para sem registro

        $lote->inserirDetalhe(array(
            'codigo_movimento' => 1, //1 = Entrada de título, para outras opçoes ver nota explicativa C004 manual Cnab_SIGCB na pasta docs
            'nosso_numero' => 50, // numero sequencial de boleto
            'seu_numero' => 43, // se nao informado usarei o nosso numero 

            /* campos necessarios somente para itau e siccob,  não precisa comentar se for outro layout    */
            'carteira_banco' => 109, // codigo da carteira ex: 109,RG esse vai o nome da carteira no banco
            'cod_carteira' => "01", // I para a maioria ddas carteiras do itau
            /* ----------------------------------------------------------------------------------------    */
            'especie_titulo' => "DM", // informar dm e sera convertido para codigo em qualquer laytou conferir em especie.php
            'valor' => 100.00, // Valor do boleto como float valido em php
            'emissao_boleto' => 2, // tipo de emissao do boleto informar 2 para emissao pelo beneficiario e 1 para emissao pelo banco
            'protestar' => 3, // 1 = Protestar com (Prazo) dias, 3 = Devolver após (Prazo) dias
            'prazo_protesto' => 5, // Informar o numero de dias apos o vencimento para iniciar o protesto
            'nome_pagador' => "JOSÉ da SILVA ALVES", // O Pagador é o cliente, preste atenção nos campos abaixo
            'emissao_boleto' => 2, // tipo de emissao do boleto informar 2 para emissao pelo beneficiario e 1 para emissao pelo banco
            'protestar' => 3, // 1 = Protestar com (Prazo) dias, 3 = Devolver após (Prazo) dias. 
            'nome_pagador' => "JOSÉ da SILVA ALVES", // O Pagador é o cliente, preste atenção nos campos abaixo
            'tipo_inscricao' => 1, //campo fixo, escreva '1' se for pessoa fisica, 2 se for pessoa juridica
            'numero_inscricao' => '123.122.123-56', //cpf ou ncpj do pagador
            'endereco_pagador' => 'Rua dos developers,123 sl 103',
            'bairro_pagador' => 'Bairro da insonia',
            'cep_pagador' => '12345-123', // com hífem
            'cidade_pagador' => 'Londrina',
            'uf_pagador' => 'PR',
            'data_vencimento' => '2016-04-09', // informar a data neste formato
            'data_emissao' => '2016-04-09', // informar a data neste formato
            'vlr_juros' => 0.15, // Valor do juros de 1 dia'
            'data_desconto' => '2016-04-09', // informar a data neste formato
            'vlr_desconto' => '0', // Valor do desconto
            'baixar' => 1, // codigo para indicar o tipo de baixa '1' (Baixar/ Devolver) ou '2' (Não Baixar / Não Devolver)
            'prazo_baixa' => 90, // prazo de dias para o cliente pagar após o vencimento
            'mensagem' => 'JUROS de R$0,15 ao dia' . PHP_EOL . "Não receber apos 30 dias",
            'email_pagador' => 'rogerio@ciatec.net', // data da multa
            'data_multa' => '2016-04-09', // informar a data neste formato, // data da multa
            'vlr_multa' => 30.00, // valor da multa
            // campos necessários somente para o sicoob
            'taxa_multa' => 30.00, // taxa de multa em percentual
            'taxa_juros' => 30.00, // taxa de juros em percentual
        ));
        echo $arquivo->getText();
    }

    public function toArray() {
        return [
            "idParcela" => $this->idParcela,
            "dataPagamento" => $this->dataPagamento,
            "dataEmissao" => $this->dataEmissao,
            "nossoNumero" => $this->nossoNumero,
            "instrucao1 " => $this->instrucao1,
            "instrucao2 " => $this->instrucao2,
            "instrucao3 " => $this->instrucao3,
            "instrucao4 " => $this->instrucao4,
            "status " => $this->status
        ];
    }

}
