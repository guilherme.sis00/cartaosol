<?php

class Parceiro extends CI_Model {

    private $id;
    private $nome;
    private $idCidade;
    private $idEstado;
    private $endereco;
    private $parceiro;
    private $clinica;
    private $exame;
    private $logo;
    private $telefone;
    private $email;
    private $whatsapp;
    private $descricao;
    private $especialidade;

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->nome = !empty($dados['nome']) ? $dados['nome'] : NULL;
        $this->idCidade = !empty($dados['cidade']) ? $dados['cidade'] : NULL;
        $this->idEstado = !empty($dados['estado']) ? $dados['estado'] : NULL;
        $this->endereco = !empty($dados['endereco']) ? $dados['endereco'] : NULL;
        $this->parceiro = !empty($dados['parceiro']) ? $dados['parceiro'] : 0;
        $this->clinica = !empty($dados['clinica']) ? $dados['clinica'] : 0;
        $this->exame = !empty($dados['exame']) ? $dados['exame'] : 0;
        $this->logo = !empty($dados['logo']) ? $dados['logo'] : "logoPadrao.png";
        $this->especialidade = !empty($dados['especialidade']) ? $dados['especialidade'] : NULL;
        $this->email = !empty($dados['email']) ? $dados['email'] : NULL;
        $this->telefone = !empty($dados['telefone']) ? $dados['telefone'] : NULL;
        $this->whatsapp = !empty($dados['whatsapp']) ? $dados['whatsapp'] : NULL;
        $this->descricao = !empty($dados['descricao']) ? $dados['descricao'] : NULL;
    }

    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE) {
        if ($paginacao) {
            return $this->db
                            ->select("ocpt_parceiros.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->limit($limit, $offset)
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_parceiros.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_parceiros.idEstado")
                            ->order_by("ocpt_parceiros.nome", "ASC")
                            ->get("ocpt_parceiros")->result();
        } else {
            return $this->db
                            ->select("ocpt_parceiros.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_clientes.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_clientes.idEstado")
                            ->order_by("ocpt_parceiros.nome", "ASC")
                            ->get("ocpt_parceiros")->result();
        }
    }

    public function getById($id, $returnArray = TRUE) {
        if ($returnArray) {
            $response = $this->db
                            ->select("ocpt_parceiros.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->where("ocpt_parceiros.id", $id)
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_parceiros.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_parceiros.idEstado")
                            ->order_by("ocpt_parceiros.nome", "ASC")
                            ->get("ocpt_parceiros")->row();

            if (!empty($response)) {
                $especialidades = NULL;
                if ($response->exame) {
                    $especialidades = $this->db
                                    ->select("ocpt_parceiros_especialidades.*,"
                                            . "ocpt_especialidades.especialidade")
                                    ->join("ocpt_especialidades", "ocpt_especialidades.id=ocpt_parceiros_especialidades.idEspecialidade")
                                    ->where("ocpt_parceiros_especialidades.idParceiro", $response->id)
                                    ->get("ocpt_parceiros_especialidades")->result();
                }
                return ["resultado" => TRUE, "parceiro" => $response, "especialidades" => $especialidades];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum pareciro foi encontrado"];
            }
        } else {
            return $this->db
                            ->select("ocpt_parceiros.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_parceiros.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_parceiros.idEstado")
                            ->order_by("ocpt_parceiros.nome", "ASC")
                            ->get("ocpt_parceiros")->row();
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("ocpt_parceiros", $this->toArray());
        $this->id = $this->db->insert_id();
        if ($this->exame) {
            $especialidades = [];
            foreach ($this->especialidade as $especialidade) {
                array_push($especialidades, ["idParceiro" => $this->id, "idEspecialidade" => $especialidade]);
            }
            $this->db->insert_batch("ocpt_parceiros_especialidades", $especialidades);
        }
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Cliente adicionado, continuar adicionando ?"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar cliente"];
        }
    }

    public function buscar($dados) {
        $response = $this->db
                        ->select("ocpt_parceiros.*,"
                                . "ocpt_cidades.cidade,"
                                . "ocpt_estados.estado")
                        ->like($dados["tipoBusca"], $dados["termo"])
                        ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_parceiros.idCidade")
                        ->join("ocpt_estados", "ocpt_estados.id=ocpt_parceiros.idEstado")
                        ->order_by("ocpt_parceiros.nome", "ASC")
                        ->get("ocpt_parceiros")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "parceiros" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum parceiro encontrado"];
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $dados = $this->toArray();
        unset($dados["logo"]);
        $this->db
                ->set($dados)
                ->where("id", $this->id)
                ->update("ocpt_parceiros");

        $this->db
                ->where("idParceiro", $this->id)
                ->delete("ocpt_parceiros_especialidades");
        if ($this->exame) {
            $especialidades = [];
            foreach ($this->especialidade as $especialidade) {
                array_push($especialidades, ["idParceiro" => $this->id, "idEspecialidade" => $especialidade]);
            }
            $this->db->insert_batch("ocpt_parceiros_especialidades", $especialidades);
        }
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Parceiro atualizado");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao atualizar parceiro");
        }
    }

    public function alterarLogo() {
        $this->db->trans_start();
        $this->db
                ->set(["logo" => $this->logo])
                ->where("id", $this->id)
                ->update("ocpt_parceiros");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Logo do parceiro alterada");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao alterar logo do parceiro");
        }
    }

    public function deletar($id) {
        $this->db
                ->where("idParceiro", $id)
                ->delete("ocpt_parceiros_especialidades");
        $this->db
                ->where("id", $id)
                ->delete("ocpt_convenios_descontos");

        if ($this->db->affected_rows()) {
            return array("resultado" => TRUE, "msg" => "Parceiro deletado");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao deletar parceiro");
        }
    }

    private function toArray() {
        return [
            "nome" => $this->nome,
            "idCidade" => $this->idCidade,
            "idEstado" => $this->idEstado,
            "endereco" => $this->endereco,
            "parceiro" => $this->parceiro,
            "clinica" => $this->clinica,
            "exame" => $this->exame,
            "logo" => $this->logo,
            "email" => $this->email,
            "telefone" => $this->telefone,
            "whatsapp" => $this->whatsapp,
            "descricao" => $this->descricao
        ];
    }

}
