<?php

class Combo extends CI_Model {

    private $id;
    private $nome;
    private $valor;
    private $status;
    private $itens;

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->nome = !empty($dados['nome']) ? $dados['nome'] : NULL;
        $this->valor = !empty($dados['valorCombo']) ? $dados['valorCombo'] : NULL;
        $this->status = !empty($dados['status']) ? $dados['status'] : "ativo";
        $this->itens = !empty($dados['itens']) ? $dados['itens'] : NULL;
    }

    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE) {
        $this->db
                ->where("status <>", "deletado")
                ->order_by("combos.nome", "ASC");

        if ($paginacao) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->get("combos")->result();
    }

    public function getById($id, $returnArray = TRUE) {
        $response = $this->db
                        ->where([
                            "status <>" => "deletado",
                            "id" => $id
                        ])
                        ->get("combos")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "combo" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum combo foi encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function getEspecialidadesByIdCombo($id, $returnArray = TRUE) {
        $response = $this->db
                        ->select("combos_especialidades.*,especialidades.especialidade")
                        ->join("especialidades", "especialidades.id=combos_especialidades.idEspecialidade")
                        ->where("combos_especialidades.idCombo", $id)
                        ->get("combos_especialidades")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "especialidades" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma especialidade foi encontrada"];
            }
        } else {
            return $response;
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("combos", $this->toArray());
        $this->id = $this->db->insert_id();
        foreach ($this->itens as $item) {
            $this->db
                    ->set([
                        "idCombo" => $this->id,
                        "idEspecialidade" => $item["procedimento"],
                        "quantidade" => $item["quantidade"],
                        "valor" => $item["valor"],
                        "tipo" => (int) $item["quantidade"] > 0 ? "quantidade" : "valor"
                    ])
                    ->insert("combos_especialidades");
        }
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Combo adicionado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar combo"];
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->set([
                    "nome" => $this->nome,
                    "valor" => $this->valor
                ])
                ->where("id", $this->id)
                ->update("combos");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Combo atualizado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao atualizar combo"];
        }
    }

    public function buscar($dados) {
        $response = $this->db
                        ->where("status <>", "deletado")
                        ->like("nome", $dados["termo"])
                        ->order_by("nome", "ASC")
                        ->get("combos")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "combos" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum combo encontrado"];
        }
    }

    public function deletar($id) {
        $this->db->trans_start();
        $this->db
                ->where("id", $id)
                ->update("combos", ["status" => "deletado"]);
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Combo deletado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao deletar combo"];
        }
    }

    public function adicionarProcedimento($dados) {
        $this->db->trans_start();
        $this->db
                ->set([
                    "idCombo" => $dados["id"],
                    "idEspecialidade" => $dados["procedimento"],
                    "quantidade" => $dados["quantidade"],
                    "valor" => $dados["valor"]
                ])
                ->insert("combos_especialidades");

        $response = $this->db
                        ->select("combos_especialidades.*,especialidades.especialidade")
                        ->join("especialidades", "especialidades.id=combos_especialidades.idEspecialidade")
                        ->where("combos_especialidades.id", $this->db->insert_id())
                        ->get("combos_especialidades")->row();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Procedimento adicionado", "procedimento" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar procedimento"];
        }
    }

    public function deletarProcedimento($id) {
        $this->db->delete("combos_especialidades", ["id" => $id]);

        if ($this->db->affected_rows() > 0) {
            return ["resultado" => TRUE, "msg" => "Procedimento deletado do combo"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao deletar procedimento do combo"];
        }
    }

    private function toArray() {
        return [
            "nome" => $this->nome,
            "valor" => $this->valor
        ];
    }

}
