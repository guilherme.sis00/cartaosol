<?php

class Cliente extends CI_Model {

    private $id;
    private $nome;
    private $cpf;
    private $email;
    private $celular;
    private $whatsapp;
    private $dataNascimento;
    private $idCartao;
    private $idCidade;
    private $idEstado;
    private $observacao;
    private $status;
    private $numeroCartao;

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->nome = !empty($dados['nome']) ? $dados['nome'] : NULL;
        $this->cpf = !empty($dados['cpf']) ? $dados['cpf'] : NULL;
        $this->email = !empty($dados['email']) ? $dados['email'] : NULL;
        $this->celular = !empty($dados['celular']) ? $dados['celular'] : NULL;
        $this->whatsapp = !empty($dados['whatsapp']) ? $dados['whatsapp'] : NULL;
        $this->dataNascimento = !empty($dados['dataNascimento']) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados['dataNascimento']))) : NULL;
        $this->idCartao = !empty($dados['idCartao']) ? $dados['idCartao'] : NULL;
        $this->idCidade = !empty($dados['cidade']) ? $dados['cidade'] : NULL;
        $this->idEstado = !empty($dados['estado']) ? $dados['estado'] : NULL;
        $this->observacao = !empty($dados['observacao']) ? $dados['observacao'] : NULL;
        $this->status = !empty($dados['status']) ? $dados['status'] : "ativo";
        $this->numeroCartao = !empty($dados['numeroCartao']) ? $dados['numeroCartao'] : NULL;
    }

    public function count() {
        $response = $this->db
                        ->select("COUNT(ocpt_contratos.idCliente) as total")
                        ->join("contratos", "contratos.idCliente=clientes.id")
                        ->where([
                            "clientes.status" => "ativo",
                            "contratos.idVendedor" => $this->session->vendedor
                        ])
                        ->get("clientes")->row();

        return $response->total;
    }

    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE) {
        $this->db
                ->select("clientes.*,"
                        . "cidades.cidade,"
                        . "estados.estado")
                ->join("contratos", "contratos.idCliente=clientes.id")
                ->join("cidades", "cidades.id=clientes.idCidade")
                ->join("estados", "estados.id=clientes.idEstado")
                ->order_by("clientes.nome", "ASC")
                ->where([
                    "clientes.status" => "ativo",
                    "contratos.idVendedor" => $this->session->vendedor
        ]);

        if ($paginacao) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->get("clientes")->result();
    }

    public function getById($id, $returnArray = TRUE) {
        if ($returnArray) {
            $response = $this->db
                            ->select("clientes.*,"
                                    . "cartoes_clientes.id as idCartao,"
                                    . "cartoes_clientes.numero,"
                                    . "cidades.cidade,"
                                    . "estados.estado")
                            ->where("clientes.id", $id)
                            ->join("cartoes_clientes", "cartoes_clientes.idCliente=clientes.id")
                            ->join("cidades", "cidades.id=clientes.idCidade")
                            ->join("estados", "estados.id=clientes.idEstado")
                            ->get("clientes")->row();

            if (!empty($response)) {
                return ["resultado" => TRUE, "cliente" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum cliente foi encontrado"];
            }
        } else {
            return $this->db
                            ->select("ocpt_clientes.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->where("ocpt_clientes.id", $id)
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_clientes.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_clientes.idEstado")
                            ->get("ocpt_clientes")->row();
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("clientes", $this->toArray());
        $this->id = $this->db->insert_id();
        $this->db->insert("cartoes_clientes", ["idCliente" => $this->id, "numero" => $this->numeroCartao]);
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Cliente adicionado, continuar adicionando ?"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar cliente"];
        }
    }

    public function buscar($dados) {
        $response = $this->db
                        ->join("contratos", "contratos.idCliente=clientes.id")
                        ->like("clientes.{$dados["tipoBusca"]}", $dados["termo"])
                        ->where([
                            "clientes.status" => "ativo",
                            "contratos.idVendedor" => $this->session->vendedor
                        ])
                        ->get("clientes")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "clientes" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum cliente encontrado"];
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->set($this->toArray())
                ->where("id", $this->id)
                ->update("clientes");
        $this->db
                ->set("numero", $this->numeroCartao)
                ->where("id", $this->idCartao)
                ->update("cartoes_clientes");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Cliente atualizado");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao atualizar cliente");
        }
    }

    public function deletar($id) {
        $this->db->trans_start();
        $this->db
                ->set("status", "deletado")
                ->where("id", $id)
                ->update("clientes");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Cliente deletado");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao deletar cliente");
        }
    }

    private function toArray() {
        return [
            "nome" => $this->nome,
            "cpf" => $this->cpf,
            "email" => $this->email,
            "celular" => $this->celular,
            "whatsapp" => $this->whatsapp,
            "dataNascimento" => $this->dataNascimento,
            "idCidade" => $this->idCidade,
            "idEstado" => $this->idEstado,
            "observacao" => $this->observacao,
            "status" => $this->status
        ];
    }

}
