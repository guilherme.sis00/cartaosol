<!doctype html>
<?php // $this->output->enable_profiler() ?>
<html lang="pt-br">
    <head data-info="<?= base_url() ?>">
        <meta charset="utf-8" />
        <!--<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">-->
        <!--<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title><?= $title ?> | O cartão para todos</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
        <link href="<?= base_url('assets/css/animate.min.css') ?>" rel="stylesheet"/>
        <link href="<?= base_url('assets/css/paper-dashboard.css') ?>" rel="stylesheet"/>
        <link href="<?= base_url('assets/css/demo.css') ?>" rel="stylesheet" />
        <link href="<?= base_url("assets/js/jquery-toast/jquery.toast.min.css") ?>" rel="stylesheet" />
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="<?= base_url('assets/css/themify-icons.css') ?>" rel="stylesheet">
        <link href="<?= base_url('assets/css/login.css') ?>" rel="stylesheet">
    </head>