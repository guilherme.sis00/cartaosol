
<div class="sidebar" data-background-color="white" data-active-color="danger">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="<?= base_url("dashboard") ?>" class="simple-text">
                <img src="<?= base_url("assets/img/marca-reduzida.png") ?>" class="img-responsive center-block" alt="Marca O cartão para todos">
            </a>
        </div>
        <ul class="nav">
            <li class="<?= $menu == "dashboard" ? 'active' : "" ?>">
                <a href="<?= base_url("dashboard") ?>">
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="<?= $menu == "administrativo" ? 'active' : "" ?>">
                <a href="#administrativo" data-toggle="collapse" aria-expanded="false">
                    <p>Administrativo</p>
                </a>
                <div id="administrativo" class="collapse" style="background-color: #F4F3EF">
                    <ul class="nav" style="margin-top:0">
                        <li>
                            <a href="<?= base_url("clientes") ?>">
                                <i class="ti-user"></i>
                                <p>Clientes</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url("administrativo/contratos") ?>">
                                <i class="ti-write"></i>
                                <p>Contratos</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="<?= base_url("sair") ?>">
                    <p>Sair</p>
                </a>
            </li>
        </ul>
    </div>
</div>