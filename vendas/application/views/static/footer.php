<footer class="footer">
    <div class="container-fluid">
        <div class="copyright pull-right">
            &copy; <?= date("Y") ?> | Sistema OCPT Evve Comunicação | v1.0.0
        </div>
    </div>
</footer>