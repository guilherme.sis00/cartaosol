<div class="modal fade" id="modalAdicionarCategoria" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Adicionar categoria</h4>
            </div>
            <div class="modal-body">
                <form id="formAdicionarCategoria">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalAdicionarCategoriaCategoria">Categoria</label>
                                <input type="text" id="modalAdicionarCategoriaCategoria" name="modalAdicionarCategoriaCategoria" class="form-control border-input" placeholder="Digite o nome da cidade">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalAdicionarCategoriaTipo">Tipo</label>
                                <select id="modalAdicionarCategoriaTipo" name="modalAdicionarCategoriaTipo" class="form-control border-input">
                                    <option value="">--</option>
                                    <option value="entrada">Entrada</option>
                                    <option value="saida">Saída</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top:20px">
                        <div class="col-md-12 col-xs-12">
                            <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarCategoriaBtnAdicionar">Adicionar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>