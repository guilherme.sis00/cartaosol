<?php $this->load->view("static/header", ["title" => "Especialidade"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "especialidades"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Especialidade / Atualizar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAtualizarEspecialidade">
                                        <input type="hidden" id="id" name="id" value="<?= $especialidade->id ?>">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="especialidade">Especialidade</label>
                                                    <input type="text" id="especialidade" name="especialidade" class="form-control border-input" placeholder="Digite a especialidade" value="<?= $especialidade->especialidade ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="tipo">Tipo</label>
                                                    <select id="tipo" name="tipo" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($tiposEspecialidades as $tipoEspecialidade) : ?>
                                                            <?php if ($tipoEspecialidade->id == $especialidade->idTipoEspecialidade): ?>
                                                                <option selected="selected" value="<?= $tipoEspecialidade->id ?>"><?= $tipoEspecialidade->tipo ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $tipoEspecialidade->id ?>"><?= $tipoEspecialidade->tipo ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-xs-12">
                                                <a href="<?= base_url("especialidades") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAtualizarEspecialidadeBtnAtualizar">Atualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/especialidades/atualizar.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>