<?php $this->load->view("static/header", ["title" => "Cidades"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "cidades"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Cidades"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-<?= $this->session->flashdata("tipo") ?>" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <form id="formBuscarCidade">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="termo" name="termo" class="form-control border-input" placeholder="Digite um termo de busca">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <select id="tipoBusca" name="tipoBusca" class="form-control border-input">
                                                        <option value="estados.estado">Estado</option>
                                                        <option value="cidades.cidade">Cidade</option>
                                                        <!--<option value="convenios_descontos.tipo">Tipo</option>-->
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarCidadeBtnBuscar">Buscar</button>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("cidade/adicionar") ?>" class="btn btn-success btn-fill btn-block">Adicionar</a>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Estado</th>
                                                        <th>Cidade</th>
                                                        <!--<th>Tipo</th>-->
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaCidades">
                                                    <?php foreach ($cidades as $cidade) : ?>
                                                        <tr>
                                                            <td><?= $cidade->estado ?></td>
                                                            <td><?= $cidade->cidade ?></td>
                                                            <!--<td><?php // ucfirst($cidade->tipo) ?></td>-->
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#" data-id="<?= $cidade->id ?>" data-toggle="modal" data-target="#modalVisualizarCidade">Visualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="<?= base_url("cidade/{$cidade->id}") ?>">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="<?= base_url("cidade/deletar/{$cidade->id}") ?>">Deletar</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" id="divPaginacao">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8">
                                            <?= $paginacao ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <div class="modal fade" id="modalVisualizarCidade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Visualizar cidade</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarCidadeEstado">Estado</label>
                                <input type="text" disabled id="modalVisualizarCidadeEstado" name="modalVisualizarCidadeEstado" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarCidadeCidade">Cidade</label>
                                <input type="text" disabled id="modalVisualizarCidadeCidade" name="modalVisualizarCidadeCidade" class="form-control border-input">
                            </div>
                        </div>
<!--                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarCidadeTipo">Tipo</label>
                                <input type="text" disabled id="modalVisualizarCidadeTipo" name="modalVisualizarCidadeTipo" class="form-control border-input">
                            </div>
                        </div>-->
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-primary btn-fill" id="modalVisualizarCidadeBtnAtualizar">Atualizar</a>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/cidades/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
