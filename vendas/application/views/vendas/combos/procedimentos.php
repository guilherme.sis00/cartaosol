<?php $this->load->view("static/header", ["title" => "Combos"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "vendas"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Vendas / Combos / Atualizar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAtualizarProcedimentos">
                                        <input type="hidden" id="id" name="id" value="<?= $combo ?>">
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <span><strong>* Itens mensurados por quantidade, informe o valor padrão 0,00</strong></span></br>
                                                <span><strong>* Itens mensurados por valor, informe a quantidade padrão 0</strong></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="procedimento">Procedimento</label>
                                                    <select id="procedimento" name="procedimento" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($especialidades as $especialidade) : ?>
                                                            <option value="<?= $especialidade->id ?>"><?= $especialidade->especialidade ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="quantidade">Quantidade</label>
                                                    <input id="quantidade" name="quantidade" class="form-control border-input" placeholder="0" value="0" title="Informe o valor 0 caso o item não seja medido por quantidade">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="valor">Valor</label>
                                                    <input id="valor" name="valor" class="form-control border-input" placeholder="0,00" value="0,00" title="Informe o valor 0,00 caso o item não seja medido por preço">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAtualizarProcedimentosBtnAdicionar">Adicionar</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12 content table-responsive">
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Procedimento</th>
                                                            <th>Quantidade</th>
                                                            <th>Valor</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tabelaItens">
                                                        <?php foreach ($especialidadesCombo as $especialidade) : ?>
                                                            <tr id="<?= $especialidade->id ?>">
                                                                <td><?= $especialidade->idEspecialidade ?></td>
                                                                <td><?= $especialidade->especialidade ?></td>
                                                                <td><?= $especialidade->quantidade ?></td>
                                                                <td><?= number_format($especialidade->valor, 2, ",", ".") ?></td>
                                                                <td><button type="button" data-id="<?= $especialidade->id ?>" class="btn-deletar btn btn-small btn-fill btn-danger"><i class='ti-close'></i></button></td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("vendas/combos") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/vendas/combos/procedimentos.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
