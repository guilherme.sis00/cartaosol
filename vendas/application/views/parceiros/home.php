<?php $this->load->view("static/header", ["title" => "Parceiros"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "parceiros"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Parceiros"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-warning" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <form id="formBuscarParceiro">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="termo" name="termo" class="form-control border-input" placeholder="Digite um termo de busca">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <select id="tipoBusca" name="tipoBusca" class="form-control border-input">
                                                        <option value="nome">Nome</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarParceiroBtnBuscar">Buscar</button>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("parceiro/adicionar") ?>" class="btn btn-success btn-fill btn-block">Adicionar</a>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nome</th>
                                                        <th>Endereço</th>
                                                        <th>Estado</th>
                                                        <th>Telefone</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaParceiros">
                                                    <?php foreach ($parceiros as $parceiro) : ?>
                                                        <tr>
                                                            <td><?= $parceiro->nome ?></td>
                                                            <td><?= $parceiro->endereco ?></td>
                                                            <td><?= $parceiro->estado ?></td>
                                                            <td><?= $parceiro->telefone ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#" data-id="<?= $parceiro->id ?>" data-toggle="modal" data-target="#modalVisualizarParceiro">Visualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="<?= base_url("parceiro/{$parceiro->id}") ?>">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" data-id="<?= $parceiro->id ?>" data-parceiro="<?= $parceiro->nome ?>" data-logo="<?= $parceiro->logo ?>" data-toggle="modal" data-target="#modalAlterarLogo">Alterar logo</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="<?= base_url("parceiro/deletar/{$parceiro->id}") ?>">Deletar</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" id="divPaginacao">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8">
                                            <?= $paginacao ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <div class="modal fade" id="modalVisualizarParceiro" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Visualizar cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarParceiroNome">Nome</label>
                                <input type="text" disabled id="modalVisualizarParceiroNome" name="modalVisualizarParceiroNome" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarParceiroCidade">Cidade</label>
                                <input type="text" disabled id="modalVisualizarParceiroCidade" name="modalVisualizarParceiroCidade" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarParceiroEstado">Estado</label>
                                <input type="text" disabled id="modalVisualizarParceiroEstado" name="modalVisualizarParceiroEstado" class="form-control border-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarParceiroEndereco">Endereço</label>
                                <input type="text" disabled id="modalVisualizarParceiroEndereco" name="modalVisualizarParceiroEndereco" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarParceiroParceiro">Parceiro</label>
                                <input type="text" disabled id="modalVisualizarParceiroParceiro" name="modalVisualizarParceiroParceiro" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarParceiroClinica">Clínica</label>
                                <input type="text" disabled id="modalVisualizarParceiroClinica" name="modalVisualizarParceiroClinica" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarParceiroExame">Exames</label>
                                <input type="text" disabled id="modalVisualizarParceiroExame" name="modalVisualizarParceiroExame" class="form-control border-input">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom:20px">
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12 hidden" id="divEspecialidades">
                                <label for="modalVisualizarParceiroEspecialidades">Exame</label>
                                <textarea disabled class="form-control border-input" id="modalVisualizarParceiroEspecialidades" name="modalVisualizarParceiroEspecialidades"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <strong><a id="modalVisualizarParceiroLogo" target="_blank" href="#">Visualizar logo</a></strong>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarParceiroTelefone">Telefone</label>
                                <input type="text" disabled id="modalVisualizarParceiroTelefone" name="modalVisualizarParceiroTelefone" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarParceiroWhatsapp">Whatsapp</label>
                                <input type="text" disabled id="modalVisualizarParceiroWhatsapp" name="modalVisualizarParceiroWhatsapp" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarParceiroEmail">E-mail</label>
                                <input type="text" disabled id="modalVisualizarParceiroEmail" name="modalVisualizarParceiroEmail" class="form-control border-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarParceiroEmailicao">Descrição</label>
                                <textarea disabled class="form-control border-input" rows="5" id="modalVisualizarParceiroDescricao" name="modalVisualizarParceiroDescricao"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-primary btn-fill" id="modalVisualizarParceiroBtnAtualizar">Atualizar</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAlterarLogo" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Alterar logo</h4>
                </div>
                <form id="formAlterarLogo">
                    <input type="hidden" id="modalAlterarLogoIdParceiro" name="modalAlterarLogoIdParceiro">
                    <input type="hidden" id="modalAlterarLogoLogoAtual" name="modalAlterarLogoLogoAtual">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAlterarLogoParceiro">Parceiro</label>
                                    <input type="text" disabled id="modalAlterarLogoParceiro" name="modalAlterarLogoParceiro" class="form-control border-input">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAlterarLogoLogo">Logo</label>
                                    <input type="file" id="modalAlterarLogoLogo" name="modalAlterarLogoLogo">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button href="#" class="btn btn-primary btn-fill" id="formAlterarLogoBtnAlterar">Alterar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/parceiros/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
