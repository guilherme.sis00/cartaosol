<?php $this->load->view("static/header", ["title" => "Administrativo - Dados da empresa"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "dados-empresa"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Dados da empresa"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAtualizarDadosEmpresa">
                                        <input type="hidden" id="id" name="id" value="<?= $dados->id ?>">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nomeFantasia">Nome fantasia</label>
                                                    <input type="text" id="nomeFantasia" name="nomeFantasia" class="form-control border-input" placeholder="Digite o nome fantasia" value="<?= $dados->nomeFantasia ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="razaoSocial">Razão Social</label>
                                                    <input type="text" id="razaoSocial" name="razaoSocial" class="form-control border-input" placeholder="Digite a razão social " value="<?= $dados->razaoSocial ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cnpj">CNPJ</label>
                                                    <input type="text" id="cnpj" name="cnpj" class="form-control border-input" placeholder="00.000.000/0000-00" value="<?= $dados->cnpj ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="carteira">Carteira</label>
                                                    <input type="text" id="carteira" name="carteira" class="form-control border-input" placeholder="000" value="<?= $dados->carteira ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="conta">Conta</label>
                                                    <input type="text" id="conta" name="conta" class="form-control border-input" placeholder="00000000-0" value="<?= $dados->conta ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="agencia">Agência</label>
                                                    <input type="text" id="agencia" name="agencia" class="form-control border-input" placeholder="0000-0" value="<?= $dados->agencia ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="ios">IOS</label>
                                                    <input type="text" id="ios" name="ios" class="form-control border-input" placeholder="0" value="<?= $dados->ios ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="codigoTransmissao">Código de transmissão</label>
                                                    <input type="text" id="codigoTransmissao" name="codigoTransmissao" class="form-control border-input" placeholder="00000000000000" value="<?= $dados->codigoTransmissao ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="codigoPsk">Código PSK</label>
                                                    <input type="text" id="codigoPsk" name="codigoPsk" class="form-control border-input" placeholder="0000000" value="<?= $dados->codigoPsk ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="codigoBanco">Código do banco</label>
                                                    <input type="text" id="codigoBanco" name="codigoBanco" class="form-control border-input" placeholder="000" value="<?= $dados->codigoBanco ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="contaFidc">Conta FIDC</label>
                                                    <input type="text" id="contaFidc" name="contaFidc" class="form-control border-input" placeholder="00000000-0" value="<?= $dados->contaFidc ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="telefone">Telefone</label>
                                                    <input type="text" id="telefone" name="telefone" class="form-control border-input" placeholder="(00) 0000-0000" value="<?= $dados->telefone ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="celular">Celular</label>
                                                    <input type="text" id="celular" name="celular" class="form-control border-input" placeholder="(00) 00000-0000" value="<?= $dados->celular ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="email">E-mail</label>
                                                    <input type="text" id="email" name="email" class="form-control border-input" placeholder="_@_._" value="<?= $dados->email ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="instrucao1">Instrução 1</label>
                                                    <input type="text" id="instrucao1" name="instrucao1" class="form-control border-input" placeholder="Instrução 1" value="<?= $dados->instrucao1 ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="instrucao2">Instrução 2</label>
                                                    <input type="text" id="instrucao2" name="instrucao2" class="form-control border-input" placeholder="Instrução 2" value="<?= $dados->instrucao2 ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="instrucao3">Instrução 3</label>
                                                    <input type="text" id="instrucao3" name="instrucao3" class="form-control border-input" placeholder="Instrução 3" value="<?= $dados->instrucao3 ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="instrucao4">Instrução 4</label>
                                                    <input type="text" id="instrucao4" name="instrucao4" class="form-control border-input" placeholder="Instrução 4" value="<?= $dados->instrucao4 ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="instrucao5">Instrução 5</label>
                                                    <input type="text" id="instrucao5" name="instrucao5" class="form-control border-input" placeholder="Instrução 5" value="<?= $dados->instrucao5 ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cep">CEP</label>
                                                    <input type="text" id="cep" name="cep" class="form-control border-input" placeholder="00000-000" value="<?= $dados->cep ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="logradouro">Logradouro</label>
                                                    <input type="text" id="logradouro" name="logradouro" class="form-control border-input" placeholder="Rua ABC" value="<?= $dados->logradouro ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="numero">Número</label>
                                                    <input type="text" id="numero" name="numero" class="form-control border-input" placeholder="00" value="<?= $dados->numero ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="bairro">Bairro</label>
                                                    <input type="text" id="bairro" name="bairro" class="form-control border-input" placeholder="Bairro" value="<?= $dados->bairro ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-b">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="estado">Estado</label>
                                                    <select type="text" id="estado" name="estado" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($estados as $estado) : ?>
                                                            <?php if ($estado->id == $dados->idEstado): ?>
                                                                <option selected="selected" value="<?= $estado->id ?>"><?= $estado->estado ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $estado->id ?>"><?= $estado->estado ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cidade">Cidade</label>
                                                    <select type="text" id="cidade" name="cidade" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($cidades as $cidade) : ?>
                                                            <?php if ($cidade->id == $dados->idCidade): ?>
                                                                <option selected="selected" value="<?= $cidade->id ?>"><?= $cidade->cidade ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $cidade->id ?>"><?= $cidade->cidade ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAtualizarDadosEmpresaBtnAtualizar">Atualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <?= date("Y") ?> | Sistema OCTP | v1.0.0
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/administrativo/dados-empresa/atualizar.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
