<?php $this->load->view("static/header", ["title" => "Administrativo - Vendedores"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "vendedores"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Vendedores"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-warning" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <form id="formBuscar">
                                            <div class="col-md-8 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="termo" name="termo" class="form-control border-input" placeholder="Digite um termo de busca">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarBtnBuscar">Buscar</button>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("administrativo/vendedor/adicionar") ?>" class="btn btn-success btn-fill btn-block">Adicionar</a>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nome</th>
                                                        <th>CPF</th>
                                                        <th>Celular</th>
                                                        <th>Status</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaVendedores">
                                                    <?php foreach ($vendedores as $vendedor) : ?>
                                                        <tr id="<?= $vendedor->id ?>">
                                                            <td id="nome-<?= $vendedor->id ?>"><?= $vendedor->nome ?></td>
                                                            <td id="cpf-<?= $vendedor->id ?>"><?= $vendedor->cpf ?></td>
                                                            <td id="celular-<?= $vendedor->id ?>"><?= $vendedor->celular ?></td>
                                                            <td id="status-<?= $vendedor->id ?>"><?= ucfirst($vendedor->status) ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                                        <li><a href="#" data-id="<?= $vendedor->id ?>" data-toggle="modal" data-target="#modalAtualizarVendedor">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" data-id-usuario="<?= $vendedor->idUsuario ?>" data-toggle="modal" data-target="#modalAtualizarSenha">Atualizar senha</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" class="btn-atualizar-status" data-id="<?= $vendedor->id ?>" data-id-usuario="<?= $vendedor->idUsuario ?>" data-status="ativo">Ativar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" class="btn-atualizar-status" data-id="<?= $vendedor->id ?>" data-id-usuario="<?= $vendedor->idUsuario ?>" data-status="inativo">Desativar</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" id="divPaginacao">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8">
                                            <?= $paginacao ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <div class="modal fade" id="modalAtualizarVendedor" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Atualizar vendedor</h4>
                </div>
                <form id="formAtualizarVendedor">
                    <input type="hidden" id="modalAtualizarVendedorId" name="modalAtualizarVendedorId">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAtualizarVendedorNome">Nome</label>
                                    <input type="text" id="modalAtualizarVendedorNome" name="modalAtualizarVendedorNome" class="form-control border-input" placeholder="Nome do vendedor">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAtualizarVendedorCpf">CPF</label>
                                    <input type="text" id="modalAtualizarVendedorCpf" name="modalAtualizarVendedorCpf" class="form-control border-input" placeholder="000.000.000-00">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAtualizarVendedorCelular">Celular</label>
                                    <input type="text" id="modalAtualizarVendedorCelular" name="modalAtualizarVendedorClienteCelular" class="form-control border-input" placeholder="(00) 00000-0000">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-fill btn-block" id="formAtualizarVendedorBtnAtualizar">Atualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAtualizarSenha" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Atualizar vendedor</h4>
                </div>
                <form id="formAtualizarSenha">
                    <input type="hidden" id="modalAtualizarSenhaId" name="modalAtualizarSenhaId">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAtualizarSenhaSenha">Nova senha</label>
                                    <input type="password" id="modalAtualizarSenhaSenha" name="modalAtualizarSenhaSenha" class="form-control border-input" placeholder="Nova senha">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAtualizarSenhaRepetirSenha">Repetir senha</label>
                                    <input type="password" id="modalAtualizarSenhaRepetirSenha" name="modalAtualizarSenhaRepetirSenha" class="form-control border-input" placeholder="Repetir senha">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-fill btn-block" id="formAtualizarSenhaBtnAtualizar">Atualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/administrativo/vendedores/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
