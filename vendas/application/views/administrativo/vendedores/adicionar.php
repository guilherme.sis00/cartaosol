<?php $this->load->view("static/header", ["title" => "Administrativo - Vendedores"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "clientes"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Vendedores / Adicionar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAdicionarVendedor">
                                        <div class="row">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" placeholder="Nome do vendedor">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cpf">CPF</label>
                                                    <input type="text" id="cpf" name="cpf" class="form-control border-input" placeholder="000.000.000-00">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="celular">Celular</label>
                                                    <input type="text" id="celular" name="celular" class="form-control border-input" placeholder="(00) 00000-0000">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-b">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="usuario">Usuário</label>
                                                    <input type="text" id="usuario" name="usuario" class="form-control border-input" placeholder="_@_._">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="senha">Senha</label>
                                                    <input type="password" id="senha" name="senha" class="form-control border-input" placeholder="Senha do vendedor">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="repetirSenha">Repetir senha</label>
                                                    <input type="password" id="repetirSenha" name="repetirSenha" class="form-control border-input" placeholder="Repita a senha">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("administrativo/vendedores") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarVendedorBtnAdicionar">Adicionar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <?= date("Y") ?> | Sistema OCTP | v1.0.0
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/administrativo/vendedores/adicionar.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
