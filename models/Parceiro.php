<?php

require_once($_SERVER["DOCUMENT_ROOT"] . '/config/Database.php');

class Parceiro {

    private $database;

    public function __construct() {
        $this->database = new Database();
    }

    public function getAll() {
        $sql = "SELECT parceiros.*, estados.estado,estados.uf, cidades.cidade "
                . "FROM ocpt_parceiros AS parceiros "
                . "JOIN ocpt_estados AS estados ON parceiros.idEstado=estados.id "
                . "JOIN ocpt_cidades AS cidades ON parceiros.idCidade=cidades.id "
                . "WHERE parceiro=1";

        $response = mysqli_query($this->database->conexao, $sql);

        if (mysqli_num_rows($response) > 0) {
            return ["resultado" => TRUE, "parceiros" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Não foi encontrado nenhum parceiro"];
        }
    }
    
    public function getParceirosById() {
        $sql = "SELECT especialidade.especialidade, tipoEspecialidade.tipo "
                . "FROM ocpt_especialidades as especialidade "
                . "JOIN ocpt_tipos_especialidades AS tipoEspecialidade ON especialidade.idTipoEspecialidade=tipoEspecialidade.id "
                . "ORDER BY tipoEspecialidade.tipo";

        $response = mysqli_query($this->database->conexao, $sql);

        if (!empty($response)) {
            return ["resultado" => TRUE, "exames" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Não foi encontrado nenhuma especialidade"];
        }
    }

    public function getByEspecialidade($especialidade) {
        $sql = "SELECT parceiros.*, estados.estado,estados.uf, cidades.cidade "
                . "FROM ocpt_parceiros AS parceiros "
                . "JOIN ocpt_parceiros_especialidades AS parceirosEspecialidades ON parceiros.id=parceirosEspecialidades.idParceiro "
                . "JOIN ocpt_estados AS estados ON parceiros.idEstado=estados.id "
                . "JOIN ocpt_cidades AS cidades ON parceiros.idCidade=cidades.id "
                . "WHERE parceiros.exame = 1 AND parceirosEspecialidades.idEspecialidade={$especialidade}";

        $response = mysqli_query($this->database->conexao, $sql);

        if (mysqli_num_rows($response) > 0) {
            return ["resultado" => TRUE, "parceiros" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Não foi encontrado nenhum parceiros"];
        }
    }

}
