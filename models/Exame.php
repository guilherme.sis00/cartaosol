<?php

require_once($_SERVER["DOCUMENT_ROOT"] . '/config/Database.php');

class Exame {

    private $database;

    public function __construct() {
        $this->database = new Database();
    }

    public function getExames() {
        $sql = "SELECT especialidade.id, especialidade.especialidade, tipoEspecialidade.tipo "
                . "FROM ocpt_especialidades as especialidade "
                . "JOIN ocpt_tipos_especialidades AS tipoEspecialidade ON especialidade.idTipoEspecialidade=tipoEspecialidade.id "
                . "ORDER BY tipoEspecialidade.tipo";

//        var_dump(mysqli_errno($this->database->conexao));
        $response = mysqli_query($this->database->conexao, $sql);

        if (!empty($response)) {
            return ["resultado" => TRUE, "exames" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Não foi encontrado nenhuma especialidade"];
        }
    }

}
