<?php

require_once($_SERVER["DOCUMENT_ROOT"] . '/config/Database.php');

class Clinica {

    private $database;

    public function __construct() {
        $this->database = new Database();
    }

    public function getAll() {
        $sql = "SELECT parceiros.*, estados.estado,estados.uf, cidades.cidade "
                . "FROM ocpt_parceiros AS parceiros "
                . "JOIN ocpt_estados AS estados ON parceiros.idEstado=estados.id "
                . "JOIN ocpt_cidades AS cidades ON parceiros.idCidade=cidades.id "
                . "WHERE clinica=1";

        $response = mysqli_query($this->database->conexao, $sql);

        if (mysqli_num_rows($response) > 0) {
            return ["resultado" => TRUE, "clinicas" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Não foi encontrado nenhuma clínica"];
        }
    }

}
