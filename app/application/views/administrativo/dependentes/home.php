<?php $this->load->view("static/header", ["title" => "Administrativo - Dependentes"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "dependentes"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Dependentes"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-warning" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <form id="formBuscar">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" placeholder="Nome do dependente">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="titular" name="titular" class="form-control border-input" placeholder="Nome do títular">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="cpf" name="cpf" class="form-control border-input" placeholder="000.000.000-00">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12 col-md-offset-8">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarBtnBuscar">Buscar</button>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("administrativo/dependente/adicionar") ?>" class="btn btn-success btn-fill btn-block">Adicionar</a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Farol</th>
                                                        <th>Nome</th>
                                                        <th>Títular</th>
                                                        <th>Data de vinculação</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaDependentes">
                                                    <?php foreach ($dependentes as $dependente) : ?>
                                                        <tr id="<?= $dependente->id ?>">
                                                            <td class="text-center"><i class="fa fa-circle" style="color:<?= $farois[$dependente->id] ?>"></i></td>
                                                            <td id="nome-<?= $dependente->id ?>"><?= $dependente->nome ?></td>
                                                            <td id="titular-<?= $dependente->id ?>"><?= $dependente->titular ?></td>
                                                            <td id="data-vinculacao-<?= $dependente->id ?>"><?= date("d/m/Y", strtotime($dependente->dataVinculacao)) ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                                        <li><a href="#" data-id="<?= $dependente->id ?>" data-toggle="modal" data-target="#modalVisualizarDependente">Visualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="<?= base_url("administrativo/dependente/{$dependente->id}") ?>">Atualizar</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" id="divPaginacao">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8">
                                            <?= $paginacao ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <div class="modal fade" id="modalVisualizarDependente" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Visualizar dependente</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarDependenteNome">Nome</label>
                                <input type="text" disabled id="modalVisualizarDependenteNome" name="modalVisualizarDependenteNome" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarDependenteTitular">Títular</label>
                                <input type="text" disabled id="modalVisualizarDependenteTitular" name="modalVisualizarDependenteTitular" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarDependenteCpf">CPF</label>
                                <input type="text" disabled id="modalVisualizarDependenteCpf" name="modalVisualizarDependenteCpf" class="form-control border-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarDependenteNumeroCartao">Numero do cartão</label>
                                <input type="text" disabled id="modalVisualizarDependenteNumeroCartao" name="modalVisualizarDependenteNumeroCartao" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarDependenteDataNascimento">Data de nascimento</label>
                                <input type="text" disabled id="modalVisualizarDependenteDataNascimento" name="modalVisualizarDependenteDataNascimento" class="form-control border-input">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="modalVisualizarDependenteDataVinculacao">Data de vinculação</label>
                                <input type="text" disabled id="modalVisualizarDependenteDataVinculacao" name="modalVisualizarDependenteDataVinculacao" class="form-control border-input">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-primary btn-fill" id="modalVisualizarDependenteBtnAtualizar">Atualizar</a>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/administrativo/dependentes/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>