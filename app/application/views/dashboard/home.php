<?php $this->load->view("static/header", ["title" => "Dashboard"]) ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "dashboard"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Dashboard"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <div class="row">
                                        <form id="formConsultar">
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <select id="mes" name="mes" class="form-control border-input">
                                                        <option <?= date("m") == 1 ? "selected='seleted'" : "" ?> value="01">Janeiro</option>
                                                        <option <?= date("m") == 2 ? "selected='seleted'" : "" ?> value="02">Fevereiro</option>
                                                        <option <?= date("m") == 3 ? "selected='seleted'" : "" ?> value="03">Março</option>
                                                        <option <?= date("m") == 4 ? "selected='seleted'" : "" ?> value="04">Abril</option>
                                                        <option <?= date("m") == 5 ? "selected='seleted'" : "" ?> value="05">Maio</option>
                                                        <option <?= date("m") == 6 ? "selected='seleted'" : "" ?> value="06">Junho</option>
                                                        <option <?= date("m") == 7 ? "selected='seleted'" : "" ?> value="07">Julho</option>
                                                        <option <?= date("m") == 8 ? "selected='seleted'" : "" ?> value="08">Agosto</option>
                                                        <option <?= date("m") == 9 ? "selected='seleted'" : "" ?> value="09">Setembro</option>
                                                        <option <?= date("m") == 10 ? "selected='seleted'" : "" ?> value="10">Outubro</option>
                                                        <option <?= date("m") == 11 ? "selected='seleted'" : "" ?> value="11">Novembro</option>
                                                        <option <?= date("m") == 12 ? "selected='seleted'" : "" ?> value="12">Dezembro</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="ano" name="ano" class="form-control border-input" value="<?= date("Y") ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formConsultarBtnConsultar">Consultar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <div class="card">
                                <div class="content">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <h1 id="saldoInicial">R$ <?= number_format($saldoInicial, 2, ",", ".") ?></h1>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <hr/>
                                        <div class="stats">
                                            Saldo inicial de <span class="mesNominal"><?= $mesAtual ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <div class="card">
                                <div class="content">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <h1 id="receitas">R$ <?= number_format($receitas, 2, ",", ".") ?></h1>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <hr/>
                                        <div class="stats">
                                            Receitas de <span class="mesNominal"><?= $mesAtual ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="card">
                                <div class="content">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <h1 id="despesas">R$ <?= number_format($despesas, 2, ",", ".") ?></h1>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <hr/>
                                        <div class="stats">
                                            Despesas de <span class="mesNominal"><?= $mesAtual ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="card">
                                <div class="content">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <h1 id="saldo">R$ <?= number_format(($receitas - $despesas), 2, ",", ".") ?></h1>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <hr/>
                                        <div class="stats">
                                            Saldo de <span class="mesNominal"><?= $mesAtual ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <div class="card">
                                <div class="content">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <h1 id="saldoTotal">R$ <?= number_format($saldoTotal, 2, ",", ".") ?></h1>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <hr/>
                                        <div class="stats">
                                            Saldo total
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<?php $this->load->view("static/scripts") ?>
<script src="<?= base_url("assets/js/app/dashboard/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
