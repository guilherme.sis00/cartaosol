<?php $this->load->view("static/header", ["title" => "Parceiro / Usuários"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "parceiros"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Parceiro / Usuários"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAdicionarUsuarioParceiro">
                                        <input type="hidden" id="idParceiro" name="idParceiro" value="<?= $parceiro->id ?>">
                                        <div class="row">
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nome">Parceiro</label>
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" value="<?= $parceiro->nome ?>" disabled="disabled">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" placeholder="Digite o nome do usuário">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="usuario">Usuário</label>
                                                    <input type="text" id="usuario" name="usuario" class="form-control border-input" placeholder="email@email.com">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="senha">Senha</label>
                                                    <input type="password" id="senha" name="senha" class="form-control border-input" placeholder="Digite a senha">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="repetirSenha">Repetir senha</label>
                                                    <input type="password" id="repetirSenha" name="repetirSenha" class="form-control border-input" placeholder="Repita a senha">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12 col-md-offset-8">
                                                <a href="<?= base_url("parceiros") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarUsuarioParceiroBtnAdicionar">Adicionar</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12 content table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Nome</th>
                                                            <th>Usuário</th>
                                                            <th class="text-center">Status</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tabelaUsuariosParceiros">
                                                        <?php foreach ($usuarios as $usuario) : ?>
                                                            <tr id="<?= $usuario->id ?>">
                                                                <td id="nome-<?= $usuario->id ?>"><?= $usuario->nome ?></td>
                                                                <td id="usuario-<?= $usuario->id ?>"><?= $usuario->usuario ?></td>
                                                                <td id="status-<?= $usuario->id ?>" class="text-center"><?= ucfirst($usuario->status) ?></td>
                                                                <td>
                                                                    <div class="btn-group">
                                                                        <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            Ações <span class="caret"></span>
                                                                        </button>
                                                                        <ul class="dropdown-menu">
                                                                            <li><a href="#" data-id="<?= $usuario->id ?>" data-status="ativo" class="btn-status">Ativar</a></li>
                                                                            <li role="separator" class="divider"></li>
                                                                            <li><a href="#" data-id="<?= $usuario->id ?>" data-status="bloqueado" class="btn-status">Bloquear</a></li>
                                                                            <li role="separator" class="divider"></li>
                                                                            <li><a href="#" data-id="<?= $usuario->id ?>" class="btn-deletar">Deletar</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/parceiros/usuarios.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
