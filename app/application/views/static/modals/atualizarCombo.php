<div class="modal fade" id="modalAtualizarCombo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Atualizar combo</h4>
            </div>
            <form id="formAtualizarCombo">
                <input type="hidden" id="modalAtualizarComboId" name="modalAtualizarComboId">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12" id="modalAtualizarComboLoader">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalAtualizarComboNome">Nome</label>
                                <input type="text" id="modalAtualizarComboNome" name="modalAtualizarComboNome" class="form-control border-input" placeholder="Nome do combo">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalAtualizarComboValor">Valor</label>
                                <input type="text" id="modalAtualizarComboValor" name="modalAtualizarComboValor" class="form-control border-input" placeholder="0,00">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-fill" id="formAtualizarComboBtnAtualizar">Atualizar</button>
                </div>
            </form>
        </div>
    </div>
</div>