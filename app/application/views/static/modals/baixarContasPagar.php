<div class="modal fade" id="modalBaixarContaPagar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Baixar conta a pagar</h4>
            </div>
            <form id="formBaixarContaPagar">
                <input type="hidden" id="modalBaixarContaPagarId" name="modalBaixarContaPagarId">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12" id="modalBaixarContaPagarLoader">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaReceberPagarValor">Valor</label>
                                        <input type="text" disabled="disabled" id="modalBaixarContaReceberPagarValor" name="modalBaixarContaReceberPagarValor" class="form-control border-input" placeholder="R$ 00,00">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaPagarVencimento">Vencimento</label>
                                        <input type="text" disabled="disabled" id="modalBaixarContaPagarVencimento" name="modalBaixarContaPagarVencimento" class="form-control border-input" placeholder="00/00/0000">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaPagarDescricao">Descrição</label>
                                        <textarea type="text" disabled="disabled" id="modalBaixarContaPagarDescricao" name="modalBaixarContaPagarDescricao" class="form-control border-input" rows="3" placeholder="Descrição"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaPagarPagamento">Pagamento</label>
                                        <input type="text" id="modalBaixarContaPagarPagamento" name="modalBaixarContaPagarPagamento" class="form-control border-input" placeholder="00/00/0000">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaPagarFormaPagamento">Forma da pagamento</label>
                                        <select id="modalBaixarContaPagarFormaPagamento" name="modalBaixarContaPagarFormaPagamento" class="form-control border-input">
                                            <option value="">--</option>
                                            <option value="dinheiro">Dinheiro</option>
                                            <option value="boleto">Boleto</option>
                                            <option value="deposito">Depósito</option>
                                            <option value="cartao-credito">Cartão de crédito</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaPagarConta">Conta</label>
                                        <select id="modalBaixarContaPagarConta" name="modalBaixarContaPagarConta" class="form-control border-input">
                                            <option value="">--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-fill" id="formBaixarContaPagarBtnBaixar">Baixar</button>
                </div>
            </form>
        </div>
    </div>
</div>