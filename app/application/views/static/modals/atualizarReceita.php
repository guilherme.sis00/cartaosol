<div class="modal fade" id="modalAtualizarReceita" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Atualizar despesa</h4>
            </div>
            <form id="formAtualizarReceita">
                <input type="hidden" id="modalAtualizarReceitaId" name="modalAtualizarReceitaId">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12" id="modalAtualizarReceitaLoader">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalAtualizarReceitaValor">Valor</label>
                                <input type="text" id="modalAtualizarReceitaValor" name="modalAtualizarReceitaValor" class="form-control border-input" disabled="disabled">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalAtualizarReceitaBaixa">Baixa</label>
                                <input type="text" id="modalAtualizarReceitaBaixa" name="modalAtualizarReceitaBaixa" class="form-control border-input" placeholder="00/00/0000">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-fill" id="formAtualizarReceitaBtnAtualizar">Atualizar</button>
                </div>
            </form>
        </div>
    </div>
</div>