<div class="modal fade" id="modalAtualizarDespesa" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Atualizar despesa</h4>
            </div>
            <form id="formAtualizarDespesa">
                <input type="hidden" id="modalAtualizarDespesaId" name="modalAtualizarDespesaId">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12" id="modalAtualizarDespesaLoader">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalAtualizarDespesaValor">Valor</label>
                                <input type="text" id="modalAtualizarDespesaValor" name="modalAtualizarDespesaValor" class="form-control border-input" disabled="disabled">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalAtualizarDespesaBaixa">Baixa</label>
                                <input type="text" id="modalAtualizarDespesaBaixa" name="modalAtualizarDespesaBaixa" class="form-control border-input" placeholder="00/00/0000">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-fill" id="formAtualizarDespesaBtnAtualizar">Atualizar</button>
                </div>
            </form>
        </div>
    </div>
</div>