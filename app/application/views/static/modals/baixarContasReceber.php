<div class="modal fade" id="modalBaixarContaReceber">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Baixar conta a receber</h4>
            </div>
            <form id="formBaixarContaReceber">
                <input type="hidden" id="modalBaixarContaId" name="modalBaixarContaId">
                <input type="hidden" id="modalBaixarContaIdParcela" name="modalBaixarContaIdParcela">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12" id="modalBaixarContaReceberLoader">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaReceberValor">Valor</label>
                                        <input type="text" disabled="disabled" id="modalBaixarContaReceberValor" name="modalBaixarContaReceberValor" class="form-control border-input" placeholder="R$ 00,00">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaReceberVencimento">Vencimento</label>
                                        <input type="text" disabled="disabled" id="modalBaixarContaReceberVencimento" name="modalBaixarContaReceberVencimento" class="form-control border-input" placeholder="00/00/0000">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaReceberDescricao">Descrição</label>
                                        <textarea type="text" disabled="disabled" id="modalBaixarContaReceberDescricao" name="modalBaixarContaReceberDescricao" class="form-control border-input" rows="3" placeholder="Descrição"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaReceberPagamento">Pagamento</label>
                                        <input type="text" id="modalBaixarContaReceberPagamento" name="modalBaixarContaReceberPagamento" class="form-control border-input" placeholder="00/00/0000">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaReceberFormaPagamento">Forma da pagamento</label>
                                        <select id="modalBaixarContaReceberFormaPagamento" name="modalBaixarContaReceberFormaPagamento" class="form-control border-input">
                                            <option value="">--</option>
                                            <option value="dinheiro">Dinheiro</option>
                                            <option value="boleto">Boleto</option>
                                            <option value="deposito">Depósito</option>
                                            <option value="cartao-credito">Cartão de crédito</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="modalBaixarContaReceberContas">Conta</label>
                                        <select id="modalBaixarContaReceberContas" name="modalBaixarContaReceberContas" class="form-control border-input">
                                            <option value="">--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-fill" id="formBaixarContaReceberBtnBaixar">Baixar</button>
                </div>
            </form>
        </div>
    </div>
</div>