<div class="modal fade" id="modalAdicionarFornecedor" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Adicionar fornecedor</h4>
            </div>
            <form id="formAdicionarFornecedor">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="modalAdicionarFornecedorNome">Nome</label>
                                <input type="text" id="modalAdicionarFornecedorNome" name="modalAdicionarFornecedorNome" class="form-control border-input" placeholder="Nome do fornecedor">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-fill" id="formAdicionarFornecedorBtnAdicionar">Adicionar</button>
                </div>
            </form>
        </div>
    </div>
</div>