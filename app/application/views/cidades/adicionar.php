<?php $this->load->view("static/header", ["title" => "Cidade"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "cidades"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Cidade / Adicionar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAdicionarCidade">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="estado">Estado</label>
                                                    <select id="estado" name="estado" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <?php foreach ($estados as $estado) : ?>
                                                            <option value="<?= $estado->id ?>"><?= $estado->estado ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cidade">Cidade</label>
                                                    <select id="cidade" name="cidade" class="form-control border-input">
                                                        <option value="">--</option>
                                                    </select>
                                                </div>
                                            </div>
<!--                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="tipo">Tipo</label>
                                                    <select id="tipo" name="tipo" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <option value="convênio">Convênio</option>
                                                        <option value="desconto">Desconto</option>
                                                    </select>
                                                </div>
                                            </div>-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-xs-12">
                                                <a href="<?= base_url("cidades") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <button type="button" class="btn btn-primary btn-fill btn-block" data-toggle="modal" data-target="#modalAdicionarCidade">Adicionar cidade</button>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarCidadeBtnAdicionar">Adicionar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <div class="modal fade" id="modalAdicionarCidade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Adicionar cidade</h4>
                </div>
                <div class="modal-body">
                    <form id="formAdicionarCidadeModal">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAdicionarCidadeEstado">Estado</label>
                                    <select id="modalAdicionarCidadeEstado" name="modalAdicionarCidadeEstado" class="form-control border-input">
                                        <option value="">--</option>
                                        <?php foreach ($estados as $estado) : ?>
                                            <option value="<?= $estado->id ?>"><?= $estado->estado ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="modalAdicionarCidadeCidade">Cidade</label>
                                    <input type="text" id="modalAdicionarCidadeCidade" name="modalAdicionarCidadeCidade" class="form-control border-input" placeholder="Digite o nome da cidade">
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top:20px">
                            <div class="col-md-12 col-xs-12">
                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarCidadeBtnAdicionar">Adicionar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/cidades/adicionar.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
