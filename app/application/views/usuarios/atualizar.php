<?php $this->load->view("static/header", ["title" => "Usuário / Atualizar"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "usuarios"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Usuário / Atualizar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAtualizarUsuario">
                                        <input type="hidden" id="id" name="id" value="<?= $usuario->id ?>">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" value="<?= $usuario->nome ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="usuario">Usuário</label>
                                                    <input type="text" id="usuario" name="usuario" class="form-control border-input" value="<?= $usuario->usuario ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="status">Status</label>
                                                    <select class="form-control border-input" id="status" name="status">
                                                        <option <?= $usuario->status == "ativo" ? "selected='selected'" : "" ?> value="ativo">Ativo</option>
                                                        <option <?= $usuario->status == "inativo" ? "selected='selected'" : "" ?> value="inativo">Inativo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-xs-12">
                                                <a class="btn btn-default btn-fill btn-block" href="<?= base_url("usuarios") ?>">Voltar</a>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formAtualizarUsuarioBtnAtualizar">Atualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/usuarios/atualizar.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
