<?php $this->load->view("static/header", ["title" => "Cliente"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "usuarios"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Cliente / Adicionar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAdicionarUsuario">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" id="nome" name="nome" class="form-control border-input" placeholder="Digite o nome do usuário">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="usuario">Usuário</label>
                                                    <input type="text" id="usuario" name="usuario" class="form-control border-input" placeholder="Digite o usuário de acesso">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="tipo">Tipo</label>
                                                    <select id="tipo" name="tipo" class="form-control border-input">
                                                        <option value="">--</option>
                                                        <option value="gestor">Gestor</option>
                                                        <option value="financeiro">Financeiro</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="senha">Senha</label>
                                                    <input type="password" id="senha" name="senha" class="form-control border-input" placeholder="Digite a senha">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="repetirSenha">Repetir senha</label>
                                                    <input type="password" id="repetirSenha" name="repetirSenha" class="form-control border-input" placeholder="Repita a senha">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="status">Status</label>
                                                    <select class="form-control border-input" id="status" name="status">
                                                        <option value="ativo">Ativo</option>
                                                        <option value="inativo">Inativo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-xs-12">
                                                <a class="btn btn-default btn-fill btn-block" href="<?= base_url("usuarios") ?>">Voltar</a>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formAdicionarUsuarioBtnAdicionar">Adicionar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/usuarios/adicionar.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>