<?php $this->load->view("static/header", ["title" => "Financeiro - Contas a receber"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "contas-receber"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Contas a receber"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row margin-b">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-warning" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <input type="hidden" id="dataAtual" name="dataAtual" value="<?= date("Y-m-d") ?>">
                                    <form id="formBuscar">
                                        <div class="row">
                                            <div class="col-md-8 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="termo" name="termo" class="form-control border-input" placeholder="Digite o nome do cliente">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="cpf" name="cpf" class="form-control border-input" placeholder="CPF do cliente">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="codigoContrato" name="codigoContrato" class="form-control border-input" placeholder="Código do contrato">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <select id="categoria" name="categoria" class="form-control border-input">
                                                        <option value="todas">Todas</option>
                                                        <?php foreach ($categorias as $categoria) : ?>
                                                            <?php if ($categoria->tipo == "entrada"): ?>
                                                                <option value="<?= $categoria->id ?>"><?= $categoria->categoria ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="dataInicial" name="dataInicial" class="form-control border-input" placeholder="Data inicial">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" id="dataFinal" name="dataFinal" class="form-control border-input" placeholder="Data final">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12 col-md-offset-8 margin-b">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarBtnBuscar">Buscar</button>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <a href="<?= base_url("financeiro/contas-receber/adicionar") ?>" class="btn btn-success btn-fill btn-block">Adicionar</a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8">
                                            <label for="mesContasReceber">Mês atual</label>
                                            <select id="mesContasReceber" name="mesContasReceber" class="form-control border-input">
                                                <option <?= $mesAtual == "Janeiro" ? "selected='selected'" : "" ?>value="01">Janeiro</option>
                                                <option <?= $mesAtual == "Fevereiro" ? "selected='selected'" : "" ?>value="02">Fevereiro</option>
                                                <option <?= $mesAtual == "Março" ? "selected='selected'" : "" ?>value="03">Março</option>
                                                <option <?= $mesAtual == "Abril" ? "selected='selected'" : "" ?>value="04">Abril</option>
                                                <option <?= $mesAtual == "Maio" ? "selected='selected'" : "" ?>value="05">Maio</option>
                                                <option <?= $mesAtual == "Junho" ? "selected='selected'" : "" ?>value="06">Junho</option>
                                                <option <?= $mesAtual == "Julho" ? "selected='selected'" : "" ?>value="07">Julho</option>
                                                <option <?= $mesAtual == "Agosto" ? "selected='selected'" : "" ?>value="08">Agosto</option>
                                                <option <?= $mesAtual == "Setembro" ? "selected='selected'" : "" ?>value="09">Setembro</option>
                                                <option <?= $mesAtual == "Outubro" ? "selected='selected'" : "" ?>value="10">Outubro</option>
                                                <option <?= $mesAtual == "Novembro" ? "selected='selected'" : "" ?>value="11">Novembro</option>
                                                <option <?= $mesAtual == "Dezembro" ? "selected='selected'" : "" ?>value="12">Dezembro</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-xs-12 col-md-offset-8 content table-responsive">
                                            <table class="table">
                                                <tbody id="tabelaContasReceberInformacoes">
                                                    <tr class="danger">
                                                        <td id="">Total pendente</td>
                                                        <td id="total-pendente">R$ <?= number_format($pendente->total, 2, ",", ".") ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Descrição</th>
                                                        <th>Categoria</th>
                                                        <th class="text-center">Valor</th>
                                                        <th class="text-center">Data</th>
                                                        <th>Status</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaContasReceber">
                                                    <?php foreach ($contasReceber as $contaReceber) : ?>
                                                        <tr id="<?= $contaReceber->id ?>" class="<?= (strtotime($contaReceber->data) < strtotime(date("Y-m-d"))) && $contaReceber->status == "pendente" ? "danger" : ($contaReceber->status == "recebido" ? "success" : "") ?>">
                                                            <td id="baixada-<?= $contaReceber->id ?>"><?= $contaReceber->status == "recebido" ? "<i class='ti-arrow-down'></i>" : "" ?></td>
                                                            <td id="descricao-<?= $contaReceber->id ?>"><?= $contaReceber->descricao ?></td>
                                                            <td id="categoria-<?= $contaReceber->id ?>"><?= $contaReceber->categoria ?></td>
                                                            <td class="text-center" id="valor-<?= $contaReceber->id ?>">R$ <?= number_format($contaReceber->valor, 2, ",", ".") ?></td>
                                                            <td class="text-center" id="data-<?= $contaReceber->id ?>"><?= date("d/m/Y", strtotime($contaReceber->data)) ?></td>
                                                            <td id="status-<?= $contaReceber->id ?>"><?= ucfirst($contaReceber->status) ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                                        <li><a href="<?= base_url("financeiro/conta-receber/{$contaReceber->id}") ?>">Atualizar</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a target="_blank" href="<?= base_url("financeiro/contas-receber/boleto/{$contaReceber->id}") ?>">Visualizar boleto</a></li>
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" data-id="<?= $contaReceber->id ?>" data-id-parcela="<?= $contaReceber->idParcela ?>" data-status="recebido" data-toggle="modal" data-target="#modalBaixarContaReceber">Baixar</a></li>
<!--                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" class="btn-status" data-id="<? $contaReceber->id ?>" data-id-parcela="<? $contaReceber->idParcela ?>" data-status="pendente">Pendente</a></li>-->
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="#" class="btn-deletar" data-id="<?= $contaReceber->id ?>">Deletar</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
    <?php $this->load->view("static/modals/baixarContasReceber") ?>
</body>
<script src="<?= base_url("assets/js/app/financeiro/contas-receber/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
