<?php $this->load->view("static/header", ["title" => "Financeiro - Contas a receber"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "contas-receber"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Contas a receber / Adicionar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAdicionarContaReceber">
                                        <div class="row">
                                            <div class="form-group col-md-5 col-xs-12">
                                                <label for="contrato">Contrato</label>
                                                <select id="contrato" name="contrato" class="form-control border-input" autofocus="">
                                                    <option value="">--</option>
                                                    <?php foreach ($contratos as $contrato) : ?>
                                                        <?php if ($contrato->situacao == "ativo" || $contrato->situacao == "inadimplente"): ?>
                                                            <option value="<?= $contrato->id ?>"><?= "{$contrato->codigoContrato} - {$contrato->nome}" ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-9 col-xs-12">
                                                <label for="descricao">Descrição</label>
                                                <input type="text" class="form-control border-input" id="descricao" name="descricao" placeholder="Descrição da entrada">
                                            </div>
                                            <div class="form-group col-md-3 col-xs-12">
                                                <label for="valor">Valor (R$)</label>
                                                <input type="text" class="form-control border-input" id="valor" name="valor" placeholder="0,00">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-4 col-xs-12">
                                                <label for="data">Data</label>
                                                <input type="text" class="form-control border-input" id="data" name="data" value="<?= date("d/m/Y") ?>" placeholder="99/99/9999">
                                            </div>
                                            <div class="form-group col-md-4 col-xs-12">
                                                <label for="categoria">Categoria</label>
                                                <select id="categoria" name="categoria" class="form-control border-input">
                                                    <?php foreach ($categorias as $categoria) : ?>
                                                        <?php if ($categoria->tipo == "entrada"): ?>
                                                            <option value="<?= $categoria->id ?>"><?= $categoria->categoria ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 col-xs-12">
                                                <label for="status">Status</label>
                                                <select id="status" name="status" class="form-control border-input">
                                                    <option value="pendente">Pendente</option>
                                                    <option value="recebido">Recebido</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row margin-b">
                                            <div class="form-group col-md-3 col-xs-12">
                                                <label for="instrucao1">Instrução 1</label>
                                                <input id="instrucao1" name="instrucao1" class="form-control border-input" placeholder="Instrução 1">
                                            </div>
                                            <div class="form-group col-md-3 col-xs-12">
                                                <label for="instrucao2">Instrução 2</label>
                                                <input id="instrucao2" name="instrucao2" class="form-control border-input" placeholder="Instrução 2">
                                            </div>
                                            <div class="form-group col-md-3 col-xs-12">
                                                <label for="instrucao3">Instrução 3</label>
                                                <input id="instrucao3" name="instrucao3" class="form-control border-input" placeholder="Instrução 3">
                                            </div>
                                            <div class="form-group col-md-3 col-xs-12">
                                                <label for="instrucao4">Instrução 4</label>
                                                <input id="instrucao4" name="instrucao4" class="form-control border-input" placeholder="Instrução 4">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <a href="<?= base_url("financeiro/contas-receber") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <a href="#" data-toggle="modal" data-target="#modalAdicionarCategoria" class="btn btn-primary btn-fill btn-block">Categoria</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarContaReceberBtnAdicionar">Adicionar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <?= date("Y") ?> | Sistema OCTP | v1.0.0
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <?php $this->load->view("static/modals/adicionarCategoriaFinanceira") ?>
</body>
<script src="<?= base_url("assets/js/app/financeiro/contas-receber/adicionar.js") ?>"></script>
<script src="<?= base_url("assets/js/app/utils/categoriasFinanceiras.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
