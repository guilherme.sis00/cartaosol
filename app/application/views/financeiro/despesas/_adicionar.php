<?php $this->load->view("static/header", ["title" => "Financeiro - Contas a pagar"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "contas-pagar"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Contas a pagar / Adicionar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAdicionarContaPagar">
                                        <div class="row">
                                            <div class="form-group col-md-6 col-xs-12">
                                                <label for="descricao">Descrição</label>
                                                <input type="text" class="form-control border-input" id="descricao" name="descricao" placeholder="Descrição da saída" autofocus="">
                                            </div>
                                            <div class="form-group col-md-3 col-xs-12">
                                                <label for="fornecedor">Fornecedor</label>
                                                <select class="form-control border-input" id="fornecedor" name="fornecedor">
                                                    <option value="">--</option>
                                                    <?php foreach ($fornecedores as $fornecedor) : ?>
                                                        <option value="<?= $fornecedor->id ?>"><?= $fornecedor->nome ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3 col-xs-12">
                                                <label for="valor">Valor (R$)</label>
                                                <input type="text" class="form-control border-input" id="valor" name="valor" placeholder="0,00">
                                            </div>
                                        </div>
                                        <div class="row margin-b">
                                            <div class="form-group col-md-4 col-xs-12">
                                                <label for="data">Data</label>
                                                <input type="text" class="form-control border-input" id="data" name="data" value="<?= date("d/m/Y") ?>" placeholder="99/99/9999">
                                            </div>
                                            <div class="form-group col-md-4 col-xs-12">
                                                <label for="categoria">Categoria</label>
                                                <select id="categoria" name="categoria" class="form-control border-input">
                                                    <option value="">--</option>
                                                    <?php foreach ($categorias as $categoria) : ?>
                                                        <?php if ($categoria->tipo == "saida"): ?>
                                                            <option value="<?= $categoria->id ?>"><?= $categoria->categoria ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 col-xs-12">
                                                <label for="status">Status</label>
                                                <select id="status" name="status" class="form-control border-input">
                                                    <option value="pendente">Pendente</option>
                                                    <option value="pago">Pago</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <a href="<?= base_url("financeiro/contas-pagar") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <a href="#" data-toggle="modal" data-target="#modalAdicionarCategoria" class="btn btn-primary btn-fill btn-block">Categoria</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAdicionarContaPagarBtnAdicionar">Adicionar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <?= date("Y") ?> | Sistema OCTP | v1.0.0
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <?php $this->load->view("static/modals/adicionarCategoriaFinanceira") ?>
</body>
<script src="<?= base_url("assets/js/app/financeiro/contas-pagar/adicionar.js") ?>"></script>
<script src="<?= base_url("assets/js/app/utils/categoriasFinanceiras.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
