<?php $this->load->view("static/header", ["title" => "Remessas"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "financeiro"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Financeiro / Remessas"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-warning" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <form id="formBuscar">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cliente">Cliente</label>
                                                    <select id="cliente" name="cliente" class="form-control" data-placeholder="Selecionar cliente" style="height: 30px">
                                                        <option value="todos">Todos</option>
                                                        <?php foreach ($clientes as $cliente) : ?>
                                                            <option value="<?= $cliente->id ?>"><?= $cliente->nome ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="dataInicial">Data inicial</label>
                                                    <input type="text" id="dataInicial" name="dataInicial" class="form-control border-input" placeholder="00/00/0000">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="dataFinal">Data final</label>
                                                    <input type="text" id="dataFinal" name="dataFinal" class="form-control border-input" placeholder="00/00/0000">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="exportado">Exportado</label>
                                                    <select id="exportado" name="exportado" class="form-control border-input">
                                                        <option value="1">Não</option>
                                                        <option value="2">Sim</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12 col-md-offset-10">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarBtnBuscar">Buscar</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <hr>
                                        </div>
                                    </div>
                                    <form id="formGerarRemessa">
                                        <div class="row">
                                            <!--<input type="hidden" id="file" name="file">-->
                                            <div class="col-md-2 col-xs-12 col-md-offset-8">
                                                <a href="#" class="disabled btn btn-info btn-fill btn-block" id="btnBaixar">Baixar arquivo</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formGerarRemessaBtnGerar">Gerar remessa</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12 content table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center"><input type="checkbox" id="marcarTodos" name="marcarTodos" checked="checked"></th>
                                                            <th>Cliente</th>
                                                            <th>Emissao</th>
                                                            <th>Vencimento</th>
                                                            <th>Remessa</th>
                                                            <th>Valor</th>
                                                            <th>Situação</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tabelaBoletos">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/chosen/chosen.jquery.min.js") ?>"></script>
<script src="<?= base_url("assets/js/app/financeiro/remessas/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
