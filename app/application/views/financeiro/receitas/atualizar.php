<?php $this->load->view("static/header", ["title" => "Financeiro - Receitas"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "financeiro"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Receitas / Atualizar"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formAtualizarContaReceber">
                                        <input type="hidden" id="id" name="id" value="<?= $contaReceber->id ?>">
                                        <input type="hidden" id="idParcela" name="idParcela" value="<?= $contaReceber->idParcela ?>">
                                        <input type="hidden" id="idBoleto" name="idBoleto" value="<?= $contaReceber->idBoleto ?>">
                                        <div class="row">
                                            <div class="form-group col-md-5 col-xs-12">
                                                <label for="contrato">Contrato</label>
                                                <select id="contrato" name="contrato" class="form-control border-input" autofocus="">
                                                    <option value="">--</option>
                                                    <?php foreach ($contratos as $contrato) : ?>
                                                        <?php if ($contrato->situacao == "ativo" || $contrato->situacao == "inadimplente"): ?>
                                                            <?php if ($contrato->id == $contaReceber->idContrato): ?>
                                                                <option selected="selected" value="<?= $contrato->id ?>"><?= "{$contrato->codigoContrato} - {$contrato->nome}" ?></option>
                                                            <?php else: ?>
                                                                <option value="<?= $contrato->id ?>"><?= "{$contrato->codigoContrato} - {$contrato->nome}" ?></option>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-9 col-xs-12">
                                                <label for="descricao">Descrição</label>
                                                <input type="text" class="form-control border-input" id="descricao" name="descricao" placeholder="Descrição da entrada" value="<?= $contaReceber->descricao ?>" autofocus="">
                                            </div>
                                            <div class="form-group col-md-3 col-xs-12">
                                                <label for="valor">Valor (R$)</label>
                                                <input type="text" class="form-control border-input" id="valor" name="valor" value="<?= number_format($contaReceber->valor, 2, ".", ",") ?>" placeholder="0,00">
                                            </div>
                                        </div>
                                        <div class="row margin-b">
                                            <div class="form-group col-md-2 col-xs-12">
                                                <label for="data">Vencimento</label>
                                                <input type="text" class="form-control border-input" id="data" name="data" value="<?= date("d/m/Y", strtotime($contaReceber->data)) ?>" placeholder="99/99/9999">
                                            </div>
                                            <div class="form-group col-md-2 col-xs-12">
                                                <label for="pagamento">Baixa</label>
                                                <input type="text" class="form-control border-input" id="pagamento" name="pagamento" value="<?= date("d/m/Y", strtotime($contaReceber->pagamento)) ?>" placeholder="99/99/9999">
                                            </div>
                                            <div class="form-group col-md-4 col-xs-12">
                                                <label for="categoria">Categoria</label>
                                                <select id="categoria" name="categoria" class="form-control border-input">
                                                    <option value="">--</option>
                                                    <?php foreach ($categorias as $categoria) : ?>
                                                        <?php if ($contaReceber->idCategoria == $categoria->id): ?>
                                                            <option selected="selected" value="<?= $categoria->id ?>"><?= $categoria->categoria ?></option>
                                                        <?php else: ?>
                                                            <?php if ($categoria->tipo == "entrada"): ?>
                                                                <option value="<?= $categoria->id ?>"><?= $categoria->categoria ?></option>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 col-xs-12">
                                                <label for="status">Status</label>
                                                <select id="status" name="status" class="form-control border-input">
                                                    <option <?= $contaReceber->status == "recebido" ? "selected='selected'" : "" ?> value="recebido">Recebido
                                                    <option <?= $contaReceber->status == "pendente" ? "selected='selected'" : "" ?> value="pendente">Pendente</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <a href="<?= base_url("financeiro/contas-receber") ?>" class="btn btn-default btn-fill btn-block">Voltar</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <a href="#" data-toggle="modal" data-target="#modalAdicionarCategoria" class="btn btn-primary btn-fill btn-block">Categoria</a>
                                            </div>
                                            <div class="col-md-2 col-xs-12 margin-b">
                                                <button type="submit" class="btn btn-success btn-fill btn-block" id="formAtualizarContaReceberBtnAtualizar">Atualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <?= date("Y") ?> | Sistema OCTP | v1.0.0
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <?php $this->load->view("static/modals/adicionarCategoriaFinanceira") ?>
</body>
<script src="<?= base_url("assets/js/app/financeiro/receitas/atualizar.js") ?>"></script>
<script src="<?= base_url("assets/js/app/utils/categoriasFinanceiras.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
