<?php $this->load->view("static/header", ["title" => "Financeiro - Extrato"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "financeiro"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Extrato"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <?php if ($this->session->flashdata("msg")): ?>
                                        <div class="row margin-b">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="alert alert-warning" role="alert">
                                                    <?= $this->session->flashdata("msg") ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <input type="hidden" id="dataAtual" name="dataAtual" value="<?= date("Y-m-d") ?>">
                                    <form id="formBuscar">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="formaPagamento">Forma pagt.</label>
                                                    <select class="form-control border-input" id="formaPagamento" name="formaPagamento">
                                                        <option value="todas">Todas</option>
                                                        <option value="dinheiro">Dinheiro</option>
                                                        <option value="boleto">Boleto</option>
                                                        <option value="deposito">Depósito</option>
                                                        <option value="cartao-credito">Cartão de crédito</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="conta">Conta</label>
                                                    <select class="form-control border-input" id="conta" name="conta">
                                                        <option value="todas">Todas</option>
                                                        <option value="caixa-vendas">Caixa de vendas</option>
                                                        <option value="caixa-central">Caixa central</option>
                                                        <option value="banco-santander">Banco Santander</option>
                                                        <option value="banco-brasil">Banco do Brasil</option>
                                                        <option value="cartao-credito">Cartão de crédit</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="dataInicial">Data inicial</label>
                                                    <input type="text" id="dataInicial" name="dataInicial" class="form-control border-input" placeholder="Data inicial">
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="dataFinal">Data final</label>
                                                    <input type="text" id="dataFinal" name="dataFinal" class="form-control border-input" placeholder="Data final">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12 col-md-offset-10 margin-b">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formBuscarBtnBuscar">Buscar</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12">
                                            <label for="totalRecebido">Recebido: </label>
                                            <span id="totalRecebido">R$ <?= number_format($contasReceber["total"], 2, ",", ".") ?></span>
                                        </div>
                                        <div class="col-md-3 col-xs-12">
                                            <label for="totalPago">Pago: </label>
                                            <span id="totalPago">R$ <?= number_format($contasPagar["total"], 2, ",", ".") ?></span>
                                        </div>
                                        <div class="col-md-3 col-xs-12">
                                            <label for="total">Total: </label>
                                            <span id="total">R$ <?= number_format($contasReceber["total"] - $contasPagar["total"], 2, ",", ".") ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                        </th>
                                                        <th>Forma pagt.<br>Conta</th>
                                                        <th>Descrição</th>
                                                        <th>Categoria</th>
                                                        <th class="text-center">Valor</th>
                                                        <th class="text-center">Baixa</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaExtrato">
                                                    <?php foreach ($dias as $dia) : ?>
                                                        <?php foreach ($contasReceber["contasReceber"] as $contaReceber) : ?>
                                                            <?php if ($contaReceber->pagamento == $dia): ?>
                                                                <tr>
                                                                    <td class="text-center"><i class="fa fa-circle" style="color:green"></i></td>
                                                                    <td>- <?= $formaPagamento[$contaReceber->formaPagamento] ?><br>- <?= $conta[$contaReceber->conta] ?></td>
                                                                    <td><?= $contaReceber->descricao ?></td>
                                                                    <td><?= $contaReceber->categoria ?></td>
                                                                    <td class="text-center">R$ <?= number_format($contaReceber->valor, 2, ",", ".") ?></td>
                                                                    <td class="text-center"><?= date("d/m/Y", strtotime($contaReceber->pagamento)) ?></td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                        <?php foreach ($contasPagar["contasPagar"] as $contaPagar) : ?>
                                                            <?php if ($contaPagar->pagamento == $dia): ?>
                                                                <tr>
                                                                    <td class="text-center"><i class="fa fa-circle" style="color:red"></i></td>
                                                                    <td>- <?= $formaPagamento[$contaPagar->formaPagamento] ?><br>- <?= $conta[$contaPagar->conta] ?></td>
                                                                    <td><?= $contaPagar->descricao ?></td>
                                                                    <td><?= $contaPagar->categoria ?></td>
                                                                    <td class="text-center">R$ <?= number_format($contaPagar->valor, 2, ",", ".") ?></td>
                                                                    <td class="text-center"><?= date("d/m/Y", strtotime($contaPagar->pagamento)) ?></td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/financeiro/extrato/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
