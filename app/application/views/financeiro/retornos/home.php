<?php $this->load->view("static/header", ["title" => "Financeiro >> Retornos"]) ?>
<?php $this->load->view("static/scripts") ?>
<body>
    <div class="wrapper">
        <?php $this->load->view("static/menu-principal", ["menu" => "financeiro"]) ?>
        <div class="main-panel">
            <?php $this->load->view("static/menu-superior", ["menuAtual" => "Financeiro / Retornos"]) ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="content">
                                    <form id="formRetorno">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="retorno">Enviar retorno</label>
                                                    <input type="file" id="retorno" name="retorno">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2 col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-fill btn-block" id="formRetornoBtnEnviar">Enviar</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 content table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Retorno</th>
                                                        <th>Arquivo</th>
                                                        <th>Data de upload</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaRetornos">
                                                    <?php if (!empty($retornos)): ?>
                                                        <?php foreach ($retornos as $retorno) : ?>
                                                            <tr>
                                                                <td><?= $retorno->arquivo ?></td>
                                                                <td><a href="<?= base_url("download/retorno/{$retorno->arquivo}") ?>">Link arquivo</a></td>
                                                                <td><?= date("d/m/Y", strtotime($retorno->created_at)) ?></td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    <?php else: ?>
                                                        <tr>
                                                            <td colspan="3" class="text-center" style="font-size: 1.3em">Nenhum retorno registrado até o momento</td>
                                                        </tr>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div
                </div>
            </div>
            <?php $this->load->view("static/footer") ?>
        </div>
    </div>
</body>
<script src="<?= base_url("assets/js/app/financeiro/retornos/home.js") ?>"></script>
<?php $this->load->view("static/end-page") ?>
