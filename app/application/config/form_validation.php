<?php

$config = array(
    "login/autenticar" => [
        [
            'field' => 'usuario',
            'label' => 'usuario',
            'rules' => 'trim|required|valid_email'
        ],
        [
            'field' => 'senha',
            'label' => 'senha',
            'rules' => 'trim|required'
        ]
    ],
    "meusDados/atualizar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'cliente',
            'label' => 'cliente',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'usuario',
            'label' => 'usuario',
            'rules' => 'trim|required|valid_email'
        ],
        [
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'trim|required'
        ]
    ],
    "dadosEmpresa/atualizar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'nomeFantasia',
            'label' => 'nomeFantasia',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'razaoSocial',
            'label' => 'razaoSocial',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'cnpj',
            'label' => 'cnpj',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'carteira',
            'label' => 'carteira',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'conta',
            'label' => 'conta',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'agencia',
            'label' => 'agencia',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'ios',
            'label' => 'ios',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'codigoTransmissao',
            'label' => 'codigoTransmissao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'codigoPsk',
            'label' => 'codigoPsk',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'codigoBanco',
            'label' => 'codigoBanco',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'contaFidc',
            'label' => 'contaFidc',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'telefone',
            'label' => 'telefone',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'celular',
            'label' => 'celular',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'email',
            'label' => 'email',
            'rules' => 'trim|required|valid_email'
        ],
        [
            'field' => 'cep',
            'label' => 'cep',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'logradouro',
            'label' => 'logradouro',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'numero',
            'label' => 'numero',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'bairro',
            'label' => 'bairro',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'estado',
            'label' => 'estado',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'cidade',
            'label' => 'cidade',
            'rules' => 'trim|required'
        ]
    ],
    "usuarios/adicionar" => [
        [
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'usuario',
            'label' => 'usuario',
            'rules' => 'trim|required|valid_email'
        ],
        [
            'field' => 'tipo',
            'label' => 'tipo',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'senha',
            'label' => 'senha',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'repetirSenha',
            'label' => 'repetirSenha',
            'rules' => 'trim|required|matches[senha]'
        ],
        [
            'field' => 'status',
            'label' => 'status',
            'rules' => 'trim|required'
        ]
    ],
    "usuarios/atualizar" => [
        [
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'required'
        ],
        [
            'field' => 'usuario',
            'label' => 'usuario',
            'rules' => 'required|valid_email'
        ],
        [
            'field' => 'status',
            'label' => 'status',
            'rules' => 'required'
        ]
    ],
    "usuarios/alterarSenha" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'required|integer'
        ],
        [
            'field' => 'senha',
            'label' => 'senha',
            'rules' => 'required'
        ],
        [
            'field' => 'repetirSenha',
            'label' => 'repetirSenha',
            'rules' => 'required|matches[senha]'
        ]
    ],
    "contratos/adicionar" => [
        [
            'field' => 'cliente',
            'label' => 'cliente',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'vendedor',
            'label' => 'vendedor',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'formaPagamento',
            'label' => 'formaPagamento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'codigoContrato',
            'label' => 'codigoContrato',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'ano',
            'label' => 'ano',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'duracao',
            'label' => 'duracao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'situacao',
            'label' => 'situacao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'dataAssinatura',
            'label' => 'dataAssinatura',
            'rules' => 'trim|required'
        ]
    ],
    "contratos/atualizar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'cliente',
            'label' => 'cliente',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'vendedor',
            'label' => 'vendedor',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'formaPagamento',
            'label' => 'formaPagamento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'codigoContrato',
            'label' => 'codigoContrato',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'ano',
            'label' => 'ano',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'duracao',
            'label' => 'duracao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'situacao',
            'label' => 'situacao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'dataAssinatura',
            'label' => 'dataAssinatura',
            'rules' => 'trim|required'
        ]
    ],
    "contratos/gerarParcelas" => [
        [
            'field' => 'contrato',
            'label' => 'contrato',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'valor',
            'label' => 'valor',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'vencimento',
            'label' => 'vencimento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'parcelas',
            'label' => 'parcelas',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'categoria',
            'label' => 'categoria',
            'rules' => 'trim|required|integer'
        ]
    ],
    "vendedores/adicionar" => [
        [
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'usuario',
            'label' => 'usuario',
            'rules' => 'trim|required|valid_email'
        ],
        [
            'field' => 'senha',
            'label' => 'senha',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'repetirSenha',
            'label' => 'repetirSenha',
            'rules' => 'trim|required|matches[senha]'
        ]
    ],
    "vendedores/atualizar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'trim|required'
        ]
    ],
    "contasReceber/adicionar" => [
        [
            'field' => 'contrato',
            'label' => 'contrato',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'descricao',
            'label' => 'descricao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'valor',
            'label' => 'valor',
            'rules' => 'required'
        ],
        [
            'field' => 'data',
            'label' => 'data',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'categoria',
            'label' => 'categoria',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'status',
            'label' => 'status',
            'rules' => 'trim|required'
        ]
    ],
    "contasReceber/atualizar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'descricao',
            'label' => 'descricao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'valor',
            'label' => 'valor',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'data',
            'label' => 'data',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'pagamento',
            'label' => 'pagamento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'categoria',
            'label' => 'categoria',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'status',
            'label' => 'status',
            'rules' => 'trim|required'
        ]
    ],
    "contasReceber/baixar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'idParcela',
            'label' => 'idParcela',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'dataPagamento',
            'label' => 'dataPagamento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'formaPagamento',
            'label' => 'formaPagamento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'conta',
            'label' => 'conta',
            'rules' => 'trim|required'
        ]
    ],
    "contasPagar/baixar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'dataPagamento',
            'label' => 'dataPagamento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'formaPagamento',
            'label' => 'formaPagamento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'conta',
            'label' => 'conta',
            'rules' => 'trim|required'
        ]
    ],
    "receitas/atualizar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'idParcela',
            'label' => 'idParcela',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'contrato',
            'label' => 'contrato',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'descricao',
            'label' => 'descricao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'valor',
            'label' => 'valor',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'data',
            'label' => 'data',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'categoria',
            'label' => 'categoria',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'status',
            'label' => 'status',
            'rules' => 'trim|required'
        ]
    ],
    "contasPagar/adicionar" => [
        [
            'field' => 'descricao',
            'label' => 'descricao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'fornecedor',
            'label' => 'fornecedor',
            'rules' => 'required'
        ],
        [
            'field' => 'valor',
            'label' => 'valor',
            'rules' => 'required'
        ],
        [
            'field' => 'data',
            'label' => 'data',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'categoria',
            'label' => 'categoria',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'status',
            'label' => 'status',
            'rules' => 'trim|required'
        ]
    ],
    "contasPagar/atualizar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'fornecedor',
            'label' => 'fornecedor',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'valor',
            'label' => 'valor',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'descricao',
            'label' => 'descricao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'data',
            'label' => 'data',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'categoria',
            'label' => 'categoria',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'status',
            'label' => 'status',
            'rules' => 'trim|required'
        ]
    ],
    "parcelas/atualizar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'idContaReceber',
            'label' => 'idContaReceber',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'valor',
            'label' => 'valor',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'vencimento',
            'label' => 'vencimento',
            'rules' => 'trim|required'
        ]
    ],
    "clientes/adicionar" => [
        [
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'cpf',
            'label' => 'cpf',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'celular',
            'label' => 'celular',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'estado',
            'label' => 'estado',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'cidade',
            'label' => 'cidade',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'dataNascimento',
            'label' => 'dataNascimento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'numeroCartao',
            'label' => 'numeroCartao',
            'rules' => 'trim|required'
        ]
    ],
    "clientes/atualizar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'idCartao',
            'label' => 'idCartao',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'cpf',
            'label' => 'cpf',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'celular',
            'label' => 'celular',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'numeroCartao',
            'label' => 'numeroCartao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'estado',
            'label' => 'estado',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'cidade',
            'label' => 'cidade',
            'rules' => 'trim|required|integer'
        ]
    ],
    "dependentes/adicionar" => [
        [
            'field' => 'titular',
            'label' => 'titular',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'cpf',
            'label' => 'cpf',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'numeroCartao',
            'label' => 'numeroCartao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'dataNascimento',
            'label' => 'dataNascimento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'dataVinculacao',
            'label' => 'dataVinculacao',
            'rules' => 'trim|required'
        ]
    ],
    "dependentes/atualizar" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'titular',
            'label' => 'titular',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'cpf',
            'label' => 'cpf',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'numeroCartao',
            'label' => 'numeroCartao',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'dataNascimento',
            'label' => 'dataNascimento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'dataVinculacao',
            'label' => 'dataVinculacao',
            'rules' => 'trim|required'
        ]
    ],
    "cidades/adicionar" => array(
        array(
            'field' => 'estado',
            'label' => 'estado',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'cidade',
            'label' => 'cidade',
            'rules' => 'required|integer'
        )
//        array(
//            'field' => 'tipo',
//            'label' => 'tipo',
//            'rules' => 'required'
//        )
    ),
    "cidades/atualizar" => array(
        array(
            'field' => 'estado',
            'label' => 'estado',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'cidade',
            'label' => 'cidade',
            'rules' => 'required|integer'
        )
//        array(
//            'field' => 'tipo',
//            'label' => 'tipo',
//            'rules' => 'required'
//        )
    ),
    "especialidades/atualizar" => array(
        array(
            'field' => 'id',
            'label' => 'id',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'especialidade',
            'label' => 'especialidade',
            'rules' => 'required'
        ),
        array(
            'field' => 'tipo',
            'label' => 'tipo',
            'rules' => 'required|integer'
        )
    ),
    "parceiros/adicionar" => array(
        array(
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'required'
        ),
        array(
            'field' => 'endereco',
            'label' => 'endereco',
            'rules' => 'required'
        ),
        array(
            'field' => 'estado',
            'label' => 'estado',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'cidade',
            'label' => 'cidade',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'telefone',
            'label' => 'telefone',
            'rules' => 'required'
        )
    ),
    "parceiros/atualizar" => array(
        array(
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'required'
        ),
        array(
            'field' => 'endereco',
            'label' => 'endereco',
            'rules' => 'required'
        ),
        array(
            'field' => 'estado',
            'label' => 'estado',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'cidade',
            'label' => 'cidade',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'telefone',
            'label' => 'telefone',
            'rules' => 'required'
        )
    ),
    "atualizar-senha" => [
        [
            'field' => "id",
            'label' => "id",
            'rules' => "trim|required|integer"
        ],
        [
            'field' => "senha",
            'label' => "senha",
            'rules' => "trim|required"
        ],
        [
            'field' => "repetirSenha",
            'label' => "repetirSenha",
            'rules' => "trim|required|matches[senha]"
        ]
    ],
    "combos/adicionarProcedimento" => [
        [
            'field' => 'id',
            'label' => 'id',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'procedimento',
            'label' => 'procedimento',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'quantidade',
            'label' => 'quantidade',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'valor',
            'label' => 'valor',
            'rules' => 'trim|required'
        ]
    ],
    "parceiros/adicionarUsuario" => [
        [
            'field' => 'idParceiro',
            'label' => 'idParceiro',
            'rules' => 'trim|required|integer'
        ],
        [
            'field' => 'nome',
            'label' => 'nome',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'usuario',
            'label' => 'usuario',
            'rules' => 'trim|required|valid_email'
        ],
        [
            'field' => 'senha',
            'label' => 'senha',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'repetirSenha',
            'label' => 'repetirSenha',
            'rules' => 'trim|required|matches[senha]'
        ]
    ],
    "extrato/buscar" => [
        [
            'field' => 'formaPagamento',
            'label' => 'formaPagamento',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'conta',
            'label' => 'conta',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'dataInicial',
            'label' => 'dataInicial',
            'rules' => 'trim|required'
        ],
        [
            'field' => 'dataFinal',
            'label' => 'dataFinal',
            'rules' => 'trim|required'
        ]
    ]
);
