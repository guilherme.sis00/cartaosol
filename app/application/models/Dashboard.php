<?php

class Dashboard extends CI_Model {

    public function getSaldoInicial($returnArray = TRUE) {
        $dataFinal = date("d/m/Y", strtotime("-1 days", strtotime(date("Y-m") . "-01")));

        $receitas = $this->db
                        ->select("COALESCE(SUM(valor),0) AS total")
                        ->where("status", "recebido")
                        ->where("pagamento <=", $dataFinal)
                        ->get("contas_receber")->row();

        $despesas = $this->db
                        ->select("COALESCE(SUM(valor),0) AS total")
                        ->where("status", "pago")
                        ->where("pagamento <=", $dataFinal)
                        ->get("contas_pagar")->row();

        if ($returnArray) {
            return["saldoInicial" => ($receitas->total - $despesas->total)];
        } else {
            return $receitas->total - $despesas->total;
        }
    }

}
