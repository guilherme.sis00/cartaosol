<?php

class Cliente extends CI_Model {

    private $id;
    private $nome;
    private $cpf;
    private $email;
    private $celular;
    private $whatsapp;
    private $dataNascimento;
    private $idCartao;
    private $idCidade;
    private $idEstado;
    private $observacao;
    private $status;
    private $numeroCartao;

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->nome = !empty($dados['nome']) ? $dados['nome'] : NULL;
        $this->cpf = !empty($dados['cpf']) ? $dados['cpf'] : NULL;
        $this->email = !empty($dados['email']) ? $dados['email'] : NULL;
        $this->celular = !empty($dados['celular']) ? $dados['celular'] : NULL;
        $this->whatsapp = !empty($dados['whatsapp']) ? $dados['whatsapp'] : NULL;
        $this->dataNascimento = !empty($dados['dataNascimento']) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados['dataNascimento']))) : NULL;
        $this->idCartao = !empty($dados['idCartao']) ? $dados['idCartao'] : NULL;
        $this->idCidade = !empty($dados['cidade']) ? $dados['cidade'] : NULL;
        $this->idEstado = !empty($dados['estado']) ? $dados['estado'] : NULL;
        $this->observacao = !empty($dados['observacao']) ? $dados['observacao'] : NULL;
        $this->status = !empty($dados['status']) ? $dados['status'] : "ativo";
        $this->numeroCartao = !empty($dados['numeroCartao']) ? $dados['numeroCartao'] : NULL;
    }

    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE) {
        $this->db
                ->select("ocpt_clientes.*,"
                        . "ocpt_cidades.cidade,"
                        . "ocpt_estados.estado");
        if ($paginacao) {
            $this->db->limit($limit, $offset);
        }

        $responseClientes = $this->db->join("ocpt_cidades", "ocpt_cidades.id=ocpt_clientes.idCidade")
                        ->join("ocpt_estados", "ocpt_estados.id=ocpt_clientes.idEstado")
                        ->order_by("ocpt_clientes.nome", "ASC")
                        ->where("clientes.status", "ativo")
                        ->get("ocpt_clientes")->result();

        return $responseClientes;
    }

    public function getById($id, $returnArray = TRUE) {
        if ($returnArray) {
            $response = $this->db
                            ->select("clientes.*,"
                                    . "cartoes_clientes.id as idCartao,"
                                    . "cartoes_clientes.numero,"
                                    . "cidades.cidade,"
                                    . "estados.estado")
                            ->where("clientes.id", $id)
                            ->join("cartoes_clientes", "cartoes_clientes.idCliente=clientes.id")
                            ->join("cidades", "cidades.id=clientes.idCidade")
                            ->join("estados", "estados.id=clientes.idEstado")
                            ->get("clientes")->row();

            if (!empty($response)) {
                return ["resultado" => TRUE, "cliente" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum cliente foi encontrado"];
            }
        } else {
            return $this->db
                            ->select("ocpt_clientes.*,"
                                    . "ocpt_cidades.cidade,"
                                    . "ocpt_estados.estado")
                            ->where("ocpt_clientes.id", $id)
                            ->join("ocpt_cidades", "ocpt_cidades.id=ocpt_clientes.idCidade")
                            ->join("ocpt_estados", "ocpt_estados.id=ocpt_clientes.idEstado")
                            ->get("ocpt_clientes")->row();
        }
    }

    public function getExtratoPagos($idCliente, $returnArray = TRUE) {
        $response = $this->db
                        ->select("contas_receber.id, "
                                . "contratos.codigoContrato,"
                                . "contas_receber.descricao,"
                                . "contas_receber.data as vencimento,"
                                . "contas_receber.pagamento,"
                                . "contas_receber.valor,"
                                . "contas_receber.formaPagamento")
                        ->join("parcelas", "boletos.idParcela=parcelas.id")
                        ->join("contratos", "parcelas.idContrato=contratos.id")
                        ->join("boletos_contas_receber", "boletos_contas_receber.idBoleto=boletos.id")
                        ->join("contas_receber", "boletos_contas_receber.idContaReceber=contas_receber.id")
                        ->where([
                            "contratos.idCliente" => $idCliente,
                            "contas_receber.status" => "recebido"
                        ])
                        ->order_by("contas_receber.pagamento", "DESC")
                        ->get("boletos")->result();

        if ($returnArray) {
            if (!empty($response)) {
                $formasPagamento = [
                    "dinheiro" => "Dinheiro",
                    "boleto" => "Boleto",
                    "deposito" => "Depósito",
                    "cartao->credito" => "Cartão de crédito"
                ];
                return ["resultado" => TRUE, "ocorrencias" => $response, "formasPagamento" => $formasPagamento];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma receita encontrada"];
            }
        } else {
            return $response;
        }
    }

    public function getExtratoPendentes($idCliente, $returnArray = TRUE) {
        $response = $this->db
                        ->select("contas_receber.id, "
                                . "contratos.codigoContrato,"
                                . "contas_receber.descricao,"
                                . "contas_receber.data as vencimento,"
                                . "contas_receber.pagamento,"
                                . "contas_receber.valor,"
                                . "contas_receber.formaPagamento")
                        ->join("parcelas", "boletos.idParcela=parcelas.id")
                        ->join("contratos", "parcelas.idContrato=contratos.id")
                        ->join("boletos_contas_receber", "boletos_contas_receber.idBoleto=boletos.id")
                        ->join("contas_receber", "boletos_contas_receber.idContaReceber=contas_receber.id")
                        ->where('parcelas.vencimento >= "' . date("Y-m-d") . '"')
                        ->where([
                            "contratos.idCliente" => $idCliente,
                            "contas_receber.status" => "pendente"
                        ])
                        ->order_by("contas_receber.data")
                        ->get("boletos")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "ocorrencias" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma receita encontrada"];
            }
        } else {
            return $response;
        }
    }

    public function getExtratoAtrasados($idCliente, $returnArray = TRUE) {
        $response = $this->db
                        ->select("contas_receber.id, "
                                . "contratos.codigoContrato,"
                                . "contas_receber.descricao,"
                                . "contas_receber.data as vencimento,"
                                . "contas_receber.pagamento,"
                                . "contas_receber.valor,"
                                . "contas_receber.formaPagamento")
                        ->join("parcelas", "boletos.idParcela=parcelas.id")
                        ->join("contratos", "parcelas.idContrato=contratos.id")
                        ->join("boletos_contas_receber", "boletos_contas_receber.idBoleto=boletos.id")
                        ->join("contas_receber", "boletos_contas_receber.idContaReceber=contas_receber.id")
                        ->where('parcelas.vencimento <= "' . date("Y-m-d") . '"')
                        ->where([
                            "contratos.idCliente" => $idCliente,
                            "contas_receber.status" => "pendente"
                        ])
                        ->order_by("contas_receber.data")
                        ->get("boletos")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "ocorrencias" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma receita encontrada"];
            }
        } else {
            return $response;
        }
    }
    
    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("clientes", $this->toArray());
        $this->id = $this->db->insert_id();
        $this->db->insert("cartoes_clientes", ["idCliente" => $this->id, "numero" => $this->numeroCartao]);
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Cliente adicionado, continuar adicionando ?"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar cliente"];
        }
    }

    public function buscar($dados) {
        $response = $this->db
                        ->like($dados["tipoBusca"], $dados["termo"])
                        ->order_by("clientes.nome", "ASC")
                        ->where("status", "ativo")
                        ->get("ocpt_clientes")->result();

        if (!empty($response)) {
            $responseFarois = $this->getFarois($response);
            return ["resultado" => TRUE, "clientes" => $response, "farois" => $responseFarois];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum cliente encontrado"];
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->set($this->toArray())
                ->where("id", $this->id)
                ->update("clientes");
        $this->db
                ->set("numero", $this->numeroCartao)
                ->where("id", $this->idCartao)
                ->update("cartoes_clientes");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Cliente atualizado");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao atualizar cliente");
        }
    }

    public function deletar($id) {
        $this->db->trans_start();
        $this->db
                ->set("status", "deletado")
                ->where("id", $id)
                ->update("clientes");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Cliente deletado");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao deletar cliente");
        }
    }

    public function getFarois($clientes) {
        if (!empty($clientes)) {
            $farois = [];
            foreach ($clientes as $cliente) {
                $idCliente = is_object($cliente) ? $cliente->id : $cliente["id"];

                $responseFarol = $this->db
                                ->select("COUNT(ocpt_boletos.id) as total")
                                ->join("parcelas", "boletos.idParcela=parcelas.id")
                                ->join("contratos", "parcelas.idContrato=contratos.id")
                                ->join("boletos_contas_receber", "boletos_contas_receber.idBoleto=boletos.id")
                                ->join("contas_receber", "boletos_contas_receber.idContaReceber=contas_receber.id")
                                ->where('parcelas.vencimento <= "' . date("Y-m-d") . '"')
                                ->where([
                                    "contratos.idCliente" => $idCliente,
                                    "contas_receber.status" => "pendente"
                                ])
                                ->get("boletos")->row();

                $farois[$idCliente] = $responseFarol->total == 0 ? "green" : ($responseFarol->total <= 2 ? "yellow" : "red");
            }

            return $farois;
        } else {
            return FALSE;
        }
    }

    private function toArray() {
        return [
            "nome" => $this->nome,
            "cpf" => $this->cpf,
            "email" => $this->email,
            "celular" => $this->celular,
            "whatsapp" => $this->whatsapp,
            "dataNascimento" => $this->dataNascimento,
            "idCidade" => $this->idCidade,
            "idEstado" => $this->idEstado,
            "observacao" => $this->observacao,
            "status" => $this->status
        ];
    }

}
