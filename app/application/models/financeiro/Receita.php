<?php

class Receita extends CI_Model {

    private $id;
    private $descricao;
    private $valor;
    private $idCategoria;
    private $data;
    private $status;
    private $dados;

    public function preencherDados($dados) {
        $this->id = !empty($dados["id"]) ? $dados['id'] : NULL;
        $this->descricao = !empty($dados["descricao"]) ? $dados['descricao'] : NULL;
        $this->valor = !empty($dados["valor"]) ? $dados["valor"] : NULL;
        $this->idCategoria = !empty($dados["categoria"]) ? $dados['categoria'] : NULL;
        $this->data = !empty($dados["data"]) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados['data']))) : NULL;
        $this->status = !empty($dados["status"]) ? $dados['status'] : NULL;
        $this->dados = $dados;
    }

    public function get($atributo) {
        switch ($atributo) {
            case "id":
                return $this->id;
        }
    }

    public function getByPeriodo($dataInicial, $dataFinal, $returnArray = TRUE) {
        $response = $this->db
                        ->select("contas_receber.*, boletos.idParcela, categorias_financeiras.categoria")
                        ->join("categorias_financeiras", "categorias_financeiras.id=contas_receber.idCategoria")
                        ->join("boletos_contas_receber", "contas_receber.id=boletos_contas_receber.idContaReceber")
                        ->join("boletos", "boletos.id=boletos_contas_receber.idBoleto")
                        ->where('contas_receber.pagamento BETWEEN "' . $dataInicial . '" AND "' . $dataFinal . '"')
                        ->where('contas_receber.status', "recebido")
                        ->order_by("contas_receber.pagamento", "ASC")
                        ->order_by("contas_receber.descricao", "ASC")
                        ->get("contas_receber")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "contasReceber" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma conta a receber encontrada nesse período"];
            }
        } else {
            return $response;
        }
    }

    public function getById($id, $returnArray = TRUE) {
        $response = $this->db
                        ->select("contas_receber.*,"
                                . "boletos.id as idBoleto,"
                                . "boletos.idParcela,"
                                . "parcelas.idContrato,"
                                . "categorias_financeiras.categoria")
                        ->join("categorias_financeiras", "categorias_financeiras.id=contas_receber.idCategoria")
                        ->join("boletos_contas_receber", "boletos_contas_receber.idContaReceber=contas_receber.id")
                        ->join("boletos", "boletos_contas_receber.idBoleto=boletos.id")
                        ->join("parcelas", "parcelas.id=boletos.idParcela")
                        ->where("contas_receber.id", $id)
                        ->where("contas_receber.status", "recebido")
                        ->get("contas_receber")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "contaReceber" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma conta a receber encontrada"];
            }
        } else {
            return $response;
        }
    }

    public function getTotalByStatus($status, $dataInicial, $dataFinal) {
        return $this->db
                        ->select("COALESCE(SUM(valor),0) AS total")
                        ->where("status", $status)
                        ->where('(contas_receber.pagamento BETWEEN "' . $dataInicial . '" AND "' . $dataFinal . '")')
                        ->get("contas_receber")->row();
    }

    public function buscar($dados) {
        $this->db
                ->select("contas_receber.*, boletos.idParcela, categorias_financeiras.categoria")
                ->join("boletos_contas_receber", "boletos_contas_receber.idContaReceber=contas_receber.id")
                ->join("boletos", "boletos_contas_receber.idBoleto=boletos.id")
                ->join("parcelas", "boletos.idParcela=parcelas.id")
                ->join("contratos", "parcelas.idContrato=contratos.id")
                ->join("clientes", "contratos.idCliente=clientes.id")
                ->join("categorias_financeiras", "categorias_financeiras.id=contas_receber.idCategoria")
                ->where('contas_receber.pagamento BETWEEN "' . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataInicial"]))) . '" AND "' . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataFinal"]))) . '"')
                ->where('contas_receber.status', "recebido")
                ->order_by("contas_receber.data", "ASC")
                ->order_by("contas_receber.descricao", "ASC");

        if (!empty($dados["termo"])) {
            $this->db->like("clientes.nome", $dados["termo"]);
        }

        if (!empty($dados["cpf"])) {
            $this->db->where("clientes.cpf", $dados["cpf"]);
        }

        if (!empty($dados["codigoContrato"])) {
            $this->db->where("contratos.codigoContrato", $dados["codigoContrato"]);
        }

        if ($dados["categoria"] != "todas") {
            $this->db->where("contas_receber.idCategoria", $dados["categoria"]);
        }

        $response = $this->db->get("contas_receber")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "contasReceber" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhuma conta a receber encontrada"];
        }
    }

    public function atualizarBaixa($dados) {
        $this->db->trans_start();
        $this->db
                ->where("id", $dados["id"])
                ->update("contas_receber", ["pagamento" => date("Y-m-d", strtotime(str_replace("/", "-", $dados['baixa'])))]);
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            $responseReceita = $this->getById($dados["id"], FALSE);
            return ["resultado" => TRUE, "msg" => "Receita atualizada", "receita" => $responseReceita];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao atualizar receita"];
        }
    }

    public function atualizarStatus($dados) {
        $this->db->trans_start();
        $this->db
                ->where("id", $dados["id"])
                ->update("contas_receber", ["pagamento" => "0000-00-00", "status" => "pendente"]);
        $this->db
                ->where("idParcela", $dados["idParcela"])
                ->update("boletos", ["dataPagamento" => "0000-00-00", "status" => 1]);

        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Status atualizado, receita movida para as contas as receber"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao atualizar status"];
        }
    }

    public function deletar($id) {
        $responseBoletoContaReceber = $this->db
                        ->where("idContaReceber", $id)
                        ->get("boletos_contas_receber")->row();
        $responseBoleto = $this->db
                        ->where("id", $responseBoletoContaReceber->idBoleto)
                        ->get("boletos")->row();

        $this->db->delete("boletos_contas_receber", ["id" => $responseBoletoContaReceber->id]);
        $this->db->delete("boletos", ["id" => $responseBoleto->id]);
        $this->db->delete("contas_receber", ["id" => $id]);
        $this->db->delete("parcelas", ["id" => $responseBoleto->idParcela]);

        if ($this->db->affected_rows() > 0) {
            return ["resultado" => TRUE, "msg" => "Receita deletada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao deletar receita"];
        }
    }

    private function configurarParcela() {
        return [
            "contrato" => $this->dados["contrato"],
            "valor" => $this->valor,
            "vencimento" => $this->data,
            "parcela" => 1,
            "totalParcelamento" => 1
        ];
    }

    public function toArray() {
        return [
            'descricao' => $this->descricao,
            'valor' => $this->valor,
            'idCategoria' => $this->idCategoria,
            'data' => $this->data,
            'pagamento' => $this->pagamento,
            'status' => $this->status
        ];
    }

}
