<?php

use CnabPHP\Retorno;

class Ret extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model("financeiro/parcela");
    }
    public function getAll() {
        return $this->db->get("retornos")->result();
    }

    public function processar($file) {
        $fileContent = file_get_contents(base_url("assets/arquivos/retornos/{$file}"));
        $arquivo = new Retorno($fileContent);
        $registros = $arquivo->getRegistros();
        $baixas = 0;
        $temp = [];

        $this->db->trans_start();
        foreach ($registros as $registro) {
            if ($registro->codigo_movimento == 2) {
//                $this->db
//                        ->where("idParcela", substr($registro->seu_numero, 5, 5))
//                        ->update("boletos", ["status" => 2, "dataPagamento" => date("Y-m-d", strtotime(str_replace("/","-",$registro->data_ocorrencia)))]);
//                $this->db
//                        ->where("id", intval($registro->seu_numero))
//                        ->update("boletos", ["status" => "pago"]);
                $this->parcela->baixar(["id"=>$registro->seu_numero]);
                $baixas++;
            }
        }

        $this->db->insert("retornos", ["arquivo" => $file]);
        $responseRetornos = $this->getAll();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return [
                "temp"=>$temp,
                "resultado" => TRUE,
                "msg" => "Foi dado baixa em ${baixas} boletos",
                "retornos" => $responseRetornos
            ];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha no processamento do arquivo"];
        }
    }

}
