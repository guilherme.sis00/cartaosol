<?php

class Categoria extends CI_Model {

    private $id;
    private $categoria;
    private $tipo;

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->categoria = !empty($dados['categoria']) ? $dados['categoria'] : NULL;
        $this->tipo = !empty($dados['tipo']) ? $dados['tipo'] : NULL;
    }

    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE, $returnArray = TRUE) {
        if ($paginacao) {
            $this->db->limit($limit, $offset);
        }

        $response = $this->db
                        ->order_by("categoria", "ASC")
                        ->get("categorias_financeiras")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => FALSE, "categorias" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma categoria financeira encontrada"];
            }
        } else {
            return $response;
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("categorias_financeiras", ["categoria" => $this->categoria,"tipo" => $this->tipo]);
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Categoria adicionada","tipo"=>$this->tipo, "categorias" => $this->getAll(NULL, NULL, FALSE, FALSE)];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar categoria"];
        }
    }

}
