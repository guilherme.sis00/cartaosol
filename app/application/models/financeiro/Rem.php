<?php

use CnabPHP\Remessa;

class Rem extends CI_Model {

    private $id;
    private $tipo;
    private $data;
    private $conteudo;
    private $path;
    private $status;

    public function __construct() {
        parent::__construct();
        $this->load->model(["administrativo/empresa", "financeiro/boleto"]);
    }

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->tipo = !empty($dados["tipo"]) ? $dados["tipo"] : "";
        $this->data = !empty($dados['data']) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados['data']))) : "0000-00-00";
        $this->conteudo = !empty($dados["conteudo"]) ? $dados["conteudo"] : "";
        $this->path = !empty($dados["path"]) ? $dados["path"] : "";
        $this->status = !empty($dados["status"]) ? $dados["status"] : 1;
    }

    public function get($atributo) {
        switch ($atributo) {
            case "id":
                return $this->id;
            case "conteudo":
                return $this->conteudo;
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db
                ->set($this->toArray())
                ->insert("remessas");
        $this->id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Remessa gerada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao gerar remessa"];
        }
    }

    public function atualizar() {
        $this->db->trans_start();
        $this->db
                ->set($this->toArray())
                ->where("id", $this->id)
                ->update("remessas");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Remessa atualizada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao gerar remessa"];
        }
    }

    public function gerar($dados) {
        $this->tipo = "remessa";
        $this->data = date("Y-m-d");
        $this->conteudo = "";
        $this->path = "";
        $this->status = 1;

        $this->adicionar();
        $responseEmpresa = $this->empresa->get(FALSE);

        $agencia = explode("-", $responseEmpresa->agencia);
        $conta = explode("-", $responseEmpresa->conta);

        $arquivo = new Remessa("033", 'cnab240', array(
            'nome_empresa' => "{$responseEmpresa->nomeFantasia}", // seu nome de empresa
            'tipo_inscricao' => 2, // 1 para cpf, 2 cnpj
            'numero_inscricao' => "{$responseEmpresa->cnpj}", // seu cpf ou cnpj completo
            'agencia' => $agencia[0], // agencia sem o digito verificador
            'agencia_dv' => $agencia[1], // somente o digito verificador da agencia
            'conta' => "{$conta[0]}", // número da conta
            'conta_dv' => "{$conta[1]}", // digito da conta
            'codigo_beneficiario' => substr("9452079", 0,6),
            'codigo_beneficiario_dv' => substr("9452079", 6,7), // codigo fornecido pelo banco
            'codigo_transmissao' => "{$responseEmpresa->codigoTransmissao}",
            'numero_sequencial_arquivo' => $this->id,
            'situacao_arquivo' => 'P' // use T para teste e P para produção
        ));

        $lote = $arquivo->addLote(array('tipo_servico' => 1, 'codigo_transmissao' => $responseEmpresa->codigoTransmissao)); // tipo_servico  = 1 para cobrança registrada, 2 para sem registro

        $responseBoletos = $this->boleto->getByIdsParcelas($dados["boleto"], FALSE);
        $idsBoletos = [];

        foreach ($responseBoletos as $boleto) {
            $lote->inserirDetalhe([
                'conta_cobranca' => "{$conta[0]}",
                'especie_titulo' => "DS",
                'tipo_cobranca' => 5,
                'nosso_numero' => $boleto->idParcela, // numero sequencial de boleto
                'seu_numero' => $boleto->idParcela, // numero sequencial de boleto
                'data_vencimento' => $boleto->vencimento, // informar a data neste formato
                'valor' => $boleto->valor, // Valor do boleto como float valido em php
                'data_emissao' => $boleto->dataEmissao, // informar a data neste formato
                'codigo_juros' => '2',
                'data_juros' => date("Y-m-d", strtotime("+1 days", strtotime($boleto->vencimento))),
                'vlr_juros' => "199",
                'codigo_multa' => '2',
                'data_multa' => date("Y-m-d", strtotime("+30 days", strtotime($boleto->vencimento))),
                'vlr_multa' => 200 ,
                'codigo_desconto' => '0', // Valor fixo ate a data informada – Informar o valor no campo "valor de desconto a ser concedido"
                'data_desconto' => '0',
                'vlr_desconto' => '0',
                'protestar' => '0', // NÃO PROTESTAR
                'prazo_protesto' => '0', // Informar o número de dias apos o vencimento para iniciar o protesto
                'baixar' => '2', // NÃO BAIXAR
                'prazo_baixar' => '0',
                'nome_pagador' => $boleto->nome, // O Pagador é o cliente, preste atenção nos campos abaixo
                'tipo_inscricao' => 1, //campo fixo, escreva '1' se for pessoa fisica, 2 se for pessoa juridica
                'numero_inscricao' => "{$boleto->cpf}", //cpf ou ncpj do pagador
                'cidade_pagador' => $boleto->cidade,
                'uf_pagador' => $boleto->uf,
                'endereco_pagador' => "Rua papoula",
                'bairro_pagador' => 'Tancredo Neves',
                'cep_pagador' => '69087-170', // com hífem
                'mensagem' => "Após vencimento, juros de 1,99% ao mês"
            ]);
            array_push($idsBoletos, $boleto->id);
        }

        $resultado = utf8_decode($arquivo->getText()); // observar a header do seu php para não gerar comflitos de codificação de caracteres;
        $file = 'remessa-' . date("Y-m-d_H:i:s") . '.rem';
        file_put_contents("./assets/arquivos/remessas/{$file}", $resultado);

        if (file_exists("./assets/arquivos/remessas/{$file}")) {
            $this->conteudo = $resultado;
            $this->path = $file;
            $this->status = 1;
            $this->atualizar();

            $this->boleto->baixaRemessa($idsBoletos, $this->data);
            return ["resultado" => TRUE, "msg" => "Remessa gerada", "file" => $file];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao gerar remessa"];
        }
    }

    public function toArray() {
        return [
            "tipo" => $this->tipo,
            "data" => $this->data,
            "conteudo" => $this->conteudo,
            "path" => $this->path,
            "status " => $this->status
        ];
    }

}
