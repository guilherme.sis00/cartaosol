<?php

class Parcela extends CI_Model {

    private $id;
    private $idContrato;
    private $valor;
    private $vencimento;
    private $parcela;
    private $totalParcelamento;

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->idContrato = !empty($dados['contrato']) ? $dados['contrato'] : NULL;
        $this->valor = !empty($dados['valor']) ? $dados['valor'] : NULL;
        $this->vencimento = !empty($dados['vencimento']) ? $dados['vencimento'] : NULL;
        $this->parcela = !empty($dados['parcela']) ? $dados['parcela'] : NULL;
        $this->totalParcelamento = !empty($dados['totalParcelamento']) ? $dados['totalParcelamento'] : NULL;
    }

    public function get($atributo) {
        switch ($atributo) {
            case "id":
                return $this->id;
        }
    }

    public function getById($id, $returnArray = TRUE) {
        $response = $this->db
                        ->select("parcelas.*,"
                                . "boletos_contas_receber.idContaReceber")
                        ->join("boletos", "boletos.idParcela=parcelas.id")
                        ->join("boletos_contas_receber", "boletos_contas_receber.idBoleto=boletos.id")
                        ->where("parcelas.id", $id)
                        ->get("parcelas")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "parcela" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma parcela foi encontrada"];
            }
        } else {
            return $response;
        }
    }

    public function getByIdContrato($idContrato, $returnArray = TRUE) {
        $response = $this->db
                        ->select("parcelas.*,"
                                . "boletos.dataPagamento,"
                                . "boletos_contas_receber.idContaReceber,"
                                . "boletos.status")
//                                . "contas_receber.status")
                        ->join("boletos", "boletos.idParcela=parcelas.id")
                        ->join("boletos_contas_receber", "boletos_contas_receber.idBoleto=boletos.id")
//                        ->join("contas_receber", "boletos_contas_receber.idContaReceber=contas_receber.id")
                        ->where("parcelas.idContrato", $idContrato)
                        ->order_by("parcelas.vencimento", "ASC")
                        ->get("parcelas")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "parcelas" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma parcela foi encontrada"];
            }
        } else {
            return $response;
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("parcelas", $this->toArray());
        $this->id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Parcela adicionada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar parcela"];
        }
    }

    public function atualizar($dados) {
        $this->db->trans_start();
        $this->db
                ->set(["valor" => $dados["valor"], "vencimento" => date("Y-m-d", strtotime(str_replace("/", "-", $dados["vencimento"])))])
                ->where("id", $dados["id"])
                ->update("parcelas");
        $this->db
                ->set(["valor" => $dados["valor"], "data" => date("Y-m-d", strtotime(str_replace("/", "-", $dados["vencimento"])))])
                ->where("id", $dados["idContaReceber"])
                ->update("contas_receber");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Parcela atualizada");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao atualizar parcela");
        }
    }

    public function deletar($id) {
        $response = $this->db
                        ->select("boletos_contas_receber.id,"
                                . "boletos_contas_receber.idContaReceber,"
                                . "boletos_contas_receber.idBoleto")
                        ->join("boletos", "boletos_contas_receber.idBoleto=boletos.id")
                        ->join("parcelas", "boletos.idParcela=parcelas.id")
                        ->where("parcelas.id", $id)
                        ->get("boletos_contas_receber")->row();

        $this->db->delete("boletos_contas_receber", ["id" => $response->id]);
        $this->db->delete("contas_receber", ["id" => $response->idContaReceber]);
        $this->db->delete("boletos", ["id" => $response->idBoleto]);
        $this->db->delete("parcelas", ["id" => $id]);
        $this->db->trans_complete();

        if ($this->db->affected_rows() > 0) {
            return array("resultado" => TRUE, "msg" => "Parcela deletada");
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao deletar parcela");
        }
    }

    public function baixar($dados) {
        $this->db->trans_start();
        $this->db
                ->set(["dataPagamento" => date("Y-m-d"), "status" => 2])
                ->where("idParcela", $dados["id"])
                ->update("boletos");
        $this->db
                ->set([
                    "status" => "recebido",
                    "pagamento" => date("Y-m-d"),
                    "formaPagamento" => "boleto",
                    "conta" => "banco-santander"
                ])
                ->where("id", $dados["idContaReceber"])
                ->update("contas_receber");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return array("resultado" => TRUE, "msg" => "Parcela baixada", "dataBaixa" => date("d/m/Y"));
        } else {
            return array("resultado" => FALSE, "msg" => "Falha ao dar baixa parcela");
        }
    }

    public function toArray() {
        return [
            "idContrato" => $this->idContrato,
            "valor" => $this->valor,
            "vencimento" => $this->vencimento,
            "parcela" => $this->parcela,
            "totalParcelamento" => $this->totalParcelamento,
        ];
    }

    //NÃO FUNCIONAL
    public function getAll($limit = NULL, $offset = NULL, $paginacao = TRUE, $returnArray = TRUE) {
        if ($paginacao) {
            $this->db->limit($limit, $offset);
        }

        $response = $this->db
                        ->order_by("categoria", "ASC")
                        ->get("categorias_financeiras")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => FALSE, "categorias" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhuma categoria financeira encontrada"];
            }
        } else {
            return $response;
        }
    }

    public function buscar($dados) {
        $response = $this->db
                        ->like($dados["tipoBusca"], $dados["termo"])
                        ->get("ocpt_clientes")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "clientes" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum cliente encontrado"];
        }
    }

}
