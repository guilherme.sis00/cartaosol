<?php

use OpenBoleto\Banco\Santander;
use OpenBoleto\Agente;

class Boleto extends CI_Model {

    private $id;
    private $idParcela;
    private $dataPagamento;
    private $dataEmissao;
    private $nossoNumero;
    private $instrucao1;
    private $instrucao2;
    private $instrucao3;
    private $instrucao4;
    private $status;

    public function __construct() {
        parent::__construct();
        $this->load->model(["administrativo/empresa", "cliente"]);
    }

    public function preencherDados($dados) {
        $this->id = !empty($dados['id']) ? $dados['id'] : NULL;
        $this->idParcela = !empty($dados["parcela"]) ? $dados["parcela"] : NULL;
        $this->dataPagamento = !empty($dados['dataPagamento']) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados['dataPagamento']))) : "0000-00-00";
        $this->dataEmissao = !empty($dados["dataEmissao"]) ? date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataEmissao"]))) : date("Y-m-d");
        $this->nossoNumero = !empty($dados["nossoNumero"]) ? $dados["nossoNumero"] : 0;
        $this->instrucao1 = !empty($dados['instrucao1']) ? $dados['instrucao1'] : "";
        $this->instrucao2 = !empty($dados["instrucao2"]) ? $dados["instrucao2"] : "";
        $this->instrucao3 = !empty($dados['instrucao3']) ? $dados['instrucao3'] : "";
        $this->instrucao4 = !empty($dados["instrucao4"]) ? $dados["instrucao4"] : "";
        $this->status = !empty($dados["status"]) ? $dados["status"] : 0;
    }

    public function get($atributo) {
        switch ($atributo) {
            case "id":
                return $this->id;
        }
    }

    public function getByIdParcela($idParcela, $returnArray = TRUE) {
        $response = $this->db
                        ->select("boletos.*,"
                                . "parcelas.parcela,"
                                . "parcelas.totalParcelamento,"
                                . "parcelas.valor,"
                                . "parcelas.vencimento,"
                                . "contratos.idCliente")
                        ->where("boletos.idParcela", $idParcela)
                        ->join("parcelas", "parcelas.id=boletos.idParcela")
                        ->join("contratos", "parcelas.idContrato=contratos.id")
                        ->get("boletos")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "boleto" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum boleto encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function getByIdReceita($idReceita, $returnArray = TRUE) {
        $response = $this->db
                        ->select("boletos.*,"
                                . "parcelas.parcela,"
                                . "parcelas.totalParcelamento,"
                                . "parcelas.valor,"
                                . "parcelas.vencimento,"
                                . "contratos.idCliente")
                        ->where("boletos_contas_receber.idContaReceber", $idReceita)
                        ->join("boletos_contas_receber", "boletos.id=boletos_contas_receber.idBoleto")
                        ->join("parcelas", "parcelas.id=boletos.idParcela")
                        ->join("contratos", "parcelas.idContrato=contratos.id")
                        ->get("boletos")->row();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "boleto" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum boleto encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function getByIdsParcelas($ids, $returnArray = TRUE) {
        $primeiro = FALSE;

        $this->db
                ->select("boletos.*,"
                        . "parcelas.parcela,"
                        . "parcelas.totalParcelamento,"
                        . "parcelas.valor,"
                        . "parcelas.vencimento,"
                        . "contratos.idCliente,"
                        . "clientes.nome,"
                        . "clientes.cpf,"
                        . "cidades.cidade,"
                        . "estados.uf")
                ->join("parcelas", "parcelas.id=boletos.idParcela")
                ->join("contratos", "parcelas.idContrato=contratos.id")
                ->join("clientes", "contratos.idCliente=clientes.id")
                ->join("cidades", "clientes.idCidade=cidades.id")
                ->join("estados", "clientes.idEstado=estados.id");

        foreach ($ids as $id) {
            if ($primeiro) {
                $this->db->where("boletos.id", $id);
                $primeiro = FALSE;
            } else {
                $this->db->or_where("boletos.id", $id);
            }
        }

        $response = $this->db->get("boletos")->result();

        if ($returnArray) {
            if (!empty($response)) {
                return ["resultado" => TRUE, "boletos" => $response];
            } else {
                return ["resultado" => FALSE, "msg" => "Nenhum boleto encontrado"];
            }
        } else {
            return $response;
        }
    }

    public function baixaRemessa($dados, $data) {
        $this->db->trans_start();
        $primeiro = TRUE;
        $this->db
                ->set([
                    "exportado" => 2,
                    "dataRemessa" => $data
        ]);

        foreach ($dados as $boleto) {
            if ($primeiro) {
                $this->db->where("boletos.id", $boleto);
                $primeiro = FALSE;
            } else {
                $this->db->or_where("boletos.id", $boleto);
            }
        }

        $this->db->update("boletos");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Boletos atualizados"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao atualizar boletos"];
        }
    }

    public function getBoletosParaRemessa($dados) {
        $this->db
                ->select("boletos.id,"
                        . "boletos.dataEmissao,"
                        . "boletos.dataRemessa,"
                        . "boletos.exportado,"
                        . "boletos.status,"
                        . "clientes.nome,"
                        . "parcelas.vencimento,"
                        . "parcelas.valor")
                ->join("parcelas", "parcelas.id=boletos.idParcela")
                ->join("contratos", "parcelas.idContrato=contratos.id")
                ->join("clientes", "clientes.id=contratos.idCliente")
                ->order_by("clientes.nome")
                ->order_by("parcelas.vencimento")
                ->where("boletos.exportado", $dados["exportado"])
                ->where("boletos.status", 1);

        if ($dados["cliente"] != "todos") {
            $this->db->where("clientes.id", $dados["cliente"]);
        }

        if (!empty($dados["dataInicial"]) && !empty($dados["dataFinal"])) {
            $this->db->where("parcelas.vencimento BETWEEN '" . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataInicial"]))) . "' AND '" . date("Y-m-d", strtotime(str_replace("/", "-", $dados["dataFinal"]))) . "'");
        }

        $response = $this->db->get("boletos")->result();

        if (!empty($response)) {
            return ["resultado" => TRUE, "boletos" => $response];
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhum boleto encontrado"];
        }
    }

    public function adicionar() {
        $this->db->trans_start();
        $this->db->insert("boletos", $this->toArray());
        $this->id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Boleto adicionado"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar boleto"];
        }
    }

    public function gerar($dados) {
        $responseCliente = $this->cliente->getById($dados["idCliente"], FALSE);
        $responseEmpresa = $this->empresa->get(FALSE);

        $cedente = new Agente($responseEmpresa->nomeFantasia, $responseEmpresa->cnpj, "{$responseEmpresa->cep} - {$responseEmpresa->logradouro}, {$responseEmpresa->numero}, {$responseEmpresa->cidade}/{$responseEmpresa->uf}", $responseEmpresa->telefone, $responseEmpresa->estado, $responseEmpresa->uf);
        $sacado = new Agente($responseCliente->nome, $responseCliente->cpf, '', '', $responseCliente->estado, $responseCliente->cidade);

        $agencia = explode("-", $responseEmpresa->agencia);

        $dv = \OpenBoleto\BoletoAbstract::modulo11($dados["idParcela"]);
//        $dv = \OpenBoleto\BoletoAbstract::modulo11(40);
        $boleto = new Santander([
            // Parâmetros obrigatórios
            'cedente' => $cedente,
            'carteira' => $responseEmpresa->carteira, // 101, 102 ou 201
            'ios' => $responseEmpresa->ios, // Apenas para o Santander
            'logoPath' => base_url("assets/img/marca.png"), // Logo da sua empresa
            'conta' => "{$responseEmpresa->codigoPsk}", // Código do cedente: Até 7 dígitos
            'agencia' => "{$agencia[0]}", // Até 4 dígitos
            'agenciaDv' => "{$agencia[1]}",
            'instrucoes' => [// Até 8
                "Parcela {$dados["parcela"]}/{$dados["totalParcelamento"]}",
                $dados["instrucao1"],
                $dados["instrucao2"],
                $dados["instrucao3"],
                $dados["instrucao4"]
            ],
            'dataVencimento' => new DateTime($dados["vencimento"]),
            'valor' => $dados["valor"],
            'sequencial' => $dados["idParcela"] . $dv["digito"], // Até 13 dígitos
            'sacado' => $sacado,
            // IOS – Seguradoras (Se 7% informar 7. Limitado a 9%)
            // Demais clientes usar 0 (zero)
            // Parâmetros recomendáveis
//            'contaDv' => $conta[1],
            'descricaoDemonstrativo' => [// Até 5
                "Parcela {$dados["parcela"]}/{$dados["totalParcelamento"]}",
                "Após vencimento, juros de 1,99% ao mês"
            ]
        ]);

        return $boleto;
    }

    public function analisarContratos() {
        $response = $this->db
                        ->select("boletos.id,"
                                . "boletos.idParcela,"
                                . "boletos.status,"
                                . "parcelas.idContrato,"
                                . "contratos.situacao")
                        ->join("parcelas", "boletos.idParcela=parcelas.id")
                        ->join("contratos", "parcelas.idContrato=contratos.id")
                        ->where("ocpt_boletos.status", 1)
                        ->where("date(ocpt_parcelas.vencimento) < '" . date("Y-m-d") . "'")
                        ->where("date(ocpt_parcelas.vencimento) > '" . date("Y-m-d",strtotime("2000-01-01")) . "'")
                        ->get("boletos")->result();

        $this->db->trans_start();
        $primeiro = TRUE;
        foreach ($response as $dado) {
            if ($dado->status == 1) {
                if ($primeiro) {
                    $this->db->where("id", $dado->id);
                    $primeiro = FALSE;
                } else {
                    $this->db->or_where("id", $dado->id);
                }
            }
        }
        $this->db->update("boletos", ["status" => 3]);
        $this->db->trans_complete();
    }

    public function toArray() {
        return [
            "idParcela" => $this->idParcela,
            "dataPagamento" => $this->dataPagamento,
            "dataEmissao" => $this->dataEmissao,
            "nossoNumero" => $this->nossoNumero,
            "instrucao1 " => $this->instrucao1,
            "instrucao2 " => $this->instrucao2,
            "instrucao3 " => $this->instrucao3,
            "instrucao4 " => $this->instrucao4,
            "status " => $this->status
        ];
    }

}
