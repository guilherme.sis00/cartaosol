<?php

class Utils extends CI_Model {

    public function countAll($tabela) {
        return $this->db->count_all($tabela);
    }

    public function getSaldoTotal($returnArray = TRUE) {
        $receitas = $this->db
                        ->select("COALESCE(SUM(valor),0) AS total")
                        ->where("status", "recebido")
                        ->get("contas_receber")->row();

        $despesas = $this->db
                        ->select("COALESCE(SUM(valor),0) AS total")
                        ->where("status", "pago")
                        ->get("contas_pagar")->row();

        if ($returnArray) {
            return["saldoTotal" => ($receitas->total - $despesas->total)];
        } else {
            return $receitas->total - $despesas->total;
        }
    }
    
    public function getSaldoInicial($dataReferencia, $returnArray = TRUE) {
//        $dataInicial = date("Y-m-d", strtotime("-1 month", strtotime($dataReferencia)));
        $dataInicial = "2017-01-01";
        $dataFinal = date("Y-m-d", strtotime("-1 days", strtotime($dataReferencia)));

        $receitas = $this->db
                        ->select("COALESCE(SUM(valor),0) AS total")
                        ->where("status", "recebido")
                        ->where("pagamento BETWEEN '" . $dataInicial . "' AND '" . $dataFinal . "'")
                        ->get("contas_receber")->row();

        $despesas = $this->db
                        ->select("COALESCE(SUM(valor),0) AS total")
                        ->where("status", "pago")
                        ->where("pagamento BETWEEN '" . $dataInicial . "' AND '" . $dataFinal . "'")
                        ->get("contas_pagar")->row();

        if ($returnArray) {
            return["saldoInicial" => ($receitas->total - $despesas->total)];
        } else {
            return $receitas->total - $despesas->total;
        }
    }

    public function getEspecialidades() {
        return $this->db
                        ->order_by("especialidade", "ASC")
                        ->get("ocpt_especialidades")->result();
    }

    public function getTiposEspecialidades() {
        return $this->db
                        ->order_by("tipo", "ASC")
                        ->get("ocpt_tipos_especialidades")->result();
    }

    public function getEstados() {
        return $this->db
                        ->order_by("estado", "ASC")
                        ->get("ocpt_estados")->result();
    }

    public function getCidadesByIdEstado($idEstado, $returnArray = TRUE) {
        $response = $this->db
                        ->where("idEstado", $idEstado)
                        ->order_by("cidade", "ASC")
                        ->get("ocpt_cidades")->result();

        if (!empty($response)) {
            if ($returnArray) {
                return ["resultado" => TRUE, "cidades" => $response];
            } else {
                return $response;
            }
        } else {
            return ["resultado" => FALSE, "msg" => "Nenhuma cidade encontrada"];
        }
    }

    public function adicionarCidade($dados) {
        $this->db->trans_start();
        $this->db
                ->set([
                    "cidade" => $dados["cidade"],
                    "idEstado" => $dados["estado"],
                ])
                ->insert("ocpt_cidades");
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return ["resultado" => TRUE, "msg" => "Cidade adicionada"];
        } else {
            return ["resultado" => FALSE, "msg" => "Falha ao adicionar cidade"];
        }
    }

}
