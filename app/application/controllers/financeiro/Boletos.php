<?php

//STATUS
//1 - BOLETO GERADO
//2 - BOLETO PAGO
//3 - BOLETO EM ATRASO
//"kriansa/openboleto": "^1.0",

class Boletos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["financeiro/boleto"]);
    }

    public function visualizarPorParcela() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(3) && is_numeric($this->uri->segment(3)))) {
                $responseBoleto = $this->boleto->getByIdParcela($this->uri->segment(3));
                
                if ($responseBoleto['resultado']) {
                    $responseBoletoImpressao = $this->boleto->gerar((array) $responseBoleto["boleto"]);

                    $this->load->view("financeiro/boletos/visualizar", ["boleto" => $responseBoletoImpressao->getOutput()]);
                } else {
                    $this->session->set_flashdata("msg", $responseBoleto["msg"]);
                    $this->load->view("financeiro/boletos/visualizar");
                }
            } else {
                $this->session->set_flashdata("msg", "Não foi possível visualizar o boleto");
                $this->load->view("financeiro/boletos/visualizar");
            }
        } else {
            redirect("login");
        }
    }

    public function visualizarPorReceita() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(4) && is_numeric($this->uri->segment(4)))) {
                $responseBoleto = $this->boleto->getByIdReceita($this->uri->segment(4));

                if ($responseBoleto['resultado']) {
                    $responseBoletoImpressao = $this->boleto->gerar((array) $responseBoleto["boleto"]);

                    $this->load->view("financeiro/boletos/visualizar", ["boleto" => $responseBoletoImpressao->getOutput()]);
                } else {
                    $this->session->set_flashdata("msg", $responseBoleto["msg"]);
                    $this->load->view("financeiro/boletos/visualizar");
                }
            } else {
                $this->session->set_flashdata("msg", "Não foi possível visualizar o boleto");
                $this->load->view("financeiro/boletos/visualizar");
            }
        } else {
            redirect("login");
        }
    }

}
