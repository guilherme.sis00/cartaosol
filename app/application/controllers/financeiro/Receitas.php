<?php

class Receitas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model([
            "financeiro/receita",
            "financeiro/categoria",
            "administrativo/contrato"
        ]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $ultimoDiaMes = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
            $dataInicial = date("Y-m") . "-01";
            $dataFinal = date("Y-m") . "-" . $ultimoDiaMes;

            $dados["contasReceber"] = $this->receita->getByPeriodo($dataInicial, $dataFinal, FALSE);
            $dados["categorias"] = $this->categoria->getAll(NULL, NULL, FALSE, FALSE);
            $dados["mesAtual"] = getMesExtenso(date("m"));
            $dados["recebido"] = $this->receita->getTotalByStatus("recebido", $dataInicial, $dataFinal);

            $this->load->view("financeiro/receitas/home", $dados);
        } else {
            redirect("login");
        }
    }

    public function getReceita() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->input->post("id")) {
                echo json_encode($this->receita->getById($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function vAtualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(3) && is_numeric($this->uri->segment(3)))) {
                $response = $this->receita->getById($this->uri->segment(3));

                if ($response['resultado']) {
                    $response["categorias"] = $this->categoria->getAll(NULL, NULL, FALSE, FALSE);
                    $response["contratos"] = $this->contrato->getAll(NULL, NULL, FALSE);

                    $this->load->view("financeiro/receitas/atualizar", $response);
                } else {
                    $this->session->set_flashdata("msg", "Conta a receber inexistente");
                    redirect("financeiro/receitas");
                }
            } else {
                $this->session->set_flashdata("msg", "Conta a receber inexistente");
                redirect("financeiro/receitas");
            }
        } else {
            redirect("login");
        }
    }

    public function buscar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("categoria")) && !empty($this->input->post("dataInicial")) && !empty($this->input->post("dataFinal"))) {
                echo json_encode($this->receita->buscar($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function buscarPorMes() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("mes"))) {
                $ultimoDiaMes = cal_days_in_month(CAL_GREGORIAN, date("m", strtotime($this->input->post("mes"))), date("Y"));
                $dataInicial = date("Y") . "-" . $this->input->post("mes") . "-" . "-01";
                $dataFinal = date("Y") . "-" . $this->input->post("mes") . "-" . "-" . $ultimoDiaMes;

                echo json_encode($this->receita->getByPeriodo($dataInicial, $dataFinal));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function atualizarBaixa() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id")) && !empty($this->input->post("baixa"))) {
                echo json_encode($this->receita->atualizarBaixa($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function atualizarStatus() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id")) && !empty($this->input->post("idParcela"))) {
                echo json_encode($this->receita->atualizarStatus($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function deletar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->receita->deletar($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

}
