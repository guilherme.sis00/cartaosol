<?php

class Despesas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model([
            "financeiro/despesa",
            "cadastros/fornecedor",
            "financeiro/categoria"
        ]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $ultimoDiaMes = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
            $dataInicial = date("Y-m") . "-01";
            $dataFinal = date("Y-m") . "-" . $ultimoDiaMes;

            $dados["contasPagar"] = $this->despesa->getByPeriodo($dataInicial, $dataFinal, FALSE);
            $dados["categorias"] = $this->categoria->getAll(NULL, NULL, FALSE, FALSE);
            $dados["mesAtual"] = getMesExtenso(date("m"));
            $dados["pago"] = $this->despesa->getTotalByStatus("pago", $dataInicial, $dataFinal);
//            $dados["pendente"] = $this->contaPagar->getTotalByStatus("pendente", $dataInicial, $dataFinal);

            $this->load->view("financeiro/despesas/home", $dados);
        } else {
            redirect("login");
        }
    }

    public function getDespesa() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->input->post("id")) {
                echo json_encode($this->despesa->getById($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function vAtualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(3) && is_numeric($this->uri->segment(3)))) {
                $response = $this->despesa->getById($this->uri->segment(3));

                if ($response['resultado']) {
                    $response["categorias"] = $this->categoria->getAll(NULL, NULL, FALSE, FALSE);
                    $response["fornecedores"] = $this->fornecedor->getAll(NULL, NULL, FALSE, FALSE);

                    $this->load->view("financeiro/contas-pagar/atualizar", $response);
                } else {
                    $this->session->set_flashdata("msg", "Conta a pagar inexistente");
                    redirect("financeiro/contas-pagar");
                }
            } else {
                $this->session->set_flashdata("msg", "Conta a pagar inexistente");
                redirect("financeiro/contas-pagar");
            }
        } else {
            redirect("login");
        }
    }

    public function buscar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("categoria")) && !empty($this->input->post("mes")) && !empty($this->input->post("ano"))) {
                echo json_encode($this->despesa->buscar($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function buscarPorMes() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("mes"))) {
                $ultimoDiaMes = cal_days_in_month(CAL_GREGORIAN, date("m", strtotime($this->input->post("mes"))), date("Y"));
                $dataInicial = date("Y") . "-" . $this->input->post("mes") . "-" . "-01";
                $dataFinal = date("Y") . "-" . $this->input->post("mes") . "-" . "-" . $ultimoDiaMes;

                echo json_encode($this->despesa->getByPeriodo($dataInicial, $dataFinal));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function atualizarBaixa() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id")) && !empty($this->input->post("baixa"))) {
                echo json_encode($this->despesa->atualizarBaixa($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function atualizarStatus() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->despesa->atualizarStatus($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function deletar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->despesa->deletar($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

}
