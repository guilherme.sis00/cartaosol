<?php

class Retornos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["financeiro/ret"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $dados["retornos"] = $this->ret->getAll();
            $this->load->view('financeiro/retornos/home', $dados);
//            $this->load->view('financeiro/retornos/home');
        } else {
            redirect("login");
        }
    }

    public function processar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            $config['upload_path'] = './assets/arquivos/retornos/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 0;
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload("retorno")) {
                echo json_encode($this->ret->processar($this->upload->data("file_name")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha ao fazer upload do arquivo, " . $this->upload->display_errors()]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

}
