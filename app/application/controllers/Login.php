<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["usuario"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            redirect("dashboard");
        } else {
            $this->load->view('autenticacao/login');
        }
    }

    public function autenticar() {
        if ($this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->usuario->preencherDados($this->input->post());
                $response = $this->usuario->autenticar();

                if ($response["resultado"]) {
                    $this->session->set_userdata([
                        'id' => $response['dados']->id,
                        'usuario' => $response['dados']->usuario,
                        'ocpt' => TRUE
                    ]);
                }

                echo json_encode($response);
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Dados não informados"]);
            }
        } else {
            redirect("login");
        }
    }

    public function recuperarSenha() {
        if ($this->input->post("ajax")) {
            if (!empty($this->input->post("usuario"))) {
                $size = 6;
                $seed = time();
                $novaSenha = substr(sha1($seed), 30 - min($size, 30));
                $response = $this->usuario->recuperarSenha(["usuario" => $this->input->post("usuario"), "novaSenha" => $novaSenha]);

                if ($response["resultado"]) {
                    $this->load->library("email");
                    $config['validate'] = TRUE;
                    $config['mailtype'] = 'html';

                    $this->email->initialize($config);
                    
                    $this->email->from('contato@ocartaoparatodos.com.br', 'O cartão para todos');
                    $this->email->to($this->input->post("usuario"));
                    $this->email->subject('Redefinir Senha');
                    $this->email->message("<html>"
                            . "<head>"
                            . "<meta content='text/html;charset=utf-8'>"
                            . "</head>"
                            . "<body>"
                            . "<p>Senha: " . $novaSenha . "</h3>"
                            . "<p>Recomendamos que altere este senha após acessar a plataforma.</p>"
                            . "</body>"
                            . "</html>");
                    $this->email->set_alt_message('Senha: ' . $novaSenha . ". Recomendamos que altere este senha após acessar a plataforma");

                    if ($this->email->send()) {
                        echo json_encode(["resultado" => TRUE, "msg" => "Uma nova senha foi gerada e enviada para o seu e-mail"]);
                    } else {
                        echo json_encode(["resultado" => FALSE, "msg" => "Uma nova senha foi gerada porém não conseguimos enviar para o seu e-mail, por favor tente novamente"]);
                    }
                } else {
                    echo json_encode($response);
                }
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            redirect("login");
        }
    }

    public function sair() {
        $this->session->sess_destroy();
        redirect("login");
    }

}
