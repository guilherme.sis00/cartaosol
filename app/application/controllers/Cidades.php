<?php

class Cidades extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["cidade"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $config = $this->configuracao->getConfigPagination(base_url('cidades'), $this->utils->countAll("ocpt_convenios_descontos"));
            $this->pagination->initialize($config);
            $dados['paginacao'] = $this->pagination->create_links();

            $offset = ($this->uri->segment(2)) ? (($this->uri->segment(2) - 1) * 10) : 0;
            $dados["cidades"] = $this->cidade->getAll($config['per_page'], $offset);
            $this->load->view("cidades/home", $dados);
        } else {
            redirect("login");
        }
    }

    public function getById() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->cidade->getById($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function vAdicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $dados["estados"] = $this->utils->getEstados();

            $this->load->view("cidades/adicionar", $dados);
        } else {
            redirect("login");
        }
    }

    public function vAtualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(2) && is_numeric($this->uri->segment(2)))) {
                $response = $this->cidade->getById($this->uri->segment(2));

                if ($response['resultado']) {
                    $dados["estados"] = $this->utils->getEstados();
                    $dados["cidades"] = $this->utils->getCidadesByIdEstado($response['cidade']->idEstado, FALSE);
                    $dados["cidade"] = $response["cidade"];

                    $this->load->view("cidades/atualizar", $dados);
                } else {
                    $this->session->set_flashdata([
                        "msg", "Cidade inexistente",
                        "tipo", "warning"
                    ]);
                    redirect("cidades");
                }
            } else {
                $this->session->set_flashdata([
                    "msg", "Cidade inexistente",
                    "tipo", "warning"
                ]);
                redirect("cidades");
            }
        } else {
            redirect("login");
        }
    }

    public function adicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->cidade->preencherDados($this->input->post());
                echo json_encode($this->cidade->adicionar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function atualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->cidade->preencherDados($this->input->post());
                echo json_encode($this->cidade->atualizar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function deletar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(3) && is_numeric($this->uri->segment(3)))) {
                $response = $this->cidade->deletar($this->uri->segment(3));

                if ($response['resultado']) {
                    $this->session->set_flashdata([
                        "msg" => $response["msg"],
                        "tipo" => "success",
                    ]);
                } else {
                    $this->session->set_flashdata([
                        "msg" => "Operação não executada",
                        "tipo" => "danger",
                    ]);
                }

                redirect("cidades");
            } else {
                $this->session->set_flashdata([
                    "msg" => "Operação não executada",
                    "tipo" => "danger",
                ]);
                redirect("cidades");
            }
        } else {
            redirect("login");
        }
    }

    public function buscar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("termo")) && !empty($this->input->post("tipoBusca"))) {
                echo json_encode($this->cidade->buscar($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

}
