<?php

class Support extends CI_Controller {

    public function getCidades() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->get("ajax")) {
            echo json_encode($this->utils->getCidadesByIdEstado($this->input->get("estado")));
        } else {
            if ($this->input->get("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function adicionarCidade() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("estado")) && !empty($this->input->post("cidade"))) {
                echo json_encode($this->utils->adicionarCidade($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

}
