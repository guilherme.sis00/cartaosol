<?php

class Dashboard extends CI_Controller {

    private $meses = [
        "01" => "Janeiro",
        "02" => "Fevereiro",
        "03" => "Março",
        "04" => "Abril",
        "05" => "Maio",
        "06" => "Junho",
        "07" => "Julho",
        "08" => "Agosto",
        "09" => "Setembro",
        "10" => "Outubro",
        "11" => "Novembro",
        "12" => "Dezembro",
    ];

    public function __construct() {
        parent::__construct();
        $this->load->model(["financeiro/receita", "financeiro/despesa"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $ultimoDiaMes = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
            $dataInicial = date("Y-m") . "-01";
            $dataFinal = date("Y-m") . "-" . $ultimoDiaMes;

            $responseReceitas = $this->receita->getByPeriodo($dataInicial, $dataFinal, FALSE);
            $responseDespesas = $this->despesa->getByPeriodo($dataInicial, $dataFinal, FALSE);
            $responseSaldoInicial = $this->utils->getSaldoInicial($dataInicial, FALSE);
//            $responseTotal = $this->utils->getSaldoTotal(FALSE);

            $totalReceitas = 0;
            $totalDespesas = 0;

            foreach ($responseReceitas as $receita) {
                $totalReceitas += $receita->valor;
            }

            foreach ($responseDespesas as $despesa) {
                $totalDespesas += $despesa->valor;
            }

            $this->load->view("dashboard/home", [
                "receitas" => $totalReceitas,
                "despesas" => $totalDespesas,
                "saldoInicial" => $responseSaldoInicial,
//                "saldoTotal" => $responseTotal,
                "saldoTotal" => $totalReceitas - $totalDespesas + $responseSaldoInicial,
                "mesAtual" => $this->meses[date("m")]
            ]);
        } else {
            redirect("login");
        }
    }

    public function getDadosFinanceirosMes() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("mes")) && !empty($this->input->post("ano"))) {
                $ultimoDiaMes = cal_days_in_month(CAL_GREGORIAN, $this->input->post("mes"), $this->input->post("ano"));
                $dataInicial = "{$this->input->post("ano")}-{$this->input->post("mes")}-01";
                $dataFinal = "{$this->input->post("ano")}-{$this->input->post("mes")}-{$ultimoDiaMes}";

                $responseReceitas = $this->receita->getByPeriodo($dataInicial, $dataFinal, FALSE);
                $responseDespesas = $this->despesa->getByPeriodo($dataInicial, $dataFinal, FALSE);
                $responseSaldoInicial = $this->utils->getSaldoInicial($dataInicial, FALSE);
                $responseTotal = $this->utils->getSaldoTotal(FALSE);
                $totalReceitas = 0;
                $totalDespesas = 0;

                foreach ($responseReceitas as $receita) {
                    $totalReceitas += $receita->valor;
                }

                foreach ($responseDespesas as $despesa) {
                    $totalDespesas += $despesa->valor;
                }

                echo json_encode([
                    "resultado" => TRUE,
                    "receitas" => $totalReceitas,
                    "despesas" => $totalDespesas,
                    "saldoInicial" => $responseSaldoInicial,
                    "saldoTotal" => $responseTotal,
                    "mesFiltrado" => $this->meses[$this->input->post("mes")]
                ]);
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

}
