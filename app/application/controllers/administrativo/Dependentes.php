<?php

class Dependentes extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["administrativo/dependente", "cliente"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $config = $this->configuracao->getConfigPagination(base_url('administrativo/dependentes'), $this->utils->countAll("dependentes"));
            $this->pagination->initialize($config);
            $dados['paginacao'] = $this->pagination->create_links();

            $offset = ($this->uri->segment(3)) ? (($this->uri->segment(3) - 1) * 10) : 0;
            $dados["dependentes"] = $this->dependente->getAll($config['per_page'], $offset);
            $dados["farois"] = $this->dependente->getFarois($dados["dependentes"]);
            $this->load->view("administrativo/dependentes/home", $dados);
        } else {
            redirect("login");
        }
    }

    public function getDependente() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->dependente->getById($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function vAdicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $dados["clientes"] = $this->cliente->getAll(NULL, NULL, FALSE);

            $this->load->view("administrativo/dependentes/adicionar", $dados);
        } else {
            redirect("login");
        }
    }

    public function vAtualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(3) && is_numeric($this->uri->segment(3)))) {
                $response = $this->dependente->getById($this->uri->segment(3));

                if ($response['resultado']) {
                    $response["clientes"] = $this->cliente->getAll(NULL, NULL, FALSE);

                    $this->load->view("administrativo/dependentes/atualizar", $response);
                } else {
                    $this->session->set_flashdata("msg", "Dependente inexistente");
                    redirect("dependentes");
                }
            } else {
                $this->session->set_flashdata("msg", "Dependente inexistente");
                redirect("dependentes");
            }
        } else {
            redirect("login");
        }
    }

    public function adicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->dependente->preencherDados($this->input->post());
                echo json_encode($this->dependente->adicionar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function atualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->dependente->preencherDados($this->input->post());
                echo json_encode($this->dependente->atualizar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function buscar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("nome"))) {
                echo json_encode($this->dependente->buscar($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

}
