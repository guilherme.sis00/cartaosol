<?php

class Contratos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model([
            "administrativo/vendedor",
            "administrativo/contrato",
            "financeiro/categoria",
            "financeiro/parcela",
            "cliente"
        ]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $config = $this->configuracao->getConfigPagination(base_url('administrativo/contratos'), $this->utils->countAll("contratos"), 3);
            $this->pagination->initialize($config);
            $dados['paginacao'] = $this->pagination->create_links();

            $offset = ($this->uri->segment(3)) ? (($this->uri->segment(3) - 1) * 10) : 0;
            $dados["contratos"] = $this->contrato->getAll($config['per_page'], $offset);
            $dados["formasPagamento"] = ["boleto" => "Boleto", "carne" => "Carnê", "cartaoCredito" => "Cartão de crédito"];

            $this->load->view("administrativo/contratos/home", $dados);
        } else {
            redirect("login");
        }
    }

    public function vAdicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $dados["clientes"] = $this->cliente->getAll(NULL, NULL, FALSE);
            $dados["vendedores"] = $this->vendedor->getAll(NULL, NULL, FALSE);
            $this->load->view("administrativo/contratos/adicionar", $dados);
        } else {
            redirect("login");
        }
    }

    public function vAtualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(3) && is_numeric($this->uri->segment(3)))) {
                $response = $this->contrato->getById($this->uri->segment(3));

                if ($response['resultado']) {
                    $response["vendedores"] = $this->vendedor->getAll(NULL, NULL, FALSE);

                    $this->load->view("administrativo/contratos/atualizar", $response);
                } else {
                    $this->session->set_flashdata("msg", "Contrato inexistente");
                    redirect("administrativo/contratos");
                }
            } else {
                $this->session->set_flashdata("msg", "Contrato inexistente");
                redirect("administrativo/contratos");
            }
        } else {
            redirect("login");
        }
    }

    public function vGerarParcelas() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(4) && is_numeric($this->uri->segment(4)))) {
                $response = $this->contrato->getById($this->uri->segment(4));

                if ($response['resultado']) {
                    $response["categorias"] = $this->categoria->getAll(NULL, NULL, FALSE, FALSE);
                    $response["parcelas"] = $this->parcela->getByIdContrato($this->uri->segment(4), FALSE);
                    $this->load->view("administrativo/contratos/gerar-parcelas", $response);
                } else {
                    $this->session->set_flashdata("msg", $response["msg"]);
                    redirect("administrativo/contratos");
                }
            } else {
                $this->session->set_flashdata("msg", "Contrato inexistente");
                redirect("administrativo/contratos");
            }
        } else {
            redirect("login");
        }
    }

    public function buscar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("codigoContrato")) || !empty($this->input->post("cliente"))) {
                echo json_encode($this->contrato->buscar($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function adicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->contrato->preencherDados($this->input->post());
                echo json_encode($this->contrato->adicionar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function atualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->contrato->preencherDados($this->input->post());
                echo json_encode($this->contrato->atualizar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function atualizarStatus() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id")) && !empty($this->input->post("status"))) {
                echo json_encode($this->contrato->atualizarStatus($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function deletar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->contrato->deletar($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function gerarParcelas() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                echo json_encode($this->contrato->gerarParcelas($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na valiação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

}
