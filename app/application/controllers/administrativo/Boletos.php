<?php

use OpenBoleto\BoletoAbstract;

class Boletos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["administrativo/empresa", "financeiro/boleto"]);
    }

    public function abc() {
//        $file = 'remessa.txt';
//        $this->load->helper('download');
//        force_download($file, file_get_contents("./assets/arquivos/{$file}"));
        echo "9452079";
        echo "</br>";
        echo substr("9452079", 0, 6);
        echo "</br>";
        echo substr("9452079", 6, 7);
    }

    public function info() {
        $responseDados = $this->empresa->get(FALSE);
        $remessa = $this->db->where("id", 60)->get("remessas")->row();
        $agencia = explode("-", $responseDados->agencia);
        $conta = explode("-", $responseDados->conta);
        $contaFidc = explode("-", $responseDados->contaFidc);

        # REGISTRO HEADER DO ARQUIVO REMESSA
        $dados[] = [
            [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
            [4, 0000, 0, 'Lote de serviço'],
            [1, 0, 0, 'Tipo de registro'],
            [8, '', ' ', 'Reservado (uso Banco)'],
            [1, 2, null, 'Tipo de inscrição da empresa'],
            [15, $responseDados->cnpj, 0, 'Nº de inscrição da empresa'],
            [15, $responseDados->codigoTransmissao, 0, 'Código de Transmissão'],
            [25, '', ' ', 'Reservado (uso Banco)'],
            [30, $responseDados->razaoSocial, ' ', 'Nome da empresa'],
            [30, 'Banco Santander', ' ', 'Nome do Banco'],
            [10, '', ' ', 'Reservado (uso Banco)'],
            [1, 1, 0, 'Código remessa'],
            [8, date("dmY", strtotime($remessa->data)), 0, 'Data de geração do arquivo'],
            [6, '', ' ', 'Reservado (uso Banco)'],
            [6, $remessa->id, 0, 'Nº seqüencial do arquivo'],
            [3, '040', 0, 'Nº da versão do layout do arquivo'],
            [74, '', ' ', 'Reservado (uso Banco)'],
        ];

        # REGISTRO HEADER DO LOTE REMESSA
        $dados[] = [
            [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
            [4, 1, 0, 'Numero do lote remessa'],
            [1, 1, 0, 'Tipo de registro'],
            [1, 'R', 0, 'Tipo de operação'],
            [2, 01, 0, 'Tipo de serviço'],
            [2, '', ' ', 'Reservado (uso Banco)'],
            [3, '030', 0, 'Nº da versão do layout do lote'],
            [1, '', ' ', 'Reservado (uso Banco)'],
            [1, 2, 0, 'Tipo de inscrição da empresa'],
            [15, $responseDados->cnpj, 0, 'Nº de inscrição da empresa'],
            [20, '', ' ', 'Reservado (uso Banco)'],
            [15, $responseDados->codigoTransmissao, '0', 'Código de Transmissão'],
            [5, '', ' ', 'Reservado uso Banco'],
            [30, preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim($responseDados->razaoSocial))), ' ', 'Nome do Beneficiário'],
            [40, preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim($responseDados->instrucao1))), ' ', 'Mensagem 1'],
            [40, preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim($responseDados->instrucao2))), ' ', 'Mensagem 2'],
            [8, 0, 0, 'Número remessa/retorno'],
            [8, date("dmY", strtotime($remessa->data)), 0, 'Data da gravação remessa/retorno'],
            [41, '', ' ', 'Reservado (uso Banco)'],
        ];

        $sequencial = 0;

        $boletos = $this->boleto->getByIdsParcelas([445, 446, 447], FALSE);
        foreach ($boletos as $i => $v) {
//            $juros = max((float) ($v->valor * 0.33 / 100), 0.01);

            $sequencial++;
            $nossoNumero = str_pad($v->idParcela, 12, '0', STR_PAD_LEFT);
            $dv = BoletoAbstract::modulo11($nossoNumero, 9);

            $dados[] = [
                [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
                [4, 1, 0, 'Numero do lote remessa'],
                [1, 3, 0, 'Tipo de registro'],
                [5, $sequencial, 0, 'Nº seqüencial do registro de lote'],
                [1, 'P', ' ', 'Cód. Segmento do registro detalhe'],
                [1, '', ' ', 'Reservado (uso Banco)'],
                [2, 1, 0, 'Código de movimento remessa'],
                [4, $agencia[0], 0, 'Agência do Destinatária FIDC'],
                [1, $agencia[1], 0, 'Dígito da Ag do Destinatária FIDC'],
                [9, $conta[0], 0, 'Número da conta corrente'],
                [1, $conta[1], 0, 'Dígito verificador da conta'],
                [9, $contaFidc[0], 0, 'Conta cobrança Destinatária FIDC'],
                [1, $contaFidc[1], 0, 'Dígito da conta cobrança Destinatária FIDC'],
                [2, '', ' ', 'Reservado (uso Banco)'],
                [13, $nossoNumero . $dv["digito"], 0, 'Identificação do título no Banco'],
                [1, 5, 0, 'Tipo de cobrança'],
                [1, 1, 0, 'Forma de Cadastramento'],
                [1, 1, 0, 'Tipo de documento'],
                [1, '', ' ', 'Reservado (uso Banco)'],
                [1, '', ' ', 'Reservado (uso Banco)'],
                [15, $v->id, 0, 'Nº do documento'],
                [8, $v->vencimento, 0, 'Data de vencimento do título'],
                [15, $v->valor, 'float', 'Valor nominal do título'],
                [4, 0, 0, 'Agência encarregada da cobrança'],
                [1, 0, 0, 'Dígito da Agência do Beneficiário'],
                [1, '', ' ', 'Reservado (uso Banco)'],
                [2, '02', ' ', 'Espécie do título'],
                [1, 'N', ' ', 'Identif. de título Aceito/Não Aceito'],
                [8, $v->dataEmissao, 0, 'Data da emissão do título'],
                [1, 1, 0, 'Código do juros de mora'],
                [8, $v->vencimento, 0, 'Data do juros de mora'],
                [15, 199, 0, 'Valor da mora/dia ou Taxa mensal'],
                [1, 0, 0, 'Código do desconto 1'],
                [8, 0, 0, 'Data de desconto 1'],
                [15, 0, 'float', 'Valor ou Percentual do desconto concedido'],
                [15, 0, 'float', 'Valor do IOF a ser recolhido'],
                [15, 0, 'float', 'Valor do abatimento'],
                [25, $v->id, 0, 'Identificação do título na empresa'],
                [1, 0, 0, 'Código para protesto'],
                [2, 0, 0, 'Número de dias para protesto'],
                [1, 0, 0, 'Código para Baixa/Devolução'],
                [1, 0, 0, 'Reservado (uso Banco)'],
                [2, 0, 0, 'Número de dias para Baixa/Devolução'],
                [2, 00, 0, 'Código da moeda'],
                [11, '', ' ', 'Reservado (uso Banco)'],
            ];

//            $cliente = $v->voCliente();

            $sequencial++;
            $dados[] = [
                [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
                [4, 1, 0, 'Numero do lote remessa'],
                [1, 3, 0, 'Tipo de registro'],
                [5, $sequencial, 0, 'Nº seqüencial do registro no lote'],
                [1, 'Q', ' ', 'Cód. segmento do registro detalhe'],
                [1, '', ' ', 'Reservado (uso Banco)'],
                [2, 1, 0, 'Código de movimento remessa'],
                [1, 1, 0, 'Tipo de inscrição do Pagador'],
                [15, $v->cpf, 0, 'Número de inscrição do Pagador'],
                [40, preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim($v->nome))), ' ', 'Nome Pagador'],
                [40, preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim("Endereço cliente"))), ' ', 'Endereço Pagador'],
                [15, "Tancredo Neves", ' ', 'Bairro Pagador'],
                [5 + 3, "69087170", 0, 'Cep Pagador'],
                [15, $v->cidade, ' ', 'Cidade do Pagador'],
                [2, $v->uf, ' ', 'Unidade da federação do Pagador'],
                [1, 0, 0, 'Tipo de inscrição Sacador/avalista'],
                [15, 0, 0, 'Nº de inscrição Sacador/avalista'],
                [40, '', ' ', 'Nome do Sacador/avalista'],
                [3, 0, 0, 'Identificador de carne'],
                [3, 0, 0, 'Seqüencial da Parcela ou número inicial da parcela'],
                [3, 0, 0, 'Quantidade total de parcelas'],
                [3, 0, 0, 'Número do plano'],
                [19, '', ' ', 'Reservado (uso Banco)'],
            ];

            $sequencial++;

            $dados[] = [
                [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
                [4, 1, 0, 'Numero do lote remessa'],
                [1, 3, 0, 'Tipo de registro'],
                [5, $sequencial, 0, 'Nº seqüencial do registro de lote'],
                [1, 'R', ' ', 'Código segmento do registro detalhe'],
                [1, '', ' ', 'Reservado (uso Banco)'],
                [2, 1, 0, 'Código de movimento'],
                [1, 0, 0, 'Código do desconto'],
                [8, 0, 0, 'Data do desconto'],
                [15, 0, 'float', 'Valor/Percentual a ser concedido'],
                [24, '', ' ', 'Reservado (uso Banco)'],
                [1, 2, 0, 'Código da multa'],
                [8, $v->vencimento, 0, 'Data da multa'],
                [15, 200, 'float', 'Valor/Percentual a ser aplicado'],
                [10, '', ' ', 'Reservado (uso Banco)'],
                [40, '', ' ', 'Mensagem'],
                [40, '', ' ', 'Mensagem'],
                [61, '', ' ', 'Reservado'],
            ];
        }

        # TRAILER DE LOTE REMESSA
        $dados[] = [
            [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
            [4, 0001, 0, 'Numero do lote remessa'],
            [1, 5, 0, 'Tipo de registro'],
            [9, '', ' ', 'Reservado (uso Banco)'],
            [6, 3 * count($boletos) + 2, 0, 'Quantidade de registros do lote'],
            [217, '', ' ', 'Reservado (uso Banco)'],
        ];

        # TRAILER DE ARQUIVO REMESSA
        $dados[] = [
            [3, $responseDados->codigoBanco, 0, 'Código do Banco na compensação'],
            [4, 9999, 0, 'Numero do lote remessa'],
            [1, 9, 0, 'Tipo de registro'],
            [9, '', ' ', 'Reservado (uso Banco)'],
            [6, 1, 0, 'Quantidade de lotes do arquivo'],
            [6, 1 + 1 + 0 + count($boletos) * 2 + 1 + 1, 0, 'Quantidade de registros do arquivo'],
            [211, '', ' ', 'Reservado (uso Banco)'],
        ];

        $replace = [
            '/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/' => '$3$2$1', // Data
            '/^([0-9]{4})\-([0-9]{2})\-([0-9]{2}) [0-9]{2}:[0-9]{2}:[0-9]{2}$/' => '$3$2$1', // Data
            '/^([0-9]{5})\-([0-9]{3})$/' => '$1$2',
            '/^([0-9]{2})\.([0-9]{3})\.([0-9]{3})\/([0-9]{4})\-([0-9]{2})$/' => '$1$2$3$4$5', // CNPJ
            '/^([0-9]{3})\.([0-9]{3})\.([0-9]{3})\-([0-9]{2})$/' => '$1$2$3$4' // CPF
        ];

        foreach ($dados as $key => $registros) {
            $linha = '';
            $linhaSize = 0;
            $posicao = 0;
            foreach ($registros as $values) {
                list($size, $value, $padValue, $descricao) = [$values[0], $values[1], $values[2], $values[3]];

                if ($padValue === 'float') {
                    $value = (int) ($value * 100);
                    $padValue = 0;
                } else {
                    $value = preg_replace(array_keys($replace), array_values($replace), $value);
                }

                $value = str_pad($value, $size, $padValue, $padValue === ' ' ? STR_PAD_RIGHT : STR_PAD_LEFT);

                $value = substr($value, 0, $size);

                if (FALSE) {

                    $posicao++;
                    $posicaoFinal = $posicao + $size - 1;
                    $size = zerofill($size, 2);

                    $linhaSize += strlen($value);
                    $linha .= "<div> {$size}({$posicao}-{$posicaoFinal}){";
                    $linha .= $value;
                    $linha .= "} // {$descricao}</div>";

                    $posicao = $posicaoFinal;
                } else {
                    $linha .= $value;
                }
            }

            if (FALSE) {
                $linha .= "<br /> Total de {$linhaSize} caracteres<hr />";
            }

            $dados[$key] = $linha;
        }

        $conteudo = implode("\n", $dados);
        $file = 'remessa.rem';
        file_put_contents("./assets/arquivos/{$file}", $conteudo);
//        ob_end_clean();
//        $this->load->helper('download');
//        force_download($file, file_get_contents("./assets/arquivos/{$file}"));
//        if (!FALSE) {
//            header('Content-Disposition: attachment; filename="remessa.rem"');
//            header("Content-type: text/plain; charset=UTF-8");
//            $remessa->save();
//            \system\crud\Conn::commit();
//        } else {
//            header("Content-type: text/html; charset=UTF-8");
//            \system\crud\Conn::rollBack();
//        }
//        exit($remessa->conteudo);
    }

}
