<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["financeiro/boleto"]);
    }

    public function analisarContratos() {
        if ($this->input->post("usuario") == "cartaosol" && $this->input->post("senha") == "#abcd!0000CARTAOSOL") {
            $this->boleto->analisarContratos();
        } else {
            echo "Acesso não autorizado";
        }
    }

    public function teste() {
        $this->boleto->analisarContratos();
    }

}
