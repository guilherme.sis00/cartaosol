<?php

class Usuarios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["usuario"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $config = $this->configuracao->getConfigPagination(base_url('usuarios'), $this->utils->countAll("ocpt_usuarios"));
            $this->pagination->initialize($config);
            $dados['paginacao'] = $this->pagination->create_links();

            $offset = ($this->uri->segment(2)) ? (($this->uri->segment(2) - 1) * 10) : 0;
            $dados["usuarios"] = $this->usuario->getAll($config['per_page'], $offset);
            $this->load->view("usuarios/home", $dados);
        } else {
            redirect("login");
        }
    }

    public function getById() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->usuario->getById($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function vAdicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $this->load->view("usuarios/adicionar");
        } else {
            redirect("login");
        }
    }

    public function vAtualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(2) && is_numeric($this->uri->segment(2)))) {
                $response = $this->usuario->getById($this->uri->segment(2));

                if ($response['resultado']) {
                    $this->load->view("usuarios/atualizar", $response);
                } else {
                    redirect("usuarios");
                }
            } else {
                redirect("usuarios");
            }
        } else {
            redirect("login");
        }
    }

    public function adicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->usuario->preencherDados($this->input->post());
                echo json_encode($this->usuario->adicionar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function atualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->usuario->preencherDados($this->input->post());
                echo json_encode($this->usuario->atualizarUsuario());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function alterarSenha() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                echo json_encode($this->usuario->alterarSenha($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function deletar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(3) && is_numeric($this->uri->segment(3)))) {
                $response = $this->cidade->deletar($this->uri->segment(3));

                if ($response['resultado']) {
                    $this->session->set_flashdata([
                        "msg" => $response["msg"],
                        "tipo" => "success",
                    ]);
                } else {
                    $this->session->set_flashdata([
                        "msg" => "Operação não executada",
                        "tipo" => "danger",
                    ]);
                }

                redirect("cidades");
            } else {
                $this->session->set_flashdata([
                    "msg" => "Operação não executada",
                    "tipo" => "danger",
                ]);
                redirect("cidades");
            }
        } else {
            redirect("login");
        }
    }

    public function buscar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("termo")) && !empty($this->input->post("tipoBusca"))) {
                echo json_encode($this->cidade->buscar($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

}
