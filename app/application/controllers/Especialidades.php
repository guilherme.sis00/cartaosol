<?php

class Especialidades extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(["especialidade"]);
    }

    public function index() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $config = $this->configuracao->getConfigPagination(base_url('especilidades'), $this->utils->countAll("ocpt_especialidades"));
            $this->pagination->initialize($config);
            $dados['paginacao'] = $this->pagination->create_links();

            $offset = ($this->uri->segment(2)) ? (($this->uri->segment(2) - 1) * 10) : 0;
            $dados["especialidades"] = $this->especialidade->getAll($config['per_page'], $offset);
            $dados["tiposEspecialdiades"] = $this->utils->getTiposEspecialidades();
            $this->load->view("especialidades/home", $dados);
        } else {
            redirect("login");
        }
    }

    public function getById() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("id"))) {
                echo json_encode($this->cidade->getById($this->input->post("id")));
            } else {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

    public function vAdicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            $dados["tiposEspecialidades"] = $this->utils->getTiposEspecialidades();

            $this->load->view("especialidades/adicionar", $dados);
        } else {
            redirect("login");
        }
    }

    public function vAtualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(2) && is_numeric($this->uri->segment(2)))) {
                $response = $this->especialidade->getById($this->uri->segment(2));

                if ($response['resultado']) {
                    $dados["tiposEspecialidades"] = $this->utils->getTiposEspecialidades();
                    $dados["especialidade"] = $response["especialidade"];

                    $this->load->view("especialidades/atualizar", $dados);
                } else {
                    $this->session->set_flashdata([
                        "msg", "Especialidade inexistente",
                        "tipo", "warning"
                    ]);
                    redirect("especialidades");
                }
            } else {
                $this->session->set_flashdata([
                    "msg", "Especialidade inexistente",
                    "tipo", "warning"
                ]);
                redirect("especialidades");
            }
        } else {
            redirect("login");
        }
    }

    public function adicionar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("especialidade")) && !empty($this->input->post("tipo"))) {
                $this->especialidade->preencherDados($this->input->post());
                echo json_encode($this->especialidade->adicionar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function atualizar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if ($this->form_validation->run()) {
                $this->especialidade->preencherDados($this->input->post());
                echo json_encode($this->especialidade->atualizar());
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                return ("login");
            }
        }
    }

    public function deletar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt")) {
            if (!empty($this->uri->segment(3) && is_numeric($this->uri->segment(3)))) {
                $response = $this->especialidade->deletar($this->uri->segment(3));

                if ($response['resultado']) {
                    $this->session->set_flashdata([
                        "msg" => $response["msg"],
                        "tipo" => "success",
                    ]);
                } else {
                    $this->session->set_flashdata([
                        "msg" => "Operação não executada",
                        "tipo" => "danger",
                    ]);
                }

                redirect("especialidades");
            } else {
                $this->session->set_flashdata([
                    "msg" => "Operação não executada",
                    "tipo" => "danger",
                ]);
                redirect("especialidades");
            }
        } else {
            redirect("login");
        }
    }

    public function buscar() {
        if ($this->sessao->isAutorizado($this->session, "ocpt") && $this->input->post("ajax")) {
            if (!empty($this->input->post("termo")) && !empty($this->input->post("tipoEspecialidade"))) {
                echo json_encode($this->especialidade->buscar($this->input->post()));
            } else {
                echo json_encode(["resultado" => FALSE, "msg" => "Falha na validação dos dados"]);
            }
        } else {
            if ($this->input->post("ajax")) {
                echo json_encode(["resultado" => FALSE, "sessaoExpirada" => TRUE]);
            } else {
                redirect("login");
            }
        }
    }

}
