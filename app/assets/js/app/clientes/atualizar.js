$().ready(function () {
    $("#cpf").mask("999.999.999-99");
    $("#celular").mask("(99) 99999-9999");
    $("#whatsapp").mask("(99) 99999-9999");
    $("#numeroCartao").mask("0000.0000.0000.0000");
    $("#dataNascimento").mask("00/00/0000");
});

$("#estado").on("change", function () {
    $.getJSON($("head").data("info") + "support/getCidades",
            {ajax: true, estado: $("#estado").val()},
            function (r) {
                if (r.resultado) {
                    $("#cidade")
                            .empty()
                            .append("<option value=''>--</option>");
                    $.each(r.cidades, function (key, cidade) {
                        $("#cidade").append("<option value='" + cidade.id + "'>" + cidade.cidade + "</option>");
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON"
            )
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de suporte"
                });
            });
});

$("#formAtualizarCliente").validate({
    rules: {
        id: {required: true},
        idCartao: {required: true},
        nome: {required: true},
        cpf: {
            required: true,
            minlength: 10
        },
        celular: {
            required: true,
            minlength: 10
        },
        email: {email: true},
        dataNascimento: {
            required: true,
            minlength: 9
        },
        numeroCartao: {
            required: true,
            minlength: 17
        },
        estado: {required: true},
        cidade: {required: true}
    },
    messages: {
        nome: {required: "Campo obrigatório"},
        cpf: {
            required: "Campo obrigatório",
            minlength: "Digite um CPF válido"
        },
        celular: {
            required: "Campo obrigatório",
            minlength: "Digite um celular válido"
        },
        email: {email: "Digite um e-mail válido"},
        dataNascimento: {
            required: "Campo obrigatório",
            minlength: "Digite uma data válida"
        },
        numeroCartao: {
            required: "Campo obrigatório",
            minlength: "Digite um número de cartão válido"
        },
        estado: {required: "Campo obrigatório"},
        cidade: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAtualizarCliente")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "clientes/atualizar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formAdicionarClienteBtnAtualizar")
                        .empty()
                        .append("Atualizando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    location.href = $("head").data("info") + "clientes";
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarClienteBtnAtualizar")
                    .empty()
                    .append("Adicionar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAdicionarCidade").validate({
    rules: {
        modalAdicionarCidadeEstado: {required: true},
        modalAdicionarCidadeCidade: {required: true}
    },
    messages: {
        modalAdicionarCidadeEstado: {required: "Campo obrigatório"},
        modalAdicionarCidadeCidade: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAdicionarCidade")[0]);
        dados.append("ajax", true);
        dados.append("estado", $("#modalAdicionarCidadeEstado").val());
        dados.append("cidade", $("#modalAdicionarCidadeCidade").val());
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "support/adicionarCidade",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAdicionarCidadeBtnAdicionar")
                        .empty()
                        .append("Adicionando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    $("#modalAdicionarCidade").modal("hide");
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarCidadeBtnAdicionar")
                    .empty()
                    .append("Adicionar")
                    .removeAttr("disabled");
        });
    }
});