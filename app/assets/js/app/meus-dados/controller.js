$("#formMeusDados").validate({
    rules: {
        nome: {required: true},
        usuario: {
            required: true,
            email: true
        }
    },
    messages: {
        nome: {required: "Campo obrigatório"},
        usuario: {
            required: "Campo obrigatório",
            email: "Digite um e-mail válido"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formMeusDados")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "meusDados/atualizar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formMeusDadosBtnAtualizar")
                        .empty()
                        .append("Atualizando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    location.href = $("head").data("info") + "meus-dados";
                });

            } else {
                swal({
                    title: "Atenção",
                    icon: "warning",
                    text: r.msg
                });
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formMeusDadosBtnAtualizar")
                    .empty()
                    .append("Entrar")
                    .removeAttr("disabled");
        });
    }
});