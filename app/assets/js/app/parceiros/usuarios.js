$("#formAdicionarUsuarioParceiro").validate({
    rules: {
        idParceiro: {required: true},
        nome: {required: true},
        usuario: {
            required: true,
            email: true
        },
        senha: {required: true},
        repetirSenha: {equalTo: "#senha"}
    },
    messages: {
        nome: {required: "Campo obrigatório"},
        usuario: {
            required: "Campo obrigatório",
            email: "Digite um e-mail válido"
        },
        senha: {required: "Campo obrigatório"},
        repetirSenha: {equalTo: "As senhas não coincidem"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAdicionarUsuarioParceiro")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "parceiros/adicionarUsuario",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAdicionarUsuarioParceiroBtnAdicionar")
                        .html("Adicionando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Sucesso</strong>`,
                    icon: "success",
                    position: "top-right",
                    text: r.msg,
                    beforeShow: function () {
                        $("#formAdicionarUsuarioParceiro").trigger("reset");
                        $("#tabelaUsuariosParceiros").append(`<tr id="${r.usuario.id}">
                                                                <td id="nome-${r.usuario.nome}">${r.usuario.nome}</td>
                                                                <td id="usuario-${r.usuario.id}">${r.usuario.usuario}</td>
                                                                <td id="status-${r.usuario.id}" class="text-center">${r.usuario.status.charAt(0).toUpperCase() + r.usuario.status.slice(1)}</td>
                                                                <td>
                                                                    <div class="btn-group">
                                                                        <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            Ações <span class="caret"></span>
                                                                        </button>
                                                                        <ul class="dropdown-menu">
                                                                            <li><a href="#" data-id="${r.usuario.id}" data-status="ativo" class="btn-status">Ativar</a></li>
                                                                            <li role="separator" class="divider"></li>
                                                                            <li><a href="#" data-id="${r.usuario.id}" data-status="bloqueado" class="btn-status">Bloquear</a></li>
                                                                            <li role="separator" class="divider"></li>
                                                                            <li><a href="#" data-id="${r.usuario.id}" class="btn-deletar">Deletar</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                            </tr>`);
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarUsuarioParceiroBtnAdicionar")
                    .html("Adicionar")
                    .removeAttr("disabled");
        });
    }
});

$("#tabelaUsuariosParceiros").on("click", ".btn-status", function () {
    let toast = $.toast({
        heading: `<strong style="color:white">Processando</strong>`,
        icon: "info",
        position: "top-right",
        text: "Alterando o status do usuário",
        hideAfter: false
    });
    let elemento = $(this);

    $.post($("head").data("info") + "parceiros/alterarStatusUsuario",
            {ajax: true, id: elemento.data("id"), status: elemento.data("status")},
            function (r) {
                if (r.resultado) {
                    $(`#status-${elemento.data("id")}`).html(elemento.data("status").charAt(0).toUpperCase() + elemento.data("status").slice(1));
                    $.toast({
                        heading: `<strong style="color:white">Sucesso</strong>`,
                        icon: "success",
                        position: "top-right",
                        text: r.msg
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

$("#tabelaUsuariosParceiros").on("click", ".btn-deletar", function () {
    swal({
        title: "Atenção",
        icon: "warning",
        text: "Você deseja realmente excluir esse usuário ?",
        dangerMode: true,
        buttons: {
            confirm: {
                text: "Deletar",
                value: true,
                visible: true,
                closeModal: true
            },
            cancel: {
                text: "Cancelar",
                value: false,
                visible: true,
                closeModal: true
            }
        }
    }).then((value) => {
        if (value) {
            let toast = $.toast({
                heading: `<strong style="color:white">Processando</strong>`,
                icon: "info",
                position: "top-right",
                text: "Deletando usuário",
                hideAfter: false
            });
            let elemento = $(this);

            $.post($("head").data("info") + "parceiros/deletarUsuario",
                    {ajax: true, id: elemento.data("id")},
                    function (r) {
                        if (r.resultado) {
                            $(`#${elemento.data("id")}`).remove();
                            $.toast({
                                heading: `<strong style="color:white">Sucesso</strong>`,
                                icon: "success",
                                position: "top-right",
                                text: r.msg
                            });
                        } else {
                            if (r.sessaoExpirada) {
                                swal({
                                    title: "Atenção",
                                    icon: "info",
                                    text: "Sua sessão expirou"
                                }).then(() => {
                                    location.href = $("head").data("info");
                                });
                            } else {
                                swal({
                                    title: "Atenção",
                                    icon: "warning",
                                    text: r.msg
                                });
                            }
                        }
                    },
                    "JSON")
                    .fail(function () {
                        swal({
                            title: "Falha",
                            icon: "error",
                            text: "Por favor, entre em contato com a equipe de suporte"
                        });
                    })
                    .always(function () {
                        toast.reset();
                    });
        }
    });
});