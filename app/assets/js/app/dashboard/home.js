$().ready(function () {
    $("#ano").mask("0000");
});

$("#formConsultar").validate({
    rules: {
        mes: {required: true},
        ano: {
            required: true,
            minlength: 4
        }
    },
    messages: {
        mes: {required: "Campo obrigatório"},
        ano: {
            required: "Campo obrigatório",
            minlength: "Digite um CPF válido"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formConsultar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "dashboard/getDadosFinanceirosMes",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formConsultarBtnConsultar")
                        .empty()
                        .append("Consultando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $(".mesNominal").html(r.mesFiltrado);
                $("#saldoInicial").html(`R$ ${r.saldoInicial.toLocaleString("pt-BR",{minimumFractionDigits: 2})}`);
                $("#receitas").html(`R$ ${r.receitas.toLocaleString("pt-BR",{minimumFractionDigits: 2})}`);
                $("#despesas").html(`R$ ${r.despesas.toLocaleString("pt-BR",{minimumFractionDigits: 2})}`);
                $("#saldo").html(`R$ ${(parseFloat(r.receitas) - (parseFloat(r.despesas))).toLocaleString("pt-BR",{minimumFractionDigits: 2})}`);
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formConsultarBtnConsultar")
                    .empty()
                    .append("Consultar")
                    .removeAttr("disabled");
        });
    }
});