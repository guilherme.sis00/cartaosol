$("#formAdicionarCategoria").validate({
    rules: {
        modalAdicionarCategoriaCategoria: {required: true},
        modalAdicionarCategoriaTipo: {required: true}
    },
    messages: {
        modalAdicionarCategoriaCategoria: {required: "Campo obrigatório"},
        modalAdicionarCategoriaTipo: {required: "Campo obrigatório"}
    },
    errorClass: 'help-block',
    validClass: "help-block",
    errorElement: "small",
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        var dados = new FormData();
        dados.append("ajax", true);
        dados.append("categoria", $("#modalAdicionarCategoriaCategoria").val());
        dados.append("tipo", $("#modalAdicionarCategoriaTipo").val());

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/categoriasFinanceiras/adicionar",
            data: dados,
            dataType: "JSON",
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAdicionarCategoriaBtnAdicionar")
                        .html("Adicionando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#formAdicionarCategoria").trigger("reset");
                $("#modalAdicionarCategoria").modal("hide");

                $.toast({
                    heading: "<strong style='color:white'>Tudo certo</strong>",
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    beforeShow: function () {
                        $("#categoria").empty();
                        $.each(r.categorias, function (key, categoria) {
                            if (r.tipo == categoria.tipo) {
                                $("#categoria").append(`<option value="${categoria.id}">${categoria.categoria}</option>`);
                            }
                        });
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });

                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha de comunicação",
                icon: "error",
                text: "Por favor entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarCategoriaBtnAdicionar")
                    .html("Adicionar")
                    .removeAttr("disabled");
        });
    }
});