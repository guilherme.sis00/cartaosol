function formatarValor(valor) {
    var numero = valor.toFixed(2).split('.');
    numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
}

function formatarData(data) {
    let dataFragmentada = data.split("-");
    return [dataFragmentada[2], dataFragmentada[1], dataFragmentada[0]].join('/');
}

function desformatarData(data) {
    let dataFragmentada = data.split("/");
    return [dataFragmentada[2], dataFragmentada[1], dataFragmentada[0]].join('-');
}