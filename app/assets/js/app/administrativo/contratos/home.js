$().ready(function () {
    $("#cpf").mask("000.000.000-00");
});

$("#formBuscar").validate({
    rules: {
        codigoContrato: {digits: true},
        cliente: {minlength: 3},
        cpf: {minlength: 14}
    },
    messages: {
        codigoContrato: {digits: "Código inválido"},
        cliente: {minlength: "Digite pelo menos 3 caracteres"},
        cpf: {minlength: "Digite um CPF válido"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        if ($("#codigoContrato").val() != "" || $("#cliente").val() != "") {
            let dados = new FormData($("#formBuscar")[0]);
            dados.append("ajax", true);

            $.ajax({
                type: 'POST',
                url: $("head").data("info") + "administrativo/contratos/buscar",
                data: dados,
                dataType: 'JSON',
                processData: false,
                contentType: false,
                beforeSend: function (xhr) {
                    $("#formBuscarBtnBuscar")
                            .html("Buscando")
                            .attr("disabled", "disabled");
                }
            }).done(function (r) {
                if (r.resultado) {
                    $("#tabelaContratos").empty();
                    $("#divPaginacao").empty();

                    $.each(r.contratos, function (key, contrato) {
                        tabelaContratosAdicionar(contrato);
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            }).fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de suporte"
                });
            }).always(function () {
                $("#formBuscarBtnBuscar")
                        .html("Buscar")
                        .removeAttr("disabled");
            });
        } else {
            $.toast({
                heading: `<strong style="color:white">Atenção</strong>`,
                icon: "warning",
                position: "top-right",
                hideAfter: 5000,
                text: "Preencha o código do contrato ou o nome do cliente"
            });
        }
    }
});

$("#tabelaContratos").on("click", ".btn-deletar", function () {
    let id = $(this).data("id");

    swal({
        title: "Atenção",
        icon: "warning",
        text: "Você deseja realmente excluir esse contrato ?",
        dangerMode: true,
        buttons: {
            cancel: {
                text: "Cancelar",
                value: false,
                visible: true,
                closeModal: true
            },
            confirm: {
                text: "Deletar",
                value: true,
                visible: true,
                closeModal: true
            }
        }
    }).then((value) => {
        if (value) {
            let toast = $.toast({
                heading: `<strong style="color:white">Processando</strong>`,
                icon: "info",
                text: "Deletando contrato",
                position: "top-right",
                hideAfter: false
            });

            $.post($("head").data("info") + "administrativo/contratos/deletar",
                    {ajax: true, id: id},
                    function (r) {
                        if (r.resultado) {
                            $.toast({
                                heading: `<strong style="color:white">Tudo certo</strong>`,
                                icon: "success",
                                text: "Contrato deletado",
                                position: "top-right",
                                hideAfter: 4000,
                                beforeShow: function () {
                                    $(`#${id}`).remove();
                                }
                            });
                        } else {
                            if (r.sessaoExpirada) {
                                swal({
                                    title: "Atenção",
                                    icon: "info",
                                    text: "Sua sessão expirou"
                                }).then(() => {
                                    location.href = $("head").data("info");
                                });
                            } else {
                                swal({
                                    title: "Atenção",
                                    icon: "warning",
                                    text: r.msg
                                });
                            }
                        }
                    },
                    "JSON"
                    )
                    .fail(function () {
                        swal({
                            title: "Falha de comunicação",
                            icon: "error",
                            text: "Por favor entre em contato com a equipe de suporte"
                        });
                    })
                    .always(function () {
                        toast.reset();
                    });
        }
    });
});

$("#tabelaContratos").on("click", ".btn-status", function () {
    let elemento = $(this);
    let toast = $.toast({
        heading: "<strong style='color:white'>Processando</strong>",
        icon: "info",
        position: "top-right",
        text: "Atualizando status",
        hideAfter: false
    });

    $.post($("head").data("info") + "administrativo/contratos/atualizarStatus",
            {ajax: true, id: elemento.data("id"), status: elemento.data("status")},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: "<strong style='color:white'>Sucesso</strong>",
                        icon: "success",
                        position: "top-right",
                        text: r.msg,
                        hideAfter: 4000,
                        beforeShow: function () {
                            $(`#situacao-${elemento.data("id")}`).html(elemento.data("status").charAt(0).toUpperCase() + elemento.data("status").slice(1));
                        }
                    });
                } else {
                    if (r.sessaExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sessão expirada"
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

function tabelaContratosAdicionar(contrato) {
    let formasPagamento = {
        boleto: "Boleto",
        carne: "Carnê",
        cartaoCredito: "Cartão de crédito"
    };

    $("#tabelaContratos").append(`<tr id="${contrato.id}">
                                    <td id="contrato-${contrato.id}" class="text-center">${contrato.codigoContrato}</td>
                                    <td id="nome-${contrato.id}">${contrato.nome}</td>
                                    <td id="duracao-${contrato.id}">${contrato.duracao} meses</td>
                                    <td id="ano-${contrato.id}">${contrato.ano}</td>
                                    <td id="forma-pagamento-${contrato.id}">${formasPagamento[contrato.formaPagamento]}</td>
                                    <td id="situacao-${contrato.id}">${contrato.situacao.charAt(0).toUpperCase() + contrato.situacao.slice(1)}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Ações <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                ${contrato.formaPagamento == "boleto" ? `<li><a class="${contrato.situacao == "finalizado" ? "disabled" : ""}" href="${$("head").data("info") + `administrativo/contrato/gerar-parcelas/${contrato.id}`}">Parcelas</a></li>` : ""}
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#" class="btn-status" data-id="${contrato.id}" data-status="ativo">Ativo</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#" class="btn-status" data-id="${contrato.id}" data-status="inativo">Inativo</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#" class="btn-status" data-id="${contrato.id}" data-status="inadimplente">Inadimplente</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#" class="btn-status" data-id="${contrato.id}" data-status="finalizado">Finalizado</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#" class="btn-deletar" data-id="${contrato.id}">Deletar</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>`);
}