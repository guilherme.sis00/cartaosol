$().ready(function () {
    $("#cliente").chosen({no_results_text: "Nenhum cliente encontrado"});
    $("#dataInicial").mask("00/00/0000");
    $("#dataFinal").mask("00/00/0000");
});

$("#formBuscar").on("submit", function (e) {
    e.preventDefault();

    if ($("#cliente").val() !== "") {
        let dados = new FormData($("#formBuscar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/remessas/getBoletos",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formBuscarBtnBuscar")
                        .html("Buscando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#tabelaBoletos").empty();

                $.each(r.boletos, function (key, boleto) {
                    tabelaBoletosAdicionar(boleto);
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarBtnBuscar")
                    .html("Buscar")
                    .removeAttr("disabled");
        });
    } else {
        $.toast({
            heading: `<strong style="color:white">Atenção</strong>`,
            icon: "warning",
            position: "top-right",
            text: "Selecione um cliente",
            hideAfter: 4000
        });
    }
});

$("#formGerarRemessa").on("submit", function (e) {
    e.preventDefault();
    let boletos = $("#tabelaBoletos input:checked");
    if (boletos.length > 0) {
        let dados = new FormData($("#formGerarRemessa")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/remessas/gerar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formGerarRemessaBtnGerar")
                        .html("Gerando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Tudo certo</strong>`,
                    icon: "success",
                    text: "Arquivo de remessa gerado, clique no botão BAIXAR ARQUIVO",
                    position: "top-right",
                    hideAfter: 5000,
                    beforeShow: function () {
                        $("#tabelaBoletos").empty();
                        $("#btnBaixar")
                                .removeClass("disabled")
                                .attr("href", $("head").data("info") + `financeiro/remessas/baixar?file=${r.file}`);
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formGerarRemessaBtnGerar")
                    .html("Gerar remessa")
                    .removeAttr("disabled");
        });
    } else {
        swal({
            title: "Atenção",
            icon: "warning",
            text: "Você não selecionou nenhum boleto"
        });
    }
});

$("#marcarTodos").on("click", function () {
    let boletos = $("#tabelaBoletos").find("input[type=checkbox]");

    if ($(this).prop("checked")) {
        $.each(boletos, function (key, boleto) {
            $(boleto).prop("checked", true);
        });
    } else {
        $.each(boletos, function (key, boleto) {
            $(boleto).prop("checked", false);
        });
    }
});

function tabelaBoletosAdicionar(boleto) {
    $("#tabelaBoletos").append(`<tr id="${boleto.id}">
                                    <td id="checkbox-${boleto.id}" class="text-center"><input type="checkbox" id="boleto" name="boleto[]" checked="checked" value="${boleto.id}"></td>
                                    <td id="cliente-${boleto.id}">${boleto.nome}</td>
                                    <td id="emissao-${boleto.id}">${formatarData(boleto.dataEmissao)}</td>
                                    <td id="vencimento-${boleto.id}">${formatarData(boleto.vencimento)}</td>
                                    <td id="remessa-${boleto.id}">${boleto.exportado == 2 ? formatarData(boleto.dataRemessa) : ""}</td>
                                    <td id="valor-${boleto.id}">R$ ${formatarValor(parseFloat(boleto.valor))}</td>
                                    <td id="situacao-${boleto.id}">${formatarStatus(boleto.status)}</td>
                                </tr>`);
}

function formatarStatus(status) {
    switch (status) {
        case "1":
            return "A pagar";
        case "2":
            return "Pago";
        case "3":
            return "Atrasado";
    }
}