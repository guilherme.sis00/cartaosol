$().ready(function () {
    $("#cpf").mask("000.000.000-00");
    $("#dataInicial").mask("99/99/9999");
    $("#dataFinal").mask("99/99/9999");
    $("#modalAtualizarReceitaBaixa").mask("00/00/0000");
});

$("#formBuscar").validate({
    rules: {
        codigoContrato: {digits: true},
        categoria: {required: true},
        dataInicial: {required: true},
        dataFinal: {
            required: true,
            minlength: 8
        }
    },
    messages: {
        codigoContrato: {digits: "Código inválido"},
        categoria: {required: "Campo obrigatório"},
        dataInicial: {required: "Campo obrigatório"},
        dataFinal: {
            required: "Campo obrigatório",
            minlength: "Digite uma data válida"
        }
    },
    errorClass: 'help-block',
    validClass: "help-block",
    errorElement: "small",
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        var dados = new FormData($("#formBuscar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/receitas/buscar",
            data: dados,
            dataType: "JSON",
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formBuscarBtnBuscar")
                        .html("Buscando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                let recebido = 0;
                let pendente = 0;

                $("#tabelaContasReceber").empty();
                $.each(r.contasReceber, function (key, contaReceber) {
                    switch (contaReceber.status) {
                        case "recebido":
                            recebido += parseFloat(contaReceber.valor);
                            break;
                        case "pendente":
                            pendente += parseFloat(contaReceber.valor);
                            break;
                    }
                    tabelaContasReceberAdicionar(contaReceber);
                });

                $("#total-recebido").html(`R$ ${formatarValor(recebido)}`);
                $("#total-pendente").html(`R$ ${formatarValor(pendente)}`);
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });

                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha de comunicação",
                icon: "error",
                text: "Por favor entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarBtnBuscar")
                    .html("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAtualizarReceita").validate({
    rules: {
        modalAtualizarReceitaId: {required: true},
        modalAtualizarReceitaBaixa: {
            required: true,
            minlength: 8
        }
    },
    messages: {
        modalAtualizarReceitaBaixa: {
            required: "Campo obrigatório",
            minlength: "Digite uma data válida"
        }
    },
    errorClass: 'help-block',
    validClass: "help-block",
    errorElement: "small",
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        var dados = new FormData();
        dados.append("ajax", true);
        dados.append("id", $("#modalAtualizarReceitaId").val());
        dados.append("baixa", $("#modalAtualizarReceitaBaixa").val());

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/receitas/atualizarBaixa",
            data: dados,
            dataType: "JSON",
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAtualizarReceitaBtnAtualizar")
                        .html("Atualizando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#formAtualizarReceita").trigger("reset");
                $("#modalAtualizarReceita").modal("hide");

                $(`#data-${r.receita.id}`).html(`${formatarData(r.receita.data)}<br>${formatarData(r.receita.pagamento)}`)
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });

                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha de comunicação",
                icon: "error",
                text: "Por favor entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAtualizarReceitaBtnAtualizar")
                    .html("Atualizar")
                    .removeAttr("disabled");
        });
    }
});

$("#mesContasReceber").on("change", function () {
    let mes = $(this).val();
    let toast = $.toast({
        heading: "<strong style='color: white'>Processando</strong>",
        icon: "info",
        text: "Buscando contas a pagar",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "financeiro/receitas/buscarPorMes",
            {ajax: true, mes: mes},
            function (r) {
                if (r.resultado) {
                    let recebido = 0;
                    let pendente = 0;

                    $("#tabelaContasReceber").empty();
                    $.each(r.contasReceber, function (key, contaReceber) {
                        switch (contaReceber.status) {
                            case "recebido":
                                recebido += parseFloat(contaReceber.valor);
                                break;
                            case "pendente":
                                pendente += parseFloat(contaReceber.valor);
                                break;
                        }
                        tabelaContasReceberAdicionar(contaReceber);
                    });

                    $("#total-recebido").html(`R$ ${formatarValor(recebido)}`);
                    $("#total-pendente").html(`R$ ${formatarValor(pendente)}`);
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });

                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

$("#tabelaContasReceber").on("click", ".btn-deletar", function () {
    swal({
        title: "Atenção",
        icon: "warning",
        text: "Deseja realmente excluir essa receita ?",
        dangerMode: true,
        buttons: {
            cancel: {
                text: "Cancelar",
                value: false,
                visible: true,
                closeModal: true
            },
            confirm: {
                text: "Deletar",
                value: true,
                visible: true,
                closeModal: true
            }
        }
    }).then((value) => {
        if (value) {
            let id = $(this).data("id");
            let status = $(`#status-${id}`).text();

            let toast = $.toast({
                heading: "<strong style='color: white'>Processando</strong>",
                icon: "info",
                text: "Deletando conta a receber",
                position: "top-right",
                hideAfter: false
            });

            $.post($("head").data("info") + "financeiro/receitas/deletar",
                    {ajax: true, id: id},
                    function (r) {
                        if (r.resultado) {
                            $.toast({
                                heading: "<strong style='color: white'>Tudo certo</strong>",
                                icon: "success",
                                text: r.msg,
                                position: "top-right",
                                beforeShow: function () {
                                    let valorContaPagar = $(`#valor-${id}`).text().slice(3).replace(",", ".");

                                    switch (status) {
                                        case "Recebido":
                                            let totalPago = $(`#total-recebido`).text().slice(3).replace(",", ".");
                                            $("#total-recebido").html(`R$ ${formatarValor(parseFloat(totalPago) - parseFloat(valorContaPagar))}`);
                                            break;
                                        case "Pendente":
                                            let totalPendente = $(`#total-pendente`).text().slice(3).replace(",", ".");
                                            $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) - parseFloat(valorContaPagar))}`);
                                            break;
                                    }

                                    $(`#${id}`).remove();
                                },
                                hideAfter: 4000
                            });
                        } else {
                            if (r.sessaoExpirada) {
                                swal({
                                    title: "Atenção",
                                    icon: "info",
                                    text: "Sua sessão expirou"
                                }).then(() => {
                                    location.href = $("head").data("info");
                                });

                            } else {
                                swal({
                                    title: "Atenção",
                                    icon: "warning",
                                    text: r.msg
                                });
                            }
                        }
                    },
                    "JSON")
                    .fail(function () {
                        swal({
                            title: "Falha de comunicação",
                            icon: "error",
                            text: "Por favor entre em contato com a equipe de suporte"
                        });
                    })
                    .always(function () {
                        toast.reset();
                    });
        }
    });
});

$("#tabelaContasReceber").on("click", ".btn-status", function () {
    let elemento = $(this);

    let toast = $.toast({
        heading: "<strong style='color: white'>Processando</strong>",
        icon: "info",
        text: "Atualizando status",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "financeiro/receitas/atualizarStatus",
            {ajax: true, id: elemento.data("id"), idParcela: elemento.data("id-parcela")},
            function (r) {
                if (r.resultado) {
                    $.toast({
                        heading: "<strong style='color: white'>Tudo certo</strong>",
                        icon: "success",
                        text: r.msg,
                        position: "top-right",
                        beforeShow: function () {
                            $(`#${elemento.data("id")}`).remove();
                        },
                        hideAfter: 4000
                    });
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });

                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

$("#modalAtualizarReceita").on("show.bs.modal", function (e) {
    $("#modalAtualizarReceitaLoader").html(`<div class="alert alert-info"><i class="fa fa-circle-o-notch fa-spin"></i> Buscando receita</div>`);

    $.post($("head").data("info") + "financeiro/receitas/getReceita",
            {ajax: true, id: $(e.relatedTarget).data("id")},
            function (r) {
                if (r.resultado) {
                    $("#modalAtualizarReceitaId").val(r.contaReceber.id);
                    $("#modalAtualizarReceitaValor").val(`${formatarValor(parseFloat(r.contaReceber.valor))}`);
                    $("#modalAtualizarReceitaBaixa").val(formatarData(r.contaReceber.pagamento));
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });

                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                $("#modalAtualizarReceitaLoader").empty();
            });
});

function analisarStatus(params) {
    let valorContaPagar = $(`#valor-${params.id}`).text().slice(3).replace(",", ".");
    let totalPago = $(`#total-recebido`).text().slice(3).replace(",", ".");
    let totalPendente = $(`#total-pendente`).text().slice(3).replace(",", ".");

    switch (params.status) {
        case "recebido":
            $(`#${params.id}`).removeClass("danger");
            $(`#${params.id}`).addClass("success");
            $(`#baixada-${params.id}`).html(`<i class="ti-arrow-down"></i>`);

            $("#total-recebido").html(`R$ ${formatarValor(parseFloat(totalPago) + parseFloat(valorContaPagar))}`);
            $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) - parseFloat(valorContaPagar))}`);

            break;
        case "pendente":
            let dataAtual = new Date($("#dataAtual").val());
            let dataContaReceber = new Date(desformatarData($(`#data-${params.id}`).text()));

            $("#total-recebido").html(`R$ ${formatarValor(parseFloat(totalPago) - parseFloat(valorContaPagar))}`);
            $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) + parseFloat(valorContaPagar))}`);
            $(`#baixada-${params.id}`).empty();
            $(`#${params.id}`).removeClass("success");

            if (dataAtual > dataContaReceber) {
                $(`#${params.id}`).addClass("danger");
            }

            break;
    }
}

function tabelaContasReceberAdicionar(contaReceber) {
    let atrasada = false;
    if (contaReceber.status == "pendente") {
        let dataAtual = new Date($("#dataAtual").val());
        let dataContaReceber = new Date(contaReceber.data);

        atrasada = dataAtual > dataContaReceber;
    }

    $("#tabelaContasReceber").append(`<tr id="${contaReceber.id}" class="${atrasada ? "danger" : (contaReceber.status == "recebido" ? "success" : "")}">
                                        <td id="baixada-${contaReceber.id}">${contaReceber.status == "recebido" ? `<i class="ti-arrow-down"></i>` : ""}</td>
                                        <td id="descricao-${contaReceber.id}">${contaReceber.descricao}</td>
                                        <td id="categoria-${contaReceber.id}">${contaReceber.categoria}</td>
                                        <td id="valor-${contaReceber.id}">R$ ${formatarValor(parseFloat(contaReceber.valor))}</td>
                                        <td id="data-${contaReceber.id}">${formatarData(contaReceber.data)}</td>
                                        <td id="status-${contaReceber.id}">${contaReceber.status.charAt(0).toUpperCase() + contaReceber.status.slice(1)}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Ações <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#" data-toggle="modal" data-target="#modalAtualizarReceita" data-id="${contaReceber.id}">Atualizar</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-deletar" data-id="${contaReceber.id}">Deletar</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>`);
}