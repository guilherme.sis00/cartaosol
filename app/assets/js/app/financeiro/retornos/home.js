$("#formRetorno").validate({
    rules: {
        retorno: {required: true}
    },
    messages: {
        retorno: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formRetorno")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/retornos/processar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formRetornoBtnEnviar")
                        .html("Enviando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    $("#formRetorno").trigger("reset");
                    $("#tabelaRetornos").empty();
                    $.each(r.retornos, function (key, retorno) {
                        $("#tabelaRetornos").append(`<tr>
                                                        <td>${retorno.arquivo}</td>
                                                        <td><a href="${$("head").data("info") + `download/retorno/${retorno.arquivo}`}">Link arquivo</a></td>
                                                        <td>${formatarTimestamp(retorno.created_at)}</td>
                                                    </tr>`);
                    });
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formRetornoBtnEnviar")
                    .html("Enviar")
                    .removeAttr("disabled");
        });
    }
});