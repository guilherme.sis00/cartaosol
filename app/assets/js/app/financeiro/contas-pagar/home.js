$().ready(function () {
    $("#ano").mask("0000");
    $("#modalBaixarContaPagarPagamento").mask("00/00/0000");
});

$("#formBuscar").validate({
    rules: {
        mes: {required: true},
        ano: {required: true},
        categoria: {required: true}
    },
    messages: {
        mes: {required: "Campo obrigatório"},
        ano: {required: "Campo obrigatório"},
        categoria: {required: "Campo obrigatório"}
    },
    errorClass: 'help-block',
    validClass: "help-block",
    errorElement: "small",
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        var dados = new FormData($("#formBuscar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/contasPagar/buscar",
            data: dados,
            dataType: "JSON",
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formBuscarBtnBuscar")
                        .html("Buscando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                let pago = 0;
                let pendente = 0;

                $("#tabelaContasPagar").empty();
                $.each(r.contasPagar, function (key, contaPagar) {
                    switch (contaPagar.status) {
                        case "pago":
                            pago += parseFloat(contaPagar.valor);
                            break;
                        case "pendente":
                            pendente += parseFloat(contaPagar.valor);
                            break;
                    }
                    tabelaContasPagarAdicionar(contaPagar);
                });

                $("#total-pago").html(`R$ ${formatarValor(pago)}`);
                $("#total-pendente").html(`R$ ${formatarValor(pendente)}`);
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });

                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha de comunicação",
                icon: "error",
                text: "Por favor entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarBtnBuscar")
                    .html("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#formBaixarContaPagar").validate({
    rules: {
        modalBaixarContaPagarId: {digits: true},
        modalBaixarContaPagarPagamento: {
            required: true,
            minlength: 8
        },
        modalBaixarContaPagarFormaPagamento: {required: true},
        modalBaixarContaPagarConta: {required: true}
    },
    messages: {
        modalBaixarContaPagarPagamento: {
            required: "Campo obrigatório",
            minlength: "Digite uma data válida"
        },
        modalBaixarContaPagarFormaPagamento: {required: "Campo obrigatório"},
        modalBaixarContaPagarConta: {required: "Campo obrigatório"}
    },
    errorClass: 'help-block',
    validClass: "help-block",
    errorElement: "small",
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        var dados = new FormData();
        dados.append("ajax", true);
        dados.append("id", $("#modalBaixarContaPagarId").val());
        dados.append("dataPagamento", $("#modalBaixarContaPagarPagamento").val());
        dados.append("formaPagamento", $("#modalBaixarContaPagarFormaPagamento").val());
        dados.append("conta", $("#modalBaixarContaPagarConta").val());

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/contasPagar/baixar",
            data: dados,
            dataType: "JSON",
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formBaixarContaPagarBtnBaixar")
                        .html("Baixando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: "<strong style='color: white'>Tudo certo</strong>",
                    icon: "success",
                    text: r.msg,
                    position: "top-right",
                    beforeShow: function () {
                        let totalPendente = $(`#total-pendente`).text().slice(3).replace(",", ".");
                        totalPendente = totalPendente.replace(".", "");
                        $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) - parseFloat(r.contaPagar.valor))}`);

                        $("#formBaixarContaPagar").trigger("reset");
                        $("#modalBaixarContaPagar").modal("hide");

                        $(`#${r.contaPagar.id}`).remove();
                    },
                    hideAfter: 4000
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });

                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha de comunicação",
                icon: "error",
                text: "Por favor entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBaixarContaPagarBtnBaixar")
                    .html("Baixar")
                    .removeAttr("disabled");
        });
    }
});

$("#mesContasReceber").on("change", function () {
    let mes = $(this).val();
    let toast = $.toast({
        heading: "<strong style='color: white'>Processando</strong>",
        icon: "info",
        text: "Buscando contas a pagar",
        position: "top-right",
        hideAfter: false
    });

    $.post($("head").data("info") + "financeiro/contasPagar/buscarPorMes",
            {ajax: true, mes: mes},
            function (r) {
                if (r.resultado) {
                    let pago = 0;
                    let pendente = 0;

                    $("#tabelaContasPagar").empty();
                    $.each(r.contasPagar, function (key, contaPagar) {
                        switch (contaPagar.status) {
                            case "pago":
                                pago += parseFloat(contaPagar.valor);
                                break;
                            case "pendente":
                                pendente += parseFloat(contaPagar.valor);
                                break;
                        }
                        tabelaContasPagarAdicionar(contaPagar);
                    });

                    $("#total-pago").html(`R$ ${formatarValor(pago)}`);
                    $("#total-pendente").html(`R$ ${formatarValor(pendente)}`);
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });

                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                toast.reset();
            });
});

$("#modalBaixarContaPagarFormaPagamento").on("change", function () {
    let formaPagamento = $(this).val();
    if (formaPagamento !== "") {
        switch (formaPagamento) {
            case "dinheiro":
                $("#modalBaixarContaPagarConta")
                        .html(`<option value="">--</option>
                                <option value="caixa-vendas">Caixa vendas</option>
                                <option value="caixa-central">Caixa central</option>`);
                break;
            case "cartao-credito":
                $("#modalBaixarContaPagarConta")
                        .html(`<option value="">--</option>
                                <option value="cartao-credito">Cartão de crédito</option>`);
                break;
            case "boleto":
                $("#modalBaixarContaPagarConta")
                        .html(`<option value="">--</option>
                                <option value="banco-santander">Banco Santander</option>
                                <option value="banco-brasil">Banco do Brasil</option>`);
                break;
            case "deposito":
                $("#modalBaixarContaPagarConta")
                        .html(`<option value="">--</option>
                                <option value="banco-santander">Banco Santander</option>
                                <option value="banco-brasil">Banco do Brasil</option>`);
                break;
            default:
                $("#modalBaixarContaPagarConta").html(`<option value="">--</option>`);
                break;
        }
    } else {
        $("#modalBaixarContaPagarConta").html(`<option value="">--</option>`);
    }
});

$("#tabelaContasPagar").on("click", ".btn-deletar", function () {
    swal({
        title: "Atenção",
        icon: "warning",
        text: "Deseja realmente deletar essa conta a pagar ?",
        dangerMode: true,
        buttons: {
            cancel: {
                text: "Cancelar",
                visible: true,
                value: false,
                closeModal: true
            },
            confirm: {
                text: "Deletar",
                visible: true,
                value: true,
                closeModal: true
            }
        }
    }).then((value) => {
        if (value) {
            let id = $(this).data("id");
            let status = $(`#status-${id}`).text();

            let toast = $.toast({
                heading: "<strong style='color: white'>Processando</strong>",
                icon: "info",
                text: "Deletando conta a pagar",
                position: "top-right",
                hideAfter: false
            });

            $.post($("head").data("info") + "financeiro/contasPagar/deletar",
                    {ajax: true, id: id},
                    function (r) {
                        if (r.resultado) {
                            $.toast({
                                heading: "<strong style='color: white'>Tudo certo</strong>",
                                icon: "success",
                                text: r.msg,
                                position: "top-right",
                                beforeShow: function () {
                                    let valorContaPagar = $(`#valor-${id}`).text().slice(3).replace(",", ".");

                                    switch (status) {
                                        case "Pago":
                                            let totalPago = $(`#total-pago`).text().slice(3).replace(",", ".");
                                            $("#total-pago").html(`R$ ${formatarValor(parseFloat(totalPago) - parseFloat(valorContaPagar))}`);
                                            break;
                                        case "Pendente":
                                            let totalPendente = $(`#total-pendente`).text().slice(3).replace(",", ".");
                                            $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) - parseFloat(valorContaPagar))}`);
                                            break;
                                    }
                                    $(`#${id}`).remove();
                                },
                                hideAfter: 4000
                            });
                        } else {
                            if (r.sessaoExpirada) {
                                swal({
                                    title: "Atenção",
                                    icon: "info",
                                    text: "Sua sessão expirou"
                                }).then(() => {
                                    location.href = $("head").data("info");
                                });

                            } else {
                                swal({
                                    title: "Atenção",
                                    icon: "warning",
                                    text: r.msg
                                });
                            }
                        }
                    },
                    "JSON")
                    .fail(function () {
                        swal({
                            title: "Falha de comunicação",
                            icon: "error",
                            text: "Por favor entre em contato com a equipe de suporte"
                        });
                    })
                    .always(function () {
                        toast.reset();
                    });
        }
    });
});

$("#modalBaixarContaPagar").on("show.bs.modal", function (e) {
    $("#modalBaixarContaPagarLoader").html(`<div class="alert alert-info"><i class="fa fa-circle-o-notch fa-spin"></i> Buscando</div>`);
    $("#formBaixarContaPagar").trigger("reset");

    $("#modalBaixarContaPagarId").val($(e.relatedTarget).data("id"));

    let id = $(e.relatedTarget).data("id");
    $.post($("head").data("info") + "financeiro/contasPagar/getContaPagar",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $("#modalBaixarContaReceberPagarValor").val(`R$ ${formatarValor(parseFloat(r.contaPagar.valor))}`);
                    $("#modalBaixarContaPagarVencimento").val(formatarData(r.contaPagar.data));
                    $("#modalBaixarContaPagarDescricao").val(r.contaPagar.descricao);
                    $("#modalBaixarContaPagarPagamento").focus();
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });

                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON")
            .fail(function () {
                swal({
                    title: "Falha de comunicação",
                    icon: "error",
                    text: "Por favor entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                $("#modalBaixarContaPagarLoader").empty();
            });
    ;
});

function analisarStatus(params) {
    let valorContaPagar = $(`#valor-${params.id}`).text().slice(3).replace(",", ".");
    let totalPago = $(`#total-pago`).text().slice(3).replace(",", ".");
    let totalPendente = $(`#total-pendente`).text().slice(3).replace(",", ".");

    switch (params.status) {
        case "pago":
            $(`#${params.id}`).removeClass("danger");

            $("#total-pago").html(`R$ ${formatarValor(parseFloat(totalPago) + parseFloat(valorContaPagar))}`);
            $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) - parseFloat(valorContaPagar))}`);

            break;
        case "pendente":
            let dataAtual = new Date($("#dataAtual").val());
            let dataContaReceber = new Date($(`#data-${params.id}`).text());

            $("#total-pago").html(`R$ ${formatarValor(parseFloat(totalPago) - parseFloat(valorContaPagar))}`);
            $("#total-pendente").html(`R$ ${formatarValor(parseFloat(totalPendente) + parseFloat(valorContaPagar))}`);

            if (dataAtual > dataContaReceber) {
                $(`#${params.id}`).addClass("danger");
            }

            break;
    }
}

function tabelaContasPagarAdicionar(contaPagar) {
    let atrasada = false;
    if (contaPagar.status === "pendente") {
        let dataAtual = new Date($("#dataAtual").val());
        let dataContaReceber = new Date(formatarData(contaPagar.data));

        atrasada = dataAtual > dataContaReceber;
    }

    $("#tabelaContasPagar").append(`<tr id="${contaPagar.id}" class="${atrasada ? "danger" : ""}">
                                        <td id="descricao-${contaPagar.id}">${contaPagar.descricao}</td>
                                        <td id="fornecedor-${contaPagar.id}">${contaPagar.nome}</td>
                                        <td id="categoria-${contaPagar.id}">${contaPagar.categoria}</td>
                                        <td id="valor-${contaPagar.id}">R$ ${formatarValor(parseFloat(contaPagar.valor))}</td>
                                        <td id="data-${contaPagar.id}">${formatarData(contaPagar.data)}</td>
                                        <td id="status-${contaPagar.id}>">${contaPagar.status.charAt(0).toUpperCase() + contaPagar.status.slice(1)}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Ações <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="${$("head").data("info") + "financeiro/conta-pagar/" + contaPagar.id}")">Atualizar</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" data-id="${contaPagar.id}" data-status="pago" data-toggle="modal" data-target="#modalBaixarContaPagar">Pago</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#" class="btn-deletar" data-id="${contaPagar.id}">Deletar</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>`);
}