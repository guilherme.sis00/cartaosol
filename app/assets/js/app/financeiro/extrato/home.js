$().ready(function () {
    $("#dataInicial").mask("00/00/0000");
    $("#dataFinal").mask("00/00/0000");
});

$("#formBuscar").validate({
    rules: {
        formaPagamento: {required: true},
        conta: {required: true},
        dataInicial: {
            required: true,
            minlength: 8
        },
        dataFinal: {
            required: true,
            minlength: 8
        }
    },
    messages: {
        formaPagamento: {required: "Campo obrigatório", },
        conta: {required: "Campo obrigatório", },
        dataInicial: {
            required: "Campo obrigatório",
            minlength: "Digite uma data válida"
        },
        dataFinal: {
            required: "Campo obrigatório",
            minlength: "Digite uma data válida"
        }
    },
    errorClass: 'help-block',
    validClass: "help-block",
    errorElement: "small",
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        $("#tabelaExtrato").empty();
        $("#totalRecebido").html(`R$ 0,00`);
        $("#totalPago").html(`R$ 0,00`);
        $("#total").html(`R$ 0,00`);

        var dados = new FormData($("#formBuscar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "financeiro/extrato/buscar",
            data: dados,
            dataType: "JSON",
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formBuscarBtnBuscar")
                        .html("Buscando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.contasReceber.resultado || r.contasPagar.resultado) {
                $("#totalRecebido").html(`R$ ${formatarValor(parseFloat(r.contasReceber.total))}`);
                $("#totalPago").html(`R$ ${formatarValor(parseFloat(r.contasPagar.total))}`);
                $("#total").html(`R$ ${formatarValor(parseFloat(r.contasReceber.total) - parseFloat(r.contasPagar.total))}`);

                $.each(r.dias, function (key, dia) {
                    $.each(r.contasReceber.contasReceber, function (key, contaReceber) {
                        if (dia == contaReceber.pagamento) {
                            tabelaExtratoAdicionar(r.formaPagamento, r.conta, contaReceber, "green");
                        }
                    });

                    $.each(r.contasPagar.contasPagar, function (key, contaPagar) {
                        if (dia == contaPagar.pagamento) {
                            tabelaExtratoAdicionar(r.formaPagamento, r.conta, contaPagar, "red");
                        }
                    });
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });

                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.contasReceber.msg || r.contasReceber.msg ? r.contasReceber.msg ? r.contasReceber.msg : r.contasPagar.msg : r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha de comunicação",
                icon: "error",
                text: "Por favor entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarBtnBuscar")
                    .html("Buscar")
                    .removeAttr("disabled");
        });
    }
});


function tabelaExtratoAdicionar(formaPagamento, contas, conta, tipo) {
    $("#tabelaExtrato").append(`<tr>
                                        <td class="text-center"><i class="fa fa-circle" style="color:${tipo}"></i></td>
                                        <td>${formaPagamento[conta.formaPagamento]}<br>${contas[conta.conta]}</td>
                                        <td>${conta.descricao}</td>
                                        <td>${conta.categoria}</td>
                                        <td class="text-center">R$ ${formatarValor(parseFloat(conta.valor))}</td>
                                        <td class="text-center">${formatarData(conta.data)}<br>${formatarData(conta.pagamento)}</td>
                                    </tr>`);
}