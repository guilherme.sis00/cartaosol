$().ready(function () {
    $("#modalAtualizarComboValor").mask("#.##0,00", {reverse: true});
});

$("#formBuscar").validate({
    rules: {
        termo: {
            required: true,
            minlength: 3
        }
    },
    messages: {
        termo: {
            required: "Campo obrigatório",
            minlength: "Digite pelo menos 3 caracteres"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formBuscar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "vendas/combos/buscar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formBuscarBtnBuscar")
                        .html("Buscando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $("#tabelaCombos").empty();
                $("#divPaginacao").empty();

                $.each(r.combos, function (key, combo) {
                    $("#tabelaCombos").append(`<tr id="${combo.id}">
                                                    <td id="nome-${combo.id}">${combo.nome}</td>
                                                    <td id="valor-${combo.id}">R$ ${formatarValor(parseFloat(combo.valor))}</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info btn-fill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Ações <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#" data-toggle="modal" data-target="#modalAtualizarCombo" data-id="${combo.id}">Atualizar</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="${$("head").data("info") + `vendas/combos/procedimentos/${combo.id}`}">Procedimentos</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="#" class="btn-deletar" data-id="${combo.id}">Deletar</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>`);
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formBuscarBtnBuscar")
                    .html("Buscar")
                    .removeAttr("disabled");
        });
    }
});

$("#formAtualizarCombo").validate({
    rules: {
        modalAtualizarComboId: {required: true},
        modalAtualizarComboNome: {required: true},
        modalAtualizarComboValor: {required: true}
    },
    messages: {
        modalAtualizarComboNome: {required: "Campo obrigatório"},
        modalAtualizarComboValor: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let valorCombo = $("#modalAtualizarComboValor").val().replace(".", "");
        valorCombo = parseFloat(valorCombo.replace(",", "."));

        let dados = new FormData();
        dados.append("ajax", true);
        dados.append("id", $("#modalAtualizarComboId").val());
        dados.append("nome", $("#modalAtualizarComboNome").val());
        dados.append("valorCombo", valorCombo);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "vendas/combos/atualizar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formAtualizarComboBtnAtualizar")
                        .html("Atualizando")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                $.toast({
                    heading: `<strong style="color:white">Tudo certo</strong>`,
                    icon: "success",
                    position: "top-right",
                    text: r.msg,
                    hideAfter: 3000,
                    beforeShow: function () {
                        $(`#nome-${dados.get("id")}`).html(dados.get("nome"));
                        $(`#valor-${dados.get("id")}`).html(`R$ ${formatarValor(parseFloat(dados.get("valorCombo")))}`);
                        $("#formAtualizarCombo").trigger("reset");
                        $("#modalAtualizarCombo").modal("hide");
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAtualizarComboBtnAtualizar")
                    .html("Atualizar")
                    .removeAttr("disabled");
        });
    }
});

$("#modalAtualizarCombo").on("show.bs.modal", function (e) {
    let id = $(e.relatedTarget).data("id");

    $("#modalAtualizarComboLoader").html(`<div class="alert alert-info">
                                                <span>Buscando dados</span>
                                            </div>`);

    $.post($("head").data("info") + "vendas/combos/get",
            {ajax: true, id: id},
            function (r) {
                if (r.resultado) {
                    $("#modalAtualizarComboId").val(r.combo.id);
                    $("#modalAtualizarComboNome").val(r.combo.nome);
                    $("#modalAtualizarComboValor").val(formatarValor(parseFloat(r.combo.valor)));
                } else {
                    if (r.sessaoExpirada) {
                        swal({
                            title: "Atenção",
                            icon: "info",
                            text: "Sua sessão expirou"
                        }).then(() => {
                            location.href = $("head").data("info");
                        });
                    } else {
                        swal({
                            title: "Atenção",
                            icon: "warning",
                            text: r.msg
                        });
                    }
                }
            },
            "JSON"
            )
            .fail(function () {
                swal({
                    title: "Falha",
                    icon: "error",
                    text: "Por favor, entre em contato com a equipe de suporte"
                });
            })
            .always(function () {
                $("#modalAtualizarComboLoader").empty();
            });
});

$("#tabelaCombos").on("click", ".btn-deletar", function () {
    let id = $(this).data("id");

    swal({
        title: "Atenção",
        icon: "warning",
        text: "Você deseja realmente deletar este combo?",
        dangerMode: true,
        buttons: {
            cancel: {
                text: "Cancelar",
                value: false,
                visible: true,
                className: "",
                closeModal: true
            },
            confirm: {
                text: "Sim",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then((value) => {
        if (value) {
            let toast = $.toast({
                heading: `<strong style="color:white">Processando</strong>`,
                icon: "info",
                text: "Deletando combo",
                position: "top-right",
                hideAfter: false
            });

            $.post($("head").data("info") + "vendas/combos/deletar",
                    {ajax: true, id: id},
                    function (r) {
                        if (r.resultado) {
                            $.toast({
                                heading: `<strong style="color:white">Tudo certo</strong>`,
                                icon: "success",
                                text: r.msg,
                                position: "top-right",
                                beforeShow: function () {
                                    $(`#${id}`).remove();
                                }
                            });
                        } else {
                            if (r.sessaoExpirada) {
                                swal({
                                    title: "Atenção",
                                    icon: "info",
                                    text: "Sua sessão expirou"
                                }).then(() => {
                                    location.href = $("head").data("info");
                                });
                            } else {
                                swal({
                                    title: "Atenção",
                                    icon: "warning",
                                    text: r.msg
                                });
                            }
                        }
                    },
                    "JSON")
                    .fail(function () {
                        swal({
                            title: "Falha",
                            icon: "error",
                            text: "Por favor entre em contato com a equipe de suporte"
                        });
                    })
                    .always(function () {
                        toast.reset();
                    });
        }
    });
});