$().ready(function () {
    $("#usuario").focus();
});


$("#formAutenticar").validate({
    rules: {
        usuario: {
            required: true,
            email: true
        },
        senha: {required: true}
    },
    messages: {
        usuario: {
            required: "Campo obrigatório",
            email: "Digite um e-mail válido"
        },
        senha: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAutenticar")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "login/autenticar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formAutenticarBtnEntrar")
                        .empty()
                        .append("Autenticando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                location.href = $("head").data("info") + "dashboard";
            } else {
                swal({
                    title: "Atenção",
                    icon: "warning",
                    text: r.msg
                });
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAutenticarBtnEntrar")
                    .empty()
                    .append("Entrar")
                    .removeAttr("disabled");
        });
    }
});

$("#formRecuperarSenha").validate({
    rules: {
        modalRecuperarSenhaUsuario: {
            required: true,
            email: true
        }
    },
    messages: {
        modalRecuperarSenhaUsuario: {
            required: "Campo obrigatório",
            email: "Digite um e-mail válido"
        }
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData();
        dados.append("ajax", true);
        dados.append("usuario", $("#modalRecuperarSenhaUsuario").val());

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "login/recuperarSenha",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $("#formRecuperarSenhaBtnRecuperarSenha")
                        .empty()
                        .append("Processando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg
                }).then(() => {
                    $("#formRecuperarSenha").trigger("reset");
                    $("#modalRecuperarSenha").modal("hide");
                });
            } else {
                swal({
                    title: "Atenção",
                    icon: "warning",
                    text: r.msg
                });
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formRecuperarSenhaBtnRecuperarSenha")
                    .empty()
                    .append("Recuperar senha")
                    .removeAttr("disabled");
        });
    }
});