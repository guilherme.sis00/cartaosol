$("#formAdicionarEspecialidade").validate({
    rules: {
        especialidade: {required: true},
        tipo: {required: true}
    },
    messages: {
        especialidade: {required: "Campo obrigatório"},
        tipo: {required: "Campo obrigatório"}
    },
    validClass: "has-success",
    errorClass: "text-danger",
    errorElement: 'small',
    errorPlacement: function (error, element) {
        element.closest('div.form-group').append(error);
    },
    highlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .addClass("has-error");
    },
    unhighlight: function (element, errorClass) {
        $(element)
                .closest('div.form-group')
                .removeClass("has-error");
    },
    success: function (element) {
        element
                .closest('div.form-group')
                .addClass("has-success");
    },
    submitHandler: function () {
        let dados = new FormData($("#formAdicionarEspecialidade")[0]);
        dados.append("ajax", true);

        $.ajax({
            type: 'POST',
            url: $("head").data("info") + "especialidades/adicionar",
            data: dados,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                $("#formAdicionarEspecialidadeBtnAdicionar")
                        .empty()
                        .append("Adicionando...")
                        .attr("disabled", "disabled");
            }
        }).done(function (r) {
            if (r.resultado) {
                swal({
                    title: "Sucesso",
                    icon: "success",
                    text: r.msg,
                    buttons: {
                        sim: "sim",
                        nao: "nao"
                    }
                }).then((value) => {
                    switch (value) {
                        case "sim":
                            $("#formAdicionarEspecialidade").trigger("reset");
                            break;
                        case "nao":
                            location.href = $("head").data("info") + "especialidades";
                        default :
                            location.href = $("head").data("info") + "especialidades";
                    }
                });
            } else {
                if (r.sessaoExpirada) {
                    swal({
                        title: "Atenção",
                        icon: "info",
                        text: "Sua sessão expirou"
                    }).then(() => {
                        location.href = $("head").data("info");
                    });
                } else {
                    swal({
                        title: "Atenção",
                        icon: "warning",
                        text: r.msg
                    });
                }
            }
        }).fail(function () {
            swal({
                title: "Falha",
                icon: "error",
                text: "Por favor, entre em contato com a equipe de suporte"
            });
        }).always(function () {
            $("#formAdicionarEspecialidadeBtnAdicionar")
                    .empty()
                    .append("Adicionar")
                    .removeAttr("disabled");
        });
    }
});