<?php include('header.php') ?>
<?php include('controller.php') ?>

<div class="adquira_clinicas" style="margin-top: 67px;bottom: 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="media">
                    <div class="media-left">
                        <i class="fa fa-credit-card fa-2x" aria-hidden="true" style="margin-top: 30px;border: 1px solid rgb(0,138,168);border-radius: 100px;padding: 10px 9px 9px 9px"></i>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading" style="padding-top: 30px;"><b>Adquira o seu cartão</b></h3>
                        <h5 class="font_chamadas">Faça parte desta rede de beneficiados que cresce todo dia, e comece a economizar diariamente em suas compras.</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-xs-12 adquira_form">

                <form name="form1"  class="form-inline" id="formulario-contato" action="mail_adquira.php" method="post" enctype="multipart/form-data">
                    <input type="nome" class="form-control" id="nome" name="nome" placeholder="SEU NOME" style="width: 100% !important;">

                    <div class="form-group">
                      <!-- <input type="celular" class="form-control" id="celular" name="celular" placeholder="CELULAR"> -->

                        <input required placeholder=" CELULAR" type="text" name="tel" id="celular"  maxlength="16" onKeyPress="MascaraTelefone(form1.tel);" onBlur="ValidaTelefone(form1.tel);" style="padding: 5px;">

                        <input type="email" class="form-control" id="email" name="email" placeholder="E-MAIL">
                    </div>

                    <button type="submit" class="btn btn-primary pull-right" style="margin-top: 1px;"><i class="fa fa-plus" aria-hidden="true"></i></button>

                </form>
            </div>
            <div class="col-md-3">
                <img class="img-responsive center-block" src="img/cartao.png" alt="">
            </div>
        </div>
    </div>
</div>



<div class="jumbotron" style="background-image: url(img/bannerclinica.png); background-size: cover; height: 100%; background-repeat: no-repeat; background-position: center;margin-bottom: 0px;padding-bottom: 0px;padding-top: 0px;">

    <div class="container">
        <div class="row text-clinica">
            <div class="col-md-12">

                <h1><b class="s">CLÍNICAS</b></h1>

                <p>ENCONTRE AS MELHORES <br> CLÍNICAS DA SUA REGIÃO</p>
            </div>
        </div>
    </div>

</div>



<!-- <div class="barraselect">
        <div class="container">
                <div class="row">
                        <div class="col-md-3">
                                <div class="checkbox">
                            <label>
                              <input type="checkbox"> HOPITAIS E CLÍNICAS
                            </label>
                        </div>
                </div>
                        <div class="col-md-6">
                                <div class="checkbox">
                            <label>
                              <input type="checkbox"> EXAMES LABORATORIAIS E COMPLEMENTARES
                            </label>
                        </div>
                        </div>
                        <div class="col-md-2">
                                <div class="checkbox">
                            <label>
                              <input type="checkbox"> PARCEIROS
                            </label>
                        </div>
                        </div>
                </div>
        </div>
  </div> -->

<div class="container">
    <div class="row">
        <br>
    </div>

    <?php $clinicas = getClinicas(); ?>
    <?php if (!empty($clinicas)): ?>
        <?php while ($clinica = mysqli_fetch_array($clinicas)): ?>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <img class="" src="/app/assets/img/logos/<?= $clinica['logo'] ?>" alt="<?= utf8_encode($clinica["nome"]) ?>">
                    <div class="caption corpo">
                        <h2><?= utf8_encode($clinica["nome"]) ?></h2>
                        <h3><?= utf8_encode($clinica["cidade"]) ?> / <?= $clinica["uf"] ?></h3>
                        <p><?= $clinica["telefone"] ?></p>
                        <p><?= utf8_encode($clinica["endereco"]) ?></p>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>

    <div class="row">
        <br>
    </div>
</div>





<?php include('footer.php') ?>

<?php include('bottom.php') ?>