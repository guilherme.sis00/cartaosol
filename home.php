<header>
    <div class="container" style="padding-top: 80px;">
        <div class="row">
          <!--LOGO-->
           <!--  <div class="col-xs-12 col-md-3 hidden-xs">
                <img class="center-block img-responsive" src="img/logo.png" alt="" class="logo_home">
            </div> -->

             <!--BTN PECA SEU CARTAO-->
            <div class="text-center col-xs-12 col-md-3">
                    <a href=""><div class="btnPadrao"><i class="fa fa-credit-card" aria-hidden="true"></i> SOLICITE JÁ O SEU</div></a>
            </div>

             <!--WHATSAPP-->
            <div class="text-center col-xs-12 col-md-3">
                 <div class="whatsapp">
                        
    
                        <h5 style="font-family: 'Roboto Condensed', sans-serif;line-height: 7px;">Peça pelo nosso Whatsapp</h5>
                        <i class="fa fa-whatsapp" style="color: #66c251" aria-hidden="true"></i>
                        ¹¹ 9 4958 2020

                  </div>
            </div>
            
            <!--TXT COMO FUNCIONA-->
            <div class="col-xs-12 col-md-3 hidden-xs">
                <div class="media funciona">
              <div class="media-left">
                  <i class="fa fa-shield fa-2x icon_menu" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                <h4 class="media-heading">COMO FUNCIONA</h4>
                <p class="font_chamadas">PROJETO E INVESTIDORES</p>
              </div>
            </div>
            </div> 
            <!--TXT LOCALIZE-->
             <div class="col-xs-12 col-md-3 hidden-xs">
                <div class="media funciona">
              <div class="media-left">
                  <i class="fa fa-map-marker fa-2x icon_menu" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                <h4 class="media-heading">LOCALIZE</h4>
                <p class="font_chamadas">CLÍNICAS E PARCEIROS</p>
              </div>
            </div>
            </div>


        </div>
    </div>

   <div class="banner_home">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-4" style=" margin-bottom: 20px;">

        <h2>Agora ficou fácil comprar</h2>
        <p>Quem tem nosso cartão tem acesso a compras exclusivas em diversas regiões, além de descontos especiais.</p>
          
        <i class="fa fa-plus fa-2x" aria-hidden="true"></i> COMPRA  

        <i class="fa fa-plus fa-2x" aria-hidden="true"></i> ECONOMIA
        </div>

   
        <div class="col-md-2 col-xs-12 hidden-xs" style=" margin-bottom: 20px;transform: rotate(-10deg);transform-origin: bottom right; padding-left: 140px;z-index: 999;">
          <a href="#" style="text-decoration: none;"><button class="btn btnDesconto">DESCONTO EM LOJAS</button></a>
        </div>
        
        <div class="col-xs-12 col-md-5 img_banner">
          <img class="center-block img-responsive" src="img/img_banner.png" alt="">
        </div>
      </div>
    </div>

   </div>

   <div class="container">
     <div class="row selecione">
       <div class="col-md-3"  style="padding-left: 0px;">
         <img class="img-responsive center-block" src="img/doctor1.png" alt="">
       </div>
       <div class="col-xs-12 col-md-3">
          <div class="media exames">
              <div class="media-left">
                  <i class="fa fa-search fa-2x" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                <h4 class="media-heading">EXAMES LABORATORIAIS <br>  E COMPLEMENTARES</h4>
                <p class="font_chamadas">Encontre um parceiro para realizar <br> seus exames laboratorias</p>
              </div>
          </div>
            <div class="chosen" >  
            <select class = "escolhido">  
            <option> POR TIPO </option>
            </select>
          </div>
          <div class="chosen" >  
            <select class = "escolhido">  
            <option> POR CIDADE </option>
            </select>
          </div>
          <div class="barra hidden-xs"><p></p></div>
       </div>
       <div class="col-md-3">
         <img class="img-responsive center-block" src="img/doctor.png" alt="">
       </div>
       <div class="col-xs-12 col-md-3">
          <div class="media exames">
              <div class="media-left">
                  <i class="fa fa-stethoscope fa-2x" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                <h4 class="media-heading">HOSPITAIS <br> E CLÍNICAS</h4>
                <p class="font_chamadas">Localize filtrando por especialistas ou <br> pela cidade mais proximas</p>
              </div>
            </div>
            <div class="chosen" >  
            <select class = "escolhido">  
            <option> POR ESPECIALIDADE </option>
            </select>
          </div>
          <div class="chosen" >  
            <select class = "escolhido">  
            <option> POR CIDADE </option>
            </select>
          </div>
          <div class="barra1 hidden-xs"><p></p></div>
       </div>

     </div>
   </div>

   <div class="container adquira">
    <div class="row">
      <div class="col-md-4">
        <div class="media">
              <div class="media-left">
                  <i class="fa fa-credit-card fa-2x" aria-hidden="true" style="margin-top: 30px;border: 1px solid rgb(0,138,168);border-radius: 100px;padding: 5px 9px 9px 9px"></i>
              </div>
              <div class="media-body" style="padding: 22px;">
                <h3 class="media-heading"><b>Adquira o seu cartão</b></h3>
                <h5 class="font_chamadas">Faça parte desta rede de beneficiado que cresce todo dia, e comece a economizar diariamente em sua compras.</h5>
              </div>
            </div>
      </div>
      <div class="col-md-5 col-xs-12 adquira_form">

        <form class="form-inline">
    <input type="nome" class="form-control" id="nome" placeholder="SEU NOME" style="width: 100% !important;">

  <div class="form-group">
    <input type="celular" class="form-control" id="celular" placeholder="CELULAR">
    <input type="email" class="form-control" id="email" placeholder="E-MAIL">
  </div>

  <button type="submit" class="btn btn-primary pull-right" style="margin-top: 1px;"><i class="fa fa-plus" aria-hidden="true"></i></button>

</form>
      </div>
      <div class="col-md-3">
        <img class="img-responsive center-block" src="img/cartao.png" alt="" style="margin-top: -20px;">
      </div>
    </div>
  </div>

 <div class="container">
   <div class="row">
    <h3 class="text-primary text-center">COMENTÁRIOS</h3>
    

        <div class="col-xs-12 col-md-4 comentarios" >
          <div class="hoverzoom">
      <img class="img-responsive center-block" src="img/testemunho_dora.jpeg.png" alt="" title="Mensagem">
      <div class="retina">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, temporibus, repellat? Placeat nihil itaque culpa eu.</p>
      </div>
    </div>
    <div class="hoverzoom">
      <img class="img-responsive center-block" src="img/testemunho_rocha.jpeg.png" alt="" title="Mensagem" style="padding-right: 0px;">
      <div class="retina">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, temporibus, repellat? Placeat nihil itaque culpa eu.</p>
      </div>
    </div>
        </div>

        <!--<div class="col-xs-12 col-md-4 comentarios">
      <div class="hoverzoom">
  <img class="img-responsive center-block" src="img/comentario3.png" alt="" title="Mensagem" style="padding-right: 0px;">
  <div class="retina">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, temporibus, repellat? Placeat nihil itaque culpa eu.</p>
  </div>
</div>
<div class="hoverzoom">
  <img class="img-responsive center-block" src="img/comentario4.png" alt="" title="Mensagem" style="padding-right: 0px;">
  <div class="retina">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, temporibus, repellat? Placeat nihil itaque culpa eu.</p>
  </div>
</div>-->
    </div>
         
    
     <div class="col-xs-12 col-md-4 facebook">
      <div class="fb-page" data-href="https://www.facebook.com/NovoTempoOficial/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
        <blockquote cite="https://www.facebook.com/NovoTempoOficial/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/NovoTempoOficial/">Jornal Novo Tempo</a>
        </blockquote>
      </div>
     </div>
   </div>
 </div>

</header>
