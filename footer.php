<div class="barrarodape">
	
</div>
<footer class="rodape" style="margin-top: 0px;">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<img class="img-responsive center-block" src="img/logofooter.png" alt="">
				<h4>Cartão Sol</h4>
				<p class="text-justify">Tenha o melhor cartão de descontos. O Cartão Sol possibilitará vários benefícios a você e sua família. Sua saúde em primeiro lugar sempre, faça seu cartão e aproveite.</p>
			</div>

			<div class="col-md-1 hidden-xs">
				<img src="img/barra.png" alt="" style="background-repeat: no-repeat;margin-top: 20px;">
			</div>

		<div class="col-md-3">
			<ul class="menu_footer">
		        <li><a href="index">PÁGINA INICIAL</a></li>
		        <li><a href="feitopravoce">FEITO PARA VOCÊ</a></li>
		        <li><a href="clinicas">CLÍNICAS E EXAMES</a></li>
		        <li><a href="sobre">SOBRE NÓS</a></li>
		        <li><a href="contato">CONTATO</a></li>
		        <li><a href="contrato">CONTRATO</a></li>

            </ul>
		</div>
		
		</div>
		<div class="row">
			<div class="pull-right col-md-3 direitos">
			<p >Todos os direitos reservados 2018</p>
			<p>Desenvolvimento <a href="http://evve.com.br/" target="_blank" style="color: white;">Evve Comunicação</a></p>
		</div>
		</div>
	</div>

</footer>